; The name of the installer
Name "SoulFu"

; The file to write
OutFile "D:\NullSoftInstaller\soulfu_installer.exe"

; The default installation directory
InstallDir C:\SoulFu

; The text to prompt the user to enter a directory
DirText "THIS SOFTWARE IS PROVIDED TO YOU AS IS, AND AARON BISHOP DISCLAIMS ALL WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED.  BY USING THIS SOFTWARE, YOU AGREE THAT YOU DO SO AT YOUR OWN RISK.  Please choose a directory..."

; The stuff to install
Section "ThisNameIsIgnoredSoWhyBother?"
  ; Set output path to the installation directory.
  SetOutPath $INSTDIR
  ; Put file there
  File "D:\SoulFu\*.*"
  SetOutPath $INSTDIR\sourcecode
  File "D:\SoulFu\sourcecode\*.*"
  SetOutPath $INSTDIR\sourcecode\notes
  File "D:\SoulFu\sourcecode\notes\*.*"
SectionEnd ; end the section

; eof

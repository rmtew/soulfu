// The "Programmer X Scripting System" by Programmer X, aka Aaron Bishop
//
// This code was abstracted into a library from the scripting system in SoulFu and
// is licensed to be distributed solely with that application, under Aaron's "Be Nice" license.
//
// file : pxss_run.h

#pragma once

#include "pxss_compile.h"
#include "soulfu_math.h"

#include <assert.h>

struct base_data_t;
//typedef struct base_data_t BASE_DATA;

// For direct code->script calls...
#define MAX_FAST_FUNCTION            16  // For predefined script functions like Spawn


//---------------------------------------------------------------------------------------
// stack related info
// use SINT, UINT, and FLOT for these variables for "automatic" 64-bit compatability

#define MAX_STACK                0x0100

enum pxss_var_type_e
{
    PXSS_VAR_UNKNOWN,
    PXSS_VAR_INT,
    PXSS_VAR_FLT,
    PXSS_VAR_PTR
};
typedef enum pxss_var_type_e PXSS_VAR_TYPE;

struct pxss_var_t
{
    PXSS_VAR_TYPE type;
    union
    {
        SINT   i;
        FLOT   f;
        Uint8* p;
    };
};
typedef struct pxss_var_t PXSS_VAR;



//-----------------------------------------------------------------------------------------------
// ---- the most basic level of the scripting system
// ---- This level of the most basic requirements of
// ---- a script system: flow control, basic operators/math,
// ---- defining and calling user defined functions, etc.

struct base_global_script_context_t
{
    PXSS_VAR stack[MAX_STACK];
    Uint16   stack_head;

    //---- SHARED INFO ----
    PXSS_VAR return_var;           // Set by pxss_run_script...

};
typedef struct base_global_script_context_t BaseGlobalScriptContext, *PBaseGlobalScriptContext;

struct base_script_context_t
{
    PBaseGlobalScriptContext pbgs;

    // execution info
    SDF_STREAM tokens;
    bool_t     running;

    Uint8  num_int_vars;
    Sint32 int_variable[MAX_VARIABLE];

    Uint8  num_flt_vars;
    float  flt_variable[MAX_VARIABLE];

    Uint16 link;

    Uint8* file_start;
    Uint8* function_start;
    Uint8* return_address;

    char * file_name;
    Uint16 line_number;
    char * line_pointer;
    char * function_name;

    //---- SHARED INFO ----
    PXSS_VAR return_var;           // Set by pxss_run_script...

};
typedef struct base_script_context_t BaseScriptContext, *PBaseScriptContext;


//-----------------------------------------------------------------------------------------------
// ---- the "plain jane" level of the scripting system
// ---- This level of the scripting system corresponds to
// ---- what is available in most scripting systems with
// ---- additional libraries: file access, string manipulation, etc.

// For FileOpen
#define FILE_NORMAL                 0
#define FILE_EXPORT                 1
#define FILE_IMPORT                 2
#define FILE_DELETE                 3
#define FILE_MAKENEW                4
#define FILE_SIZE                   5

struct global_script_context_t
{
    BaseGlobalScriptContext base;

};
typedef struct global_script_context_t GlobalScriptContext, *PGlobalScriptContext;

struct script_context_t
{
    BaseScriptContext    base;
    PGlobalScriptContext pgs;

};
typedef struct script_context_t ScriptContext, *PScriptContext;

// library management
bool_t pxss_run_setup( PGlobalScriptContext pgs );

// running a script
PScriptContext pxss_script_spawn(PGlobalScriptContext pgs, PScriptContext ps);
PScriptContext pxss_script_init(PScriptContext ps, Uint8 *file_start);
bool_t         pxss_fast_run_script(PGlobalScriptContext pgs, Uint8 *file_start, Uint16 fast_function);
bool_t         pxss_fast_rerun_script(PScriptContext ps, Uint16 fast_function);

//*************************************************************************
// functions for creating a pxss_run_script() function for extended opcodes
void   pxss_run_script_begin(PScriptContext pbs);
OPCODE pxss_run_script_loop_begin(PScriptContext ps);
bool_t pxss_run_script_loop_end(PScriptContext ps, OPCODE opcode);
void   pxss_run_script_end(PScriptContext ps);

//**********************************************************
// functions and variables that *must* be defined externally
// they can be stubbed out if they are not needed
extern Sint32 system_set(PScriptContext ps, int type, int number, int sub_number, Sint32 value);
extern Sint32 system_get(PScriptContext ps, int type, int number, int sub_number);
extern char * pxss_fast_function_name[MAX_FAST_FUNCTION];

// stack corruption check
extern bool_t pxss_looking_for_fast_run;
extern bool_t pxss_fast_run_found;
extern Uint16 pxss_fast_run_offset;
extern int    pxss_debug_level;

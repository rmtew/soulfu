#pragma once

#include "sdf_archive.h"

#pragma pack(push,1)
struct random_data_t
{
    Uint16    next;   // The next random number to return
    Uint16    size;   // The number of random numbers in the table
    SDF_PDATA table;  // The random number table
} SET_PACKED();
typedef struct random_data_t RANDOM_DATA;
#pragma pack(pop)

extern RANDOM_DATA g_rand;

Sint8 random_setup(int seed);
Uint8 random_number();
Uint8 get_random(Uint32 val);
Uint16 random_dice(Uint8 number, Uint16 sides);
Uint8* random_name(Uint8* filedata);

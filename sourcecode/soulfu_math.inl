#pragma once

//    SoulFu - 3D Rogue-like dungeon crawler
//    Copyright (C) 2007 Aaron Bishop
//
//    web:   http://www.aaronbishopgames.com
//    email: aaron@aaronbishopgames.com

#include "soulfu_config.h"
#include "soulfu_math.h"

_INLINE void  cross_product(float A_xyz[], float B_xyz[], float C_xyz[]);
_INLINE float dot_product(float A_xyz[], float B_xyz[]);
_INLINE float vector_length(float A_xyz[]);

//-----------------------------------------------------------------------------------------------
_INLINE int FTOI_DIV(float A, float B)
{
    float ftmp = A / B;
    ftmp = (ftmp < 0) ? -(FLOOR(-ftmp) * B) : FLOOR(ftmp) * B;
    return (int)ftmp;
}

//-----------------------------------------------------------------------------------------------
_INLINE int FTOI_MUL(float A, float B)
{
    float ftmp = A * B;
    ftmp = (ftmp < 0) ? -FLOOR(-ftmp) : FLOOR(ftmp);
    return (int)ftmp;
}

//-----------------------------------------------------------------------------------------------
_INLINE int FTOI(float A)
{
    float ftmp = A;
    ftmp = (ftmp < 0) ? -FLOOR(-ftmp) : FLOOR(ftmp);
    return (int)ftmp;
}

//-----------------------------------------------------------------------------------------------
_INLINE void cross_product(float A_xyz[], float B_xyz[], float C_xyz[])
{
    // <ZZ> This function crosses two vectors, A and B, and gives the result in C.  C should be
    //      a different location than either A or B...
    C_xyz[XX] = (A_xyz[YY] * B_xyz[ZZ]) - (A_xyz[ZZ] * B_xyz[YY]);
    C_xyz[YY] = (A_xyz[ZZ] * B_xyz[XX]) - (A_xyz[XX] * B_xyz[ZZ]);
    C_xyz[ZZ] = (A_xyz[XX] * B_xyz[YY]) - (A_xyz[YY] * B_xyz[XX]);
}

//-----------------------------------------------------------------------------------------------
_INLINE float dot_product(float A_xyz[], float B_xyz[])
{
    // <ZZ> This function dots two vectors, A and B, and gives the result.
    return (A_xyz[XX]*B_xyz[XX] + A_xyz[YY]*B_xyz[YY] + A_xyz[ZZ]*B_xyz[ZZ]);
}

//-----------------------------------------------------------------------------------------------
_INLINE float vector_length(float A_xyz[])
{
    // <ZZ> This function returns the length of a vector.
    return SQRT(A_xyz[XX]*A_xyz[XX] + A_xyz[YY]*A_xyz[YY] + A_xyz[ZZ]*A_xyz[ZZ]);
}

//-----------------------------------------------------------------------------------------------
// Stuff for 3d models in windows...
_INLINE void matrix_clear(matrix_4x3 MAT)
{
    MAT[0] = 1.0f;  MAT[1] = 0.0f;  MAT[2] = 0.0f;
    MAT[3] = 0.0f;  MAT[4] = 1.0f;  MAT[5] = 0.0f;
    MAT[6] = 0.0f;  MAT[7] = 0.0f;  MAT[8] = 1.0f;
    MAT[9] = 0.0f;  MAT[10] = 0.0f;  MAT[11] = 0.0f;
}

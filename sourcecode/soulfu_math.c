#include "logfile.h"
#include "soulfu_config.h"
#include "soulfu_common.h"
#include "soulfu_math.h"

//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------

float sine_table[NUM_SINE_ENTRIES];
float cosine_table[NUM_SINE_ENTRIES];

//-----------------------------------------------------------------------------------------------
void sine_table_setup()
{
    // <ZZ> This function sets up the sine and cosine tables...
    Uint32 i;
    float angle, angle_add;

    log_info(0, "Setting up sine/cosine table...");
    angle = 0.0f;
    angle_add = 2.0f * PI / NUM_SINE_ENTRIES;
    repeat(i, NUM_SINE_ENTRIES)
    {
        sine_table[i]   = SIN(angle);
        cosine_table[i] = COS(angle);
        angle += angle_add;
    }
}
// <ZZ> This file contains functions related to pages of books...

#include "soulfu_math.inl"

float page_xy[PAGE_FRAME][PAGE_COL*2];      // The page animation


//-----------------------------------------------------------------------------------------------
void page_setup(void)
{
    // <ZZ> This function loads the page data from a datafile...

    int i, j;
    Uint8* page_anim;
    SDF_PHEADER page_header;



    // Try to find the datafile...
    log_info(0, "Loading the page animation");
    page_anim   = NULL;
    page_header = sdf_archive_find_filetype("PAGEANIM", SDF_FILE_IS_DAT);

    if (page_header)
    {
        // Read in the page data...
        page_anim = sdf_file_get_data(page_header);
        repeat(j, PAGE_FRAME)
        {
            repeat(i, PAGE_COL)
            {
                // Data is stored as signed chars for portability...
                page_xy[j][(i<<1)] = DEREF( Sint8, page_anim ) / 127.0f;  page_anim++;
                page_xy[j][(i<<1)+1] = DEREF( Sint8, page_anim ) / 127.0f;  page_anim++;
            }
        }
        log_info(0, "Page animation loaded");
    }
    else
    {
        // Default to a simple book...
        repeat(j, PAGE_FRAME)
        {
            repeat(i, PAGE_COL)
            {
                page_xy[j][(i<<1)+1] = -COS((i) * PI * 1.5f / PAGE_COL) * 0.25f;
                page_xy[j][(i<<1)]   = SQRT(1.0f - (page_xy[0][(i<<1)+1] * page_xy[0][(i<<1)+1]));
            }
        }
        j = PAGE_FRAME - 1;
        repeat(i, PAGE_COL)
        {
            page_xy[j][(i<<1)] = -page_xy[j][(i<<1)];
        }
        log_info(0, "Page animation regenerated");
    }
}

//-----------------------------------------------------------------------------------------------
#ifdef DEVTOOL
void page_edit_tool(void)
{
    // <ZZ> This function lets me edit the page turn animation...  Shouldn't be preset in final
    //      release...
    static int frame = 0;
    static int point = 0;
    int i, j;
    int last_frame;
    int next_frame;
    float x, y;
    float distance;

    Uint8* page_anim;
    SDF_PHEADER page_header;




    // Draw a test book...
    // Left page
    display_book_page(NULL, 0.5f * DEFAULT_W, 0.22 * DEFAULT_H, page_xy[PAGE_FRAME-1], 7.0f, PAGE_COL - 2, 15, ktrue, 0);

    // Right page
    display_book_page(NULL, 0.5f * DEFAULT_W, 0.22 * DEFAULT_H, page_xy[0], 7.0f, PAGE_COL - 2, 15, kfalse, 0);

    // Current page
    display_book_page(NULL, 0.5f * DEFAULT_W, 0.22 * DEFAULT_H, page_xy[frame], 7.0f, PAGE_COL - 2, 15, kfalse, 0);
    display_book_page(NULL, 0.5f * DEFAULT_W, 0.22 * DEFAULT_H, page_xy[frame], 7.0f, PAGE_COL - 2, 15, ktrue, 0);



    // Draw the guideline points...
    display_pick_texture(texture_ascii);
    repeat(i, PAGE_FRAME)
    {
        if (i == frame)
        {
            display_color(green);
        }
        else
        {
            display_color(blue);
        }

        x = 200.0f - SIN((i * PI / PAGE_FRAME) - (PI_DIV_2 - PI_DIV_64)) * 185.0f;
        y = 270.0f - COS((i * PI / PAGE_FRAME) - (PI_DIV_2 - PI_DIV_64)) * 75.0f;
        display_font('%', x - 5.0f, y - 5.0f, 10.0f);
    }




    // Draw the points...
    display_color(white);
    x = 200.0f;
    y = 257.0f;
    display_font('%', x - 5.0f, y - 5.0f, 10.0f);
    repeat(i, PAGE_COL)
    {
        if (i == point)
        {
            display_color(red);
        }
        else
        {
            display_color(white);
        }

        x += (page_xy[frame][i<<1] * 7.0f);
        y += (page_xy[frame][(i<<1)+1] * 7.0f);
        display_font('%', x - 5.0f, y - 5.0f, 10.0f);
    }



    // Adjust point positions...
    if (mouse.down[2])
    {
        j = frame;
//        while(j < PAGE_FRAME-1)
//        {
        x = 200.0f;
        y = 240.0f;
        repeat(i, point + 1)
        {
            x += (page_xy[j][i<<1] * 7.0f);
            y += (page_xy[j][(i<<1)+1] * 7.0f);
        }
        x -= 5.0f;
        y -= 5.0f;
        page_xy[j][(point<<1)] += (mouse.x - x) / 7.0f;
        page_xy[j][(point<<1)+1] += (mouse.y - y) / 7.0f;
        x = page_xy[j][(point<<1)];
        y = page_xy[j][(point<<1)+1];
        distance = SQRT(x * x + y * y);

        if (distance < 0.001)  { distance = 1;  x = 1; }

        x = x / distance;
        y = y / distance;
        page_xy[j][(point<<1)] = x;
        page_xy[j][(point<<1)+1] = y;
//            j++;
//        }
    }



    // Frame and point changes...
    if (keyb.pressed[SDLK_q]) { frame = (frame - 1) & 31;}

    if (keyb.pressed[SDLK_w]) { frame = (frame + 1) & 31;}

    if (keyb.pressed[SDLK_e]) { point = (point - 1);  if (point < 0) { point = PAGE_COL - 1; } }

    if (keyb.pressed[SDLK_r]) { point = (point + 1);  if (point > PAGE_COL - 1) { point = 0; } }

    if (keyb.down[SDLK_t]) { frame = (frame - 1) & 31;}

    if (keyb.down[SDLK_y]) { frame = (frame + 1) & 31;}

    if (keyb.pressed[SDLK_p])
    {
        // Save the page anim...
        // Try to find the datafile...
        page_anim   = NULL;
        page_header = sdf_archive_find_filetype("PAGEANIM", SDF_FILE_IS_DAT);

        if (page_header)
        {
            // Read in the page data...
            page_anim = sdf_file_get_data(page_header);
            repeat(j, PAGE_FRAME)
            {
                repeat(i, PAGE_COL)
                {
                    // Data is stored as signed chars for portability...
                    DEREF( Sint8, page_anim ) = (Sint8) (page_xy[j][(i<<1)+0] * 127.0f);  page_anim++;
                    DEREF( Sint8, page_anim ) = (Sint8) (page_xy[j][(i<<1)+1] * 127.0f);  page_anim++;
                }
            }
        }

        sdf_archive_export_file("PAGEANIM.DAT", NULL);
    }


    if (keyb.pressed[SDLK_o])
    {
        // Smooth frames...
        repeat(j, PAGE_FRAME)
        {

            last_frame = j - 1;  if (last_frame < 0) last_frame = 0;

            next_frame = j + 1;  if (next_frame > PAGE_FRAME - 1) next_frame = PAGE_FRAME - 1;

            repeat(i, PAGE_COL)
            {
                page_xy[j][(i<<1)]   = ((6.0f * page_xy[j][(i<<1)]) + page_xy[next_frame][(i<<1)] + page_xy[last_frame][(i<<1)]) / 8.0f;
                page_xy[j][(i<<1)+1] = ((6.0f * page_xy[j][(i<<1)+1]) + page_xy[next_frame][(i<<1)+1] + page_xy[last_frame][(i<<1)+1]) / 8.0f;
            }
        }
    }

}
#endif

//-----------------------------------------------------------------------------------------------

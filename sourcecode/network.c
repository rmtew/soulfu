// <ZZ> This file contains functions to handle networking
//      network_blah   - Blah

#include "network.h"

#include "soulfu.h"
#include "random.h"
#include "runsrc.h"
#include "logfile.h"

#include "object.inl"

#include <SDL_net.h>

//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------

Uint16 net_seed = 0;

Uint8 network_on = kfalse;
Uint8* netlist_data = NULL;

Uint32 server_guid, net_local_guid;
static char login_string[1024];

static Sint32 needs_player_update = kfalse;

IPaddress         remote_ip_address = {0,0};
TCPsocket         remote_socket_tcp = NULL;
UDPsocket         remote_socket_udp = NULL;
bool_t            remote_connected = kfalse;
SDLNet_SocketSet  remote_socket_set = NULL;

IPaddress         local_ip_address = {0,0};
TCPsocket         local_socket_tcp = NULL;
int               local_port_udp   = -1;
UDPsocket         local_socket_udp = NULL;
bool_t            local_connected = kfalse;
SDLNet_SocketSet  local_socket_set = NULL;

int               remote_list_count = 0;
remote_data_t     remote_list[MAX_REMOTE];

Uint8  global_version_error = kfalse;
Uint16 required_executable_version = kfalse;
Uint16 required_data_version = kfalse;

NET_SCRIPT_INFO netscript;

void cl_transmit_player_update();
void cl_transmit_room_update();

void cl_crack_chat_packet(PACKET * pp);
void cl_crack_room_update_packet(PACKET * pp, IPaddress * ip);
void network_receive_room_update_tcp(PACKET * pp, IPaddress * ip);

void   cl_clean_remote_udp();
bool_t cl_resolve_remote_udp();
void   cl_clean_remote_tcp();
bool_t cl_resolve_remote_tcp();

void   cl_clean_local_udp();
bool_t cl_resolve_local_udp();
void   cl_clean_local_tcp();
bool_t cl_resolve_local_tcp();

//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------
// NETWORK
//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------
void packet_begin(PACKET * pp, Uint32 id, Uint32 type)
{
    pp->data.type     = (Uint8) type;
    pp->data.checksum = 0;
    pp->data.seed     = 0;
    pp->length        = PACKET_HEADER_SIZE;
    pp->readpos       = PACKET_HEADER_SIZE;
    packet_add_uint32(pp, id);
}
// pp->data.type     is the packet type...
// pp->data.checksum is the checksum
// pp->data.seed     is the random seed

//-----------------------------------------------------------------------------------------------
void packet_add_data(PACKET * pp, void * data, size_t sz)
{
    memcpy(&pp->data.buffer[pp->length], data, sz);
    pp->length += sz;
}


//-----------------------------------------------------------------------------------------------
void packet_add_string(PACKET * pp, char * string)
{
    pp->counter = 0;

    while (pp->length < MAX_PACKET_SIZE - 1 && string[pp->counter] != 0x00)
    {
        pp->data.buffer[pp->length] = string[pp->counter];
        pp->length++;
        pp->counter++;
    }

    pp->data.buffer[pp->length] = 0;
    pp->length++;
}

//-----------------------------------------------------------------------------------------------
void packet_add_uint32(PACKET * pp, Uint32 number)
{
    *(Uint32 *)(&pp->data.buffer[pp->length]) =  SF_Swap32(number);

    pp->length += sizeof(Uint32);
}

//-----------------------------------------------------------------------------------------------
void packet_add_uint16(PACKET * pp, Uint16 number)
{
    *(Uint16 *)(&pp->data.buffer[pp->length]) = SF_Swap16(number);

    pp->length += sizeof(Uint16);
}

//-----------------------------------------------------------------------------------------------
void packet_add_uint8(PACKET * pp, Uint8 number)
{
    pp->data.buffer[pp->length] = number;
    pp->length++;
}

//-----------------------------------------------------------------------------------------------
Sint8 packet_read_string(PACKET * pp, char * string)
{
    pp->counter = 0;

    while (pp->data.buffer[pp->readpos] != 0 && pp->readpos < MAX_PACKET_SIZE && pp->counter < 250)
    {
        if (NULL != string)
            string[pp->counter] = pp->data.buffer[pp->readpos];

        pp->counter++;
        pp->readpos++;
    }

    string[pp->counter] = 0;
    pp->readpos++;

    return ktrue;
}

//-----------------------------------------------------------------------------------------------
Sint8 packet_read_uint32(PACKET * pp, Uint32* to_set)
{
    Uint32 * tmp;

    if (pp->readpos + sizeof(Uint32) >= (Uint16)pp->length || pp->readpos + sizeof(Uint32) >= MAX_PACKET_SIZE)
        return kfalse;

    tmp = (Uint32*)(&pp->data.buffer[pp->readpos]);
    pp->readpos += sizeof(Uint32);

    if (NULL != to_set)
        *to_set = SF_Swap32(*tmp);


    return ktrue;
}

//-----------------------------------------------------------------------------------------------
Sint8 packet_read_uint16(PACKET * pp, Uint16* to_set)
{
    Uint16 * tmp;

    if (NULL == to_set) return kfalse;

    if (pp->readpos + sizeof(Uint16) >= (Uint16)pp->length || pp->readpos + sizeof(Uint16) >= MAX_PACKET_SIZE)
        return kfalse;

    tmp = (Uint16*)(&pp->data.buffer[pp->readpos]);
    pp->readpos += sizeof(Uint16);

    if (NULL != to_set)
        *to_set = SF_Swap16(*tmp);

    return ktrue;
}

//-----------------------------------------------------------------------------------------------
Sint8 packet_read_uint08(PACKET * pp, Uint8* to_set)
{
    Uint8 tmp;

    if (pp->readpos + sizeof(Uint8) >= (Uint16)pp->length || pp->readpos + sizeof(Uint8) >= MAX_PACKET_SIZE)
        return kfalse;

    tmp = pp->data.buffer[pp->readpos];
    pp->readpos++;

    if (NULL != to_set)
        *to_set = pp->data.buffer[pp->readpos];

    return ktrue;
}

//-----------------------------------------------------------------------------------------------
void packet_encrypt(PACKET * pp)
{
    pp->data.seed        = random_number();
    pp->data.checksum   = pp->data.seed;
    pp->counter = PACKET_HEADER_SIZE;

    while (pp->counter < pp->length)
    {
        pp->data.buffer[pp->counter] += get_random(pp->data.seed + 2173 - pp->counter);
        pp->counter++;
    }
}

//-----------------------------------------------------------------------------------------------
void packet_decrypt(PACKET * pp)
{
    pp->counter = PACKET_HEADER_SIZE;
    pp->data.seed = pp->data.checksum;

    while (pp->counter < pp->length)
    {
        pp->data.buffer[pp->counter] -= get_random(pp->data.seed + 2173 - pp->counter);
        pp->counter++;
    }
}

//-----------------------------------------------------------------------------------------------
void calculate_packet_checksum(PACKET * pp)
{
    pp->data.checksum = 0;
    pp->counter = PACKET_HEADER_SIZE;

    while (pp->counter < pp->length)
    {
        pp->data.checksum += pp->data.buffer[pp->counter];
        pp->counter++;
    }
}

//-----------------------------------------------------------------------------------------------
void packet_end(PACKET * pp)
{
    calculate_packet_checksum(pp);
    pp->data.seed = pp->data.checksum;
};

//-----------------------------------------------------------------------------------------------
void network_close(void)
{
    // <ZZ> This function shuts down the network...
    log_info(0, "Shutting down the network");
    SDLNet_Quit();
}

//-----------------------------------------------------------------------------------------------
Sint8 network_receive_packet_tcp(PACKET * pp, TCPsocket socket)
{
    packet_begin(pp, net_local_guid, PACKET_TYPE_NULL);
    pp->length = SDLNet_TCP_Recv(socket, pp->data.buffer, MAX_PACKET_SIZE);

    if (pp->length > 0)
    {
        packet_decrypt(pp);
    }

    return pp->length > 0;
};

//-----------------------------------------------------------------------------------------------
Sint8 network_transmit_packet_tcp(PACKET * pp, Uint16 *seed, TCPsocket socket)
{
    Uint16 temp_random;
    Sint8 retval = kfalse;

    if (NULL == socket) return kfalse;

    if (0 == pp->length) return kfalse;

    temp_random = g_rand.next;
    g_rand.next = *seed;
    packet_encrypt(pp);
    retval = (pp->length == SDLNet_TCP_Send(socket, pp->data.buffer, pp->length));
    packet_decrypt(pp);
    *seed = g_rand.next;
    g_rand.next = temp_random;

    return retval;
};

//-----------------------------------------------------------------------------------------------
Sint8 network_transmit_packet_udp(PACKET * pp, Uint16 *seed, UDPsocket socket, IPaddress *ip)
{
    Uint16    temp_random;
    UDPpacket udp_packet;
    Sint8     retval = kfalse;

    if (NULL == socket)     return retval;

    if (0 == pp->length) return retval;

    udp_packet.channel = -1;
    udp_packet.data = pp->data.buffer;
    udp_packet.len = pp->length;
    udp_packet.maxlen = MAX_PACKET_SIZE;
    udp_packet.address.host = ip->host;

    udp_packet.address.port = SDL_SwapBE16(UDP_PORT_SERVER);  // I want to cry...

    temp_random = g_rand.next;
    g_rand.next = *seed;
    packet_encrypt(pp);
    retval = SDLNet_UDP_Send(socket, -1, &udp_packet);
    packet_decrypt(pp);
    *seed = g_rand.next;
    g_rand.next = temp_random;

    return retval;
};

//-----------------------------------------------------------------------------------------------
Sint8 network_receive_packet_udp(PACKET * pp, UDPsocket socket, IPaddress *ip)
{
    // Let's try to follow SDLNet's little format...

    UDPpacket udp_packet;

    if (!network_on || NULL == socket) return kfalse;

    packet_begin(pp, net_local_guid, PACKET_TYPE_NULL);

    udp_packet.channel = -1;
    udp_packet.data    = pp->data.buffer;
    udp_packet.len     = MAX_PACKET_SIZE;
    udp_packet.maxlen  = MAX_PACKET_SIZE;

    if (SDLNet_UDP_Recv(socket, &udp_packet) < 0) return kfalse;

    // We've got a new pp->..
    log_info(0, "Got a UDP packet from %d.%d.%d.%d:%d...  Length = %d, Type = %d", ((Uint8*)&udp_packet.address.host)[0], ((Uint8*)&udp_packet.address.host)[1], ((Uint8*)&udp_packet.address.host)[2], ((Uint8*)&udp_packet.address.host)[3], (((Uint8*)&udp_packet.address.port)[0] << 8) | ((Uint8*)&udp_packet.address.port)[1], udp_packet.len, pp->data.type);
    pp->length = udp_packet.len;

    if (NULL != ip)
    {
        ip->host = udp_packet.address.host;
        ip->port = udp_packet.address.port;
    };

    packet_decrypt(pp);

    return packet_valid(pp);
};

//-----------------------------------------------------------------------------------------------
unsigned char packet_valid(PACKET * pp)
{
    calculate_packet_checksum(pp);
    return (pp->data.seed == pp->data.checksum);
}

//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------
// SERVER
//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------

#if defined(SOULFU_CLIENT) || defined(SOULFU_SERVER)
void network_clear_remote_list()
{
    // <ZZ> This function clears the list of other network players...
    Uint16 i;
    remote_list_count = 0;
    repeat(i, MAX_REMOTE)
    {
        remote_list[i].on = kfalse;
    }
}
#endif

//-----------------------------------------------------------------------------------------------
#if defined(SOULFU_CLIENT) || defined(SOULFU_SERVER)
Uint8 network_add_remote(char* remote_name)
{
    // <ZZ> This function adds a new network player by either address ("192.168.0.12") or
    //      name ("Frizzlesnitz")...  Returns ktrue if it worked (usually does)...
    Uint16 i;
    IPaddress temp_address;


    if (remote_name)
    {
        log_info(0, "Trying to add %s as a new remote...", remote_name);
        if (SDLNet_ResolveHost(&temp_address, remote_name, UDP_PORT_SERVER) == 0)
        {
            log_info(0, "Found IP...  It's %d.%d.%d.%d", ((Uint8*)&temp_address.host)[0], ((Uint8*)&temp_address.host)[1], ((Uint8*)&temp_address.host)[2], ((Uint8*)&temp_address.host)[3]);
        }
        else
        {
            log_error(0, "Couldn't find the IP address...  Oh, well...");
            return kfalse;
        }
    }
    else
    {
        return kfalse;
    }


    if (remote_list_count < MAX_REMOTE)
    {
        repeat(i, MAX_REMOTE)
        {
            if (remote_list[i].on)
            {
                if (remote_list[i].address.host == temp_address.host)
                {
                    log_error(0, "That IP address is already used by remote %d...", i);
                    return kfalse;
                }
            }
        }


        repeat(i, MAX_REMOTE)
        {
            if (remote_list[i].on == kfalse)
            {
                log_info(0, "Added new remote as remote number %d", i);
                remote_list[i].address.host = temp_address.host;
                remote_list[i].on = ktrue;
                remote_list[i].room_number = UINT16_MAX;
                remote_list[i].is_neighbor = kfalse;
                remote_list_count++;
                return ktrue;
            }
        }
    }
    log_error(0, "Too many remotes all ready...  Oh, well...");
    return kfalse;
}

#endif

//-----------------------------------------------------------------------------------------------
#if defined(SOULFU_CLIENT) || defined(SOULFU_SERVER)
void network_delete_remote(Uint16 remote)
{
    if (remote < MAX_REMOTE)
    {
        if (remote_list[remote].on)
        {
            remote_list[remote].on = kfalse;
            remote_list_count--;
        }
    }
}
#endif




//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------
// CLIENT
//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------

void cl_clean_remote_tcp()
{
    cl_clean_remote_udp();

    if( NULL != remote_socket_tcp )
    {
        SDLNet_TCP_Close(remote_socket_tcp);
        remote_socket_tcp = NULL;
    }

}

//-----------------------------------------------------------------------------------------------
bool_t cl_resolve_remote_tcp()
{
    remote_socket_tcp = NULL;

    log_info(0, "Trying to resolve the server IP address : \"%s\"...", MAIN_SERVER_NAME);

    if ( SDLNet_ResolveHost(&remote_ip_address, MAIN_SERVER_NAME, TCPIP_PORT_SERVER) >= 0 )
    {
        log_info(0, "Found IP...  It's %d.%d.%d.%d", ((Uint8*)&remote_ip_address.host)[0], ((Uint8*)&remote_ip_address.host)[1], ((Uint8*)&remote_ip_address.host)[2], ((Uint8*)&remote_ip_address.host)[3]);
        remote_socket_tcp = SDLNet_TCP_Open(&remote_ip_address);
    }

    return (NULL != remote_socket_tcp);
}


//-----------------------------------------------------------------------------------------------
void cl_clean_remote_udp()
{
    // remove any old udp info

    if( NULL != remote_socket_udp )
    {
        if( NULL != remote_socket_set )
        {
            SDLNet_UDP_DelSocket(remote_socket_set, remote_socket_udp);
        }
        remote_socket_udp = NULL;
    }

    if ( NULL != remote_socket_set )
    {
        SDLNet_FreeSocketSet(remote_socket_set);
        remote_socket_set = NULL;
    }
}

//-----------------------------------------------------------------------------------------------
bool_t cl_resolve_remote_udp()
{
    cl_clean_remote_udp();

    if( NULL == remote_socket_tcp ) return kfalse;

    // Now try to open a UDP socket for talking to other remotes...
    log_info(0, "Trying to open up port %d for remote UDP networking...", UDP_PORT_SERVER );

    // udp transmit port to the server
    remote_socket_udp = SDLNet_UDP_Open(UDP_PORT_SERVER);

    if ( remote_socket_udp )
    {
        /* Allocate the socket set for polling the network */
        remote_socket_set = SDLNet_AllocSocketSet(2);
        if ( remote_socket_set == NULL )
        {
            log_message(0, "ERROR : Couldn't create remote socket set: %s\n", SDLNet_GetError());
            exit(2);
        }

        SDLNet_TCP_AddSocket(remote_socket_set, remote_socket_tcp);
        SDLNet_UDP_AddSocket(remote_socket_set, remote_socket_udp);
    }

    return (NULL != remote_socket_set);
}

//-----------------------------------------------------------------------------------------------
void cl_clean_local_tcp()
{
    cl_clean_local_udp();

    if( NULL != local_socket_tcp )
    {
        SDLNet_TCP_Close(local_socket_tcp);
        local_socket_tcp = NULL;
    }

}

//-----------------------------------------------------------------------------------------------
bool_t cl_resolve_local_tcp()
{
    int i;
    IPaddress * paddress = NULL;

    // udp recieve port for this machine
    for ( i = 0; (local_socket_udp == NULL) && i < MAX_REMOTE; ++i )
    {
        local_port_udp   = UDP_PORT_CLIENT + i;
        local_socket_udp = SDLNet_UDP_Open(local_port_udp);
    }

    // Round about way of finding our local address...
    log_info(0, "Looking for IP Address of local machine...");
    paddress = SDLNet_UDP_GetPeerAddress(local_socket_udp, -1);
    if (NULL != paddress)
    {
        memcpy(&local_ip_address, paddress, sizeof(IPaddress));
        log_info(0, "Found IP...  It's %d.%d.%d.%d", ((Uint8*)&local_ip_address.host)[0], ((Uint8*)&local_ip_address.host)[1], ((Uint8*)&local_ip_address.host)[2], ((Uint8*)&local_ip_address.host)[3]);
    }

    local_ip_address.host = LOCALHOST;
    sprintf(run_string[0], "%s", SDLNet_ResolveIP(&local_ip_address));
    log_info(0, "Said that LOCALHOST is %s", run_string[0]);
    if (0 == SDLNet_ResolveHost(&local_ip_address, run_string[0], local_port_udp))
    {
        log_info(0, "Found IP...  It's %d.%d.%d.%d", ((Uint8*)&local_ip_address.host)[0], ((Uint8*)&local_ip_address.host)[1], ((Uint8*)&local_ip_address.host)[2], ((Uint8*)&local_ip_address.host)[3]);
    }

    if( 0 == memcmp(&local_ip_address, &remote_ip_address, sizeof(IPaddress)) )
    {
        local_socket_tcp = remote_socket_tcp;
    }
    else
    {
        local_socket_tcp = SDLNet_TCP_Open(&local_ip_address);
    }

    return (NULL != local_socket_tcp);
}

//-----------------------------------------------------------------------------------------------
void cl_clean_local_udp()
{
    // remove any old udp info

    if( NULL != local_socket_udp )
    {
        if( NULL != local_socket_set )
        {
            SDLNet_UDP_DelSocket(local_socket_set, local_socket_udp);
        }
        local_socket_udp = NULL;
    }

    if ( NULL != local_socket_set )
    {
        SDLNet_FreeSocketSet(local_socket_set);
        local_socket_set = NULL;
    }
}

//-----------------------------------------------------------------------------------------------
bool_t cl_resolve_local_udp()
{
    IPaddress * paddress = NULL;

    if( NULL == local_socket_tcp ) return kfalse;

    /* Allocate the socket set for polling the network */
    local_socket_set = SDLNet_AllocSocketSet(2);
    if ( local_socket_set == NULL )
    {
        log_message(0, "ERROR : Couldn't create local socket set: %s\n", SDLNet_GetError());
        exit(2);
    }

    SDLNet_TCP_AddSocket(local_socket_set, local_socket_tcp);
    SDLNet_UDP_AddSocket(local_socket_set, local_socket_udp);

    return (NULL != local_socket_set);
}

//-----------------------------------------------------------------------------------------------
#if defined(SOULFU_CLIENT)
Uint8 network_setup(void)
{
    // <ZZ> This function initializes all the networking stuff.  Returns ktrue if networking is
    //      available, kfalse if not.

    SDF_PHEADER pnetlist_header;


    // Turn all of our ports on and stuff
    network_on = kfalse;


    log_info(0, "------------------------------------------");
    log_info(0, "Looking for NETLIST.DAT...");

    netlist_data    = NULL;
    pnetlist_header = sdf_archive_find_filetype("NETLIST", SDF_FILE_IS_DAT);

    if (NULL == pnetlist_header)
    {
        netlist_data = sdf_file_get_data(pnetlist_header);
        log_info(0, "Found NETLIST.DAT...");
    }



    remote_connected = kfalse;
    //network_clear_remote_list();
    log_info(0, "------------------------------------------");
    log_info(0, "Trying to turn on networking...");

    if ( SDLNet_Init() < 0)
    {
        // No network this time...
        log_error(0, "Network failed!");
        log_error(0, "SDLNet told us...  %s", SDLNet_GetError());
    }
    else
    {
        // Network started up okay...
        log_info(0, "Network started okay!");

        if( !cl_resolve_remote_tcp() )
        {
            log_warning(0, "Main server was not found");
            log_message(0, "You can still use the IP Typer Inner though");
        }
        else
        {
            log_info(0, "Socket to main server open'd correctly...");
            remote_connected = ktrue;
        }

        if (remote_socket_tcp)
        {
            if( !cl_resolve_remote_udp() )
            {
                log_warning(0, "UDP channel to the server could not be opened");
            }
            else
            {
                log_info(0, "Server channel secured...");

            }
        }

        if (remote_socket_tcp)
        {
            if( !cl_resolve_local_tcp() )
            {
                log_warning(0, "Main client was not found");
                log_message(0, "You can still use the IP Typer Inner though");
            }
            else
            {
                log_info(0, "Socket to main client open'd correctly...");
            }

            if (local_socket_tcp)
            {
                if( !cl_resolve_local_udp() )
                {
                    log_warning(0, "UDP channel to the client could not be opened");
                }
                else
                {
                    log_info(0, "Client channel secured...");
                    local_connected = ktrue;
                }
            }
        }

        if( remote_connected && local_connected )
        {
            // !!!BAD!!!
            // !!!BAD!!!
            network_add_remote("FooFoo");
            network_add_remote("Frizzlesnitz");
            // !!!BAD!!!
            // !!!BAD!!!

            // Remember to turn it off
            network_on = ktrue;
            atexit(network_close);
        }
        else
        {
            log_error(0, "Uh, oh...  We couldn't open the port for some reason.");
            log_error(0, "No networking for you...");
        }

    }



    log_info(0, "------------------------------------------");
    return network_on;
}

#endif

//-----------------------------------------------------------------------------------------------
#if defined(SOULFU_SERVER)
void sv_transmit_packet(Uint8 send_code, PACKET * pp)
{
    // <ZZ> This function sends a packet to the specified computer...
    UDPpacket udp_packet;


    // !!!BAD!!!
    // !!!BAD!!!
    // !!!BAD!!!
    // !!!BAD!!!
    // !!!BAD!!!
    // !!!BAD!!!
    // !!!BAD!!!
    // !!!BAD!!!
    // !!!BAD!!!
    // !!!BAD!!!
    // !!!BAD!!!
    if (send_code == NETWORK_ALL_REMOTES_IN_ROOM)
    {
        send_code = NETWORK_ALL_REMOTES_IN_GAME;
    }
    // !!!BAD!!!
    // !!!BAD!!!
    // !!!BAD!!!
    // !!!BAD!!!
    // !!!BAD!!!
    // !!!BAD!!!
    // !!!BAD!!!
    // !!!BAD!!!
    // !!!BAD!!!



    log_info(0, "sv_transmit_packet() called");
    if (network_on)
    {
        log_info(0, "network_on");

        // Let's figger out who we're sending it to...
        if (send_code == NETWORK_ALL_REMOTES_IN_GAME || send_code == NETWORK_ALL_REMOTES_IN_ROOM || send_code == NETWORK_ALL_REMOTES_IN_NEARBY_ROOMS)
        {
            int i;

            // Now let's organize our packet into SDLNet's little format...
            log_info(0, "send via UDP");


            // Send the packet to all players who need to get it...
            repeat(i, MAX_REMOTE)
            {
                if (remote_list[i].on)
                {
                    //if (send_code == NETWORK_ALL_REMOTES_IN_GAME || remote_list[i].room_number == map_current_room || (remote_list[i].is_neighbor && send_code == NETWORK_ALL_REMOTES_IN_NEARBY_ROOMS))
                    {
                        //#ifdef ALLOW_LOCAL_PACKETS
                        //            if (ktrue)
                        //#else
                        //            if (remote_list[i].address.host != LOCALHOST && remote_list[i].address.host != local_ip_address.host)
                        //#endif
                        {
                            log_info(0, "Sending to remote %d (%d.%d.%d.%d)", i, ((Uint8*)&remote_list[i].address.host)[0], ((Uint8*)&remote_list[i].address.host)[1], ((Uint8*)&remote_list[i].address.host)[2], ((Uint8*)&remote_list[i].address.host)[3]);
                            udp_packet.channel      = -1;
                            udp_packet.data         = pp->data.buffer;
                            udp_packet.len          = pp->length;
                            udp_packet.maxlen       = MAX_PACKET_SIZE;
                            udp_packet.address.host = remote_ip_address.host;
                            udp_packet.address.port = SF_Swap16(UDP_PORT_SERVER);  // I want to cry...
                            if (!SDLNet_UDP_Send(remote_socket_udp, -1, &udp_packet))
                            {
                                log_info(0, "Got error from SDLNet...  %s", SDLNet_GetError());
                            }
                        }
                        //else
                        //{
                        //  log_info(0, "Skipping remote %d because it's the local machine", i);
                        //}
                    }
                }
            }
        }
    }
}
#endif

//-----------------------------------------------------------------------------------------------
#if defined(SOULFU_CLIENT)
Uint16 network_find_remote_character(Uint32 network_guid, Uint8 local_index_on_remote)
{
    // <ZZ> This function attempts to find the index of a character on the local computer, who is
    //      hosted on the given remote - with the given local index number on that remote...
    //      This lets me find something like character number 23 on Bob's computer, which is
    //      handled as character 42 on my computer...  It returns the index on my computer, or
    //      UINT16_MAX if a match can't be found...
    Uint16 i;
    CHR_DATA* chr_data;

    repeat(i, MAX_CHARACTER)
    {
        chr_data = chr_data_get_ptr( i );
        if (NULL == chr_data) continue;

        if (chr_data->rem_ip_add.i32 == network_guid)
        {
            if (chr_data->remote_ind == local_index_on_remote)
            {
                return i;
            }
        }
    }

    return MAX_CHARACTER;
}

#endif
//-----------------------------------------------------------------------------------------------
#if defined(SOULFU_CLIENT) || defined(SOULFU_SERVER)
void cl_crack_chat_packet(PACKET * pp)
{
    Uint8  character_class;
    Uint32 sending_guid;

    packet_read_uint32(pp, &sending_guid);         // Source machine's guid
    packet_read_uint08(pp, &character_class);     // Speaker class
    packet_read_string(pp, run_string[0]);              // Speaker name
    packet_read_string(pp, run_string[1]);              // Message
    message_add(run_string[1], run_string[0], ktrue);
}
#endif

//-----------------------------------------------------------------------------------------------
#if defined(SOULFU_CLIENT)
void cl_crack_room_update_packet(PACKET * pp, IPaddress * ip)
{
    Uint16 room_number;
    Uint16 seed;
    Uint8 door_flags;
    Uint16 i, num_char;
    Sint16 length;
    float x, y, z;
    CHR_DATA* chr_data;
    char filename[9];
    Uint32 sending_guid;

    SDF_PDATA   script_file_data;
    SDF_PHEADER script_file_header;

    packet_read_uint32(pp, &sending_guid);       // Source machine's guid
    packet_read_uint16(pp, &room_number);        // The room number this sender is in...
    packet_read_uint16(pp, &seed);               // The map seed this sender is using...

    if (room_number == map_current_room && seed == map_room_data[map_current_room][10] && netlist_data )
    {
        // Start to kill off any of this ip->host's characters...
        repeat(i, MAX_CHARACTER)
        {
            chr_data = chr_data_get_ptr( i );
            if (NULL == chr_data) continue;

            if (chr_data->rem_ip_add.i32 == sending_guid)
            {
                chr_data->hits = 0;  // Give 'em 0 hits...
            }
        }

        packet_read_uint08(pp, &door_flags);      // The door flags for this room...
        packet_read_uint16(pp, &num_char);        // The number of characters in this pp->..  (each character should have 11 or 19 bytes of data...)
        length = pp->length - pp->readpos;    // The number of bytes remaining...

        while (num_char > 0 && length >= 11)
        {
            netscript.newly_spawned = kfalse;
            packet_read_uint08(pp, &netscript.remote_index);
            packet_read_uint08(pp, &netscript.netlist_index);
            packet_read_uint08(pp, &netscript.z);
            packet_read_uint16(pp, &netscript.x);
            packet_read_uint16(pp, &netscript.y);
            netscript.x = netscript.x | ((netscript.z & 192) << 2);
            netscript.y = netscript.y | ((netscript.z & 48) << 4);
            netscript.z = netscript.z & 0x0F;
            packet_read_uint08(pp, &netscript.facing);
            packet_read_uint08(pp, &netscript.action);
            netscript.extra_data = netscript.action >> 7;
            netscript.action = netscript.action & 0x7F;
            packet_read_uint08(pp, &netscript.team);
            netscript.poison = (netscript.team >> 5) & 1;
            netscript.petrify = (netscript.team >> 4) & 1;
            netscript.alpha = (netscript.team & 8) ? (0x40) : (0xFF);
            netscript.deflect = (netscript.team >> 2) & 1;
            netscript.haste = (netscript.team >> 1) & 1;
            netscript.other_enchant = (netscript.team & 1);
            netscript.team = netscript.team >> 6;
            packet_read_uint08(pp, &netscript.eqleft);
            packet_read_uint08(pp, &netscript.eqright);
            packet_read_uint08(pp, &netscript.eqcol01);

            if (netscript.extra_data && length >= 19)
            {
                // We've got more data coming...
                packet_read_uint08(pp, &netscript.eqcol23);
                packet_read_uint08(pp, &netscript.eqspec1);
                packet_read_uint08(pp, &netscript.eqspec2);
                packet_read_uint08(pp, &netscript.eqhelm);
                packet_read_uint08(pp, &netscript.eqbody);
                packet_read_uint08(pp, &netscript.eqlegs);
                packet_read_uint08(pp, &netscript.cclass);
                packet_read_uint16(pp, &netscript.mount_index);

                if (netscript.mount_index != netscript.remote_index)
                {
                    // Character is riding a mount...
                    netscript.mount_index = network_find_remote_character(ip->host, (Uint8) netscript.mount_index);
                }
                else
                {
                    netscript.mount_index = UINT16_MAX;
                }
            }
            else
            {
                // Script shouldn't ask for these, but just in case...
                netscript.eqcol23 = 0;
                netscript.eqspec1 = 0;
                netscript.eqspec2 = 0;
                netscript.eqhelm = 0;
                netscript.eqbody = 0;
                netscript.eqlegs = 0;
                netscript.cclass = 0;
                netscript.mount_index = UINT16_MAX;
            }


            // Okay, we've read all of the data for this character, now let's see if we need to spawn it...
            i = network_find_remote_character(sending_guid, netscript.remote_index);
            {
                CHR_DATA * pchr_remote;

                pchr_remote = chr_data_get_ptr( i );

                if (NULL == pchr_remote)
                {
                    // We didn't find this character - that means we'll have to try to spawn a new one of the appropriate type...
                    x = (netscript.x - ((float)0x0200)) * 0.25f;
                    y = (netscript.y - ((float)0x0200)) * 0.25f;
                    z = room_heightmap_height(roombuffer, x, y);
                    z = z + (netscript.z * 2.0f);


                    script_file_data = netlist_data + (netscript.netlist_index << 3);
                    repeat(i, 8)
                    {
                        filename[i] = script_file_data[i];
                    }
                    filename[8] = 0;

                    pchr_remote             = NULL;
                    script_file_data   = NULL;
                    script_file_header = sdf_archive_find_filetype(filename, SDF_FILE_IS_RUN);

                    if (script_file_header)
                    {
                        script_file_data = sdf_file_get_data(script_file_header);
                        pchr_remote = (CHR_DATA*)obj_spawn(NULL, NULL, OBJECT_CHR, x, y, z, script_file_data, UINT16_MAX);

                        if (pchr_remote)
                        {
                            netscript.newly_spawned = ktrue;
                            pchr_remote->rem_ip_add.i32 = sending_guid;
                            pchr_remote->remote_ind     = netscript.remote_index;
                        }
                    }
                }

                // Now let's do this again and give the character a script function call, so we can handle the network data more precisely...
                if (NULL != pchr_remote)
                {
                    pchr_remote->event = EVENT_NETWORK_UPDATE;
                    sf_fast_rerun_script( &(pchr_remote->script_ctxt), FAST_FUNCTION_EVENT);
                }
            }

            num_char--;
            length = pp->length - pp->readpos;
        }


        // Finish killing off any character whose hits haven't been reset...
        repeat(i, MAX_CHARACTER)
        {
            chr_data = chr_data_get_ptr(i);
            if (NULL == chr_data) continue;

            if (chr_data->rem_ip_add.i32 == sending_guid)
            {
                if (chr_data->hits == 0)
                {
                    chr_data->event = EVENT_DAMAGED;
                    attack_info.index = i;
                    attack_info.spin = (chr_data->spin) + 0x8000;
                    sf_fast_rerun_script( &(chr_data->script_ctxt), FAST_FUNCTION_EVENT);
                }
            }
        }

    }

};


#endif
//-----------------------------------------------------------------------------------------------
#if defined(SOULFU_CLIENT)
void network_receive_room_update_tcp(PACKET * pp, IPaddress * ip)
{
    Uint16 room_number;
    Uint16 seed;
    Uint8 door_flags;
    Uint16 i, num_char;
    Sint16 length;
    Uint8* script_file_start;
    float x, y, z;
    CHR_DATA* chr_data;
    char filename[9];
    Uint32 sending_guid;

    packet_read_uint32(pp, &sending_guid);       // Source machine's guid
    packet_read_uint16(pp, &room_number);        // The room number this sender is in...
    packet_read_uint16(pp, &seed);               // The map seed this sender is using...

    if (room_number == map_current_room && seed == map_room_data[map_current_room][10] && netlist_data)
    {
        // Start to kill off any of this ip->host's characters...
        repeat(i, MAX_CHARACTER)
        {
            chr_data = chr_data_get_ptr( i );
            if (NULL == chr_data) continue;

            if (chr_data->rem_ip_add.i32 == sending_guid)
            {
                chr_data->hits = 0;  // Give 'em 0 hits...
            }
        }

        packet_read_uint08(pp, &door_flags);      // The door flags for this room...
        packet_read_uint16(pp, &num_char);        // The number of characters in this pp->..  (each character should have 11 or 19 bytes of data...)
        length = pp->length - pp->readpos;    // The number of bytes remaining...
        while (num_char > 0 && length >= 11)
        {
            netscript.newly_spawned = kfalse;
            packet_read_uint08(pp, &netscript.remote_index);
            packet_read_uint08(pp, &netscript.netlist_index);
            packet_read_uint08(pp, &netscript.z);
            packet_read_uint16(pp, &netscript.x);
            packet_read_uint16(pp, &netscript.y);
            netscript.x = netscript.x | ((netscript.z & 192) << 2);
            netscript.y = netscript.y | ((netscript.z & 48) << 4);
            netscript.z = netscript.z & 0x0F;
            packet_read_uint08(pp, &netscript.facing);
            packet_read_uint08(pp, &netscript.action);
            netscript.extra_data = netscript.action >> 7;
            netscript.action = netscript.action & 0x7F;
            packet_read_uint08(pp, &netscript.team);
            netscript.poison = (netscript.team >> 5) & 1;
            netscript.petrify = (netscript.team >> 4) & 1;
            netscript.alpha = (netscript.team & 8) ? (0x40) : (0xFF);
            netscript.deflect = (netscript.team >> 2) & 1;
            netscript.haste = (netscript.team >> 1) & 1;
            netscript.other_enchant = (netscript.team & 1);
            netscript.team = netscript.team >> 6;
            packet_read_uint08(pp, &netscript.eqleft);
            packet_read_uint08(pp, &netscript.eqright);
            packet_read_uint08(pp, &netscript.eqcol01);
            if (netscript.extra_data && length >= 19)
            {
                // We've got more data coming...
                packet_read_uint08(pp, &netscript.eqcol23);
                packet_read_uint08(pp, &netscript.eqspec1);
                packet_read_uint08(pp, &netscript.eqspec2);
                packet_read_uint08(pp, &netscript.eqhelm);
                packet_read_uint08(pp, &netscript.eqbody);
                packet_read_uint08(pp, &netscript.eqlegs);
                packet_read_uint08(pp, &netscript.cclass);
                packet_read_uint16(pp, &netscript.mount_index);
                if (netscript.mount_index != netscript.remote_index)
                {
                    // Character is riding a mount...
                    netscript.mount_index = network_find_remote_character(ip->host, (Uint8) netscript.mount_index);
                }
                else
                {
                    netscript.mount_index = UINT16_MAX;
                }
            }
            else
            {
                // Script shouldn't ask for these, but just in case...
                netscript.eqcol23 = 0;
                netscript.eqspec1 = 0;
                netscript.eqspec2 = 0;
                netscript.eqhelm = 0;
                netscript.eqbody = 0;
                netscript.eqlegs = 0;
                netscript.cclass = 0;
                netscript.mount_index = UINT16_MAX;
            }


            // Okay, we've read all of the data for this character, now let's see if we need to spawn it...
            i = network_find_remote_character(ip->host, netscript.remote_index);
            {
                CHR_DATA * pchr_network = chr_data_get_ptr(i);

                if ( NULL != pchr_network )
                {
                    SDF_PHEADER script_file_header;

                    // We didn't find this character - that means we'll have to try to spawn a new one of the appropriate type...
                    x = (netscript.x - ((float)0x0200)) * 0.25f;
                    y = (netscript.y - ((float)0x0200)) * 0.25f;
                    z = room_heightmap_height(roombuffer, x, y);
                    z = z + (netscript.z * 2.0f);


                    script_file_start = netlist_data + (netscript.netlist_index << 3);
                    repeat(i, 8)
                    {
                        filename[i] = script_file_start[i];
                    }
                    filename[8] = 0;
                    pchr_network = NULL;
                    script_file_start  = NULL;
                    script_file_header = sdf_archive_find_filetype(filename, SDF_FILE_IS_RUN);
                    if (script_file_header)
                    {
                        script_file_start = sdf_file_get_data(script_file_header);
                        pchr_network = (CHR_DATA*)obj_spawn(NULL, NULL, OBJECT_CHR, x, y, z, script_file_start, UINT16_MAX);
                        if (pchr_network)
                        {
                            netscript.newly_spawned = ktrue;
                            pchr_network->rem_ip_add.i32 = sending_guid;
                            pchr_network->remote_ind = netscript.remote_index;
                        }
                    }
                }

                // Now let's do this again and give the character a script function call, so we can handle the network data more precisely...
                if ( NULL != pchr_network )
                {
                    pchr_network->event = EVENT_NETWORK_UPDATE;
                    sf_fast_rerun_script(&(pchr_network->script_ctxt), FAST_FUNCTION_EVENT);
                }

            }
            num_char--;
            length = pp->length - pp->readpos;
        }


        // Finish killing off any character whose hits haven't been reset...
        repeat(i, MAX_CHARACTER)
        {
            chr_data = chr_data_get_ptr( i );
            if (NULL != chr_data) continue;

            if (chr_data->rem_ip_add.i32 == sending_guid)
            {
                if (chr_data->hits == 0)
                {
                    chr_data->event = EVENT_DAMAGED;
                    attack_info.index = i;
                    attack_info.spin = (chr_data->spin) + 0x8000;
                    sf_fast_rerun_script(&(chr_data->script_ctxt), FAST_FUNCTION_EVENT);
                }
            }
        }
    }

};
#endif

//-----------------------------------------------------------------------------------------------
#if defined(SOULFU_CLIENT)
bool_t cl_listen(void)
{
    // <ZZ> This function checks for incoming packets, and handles 'em all

    Sint8 received = kfalse;
    IPaddress ip;
    PACKET    loc_packet;

    //return if there are no packets waiting
    if ( !network_on || !SDLNet_CheckSockets(local_socket_set, 0) ) return kfalse;

    if ( network_receive_packet_udp(&loc_packet, local_socket_udp, &ip) )
    {
        received = ktrue;
    }
    else if ( network_receive_packet_tcp(&loc_packet, local_socket_tcp) )
    {
        received = ktrue;
        ip = remote_ip_address;
    };

    if ( received  )
    {
        // Screen out any packets we accidentally sent to ourself...
        // log_info(0, "Checksum is okay...");
        //#ifdef ALLOW_LOCAL_PACKETS
        //      if (ktrue)
        //#else
        //      if (udp_packet.address.host != LOCALHOST && udp_packet.address.host != local_ip_address.host)
        //#endif
        {
            log_info(0, "Packet isn't from local machine...");

            loc_packet.readpos = PACKET_HEADER_SIZE;

            switch (loc_packet.data.type)
            {
            case PACKET_TYPE_CHAT:
                cl_crack_chat_packet(&loc_packet);
                break;

            case PACKET_TYPE_SERVER_HANDSHAKE:
                //receive the server's info
                packet_read_uint32(&loc_packet,  &server_guid );
                packet_read_uint32(&loc_packet,  &net_local_guid );
                packet_read_uint16(&loc_packet,  &net_seed );
                packet_read_string(&loc_packet,  login_string );

                //send our info
                needs_player_update = ktrue;
                break;

            case PACKET_TYPE_ROOM_UPDATE:
                cl_crack_room_update_packet(&loc_packet, &ip);
                break;
            }
        }
    }

    return ktrue;
};


#endif

//-----------------------------------------------------------------------------------------------
#if defined(SOULFU_CLIENT)
Sint8 cl_talk()
{
    Sint8 retval = kfalse;

    if ( !network_on ) return retval;

    if (needs_player_update)
    {
        //send our info
        cl_transmit_player_update();
        retval = ktrue;
    };

    return retval;
};

#endif

//-----------------------------------------------------------------------------------------------
Uint8 network_find_script_index(char* filename)
{
    // <ZZ> This function finds a character script filename in the NETLIST.DAT file...  So
    //      we don't have to send the whole thing over the network...  Returns 0 if it didn't
    //      find a match...
    Uint16 i, j;
    Uint8 found;
    char* checkname;

    if (netlist_data)
    {
        checkname = netlist_data + 8;
        i = 1;

        while (i < 0x0100)
        {
            found = ktrue;
            repeat(j, 8)
            {
                if (checkname[j] == filename[j])
                {
                    if (checkname[j] == 0)
                    {
                        return ((Uint8) i);
                    }
                }
                else
                {
                    found = kfalse;
                    j = 8;
                }
            }

            if (found)
            {
                return ((Uint8) i);
            }

            checkname += 8;
            i++;
        }
    }

    return 0;
}




//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------
// Special functions to send certain types of packets...
//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------
void cl_transmit_chat(Uint8 speaker_class, char* speaker_name, char* message)
{
    // <ZZ> This function sends a chat message to all the players in the room (or if message starts with
    //      <ALL> it goes to all in the game)...

    Uint8 send_to_all;
    PACKET loc_packet;

    send_to_all = kfalse;

    if (message)
    {
        if (0 == strncmp(message, "<ALL>", 5) )
        {
            send_to_all = ktrue;
            message += 5;

            if (message[0] == ' ')
            {
                message++;
            }
        }
    }


    packet_begin(&loc_packet, net_local_guid, PACKET_TYPE_CHAT);
    packet_add_uint32(&loc_packet, send_to_all);
    packet_add_uint8(&loc_packet, speaker_class);
    packet_add_string(&loc_packet, speaker_name);
    packet_add_string(&loc_packet, message);
    packet_end(&loc_packet);

    // Spit out the message on the local computer...
    message_add(message, speaker_name, ktrue);

    network_transmit_packet_udp(&loc_packet, &net_seed, local_socket_udp, &remote_ip_address);

    //if (send_to_all)
    //{
    //  log_info(0, "Sending chat message %s to all in game from speaker %s", message, speaker_name);
    //}
    //else
    //{
    //  log_info(0, "Sending chat message %s to all in room from speaker %s", message, speaker_name);
    //}
}

//-----------------------------------------------------------------------------------------------
void cl_transmit_player_update()
{
    // <ZZ> This function sends a room update packet to all the players in the room...
    //      The packet should include all characters that are hosted on the local machine...
    Uint16 facing;
    Uint16 mount;
    float* character_xyz;
    Uint16 x, y;
    float fz;
    Uint8 z;
    Uint8 pmod, local_player_count, i;
    Uint8 action;
    Uint8 misc;
    Uint32 character;
    CHR_DATA* chr_data;

    PACKET loc_packet;


    local_player_count = 0;
    repeat(i, MAX_LOCAL_PLAYER)
    {
        character = local_player_character[i];
        chr_data = chr_data_get_ptr( character );

        if ( NULL != chr_data )
        {
            // Only need to send characters that are hosted locally...
            if ( chr_data->rem_ip_add.i32 == 0 )
            {
                local_player_count++;
            };
        };
    };

    if (local_player_count == 0)
    {
        needs_player_update = ktrue;
        return;
    };

    needs_player_update = kfalse;

    packet_begin(&loc_packet, net_local_guid, PACKET_TYPE_PLAYER_UPDATE);

    packet_add_uint8(&loc_packet, local_player_count);

    repeat(i, MAX_LOCAL_PLAYER)
    {
        CHR_DATA * pchr;

        character = local_player_character[i];
        pchr = chr_data_get_ptr( character );
        if ( NULL != pchr )
        {
            // Only need to send characters that are hosted locally...
            if ( 0 == pchr->rem_ip_add.i32 )
            {
                if (pchr->net_scr_ind)
                {
                    // This character is hosted on the local machine, so let's send it on over...
                    character_xyz = &(pchr->x);
                    packet_add_uint8(&loc_packet, i);                        // Local index number
                    packet_add_uint8(&loc_packet, pchr->net_scr_ind);      // Script index (in NETLIST.DAT)
                    x = ((Uint16)((character_xyz[XX] * ROOM_HEIGHTMAP_PRECISION) + ((float)0x0200))) & 0x03FF;
                    y = ((Uint16)((character_xyz[YY] * ROOM_HEIGHTMAP_PRECISION) + ((float)0x0200))) & 0x03FF;
                    fz = (character_xyz[ZZ] - character_xyz[11]) * 0.5f;  CLIP(0.0f, fz, 15.0f);  z = (Uint8) fz;
                    pmod = ((x >> 8) << 6) | ((y >> 8) << 4) | z;
                    packet_add_uint8(&loc_packet, pmod);                     // Position modifiers (top 2 bits for x) (mid 2 bits for y) (low 4 bits are z above floor)
                    packet_add_uint8(&loc_packet, (Uint8)x);                        // X position (with modifier should range from 0-1023)
                    packet_add_uint8(&loc_packet, (Uint8)y);                        // Y position (with modifier should range from 0-1023)
                    facing = pchr->spin;
                    packet_add_uint8(&loc_packet, (Uint8)(facing >> 8));                   // Facing (should range from 0-0xFF)
                    action = pchr->action;

                    if (pchr->flags & CHAR_FULL_NETWORK)
                    {
                        action = action | 0x80;
                    }

                    mount = pchr->mount;
                    if ( VALID_CHR_RANGE(mount) )
                    {
                        action = action | 0x80;
                    }

                    packet_add_uint8(&loc_packet, action);                   // Action (high bit used if extra character data is to be sent...)
                    misc = pchr->team << 6;

                    if ((pchr->pstimer) > 0)
                    {
                        misc = misc | 0x20;
                    }

                    if ((pchr->pttimer) > 0)
                    {
                        misc = misc | 16;
                    }

                    if (pchr->alpha < 0x80)
                    {
                        misc = misc | 8;
                    }

                    if (pchr->eflags & ENCHANT_FLAG_DEFLECT)
                    {
                        misc = misc | 4;
                    }

                    if (pchr->eflags & ENCHANT_FLAG_HASTE)
                    {
                        misc = misc | 2;
                    }

                    if (pchr->eflags & (ENCHANT_FLAG_SUMMON_3 | ENCHANT_FLAG_LEVITATE | ENCHANT_FLAG_INVISIBLE | ENCHANT_FLAG_MORPH))
                    {
                        misc = misc | 1;
                    }

                    packet_add_uint8(&loc_packet, misc);                     // Miscellaneous (top 2 bits are team) (then 1 bit for poison) (then 1 bit for petrify) (then 1 bit for low alpha) (then 1 bit for enchant_deflect) (then 1 bit for enchant_haste) (then 1 bit if enchanted in any way other than deflect & haste)
                    packet_add_uint8(&loc_packet, pchr->eqslot[0]);      // EqLeft
                    packet_add_uint8(&loc_packet, pchr->eqslot[1]);      // EqRight
                    packet_add_uint8(&loc_packet, pchr->eqcol01);      // EqCol01

                    if (action & 0x80)
                    {
                        // Character is a high-data character...  Extra character data is to be sent...
                        packet_add_uint8(&loc_packet, pchr->eqcol23);      // EqCol23
                        packet_add_uint8(&loc_packet, pchr->eqslot[2]);      // EqSpec1
                        packet_add_uint8(&loc_packet, pchr->eqslot[3]);      // EqSpec2
                        packet_add_uint8(&loc_packet, pchr->eqslot[4]);      // EqHelm
                        packet_add_uint8(&loc_packet, pchr->eqslot[5]);      // EqBody
                        packet_add_uint8(&loc_packet, pchr->eqslot[6]);      // EqLegs
                        packet_add_uint8(&loc_packet, pchr->cclass);      // Character class

                        if ( VALID_CHR_RANGE(mount) )
                        {
                            packet_add_uint8(&loc_packet, (Uint8)mount);                    // Local index number of mount
                        }
                        else
                        {
                            packet_add_uint8(&loc_packet, i);                        // Mount is not valid, so send our own local index number again (since we're obviously not riding ourself)
                        }
                    }
                }
            }
        }

        packet_end(&loc_packet);


        network_transmit_packet_udp(&loc_packet, &net_seed, local_socket_udp, &remote_ip_address);
    }
}

//-----------------------------------------------------------------------------------------------
void cl_transmit_room_update()
{
    // <ZZ> This function sends a room update packet to all the players in the room...
    //      The packet should include all characters that are hosted on the local machine...
    Uint16 facing;
    Uint16 mount;
    float* character_xyz;
    Uint16 x, y;
    float fz;
    Uint8 z;
    Uint8 pmod, local_character_count, i;
    Uint8 action;
    Uint8 misc;

    PACKET loc_packet;

    // Make sure we're in a valid room...
    if (map_current_room < MAX_MAP_ROOM)
    {
        CHR_DATA * chr_data;

        // Count how many characters we need to send over network...
        local_character_count = 0;
        repeat(i, MAX_CHARACTER)
        {
            // Only need to send characters that are used...
            chr_data = chr_data_get_ptr( i );
            if( NULL == chr_data ) continue;

            // Only need to send characters that are hosted locally...
            if ( 0 == chr_data->rem_ip_add.i32 )
            {
                // Is this character's script in NETLIST.DAT?
                if (chr_data->net_scr_ind)
                {
                    // Looks like we've got one to send...
                    local_character_count++;
                }
            }
        }


        if (local_character_count > 0xFF)
        {
            local_character_count = 0xFF;
        }

        log_info(0, "Sending room update packet to all in room (%d characters)", local_character_count);
        message_add("Sending room update packet to remotes...", "NETWORK", kfalse);


        packet_begin(&loc_packet, net_local_guid, PACKET_TYPE_ROOM_UPDATE);
        packet_add_uint16(&loc_packet, map_current_room);
        packet_add_uint16(&loc_packet, 0);       // !!!BAD!!!  map_seed
        packet_add_uint8(&loc_packet, map_room_data[map_current_room][29]);  // Door flags
        packet_add_uint8(&loc_packet, local_character_count);

        repeat (i, MAX_CHARACTER)
        {
            chr_data = chr_data_get_ptr( i );
            if( NULL == chr_data ) continue;

            if ( 0 == chr_data->rem_ip_add.i32 )
            {
                if ( chr_data->net_scr_ind )
                {
                    // This character is hosted on the local machine, so let's send it on over...
                    character_xyz = &(chr_data->x);
                    packet_add_uint8(&loc_packet, i);                        // Local index number
                    packet_add_uint8(&loc_packet, chr_data->net_scr_ind);      // Script index (in NETLIST.DAT)
                    x = ((Uint16)((character_xyz[XX] * ROOM_HEIGHTMAP_PRECISION) + ((float)0x0200))) & 0x03FF;
                    y = ((Uint16)((character_xyz[YY] * ROOM_HEIGHTMAP_PRECISION) + ((float)0x0200))) & 0x03FF;
                    fz = (character_xyz[ZZ] - character_xyz[11]) * 0.5f;  CLIP(0.0f, fz, 15.0f);  z = (Uint8) fz;
                    pmod = ((x >> 8) << 6) | ((y >> 8) << 4) | z;
                    packet_add_uint8(&loc_packet, pmod);                     // Position modifiers (top 2 bits for x) (mid 2 bits for y) (low 4 bits are z above floor)
                    packet_add_uint8(&loc_packet, (Uint8)x);                        // X position (with modifier should range from 0-1023)
                    packet_add_uint8(&loc_packet, (Uint8)y);                        // Y position (with modifier should range from 0-1023)
                    facing = chr_data->spin;
                    packet_add_uint8(&loc_packet, (Uint8)(facing >> 8));     // Facing (should range from 0-0xFF)
                    action = chr_data->action;

                    if ( chr_data->flags & CHAR_FULL_NETWORK)
                    {
                        action = action | 0x80;
                    }

                    mount = chr_data->mount;
                    if ( VALID_CHR_RANGE(mount) )
                    {
                        action = action | 0x80;
                    }

                    packet_add_uint8(&loc_packet, action);                   // Action (high bit used if extra character data is to be sent...)
                    misc = chr_data->team << 6;

                    if ( chr_data->pstimer > 0)
                    {
                        misc = misc | 0x20;
                    }

                    if ((chr_data->pttimer) > 0)
                    {
                        misc = misc | 16;
                    }

                    if (chr_data->alpha < 0x80)
                    {
                        misc = misc | 8;
                    }

                    if (chr_data->eflags & ENCHANT_FLAG_DEFLECT)
                    {
                        misc = misc | 4;
                    }

                    if (chr_data->eflags & ENCHANT_FLAG_HASTE)
                    {
                        misc = misc | 2;
                    }

                    if (chr_data->eflags & (ENCHANT_FLAG_SUMMON_3 | ENCHANT_FLAG_LEVITATE | ENCHANT_FLAG_INVISIBLE | ENCHANT_FLAG_MORPH))
                    {
                        misc = misc | 1;
                    }

                    packet_add_uint8(&loc_packet, misc);                     // Miscellaneous (top 2 bits are team) (then 1 bit for poison) (then 1 bit for petrify) (then 1 bit for low alpha) (then 1 bit for enchant_deflect) (then 1 bit for enchant_haste) (then 1 bit if enchanted in any way other than deflect & haste)
                    packet_add_uint8(&loc_packet, chr_data->eqslot[0]);      // EqLeft
                    packet_add_uint8(&loc_packet, chr_data->eqslot[1]);      // EqRight
                    packet_add_uint8(&loc_packet, chr_data->eqcol01);      // EqCol01

                    if (action & 0x80)
                    {
                        // Character is a high-data character...  Extra character data is to be sent...
                        packet_add_uint8(&loc_packet, chr_data->eqcol23);      // EqCol23
                        packet_add_uint8(&loc_packet, chr_data->eqslot[2]);      // EqSpec1
                        packet_add_uint8(&loc_packet, chr_data->eqslot[3]);      // EqSpec2
                        packet_add_uint8(&loc_packet, chr_data->eqslot[4]);      // EqHelm
                        packet_add_uint8(&loc_packet, chr_data->eqslot[5]);      // EqBody
                        packet_add_uint8(&loc_packet, chr_data->eqslot[6]);      // EqLegs
                        packet_add_uint8(&loc_packet, chr_data->cclass);      // Character class

                        if ( VALID_CHR_RANGE(mount) )
                        {
                            packet_add_uint8(&loc_packet, (Uint8)mount);                    // Local index number of mount
                        }
                        else
                        {
                            packet_add_uint8(&loc_packet, i);                        // Mount is not valid, so send our own local index number again (since we're obviously not riding ourself)
                        }
                    }

                    local_character_count--;

                    if (local_character_count == 0) break;
                }
            }
        }

        packet_end(&loc_packet);



        network_transmit_packet_udp(&loc_packet, &net_seed, local_socket_udp, &remote_ip_address);
    }
}


//-----------------------------------------------------------------------------------------------
void cl_transmit_join()
{
    // <ZZ> This function sends an "I WANNA PLAY" mesage to a server
    //      The server will respond with a "HANDSHAKE" message.

    PACKET loc_packet;

    packet_begin(&loc_packet, net_local_guid, PACKET_TYPE_I_WANNA_PLAY);
    packet_add_uint32(&loc_packet, net_local_guid);
    packet_end(&loc_packet);

    network_transmit_packet_udp(&loc_packet, &net_seed, local_socket_udp, &remote_ip_address);
}

//-----------------------------------------------------------------------------------------------
void cl_transmit_quit()
{
    PACKET loc_packet;

    packet_begin(&loc_packet, net_local_guid, PACKET_TYPE_I_WANNA_QUIT);
    packet_add_uint32(&loc_packet, net_local_guid);
    packet_end(&loc_packet);

    network_transmit_packet_udp(&loc_packet, &net_seed, local_socket_udp, &remote_ip_address);
}

#include "runsrc.h"
#include "soulfu.h"
#include "display.h"
#include "input.h"
#include "random.h"
#include "gameseed.h"
#include "network.h"
#include "input.h"
#include "dcodesrc.h"

#include "soulfu_math.inl"
#include "pxss_compile.inl"
#include "pxss_run.inl"
#include "script_extensions.inl"
#include "object.inl"


#define ALPHA_NONE 0
#define ALPHA_TRANS 0x0100
#define ALPHA_LIGHT 0x0200

// For SYS_NETWORKSCRIPT
#define NETWORK_SCRIPT_NEWLY_SPAWNED    0
#define NETWORK_SCRIPT_EXTRA_DATA       1
#define NETWORK_SCRIPT_REMOTE_INDEX     2
#define NETWORK_SCRIPT_NETLIST_INDEX    3
#define NETWORK_SCRIPT_X                4
#define NETWORK_SCRIPT_Y                5
#define NETWORK_SCRIPT_Z                6
#define NETWORK_SCRIPT_FACING           7
#define NETWORK_SCRIPT_ACTION           8
#define NETWORK_SCRIPT_TEAM             9
#define NETWORK_SCRIPT_POISON           10
#define NETWORK_SCRIPT_PETRIFY          11
#define NETWORK_SCRIPT_ALPHA            12
#define NETWORK_SCRIPT_DEFLECT          13
#define NETWORK_SCRIPT_HASTE            14
#define NETWORK_SCRIPT_OTHER_ENCHANT    15
#define NETWORK_SCRIPT_EQLEFT           16
#define NETWORK_SCRIPT_EQRIGHT          17
#define NETWORK_SCRIPT_EQCOL01          18
#define NETWORK_SCRIPT_EQCOL23          19
#define NETWORK_SCRIPT_EQSPEC1          20
#define NETWORK_SCRIPT_EQSPEC2          21
#define NETWORK_SCRIPT_EQHELM           22
#define NETWORK_SCRIPT_EQBODY           23
#define NETWORK_SCRIPT_EQLEGS           24
#define NETWORK_SCRIPT_CLASS            25
#define NETWORK_SCRIPT_MOUNT_INDEX      26

#define UPDATE_END                  1
#define UPDATE_RECOMPILE            2
#define UPDATE_SDFSAVE              3
#define UPDATE_SDFREORGANIZE        4
#define UPDATE_RELOADALLTEXTURES    5

// For modeler...
#define SELECT_ALL                  0
#define SELECT_REMOVE               1
#define SELECT_SWAP                 2
#define SELECT_INVERT               3
#define SELECT_CONNECTED            4

// For Window3D
#define WIN_CAMERA  0
#define WIN_TARGET  1
#define WIN_LIGHT   2
#define WIN_ORTHO   3
#define WIN_FRUSTUM 4
#define WIN_SCALE   5

#define POOF_SELF                   0
#define POOF_TARGET                 1
#define POOF_ALL_OTHER_WINDOWS      2
#define WARN_ALL_OTHER_WINDOWS      3
#define POOF_STUCK_PARTICLES        4
#define POOF_TARGET_STUCK_PARTICLES 5
#define POOF_ALL_PARTICLES          6
#define POOF_ALL_CHARACTERS         7

#define AUTOAIM_X                0
#define AUTOAIM_Y                1
#define AUTOAIM_Z                2
#define AUTOAIM_TARGET           3
#define AUTOAIM_CRUNCH           4
#define AUTOAIM_CRUNCH_BALLISTIC 5



// For SystemGet and SystemSet
enum system_variables_e
{
    SYS_INVALID,
    SYS_PLAYERDEVICE,
    SYS_NUMJOYSTICK,
    SYS_WINDOWSCALE,
    SYS_KEYPRESSED,
    SYS_KEYDOWN,
    SYS_TOPWINDOW,
    SYS_SFXVOLUME,
    SYS_MUSICVOLUME,
    SYS_MESSAGE,
    SYS_USERLANGUAGE,
    SYS_LANGUAGEFILE,
    SYS_QUITGAME,
    SYS_FILENAME,
    SYS_FILESIZE,
    SYS_FILEFTPFLAG,
    SYS_FILECOUNT,
    SYS_FILEFREE,
    SYS_DEVTOOL,
    SYS_CURSORDRAW,
    SYS_CURSORPOS,
    SYS_CURSORBUTTONDOWN,
    SYS_MODELVIEW,
    SYS_MODELMOVE,
    SYS_MODELPLOP,
    SYS_MODELSELECT,
    SYS_MODELDELETE,
    SYS_MODELPLOPTRIANGLE,
    SYS_MODELDELETETRIANGLE,
    SYS_MODELREGENERATE,
    SYS_MODELPLOPJOINT,
    SYS_MODELPLOPBONE,
    SYS_MODELDELETEBONE,
    SYS_MODELDELETEJOINT,
    SYS_MODELJOINTSIZE,
    SYS_MODELBONEID,
    SYS_MODELCRUNCH,
    SYS_MODELROTATEBONES,
    SYS_MODELUNROTATEBONES,
    SYS_MODELHIDE,
    SYS_MODELTRIANGLELINES,
    SYS_MODELCOPYPASTE,
    SYS_MODELWELDVERTICES,
    SYS_MODELWELDTEXVERTICES,
    SYS_MODELFLIP,
    SYS_MODELSCALE,
    SYS_MODELTEXSCALE,
    SYS_MODELANCHOR,
    SYS_MODELTEXFLAGSALPHA,
    SYS_MODELMAXFRAME,
    SYS_MODELADDFRAME,
    SYS_MODELFRAMEFLAGS,
    SYS_MODELFRAMEACTIONNAME,
    SYS_MODELACTIONSTART,
    SYS_MODELADDBASEMODEL,
    SYS_MODELFRAMEBASEMODEL,
    SYS_MODELFRAMEOFFSETX,
    SYS_MODELFRAMEOFFSETY,
    SYS_MODELEXTERNALFILENAME,
    SYS_MODELSHADOWTEXTURE,
    SYS_MODELSHADOWALPHA,
    SYS_MODELINTERPOLATE,
    SYS_MODELCENTER,
    SYS_MODELNUMTRIANGLE,
    SYS_MODELNUMCARTOONLINE,
    SYS_MODELSARELINKABLE,
    SYS_MODELDETEXTURE,
    SYS_MODELPLOPATSTRING,
    SYS_MODELMARKFRAME,
    SYS_MODELCOPYFRAME,
    SYS_MODELAUTOSHADOW,
    SYS_LASTKEYPRESSED,
    SYS_WATERLAYERSACTIVE,
    SYS_CARTOONMODE,
    SYS_CURSORLASTPOS,
    SYS_CURSORINOBJECT,
    SYS_NUMKANJI,
    SYS_NUMKANJITRIANGLE,
    SYS_KANJICOPY,
    SYS_KANJIPASTE,
    SYS_KANJIDELETE,
    SYS_CURSORSCREENPOS,
    SYS_DAMAGEAMOUNT,
    SYS_COLLISIONCHAR,
    SYS_MAINVIDEOFRAME,
    SYS_MAINGAMEFRAME,
    SYS_MAINFRAMESKIP,
    SYS_FINDDATASIZE,
    SYS_BLOCKKEYBOARD,
    SYS_BILLBOARDACTIVE,
    SYS_ARCTANANGLE,
    SYS_MAINSERVERLOCATED,
    SYS_SHARDLIST,
    SYS_SHARDLISTPING,
    SYS_SHARDLISTPLAYERS,
    SYS_VERSIONERROR,
    SYS_JOINGAME,
    SYS_STARTGAME,
    SYS_LEAVEGAME,
    SYS_NETWORKON,
    SYS_MAINGAMEACTIVE,
    SYS_NETWORKGAMEACTIVE,
    SYS_TRYINGTOJOINGAME,
    SYS_JOINPROGRESS,
    SYS_GAMESEED,
    SYS_LOCALPASSWORDCODE,
    SYS_NUMNETWORKPLAYER,
    SYS_NETWORKFINISHED,
    SYS_SERVERSTATISTICS,
    SYS_LOCALPLAYER,
    SYS_SCREENSHAKE,
    SYS_INCLUDEPASSWORD,
    SYS_FPS,
    SYS_RANDOMSEED,
    SYS_MIPMAPACTIVE,
    SYS_WATERTEXTURE,
    SYS_PLAYERCONTROLHANDLED,
    SYS_GLOBALSPAWN,
    SYS_GLOBALATTACKSPIN,
    SYS_GLOBALATTACKER,
    SYS_DEBUGACTIVE,
    SYS_CURRENTITEM,
    SYS_DAMAGECOLOR,
    SYS_ITEMREGISTRYCLEAR,
    SYS_ITEMREGISTRYSCRIPT,
    SYS_ITEMREGISTRYICON,
    SYS_ITEMREGISTRYOVERLAY,
    SYS_ITEMREGISTRYPRICE,
    SYS_ITEMREGISTRYFLAGS,
    SYS_ITEMREGISTRYNAME,
    SYS_ITEMREGISTRYSTR,
    SYS_ITEMREGISTRYDEX,
    SYS_ITEMREGISTRYINT,
    SYS_ITEMREGISTRYMANA,
    SYS_ITEMREGISTRYAMMO,
    SYS_WEAPONGRIP,
    SYS_WEAPONMODELSETUP,
    SYS_WEAPONEVENT,
    SYS_WEAPONFRAMEEVENT,
    SYS_WEAPONUNPRESSED,
    SYS_CHARFASTFUNCTION,
    SYS_FASTANDUGLY,
    SYS_DEFENSERATING,
    SYS_CLEARDEFENSERATING,
    SYS_ITEMDEFENSERATING,
    SYS_CURSORBUTTONPRESSED,
    SYS_CAMERAANGLE,
    SYS_STARTFADE,
    SYS_MEMBUFFER,
    SYS_ROOMUNCOMPRESS,
    SYS_ROOMPLOPATSTRING,
    SYS_ROOMSELECT,
    SYS_ROOMPLOPVERTEX,
    SYS_ROOMDELETEVERTEX,
    SYS_ROOMWELDVERTICES,
    SYS_ROOMCLEAREXTERIORWALL,
    SYS_ROOMPLOPEXTERIORWALL,
    SYS_ROOMEXTERIORWALLFLAGS,
    SYS_ROOMPLOPWAYPOINT,
    SYS_ROOMDELETEWAYPOINT,
    SYS_ROOMLINKWAYPOINT,
    SYS_ROOMUNLINKWAYPOINT,
    SYS_ROOMDELETEBRIDGE,
    SYS_ROOMPLOPTRIANGLE,
    SYS_ROOMPLOPFAN,
    SYS_ROOMDELETETRIANGLE,
    SYS_ROOMGROUP,
    SYS_ROOMOBJECT,
    SYS_ROOMAUTOTEXTURE,
    SYS_ROOMAUTOTRIM,
    SYS_ROOMTEXTUREFLAGS,
    SYS_ROOMHARDPLOPPER,
    SYS_ROOMCOPYPASTE,
    SYS_MOUSETEXT,
    SYS_MOUSELASTOBJECT,
    SYS_MOUSELASTITEM,
    SYS_MOUSELASTSCRIPT,
    SYS_BUMPABORT,
    SYS_MODELAUTOVERTEX,
    SYS_ITEMINDEX,
    SYS_WEAPONREFRESHXYZ,
    SYS_WEAPONREFRESHFLASH,
    SYS_WEAPONREFRESHBONENAME,
    SYS_FASTFUNCTION,
    SYS_KEEPITEM,
    SYS_MAKEINPUTACTIVE,
    SYS_FREEPARTICLE,
    SYS_GNOMIFYVECTOR,
    SYS_GNOMIFYJOINT,
    SYS_JOINTFROMVERTEX,
    SYS_ROOMWATERLEVEL,
    SYS_ROOMWATERTYPE,
    SYS_MAPSIDENORMAL,
    SYS_MESSAGESIZE,
    SYS_LASTINPUTCURSORPOS,
    SYS_MESSAGERESET,
    SYS_FASTFUNCTIONFOUND,
    SYS_INPUTACTIVE,
    SYS_LOCALPLAYERINPUT,
    SYS_ROOMMONSTERTYPE,
    SYS_SANDTEXTURE,
    SYS_PAYINGCUSTOMER,
    SYS_FILESETFLAG,
    SYS_ENCHANTCURSOR,
    SYS_CHARACTERSCRIPTFILE,
    SYS_PARTICLESCRIPTFILE,
    SYS_FLIPPAN,
    SYS_MAPCLEAR,
    SYS_MAPROOM,
    SYS_MAPAUTOMAPPRIME,
    SYS_MAPAUTOMAPDRAW,
    SYS_MAPOBJECTRECORD,
    SYS_MAPOBJECTDEFEATED,
    SYS_MAPDOOROPEN,
    SYS_CAMERARESET,
    SYS_LOCALPLAYERZ,
    SYS_RESPAWNCHARACTER,
    SYS_RESERVECHARACTER,
    SYS_SWAPCHARACTERS,
    SYS_LUCK,
    SYS_ROOMTEXTURE,
    SYS_DAMAGECHARACTER,
    SYS_NETWORKSCRIPT,
    SYS_ROOMMETALBOXITEM,
    SYS_CAMERAZOOM,
    SYS_CAMERASPIN,
    SYS_ROOMRESTOCK,
    SYS_MODELCHECKHACK
};


char system_file_name[16];
char unused_file_name[16] = "-UNUSED-";

#define word_temp color_temp






//==================================================================================================
// Forward declarations

void autoaim_helper(PSoulfuScriptContext ps, float speed_xy, float speed_z, Uint16 spin, Uint8 team, Uint8 dexterity, Uint16 cone, Uint8 height, Uint8 function);;
void emacs_copy(Uint8* call_address);
void emacs_delete(Uint8* call_address);
void emacs_paste(Uint8* call_address);
void autoaim_helper(PSoulfuScriptContext ps, float speed_xy, float speed_z, Uint16 spin, Uint8 team, Uint8 dexterity, Uint16 cone, Uint8 height, Uint8 function);
void call_enchantment_function(PSoulfuScriptContext pss);
void script_matrix_from_bone(PSoulfuScriptContext pss, Uint8 bone_name);

//void ex_register_tokens();
//bool_t ex_gopoof(PSoulfuScriptContext ps, int type);
//void ex_dismount(PSoulfuScriptContext ps);
//bool_t ex_playsound(PSoulfuScriptContext ps, Uint8 * raw_file, int pitch_skip, int volume);
//bool_t ex_play_megasound(PSoulfuScriptContext ps, Uint8* raw_start, int pitch_skip, int volume, Uint8 pan, Uint8 * loop_data);
//bool_t ex_distance_sound(PSoulfuScriptContext ps, int channel);
//bool_t ex_update_files(PSoulfuScriptContext ps, Uint8 mode);
//Uint16 ex_acquire_target(PSoulfuScriptContext ps, Uint32 flags, Uint8* script_file, float radius_max);
//void ex_autoaim(PSoulfuScriptContext ps, float speed_horz, float speed_vert, int aim_spin, int aim_team, int aim_dex, int cone_angle, int mode);
//void ex_window_border(PSoulfuScriptContext pss, bool_t draggable, float x, float y, float w, float h, Uint32 flags);
//bool_t ex_window_string(PSoulfuScriptContext ps, Uint32 color, float x, float y, char * string);
//Uint16 ex_window_minilist(PSoulfuScriptContext ps, float x, float y, float w, float h, Uint16 state, char * sz_options);
//int ex_window_slider(PSoulfuScriptContext ps, float x, float y, float w, float h, int pos);
//int ex_window_image(PSoulfuScriptContext ps, float x, float y, float w, float h, Uint8* rgb_file, char* sz_alt_txt, int state);
//bool_t ex_window_tracker(PSoulfuScriptContext ps, float x, float y, float w, float h, Uint8 * mus_file, int win_stt, int win_wid, int instrument, int volume, Uint8 pan);
//bool_t ex_window_book(PSoulfuScriptContext ps, float x, float y, float w, float h, int num_pages, Uint8 page_turn, Uint8* page_data);
//void ex_window_input(PSoulfuScriptContext ps, float x, float y, float w, char* sz_text, int offset);
//int ex_window_emacs(PSoulfuScriptContext ps,float x,float y,float w,float h, Uint32 cur_bits, Uint8 * emacs_file);
//bool_t ex_window_megaimage(PSoulfuScriptContext ps, Uint32 color, Uint32 alpha_blend, Uint8 * rgb_file);
//bool_t ex_window_3d_start(PSoulfuScriptContext ps, float x, float y, float w, float h, Uint8 detail_level);
//bool_t ex_window_3d_end(PSoulfuScriptContext ps);
//bool_t ex_window_3d_position(PSoulfuScriptContext ps, float x, float y, float z, Uint8 type);
//bool_t ex_window_3d_model(PSoulfuScriptContext ps, size_t rdy_offset, Sint32 frame, Uint32 alpha, Uint8 mode);
//void ex_tool_kanjiedit(PSoulfuScriptContext ps, Sint32 mode, float x, float y, float scale, Sint32 font_num);
//bool_t ex_window_3d_room(PSoulfuScriptContext ps, float x, float y, float z, Uint8 * srf_file, Uint32 color, int rotation, Uint32 mode);
//bool_t ex_damage_target(PSoulfuScriptContext ps, int damage_type, int damage_amount, Uint32 wound_amount);
//Uint32 ex_experience_function(PSoulfuScriptContext ps, Uint16 target, Uint32 xp_type, int xp_amount, bool_t give_to_enemies);

void ex_register_opcodes();
bool_t ex_compile_setup();
void ex_register_tokens();
bool_t ex_gopoof(PSoulfuScriptContext pss, int type);
void ex_dismount(PSoulfuScriptContext pss);
bool_t ex_playsound(PSoulfuScriptContext pss, Uint8 * raw_file, int pitch_skip, int volume);
bool_t ex_play_megasound(PSoulfuScriptContext pss, Uint8* raw_start, int pitch_skip, int volume, Uint8 pan, Uint8 * loop_data);
bool_t ex_distance_sound(PSoulfuScriptContext pss, int channel);
bool_t ex_update_files(PSoulfuScriptContext pss, Uint8 mode);
Uint16 ex_acquire_target(PSoulfuScriptContext pss, Uint32 flags, Uint8* script_file, float radius_max);
void ex_autoaim(PSoulfuScriptContext pss, float speed_horz, float speed_vert, int aim_spin, int aim_team, int aim_dex, int cone_angle, int mode);
void ex_window_border(PSoulfuScriptContext pss, bool_t draggable, float x, float y, float w, float h, Uint32 flags);
bool_t ex_window_string(PSoulfuScriptContext pss, Uint32 color, float x, float y, char * string);
Uint16 ex_window_minilist(PSoulfuScriptContext pss, float x, float y, float w, float h, Uint16 state, char * sz_options);
int ex_window_slider(PSoulfuScriptContext pss, float x, float y, float w, float h, int pos);
int ex_window_image(PSoulfuScriptContext pss, float x, float y, float w, float h, Uint8* rgb_data, char* sz_alt_txt, int state);
bool_t ex_window_tracker(PSoulfuScriptContext pss, float x, float y, float w, float h, Uint8 * mus_file, int win_stt, int win_wid, int instrument, int volume, Uint8 pan);
bool_t ex_window_book(PSoulfuScriptContext pss, float x, float y, float w, float h, int num_pages, Uint8 page_turn, Uint8* page_data);
void ex_window_input(PSoulfuScriptContext pss, float x, float y, float w, char* sz_text, int offset);
int ex_window_emacs(PSoulfuScriptContext pss, float x, float y, float w, float h, Uint32 cur_bits, Uint8 * emacs_file);
bool_t ex_window_megaimage(PSoulfuScriptContext pss, Uint32 color, Uint32 alpha_blend, Uint8 * rgb_data);
bool_t ex_window_3d_start(PSoulfuScriptContext pss, float x, float y, float w, float h, Uint8 detail_level);
bool_t ex_window_3d_end(PSoulfuScriptContext pss);
bool_t ex_window_3d_position(PSoulfuScriptContext pss, float x, float y, float z, Uint8 type);
bool_t ex_window_3d_model(PSoulfuScriptContext pss, size_t rdy_offset, Sint32 frame, Uint32 alpha, Uint8 mode);
void   ex_tool_kanjiedit(PSoulfuScriptContext pss, Sint32 mode, float x, float y, float scale, Sint32 font_num);
bool_t ex_window_3d_room(PSoulfuScriptContext pss, float x, float y, float z, Uint8 * srf_file, Uint32 color, int rotation, Uint32 mode);
bool_t ex_damage_target(PSoulfuScriptContext pss, int damage_type, int damage_amount, Uint32 wound_amount);
Uint32 ex_experience_function(PSoulfuScriptContext pss, Uint16 target, Uint32 xp_type, int xp_amount, bool_t give_to_enemies);

bool_t ex_model_assign( Uint8 * unk_ptr, size_t value );

//--------------------------------------------------------------------------------------------------

char * _ex_get_language_string(PSoulfuScriptContext pss, int j);
SINT _ex_get_INVALID(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_PLAYERDEVICE(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_NUMJOYSTICK(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_WINDOWSCALE(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_KEYPRESSED(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_KEYDOWN(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_TOPWINDOW(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_SFXVOLUME(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_MUSICVOLUME(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_MESSAGE(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_USERLANGUAGE(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_FILENAME(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_FILESIZE(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_FILEFTPFLAG(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_FILECOUNT(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_FILEFREE(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_DEVTOOL(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_CURSORPOS(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_CURSORBUTTONDOWN(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_MODELSELECT(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_MODELTEXFLAGSALPHA(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_MODELMAXFRAME(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_MODELFRAMEFLAGS(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_MODELFRAMEACTIONNAME(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_MODELACTIONSTART(PSoulfuScriptContext pss, int number, int sub_number);
#ifdef DEVTOOL
SINT _ex_get_MODELFRAMEBASEMODEL(PSoulfuScriptContext pss, int number, int sub_number);
#endif
SINT _ex_get_MODELFRAMEOFFSETX(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_MODELFRAMEOFFSETY(PSoulfuScriptContext pss, int number, int sub_number);
#ifdef DEVTOOL
SINT _ex_get_MODELEXTERNALFILENAME(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_MODELSHADOWTEXTURE(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_MODELSHADOWALPHA(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_MODELNUMTRIANGLE(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_MODELNUMCARTOONLINE(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_MODELSARELINKABLE(PSoulfuScriptContext pss, int number, int sub_number);
#endif
SINT _ex_get_LASTKEYPRESSED(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_CURSORLASTPOS(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_CURSORINOBJECT(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_NUMKANJI(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_NUMKANJITRIANGLE(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_CURSORSCREENPOS(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_DAMAGEAMOUNT(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_COLLISIONCHAR(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_MAINVIDEOFRAME(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_MAINGAMEFRAME(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_MAINFRAMESKIP(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_FINDDATASIZE(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_ARCTANANGLE(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_MAINSERVERLOCATED(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_SHARDLIST(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_SHARDLISTPING(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_SHARDLISTPLAYERS(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_VERSIONERROR(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_STARTGAME(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_NETWORKON(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_MAINGAMEACTIVE(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_NETWORKGAMEACTIVE(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_TRYINGTOJOINGAME(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_JOINPROGRESS(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_GAMESEED(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_LOCALPASSWORDCODE(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_NUMNETWORKPLAYER(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_NETWORKFINISHED(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_SERVERSTATISTICS(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_LOCALPLAYER(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_FPS(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_RANDOMSEED(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_GLOBALATTACKSPIN(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_GLOBALATTACKER(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_DEBUGACTIVE(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_DAMAGECOLOR(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_ITEMREGISTRYSCRIPT(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_ITEMREGISTRYICON(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_ITEMREGISTRYOVERLAY(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_ITEMREGISTRYPRICE(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_ITEMREGISTRYFLAGS(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_ITEMREGISTRYSTR(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_ITEMREGISTRYDEX(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_ITEMREGISTRYINT(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_ITEMREGISTRYMANA(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_ITEMREGISTRYAMMO(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_ITEMREGISTRYNAME(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_WEAPONGRIP(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_DEFENSERATING(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_CURSORBUTTONPRESSED(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_MEMBUFFER(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_ROOMWATERLEVEL(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_ROOMWATERTYPE(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_ROOMMONSTERTYPE(PSoulfuScriptContext pss, int number, int sub_number);
#ifdef DEVTOOL
SINT _ex_get_ROOMSELECT(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_ROOMEXTERIORWALLFLAGS(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_ROOMGROUP(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_ROOMOBJECT(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_ROOMTEXTUREFLAGS(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_ROOMMETALBOXITEM(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_set_ROOMPLOPATSTRING(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_ROOMSELECT(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_ROOMPLOPVERTEX(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_ROOMDELETEVERTEX(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_ROOMWELDVERTICES(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_ROOMCLEAREXTERIORWALL(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_ROOMPLOPEXTERIORWALL(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_ROOMEXTERIORWALLFLAGS(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_ROOMPLOPWAYPOINT(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_ROOMDELETEWAYPOINT(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_ROOMLINKWAYPOINT(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_ROOMUNLINKWAYPOINT(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_ROOMDELETEBRIDGE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_ROOMPLOPTRIANGLE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_ROOMPLOPFAN(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_ROOMDELETETRIANGLE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_ROOMGROUP(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_ROOMOBJECT(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_ROOMAUTOTEXTURE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_ROOMAUTOTRIM(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_ROOMTEXTUREFLAGS(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_ROOMHARDPLOPPER(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_ROOMCOPYPASTE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
#endif
SINT _ex_set_ROOMUNCOMPRESS(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_ROOMTEXTURE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_get_MOUSELASTOBJECT(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_MOUSELASTITEM(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_MOUSELASTSCRIPT(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_ITEMINDEX(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_WEAPONREFRESHXYZ(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_WEAPONREFRESHBONENAME(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_KEEPITEM(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_FREEPARTICLE(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_MAPSIDENORMAL(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_MESSAGESIZE(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_FASTFUNCTIONFOUND(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_INPUTACTIVE(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_LOCALPLAYERINPUT(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_PAYINGCUSTOMER(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_ENCHANTCURSOR(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_CHARACTERSCRIPTFILE(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_PARTICLESCRIPTFILE(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_MAPROOM(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_MAPDOOROPEN(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_LOCALPLAYERZ(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_RESERVECHARACTER(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_LUCK(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_NETWORKSCRIPT(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_CAMERAZOOM(PSoulfuScriptContext pss, int number, int sub_number);
SINT _ex_get_CAMERASPIN(PSoulfuScriptContext pss, int number, int sub_number);

//--------------------------------------------------------------------------------------------------

SINT _ex_set_INVALID(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_PLAYERDEVICE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_WINDOWSCALE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_TOPWINDOW(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_SFXVOLUME(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_MUSICVOLUME(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_USERLANGUAGE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_LANGUAGEFILE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_QUITGAME(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_FILEFTPFLAG(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_CURSORDRAW(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
#ifdef DEVTOOL
SINT _ex_set_MODELVIEW(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_MODELMOVE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_MODELPLOP(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_MODELSELECT(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_MODELDELETE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_MODELPLOPTRIANGLE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_MODELDELETETRIANGLE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_MODELREGENERATE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_MODELPLOPJOINT(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_MODELPLOPBONE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_MODELDELETEBONE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_MODELDELETEJOINT(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_MODELJOINTSIZE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_MODELBONEID(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_MODELCRUNCH(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_MODELROTATEBONES(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_MODELUNROTATEBONES(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_MODELHIDE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_MODELTRIANGLELINES(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_MODELCOPYPASTE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_MODELWELDVERTICES(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_MODELWELDTEXVERTICES(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_MODELFLIP(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_MODELSCALE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_MODELTEXSCALE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_MODELANCHOR(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_MODELTEXFLAGSALPHA(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_MODELADDFRAME(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_MODELADDBASEMODEL(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_MODELFRAMEBASEMODEL(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_MODELEXTERNALFILENAME(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_MODELSHADOWTEXTURE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_MODELSHADOWALPHA(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_MODELINTERPOLATE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_MODELCENTER(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_MODELDETEXTURE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_MODELPLOPATSTRING(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_MODELMARKFRAME(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_MODELCOPYFRAME(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_MODELAUTOSHADOW(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
#endif
SINT _ex_set_MODELFRAMEACTIONNAME(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_MODELFRAMEFLAGS(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_WATERLAYERSACTIVE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_CARTOONMODE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_KANJICOPY(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_KANJIPASTE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_KANJIDELETE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_BLOCKKEYBOARD(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_BILLBOARDACTIVE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_JOINGAME(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_STARTGAME(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_LEAVEGAME(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_LOCALPLAYER(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_SCREENSHAKE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_INCLUDEPASSWORD(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_RANDOMSEED(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_MIPMAPACTIVE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_WATERTEXTURE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_PLAYERCONTROLHANDLED(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_GLOBALSPAWN(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_GLOBALATTACKSPIN(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_GLOBALATTACKER(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_CURRENTITEM(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_ITEMREGISTRYCLEAR(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_ITEMREGISTRYSCRIPT(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_ITEMREGISTRYICON(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_ITEMREGISTRYOVERLAY(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_ITEMREGISTRYPRICE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_ITEMREGISTRYFLAGS(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_ITEMREGISTRYSTR(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_ITEMREGISTRYDEX(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_ITEMREGISTRYINT(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_ITEMREGISTRYMANA(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_ITEMREGISTRYAMMO(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_WEAPONGRIP(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_WEAPONMODELSETUP(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_WEAPONEVENT(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_WEAPONFRAMEEVENT(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_WEAPONUNPRESSED(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_CHARFASTFUNCTION(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_FASTANDUGLY(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_DEFENSERATING(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_CLEARDEFENSERATING(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_ITEMDEFENSERATING(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_CAMERAANGLE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_STARTFADE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_MOUSETEXT(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_BUMPABORT(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
#ifdef DEVTOOL
SINT _ex_set_MODELAUTOVERTEX(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
#endif
SINT _ex_set_ITEMINDEX(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_WEAPONREFRESHXYZ(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_WEAPONREFRESHFLASH(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_FASTFUNCTION(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_KEEPITEM(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_MAKEINPUTACTIVE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
#ifdef DEVTOOL
SINT _ex_set_GNOMIFYVECTOR(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_GNOMIFYJOINT(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_JOINTFROMVERTEX(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
#endif
SINT _ex_set_MESSAGESIZE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_LASTINPUTCURSORPOS(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_MESSAGERESET(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_FASTFUNCTIONFOUND(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_INPUTACTIVE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_LOCALPLAYERINPUT(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_SANDTEXTURE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_PAYINGCUSTOMER(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_FILESETFLAG(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_ENCHANTCURSOR(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_FLIPPAN(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_MAPCLEAR(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_MAPROOM(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_MAPAUTOMAPPRIME(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_MAPAUTOMAPDRAW(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_MAPOBJECTRECORD(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_MAPOBJECTDEFEATED(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_MAPDOOROPEN(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_CAMERARESET(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_RESPAWNCHARACTER(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_RESERVECHARACTER(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_SWAPCHARACTERS(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_LUCK(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_DAMAGECHARACTER(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_CAMERAZOOM(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_CAMERASPIN(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_ROOMRESTOCK(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);
SINT _ex_set_MODELCHECKHACK(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value);

//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------
void ex_register_opcodes()
{

    register_opcode_name(opcode_name, OPCODE_EX_LOCALMESSAGE, "LOCALMESSAGE");
    register_opcode_name(opcode_name, OPCODE_EX_LOGMESSAGE, "LOGMESSAGE");
    register_opcode_name(opcode_name, OPCODE_EX_FINDSELF, "FINDSELF");
    register_opcode_name(opcode_name, OPCODE_EX_DEBUGMESSAGE, "DEBUGMESSAGE");
    register_opcode_name(opcode_name, OPCODE_EX_NETWORKMESSAGE, "NETWORKMESSAGE");
    register_opcode_name(opcode_name, OPCODE_EX_STRINGLANGUAGE, "STRINGLANGUAGE");
    register_opcode_name(opcode_name, OPCODE_EX_SPAWN, "SPAWN");
    register_opcode_name(opcode_name, OPCODE_EX_GOPOOF, "GOPOOF");
    register_opcode_name(opcode_name, OPCODE_EX_DISMOUNT, "DISMOUNT");
    register_opcode_name(opcode_name, OPCODE_EX_ROLLDICE, "ROLLDICE");
    register_opcode_name(opcode_name, OPCODE_EX_PLAYSOUND, "PLAYSOUND");
    register_opcode_name(opcode_name, OPCODE_EX_PLAYMEGASOUND, "PLAYMEGASOUND");
    register_opcode_name(opcode_name, OPCODE_EX_DISTANCESOUND, "DISTANCESOUND");
    register_opcode_name(opcode_name, OPCODE_EX_PLAYMUSIC, "PLAYMUSIC");
    register_opcode_name(opcode_name, OPCODE_EX_UPDATEFILES, "UPDATEFILES");
    register_opcode_name(opcode_name, OPCODE_EX_ACQUIRETARGET, "ACQUIRETARGET");
    register_opcode_name(opcode_name, OPCODE_EX_FINDPATH, "FINDPATH");
    register_opcode_name(opcode_name, OPCODE_EX_BUTTONPRESS, "BUTTONPRESS");
    register_opcode_name(opcode_name, OPCODE_EX_AUTOAIM, "AUTOAIM");
    register_opcode_name(opcode_name, OPCODE_EX_ROOMHEIGHTXY, "ROOMHEIGHTXY");
    register_opcode_name(opcode_name, OPCODE_EX_TOTEXT, "TOTEXT");
    register_opcode_name(opcode_name, OPCODE_EX_TOSTRING, "TOSTRING");
    register_opcode_name(opcode_name, OPCODE_EX_WINDOWBORDER, "WINDOWBORDER");
    register_opcode_name(opcode_name, OPCODE_EX_WINDOWSTRING, "WINDOWSTRING");
    register_opcode_name(opcode_name, OPCODE_EX_WINDOWMINILIST, "WINDOWMINILIST");
    register_opcode_name(opcode_name, OPCODE_EX_WINDOWSLIDER, "WINDOWSLIDER");
    register_opcode_name(opcode_name, OPCODE_EX_WINDOWIMAGE, "WINDOWIMAGE");
    register_opcode_name(opcode_name, OPCODE_EX_WINDOWTRACKER, "WINDOWTRACKER");
    register_opcode_name(opcode_name, OPCODE_EX_WINDOWBOOK, "WINDOWBOOK");
    register_opcode_name(opcode_name, OPCODE_EX_WINDOWINPUT, "WINDOWINPUT");
    register_opcode_name(opcode_name, OPCODE_EX_WINDOWEMACS, "WINDOWEMACS");
    register_opcode_name(opcode_name, OPCODE_EX_WINDOWMEGAIMAGE, "WINDOWMEGAIMAGE");
    register_opcode_name(opcode_name, OPCODE_EX_WINDOW3DSTART, "WINDOW3DSTART");
    register_opcode_name(opcode_name, OPCODE_EX_WINDOW3DEND, "WINDOW3DEND");
    register_opcode_name(opcode_name, OPCODE_EX_WINDOW3DPOSITION, "WINDOW3DPOSITION");
    register_opcode_name(opcode_name, OPCODE_EX_WINDOW3DMODEL, "WINDOW3DMODEL");
    register_opcode_name(opcode_name, OPCODE_EX_MODELASSIGN, "MODELASSIGN");
    register_opcode_name(opcode_name, OPCODE_EX_PARTICLEBOUNCE, "PARTICLEBOUNCE");
    register_opcode_name(opcode_name, OPCODE_EX_WINDOWEDITKANJI, "WINDOWEDITKANJI");
    register_opcode_name(opcode_name, OPCODE_EX_WINDOW3DROOM, "WINDOW3DROOM");
    register_opcode_name(opcode_name, OPCODE_EX_INDEXISLOCALPLAYER, "INDEXISLOCALPLAYER");
    register_opcode_name(opcode_name, OPCODE_EX_FINDBINDING, "FINDBINDING");
    register_opcode_name(opcode_name, OPCODE_EX_FINDTARGET, "FINDTARGET");
    register_opcode_name(opcode_name, OPCODE_EX_FINDOWNER, "FINDOWNER");
    register_opcode_name(opcode_name, OPCODE_EX_FINDINDEX, "FINDINDEX");
    register_opcode_name(opcode_name, OPCODE_EX_FINDBYINDEX, "FINDBYINDEX");
    register_opcode_name(opcode_name, OPCODE_EX_FINDWINDOW, "FINDWINDOW");
    register_opcode_name(opcode_name, OPCODE_EX_FINDPARTICLE, "FINDPARTICLE");
    register_opcode_name(opcode_name, OPCODE_EX_ATTACHTOTARGET, "ATTACHTOTARGET");
    register_opcode_name(opcode_name, OPCODE_EX_GETDIRECTION, "GETDIRECTION");
    register_opcode_name(opcode_name, OPCODE_EX_DAMAGETARGET, "DAMAGETARGET");
    register_opcode_name(opcode_name, OPCODE_EX_EXPERIENCEFUNCTION, "EXPERIENCEFUNCTION");
}



//-----------------------------------------------------------------------------------------------
bool_t ex_compile_setup()
{
    pxss_compile_setup();

    log_error_count = 0;

    sf_defines_setup();

    ex_register_tokens();
    ex_register_opcodes();

    return ktrue;
};


//-----------------------------------------------------------------------------------------------
bool_t sf_extensions_setup(PSoulfuGlobalScriptContext psgs)
{
    if (NULL == psgs) return kfalse;

    ex_compile_setup();

    pxss_run_setup( &(psgs->base) );

    //focus_clear( &(psgs->focus) );

    psgs->scale = 6.0f;
    //psgs->screen.x = 0;
    //psgs->screen.y = 0;
    //psgs->screen.w = screen_x / psgs->screen.scale;
    //psgs->screen.h = screen_y / psgs->screen.scale;


    return ktrue;
};


char * pxss_fast_function_name[MAX_FAST_FUNCTION] =
{
    "SPAWN",           // Offset for the Spawn() function
    "REFRESH",         // Offset for the Refresh() function
    "EVENT",           // Offset for the Event() function
    "AISCRIPT",        // Offset for the AIScript() function
    "BUTTONEVENT",     // Offset for the ButtonEvent() function
    "GETNAME",        // Offset for the GetName() function
    "UNPRESSED",      // Offset for the Unpressed() function
    "FRAMEEVENT",     // Offset for the FrameEvent() function
    "MODELSETUP",     // Offset for the ModelSetup() function...
    "DEFENSERATING",  // Offset for the DefenseRating() function...
    "SETUP",          // Offset for the Setup() function...
    "DIRECTUSAGE",    // Offset for the DirectUsage() function...
    "ENCHANTUSAGE",   // Offset for the EnchantUsage() function...

    // !!!! More fast functions go here !!!!
    "",
    "",
    ""
};


SoulfuGlobalScriptContext g_sf_scr;


void autoaim_helper(PSoulfuScriptContext ps, float speed_xy, float speed_z, Uint16 spin, Uint8 team, Uint8 dexterity, Uint16 cone, Uint8 height, Uint8 function);

#ifdef DEVTOOL
// For WindowEmacs
Uint8 emacs_buffer[EMACS_SIZE];
char  emacs_horiz[EMACS_HORIZ_SIZE] = "//-----------------------------------------------------------------------";
char  emacs_bad[EMACS_BAD_SIZE] = "// !!!BAD!!!";
int   emacs_buffer_size = 0;
int   emacs_buffer_write = 0;
int   emacs_return_count = 0;

//-----------------------------------------------------------------------------------------------
// <ZZ> Emacs helper functions...
//-----------------------------------------------------------------------------------------------

void emacs_copy(Uint8* call_address)
{
    // <ZZ> Emacs helper...  Copies a line of text into the buffer
    if (emacs_buffer_write == 0) emacs_buffer_size = 0;

    if (emacs_buffer_write == 0) emacs_return_count = 0;

    while (*call_address != 0 && emacs_buffer_size < EMACS_SIZE)
    {
        emacs_buffer[emacs_buffer_size] = *call_address;
        call_address++;
        emacs_buffer_size++;
    }

    if (emacs_buffer_size < EMACS_SIZE)
    {
        emacs_buffer[emacs_buffer_size] = 0;
        emacs_buffer_size++;
        emacs_return_count++;
    }

    emacs_buffer_write = emacs_buffer_size;
}

//-----------------------------------------------------------------------------------------------
void emacs_delete(Uint8* call_address)
{
    // <ZZ> Emacs helper...  Deletes a line of text
    Uint8 keep_going;
    keep_going = ktrue;

    while (*call_address != 0 && keep_going)
    {
        keep_going = sdf_insert_data(call_address, NULL, -1);
    }

    if (*call_address == 0 && keep_going)
    {
        sdf_insert_data(call_address, NULL, -1);
    }
}


//-----------------------------------------------------------------------------------------------
void emacs_paste(Uint8* call_address)
{
    // <ZZ> Emacs helper...  Pastes contents of buffer into file
    if (emacs_buffer_size > 0)
    {
        sdf_insert_data(call_address, emacs_buffer, emacs_buffer_size);
    }
}
#endif

//-----------------------------------------------------------------------------------------------
// <ZZ> Autoaim helper function...
//-----------------------------------------------------------------------------------------------
float autoaim_velocity_xyz[3];
Uint16 autoaim_target;
void autoaim_helper(PSoulfuScriptContext ps, float speed_xy, float speed_z, Uint16 spin, Uint8 team, Uint8 dexterity, Uint16 cone, Uint8 height, Uint8 function)
{
    Uint16 i;
    Uint16 random_angle;
    Uint16 left_angle;
    Uint16 right_angle;
    float* character_xyz;
    float* trg_xyz;
    float* target_vel_xyz;
    float distance_xyz[3];
    float random_velocity_xyz[3];
    float precise_velocity_xyz[3];
    float left_normal_xy[2];
    float right_normal_xy[2];
    float flt_angle;
    float left_dot;
    float right_dot;
    float percent;
    float inverse;
    float best_distance;
    float distance;
    float frames;
    CHR_DATA* target_data;


    // !!!BAD!!!
    // !!!BAD!!!
    // !!!BAD!!! Replace all sin() and cos() calls...
    // !!!BAD!!!
    // !!!BAD!!!


    // First find the random velocity...  Do that by picking a random angle within the cone...
    i = random_number();  i = i << 8;  i = i | random_number();

    if (cone > 0)
    {
        random_angle = i % cone;
    }
    else
    {
        random_angle = 0;
    }

    random_angle = spin + random_angle - (cone >> 1);
    // We've now got our random angle...  Convert to cartesian and scale...
    flt_angle = random_angle * 0.0000958737992429f;
    random_velocity_xyz[XX] = COS(flt_angle) * speed_xy;
    random_velocity_xyz[YY] = SIN(flt_angle) * speed_xy;

    if (function == AUTOAIM_CRUNCH_BALLISTIC)
    {
        speed_z = speed_z * DEG_TO_RAD;  // Speed Z specifies maximum angle (in degrees...)
        flt_angle = 0.78539805f;  // 45 degrees...

        if (flt_angle > speed_z) { flt_angle = speed_z; }

        random_velocity_xyz[ZZ] = speed_xy * SIN(flt_angle);
        flt_angle = (COS(flt_angle));
        random_velocity_xyz[XX] *= flt_angle;
        random_velocity_xyz[YY] *= flt_angle;
    }
    else
    {
        random_velocity_xyz[ZZ] = 0.0f;
    }

    autoaim_target = UINT16_MAX;

    // Now find the precise angle...
    if (dexterity == 0)
    {
        // Don't do funky thing, since our dexterity is so low...
        precise_velocity_xyz[XX] = 0.0f;
        precise_velocity_xyz[YY] = 0.0f;
        precise_velocity_xyz[ZZ] = 0.0f;
    }
    else
    {
        // Find the best target within the cone of attack...
        best_distance = 99999999.0f;
        character_xyz = (float*) ps->self.object.chr;


        // Define the cone as two planes passing through our start location...  Each
        // plane has a different x,y normal...  Target is within' cone if dot product
        // is positive for each plane...  More like a wedge than a cone...
        left_angle = spin - (cone >> 1);
        flt_angle = left_angle * 0.0000958737992429f;
        left_normal_xy[XX] = -SIN(flt_angle);
        left_normal_xy[YY] =  COS(flt_angle);

        right_angle = spin + (cone >> 1);
        flt_angle = right_angle * 0.0000958737992429f;
        right_normal_xy[XX] =  SIN(flt_angle);
        right_normal_xy[YY] = -COS(flt_angle);



        // Now go through all characters in the room, looking for a target...
        repeat(i, MAX_CHARACTER)
        {
            target_data = chr_data_get_ptr(i);
            if (NULL == target_data) continue;

            if (target_data->team != team && target_data->action != ACTION_RIDE && (target_data->action < ACTION_KNOCK_OUT_BEGIN || target_data->action > ACTION_KNOCK_OUT_END || team != TEAM_GOOD) && !((target_data->flags) & CHAR_NO_COLLISION))
            {
                // Looks like a valid character, but is it closer than our best one?
                trg_xyz = &(target_data->x);
                distance_xyz[XX] = trg_xyz[XX] - character_xyz[XX];
                distance_xyz[YY] = trg_xyz[YY] - character_xyz[YY];
                distance_xyz[ZZ] = trg_xyz[ZZ] - character_xyz[ZZ];
                distance = distance_xyz[XX] * distance_xyz[XX] + distance_xyz[YY] * distance_xyz[YY] + (distance_xyz[ZZ] * distance_xyz[ZZ] * 0.2f);  // Actually distance squared, but that should be good enough for what we're doin'...

                if (target_data->team == TEAM_NEUTRAL)
                {
                    // Neutral targets are not as important as enemies...
                    distance += 4.0f;
                    distance *= 4.0f;
                }

                if ((trg_xyz[ZZ] + target_data->height) < room_water_level && character_xyz[ZZ] > room_water_level)
                {
                    // Targets below water are not important if character is above water...
                    distance = best_distance + 1.0f;
                }

                if (distance < best_distance)
                {
                    // It's pretty darn close, but is it within our cone?
                    left_dot = distance_xyz[XX] * left_normal_xy[XX] + distance_xyz[YY] * left_normal_xy[YY];
                    right_dot = distance_xyz[XX] * right_normal_xy[XX] + distance_xyz[YY] * right_normal_xy[YY];

                    if (left_dot > 0.0f && right_dot > 0.0f)
                    {
                        // It is...  In fact, it's our best so far...
                        autoaim_target = i;
                        best_distance = distance;
                    }
                }
            }
        }


        precise_velocity_xyz[ZZ] = random_velocity_xyz[ZZ];

        target_data = chr_data_get_ptr(autoaim_target);
        if ( NULL != target_data )
        {
            // We found a target to aim at...
            trg_xyz        = &(target_data->x);
            target_vel_xyz = &(target_data->vx);

            if (target_data->team == TEAM_NEUTRAL)
            {
                best_distance *= 0.0625;   // Neutral targets are weighted as if they were four times as far away...
            }

            distance_xyz[XX] = trg_xyz[XX] - character_xyz[XX];
            distance_xyz[YY] = trg_xyz[YY] - character_xyz[YY];
            distance_xyz[ZZ] = trg_xyz[ZZ] - character_xyz[ZZ] + ((((Sint16) target_data->height) - ((Sint16)height)) * 0.5f);
            distance = SQRT((distance_xyz[XX] * distance_xyz[XX]) + (distance_xyz[YY] * distance_xyz[YY]) + (distance_xyz[ZZ] * distance_xyz[ZZ]));


            // Figger number of frames til we hit...
            frames = 1.0f;

            if (speed_xy > 0.0f)
            {
                // Determine number of frames...
                frames = (distance / speed_xy);

                // Lead the target by that many frames...  Only lead in xy...
                distance_xyz[XX] = trg_xyz[XX] - character_xyz[XX] + (target_vel_xyz[XX] * frames);
                distance_xyz[YY] = trg_xyz[YY] - character_xyz[YY] + (target_vel_xyz[YY] * frames);
                distance = SQRT((distance_xyz[XX] * distance_xyz[XX]) + (distance_xyz[YY] * distance_xyz[YY]) + (distance_xyz[ZZ] * distance_xyz[ZZ]));

                // Recalculate number of frames to hit lead position
                frames = (distance / speed_xy);
            }


            // Find the xy velocity vector...
            distance = SQRT((distance_xyz[XX] * distance_xyz[XX]) + (distance_xyz[YY] * distance_xyz[YY]));

            if (distance > 0.0f)
            {
                distance_xyz[XX] /= distance;
                distance_xyz[YY] /= distance;
            }

            precise_velocity_xyz[XX] = (distance_xyz[XX] * speed_xy);
            precise_velocity_xyz[YY] = (distance_xyz[YY] * speed_xy);





            // Now figger the z velocity...
            if (distance > 0.0f && speed_xy > 0.0f)
            {
                if (function == AUTOAIM_CRUNCH_BALLISTIC)
                {
                    // Simple ballistics calculation...  More complicated ones seemed to not
                    // work as well...
                    flt_angle = distance_xyz[ZZ] / distance;
                    flt_angle = (ATAN(flt_angle));
                    flt_angle = flt_angle + (frames * 0.030f) / speed_xy;

                    if (distance_xyz[ZZ] < distance && flt_angle > 0.78539805f)
                    {
                        // Arc at 45' for maximum range...
                        flt_angle = 0.78539805f;
                    }


                    // Limit our firing angle based on the supplied argument...
                    // Also calculate the final xyz velocities...
                    if (flt_angle > speed_z) { flt_angle = speed_z; }

                    if (flt_angle < -speed_z) { flt_angle = -speed_z; }

                    precise_velocity_xyz[ZZ] = speed_xy * SIN(flt_angle);
                    flt_angle = COS(flt_angle);
                    precise_velocity_xyz[XX] *= flt_angle;
                    precise_velocity_xyz[YY] *= flt_angle;
                }
                else
                {
                    // Simple z velocity calculation...
                    // Z velocity is z distance over number of game frames it'll take to hit...
                    precise_velocity_xyz[ZZ] = (distance_xyz[ZZ] / frames);

                    // Limit the z velocity...
                    CLIP(-speed_z, precise_velocity_xyz[ZZ], speed_z);
                }
            }
        }
        else
        {
            // No target, so just try to shoot as straight as possible...
            flt_angle = spin * 0.0000958737992429f;
            precise_velocity_xyz[XX] = COS(flt_angle) * speed_xy;
            precise_velocity_xyz[YY] = SIN(flt_angle) * speed_xy;

            if (function == AUTOAIM_CRUNCH_BALLISTIC)
            {
                // We're arc'ing at 45' for maximum range...
                flt_angle = 0.78539805f;  // 45 degrees...

                if (flt_angle > speed_z) { flt_angle = speed_z; }

                precise_velocity_xyz[ZZ] = speed_xy * SIN(flt_angle);
                flt_angle = COS(flt_angle);
                precise_velocity_xyz[XX] *= flt_angle;
                precise_velocity_xyz[YY] *= flt_angle;
            }
        }
    }


    // Now use our dexterity to give us a weighted average between random and precise...
    // 50 dexterity is as good as it gets for aiming...
    if (dexterity > 50)
    {
        dexterity = 50;
    }

    percent = dexterity / 50.0f;
    inverse = 1.0f - percent;
    autoaim_velocity_xyz[XX] = (random_velocity_xyz[XX] * inverse) + (precise_velocity_xyz[XX] * percent);
    autoaim_velocity_xyz[YY] = (random_velocity_xyz[YY] * inverse) + (precise_velocity_xyz[YY] * percent);
    autoaim_velocity_xyz[ZZ] = precise_velocity_xyz[ZZ];

    // Don't do randomized z vel...  Makes it too hard...
    //    autoaim_velocity_xyz[ZZ] = (random_velocity_xyz[ZZ]*inverse) + (precise_velocity_xyz[ZZ]*percent);
}


void ex_register_tokens()
{
    TOKEN tmp_tok;

    tmp_tok.tag[0]            = '\0';
    tmp_tok.opcode            = OPCODE_NIL;
    tmp_tok.type              = TOKEN_UNKNOWN;
    tmp_tok.arg_list          = arg_list_none;
    tmp_tok.variable_type     = VAR_UNKNOWN;
    tmp_tok.number_to_destroy = -1;
    tmp_tok.extension         = -1;
    tmp_tok.is_in_rpn         = kfalse;
    tmp_tok.change_type       = VAR_UNKNOWN;
    tmp_tok.fix_bits          = FIX_PREFIX | FIX_INFIX | FIX_POSTFIX;

    // generic functions
    tmp_tok.type     = TOKEN_FUNCTION;
    tmp_tok.fix_bits = FIX_PREFIX;
    { strcpy(tmp_tok.tag, "DEBUGMESSAGE");      tmp_tok.opcode = OPCODE_EX_DEBUGMESSAGE;   tmp_tok.number_to_destroy = 1; tmp_tok.arg_list = arg_list_i;      tmp_tok.variable_type = VAR_INT; pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "SPAWN");             tmp_tok.opcode = OPCODE_EX_SPAWN;          tmp_tok.number_to_destroy = 5; tmp_tok.arg_list = arg_list_ifffi;  tmp_tok.variable_type = VAR_INT; pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "GOPOOF");            tmp_tok.opcode = OPCODE_EX_GOPOOF;         tmp_tok.number_to_destroy = 1; tmp_tok.arg_list = arg_list_i;      tmp_tok.variable_type = VAR_VOID;  pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "DISMOUNT");          tmp_tok.opcode = OPCODE_EX_DISMOUNT;       tmp_tok.number_to_destroy = 0; tmp_tok.arg_list = arg_list_none;   tmp_tok.variable_type = VAR_VOID; pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "ROLLDICE");          tmp_tok.opcode = OPCODE_EX_ROLLDICE;       tmp_tok.number_to_destroy = 2; tmp_tok.arg_list = arg_list_ii;     tmp_tok.variable_type = VAR_INT; pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "PLAYSOUND");         tmp_tok.opcode = OPCODE_EX_PLAYSOUND;      tmp_tok.number_to_destroy = 3; tmp_tok.arg_list = arg_list_iii;    tmp_tok.variable_type = VAR_INT; pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "PLAYMEGASOUND");     tmp_tok.opcode = OPCODE_EX_PLAYMEGASOUND;  tmp_tok.number_to_destroy = 5; tmp_tok.arg_list = arg_list_iiiii;  tmp_tok.variable_type = VAR_INT; pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "DISTANCESOUND");     tmp_tok.opcode = OPCODE_EX_DISTANCESOUND;  tmp_tok.number_to_destroy = 1; tmp_tok.arg_list = arg_list_i;      tmp_tok.variable_type = VAR_INT; pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "PLAYMUSIC");         tmp_tok.opcode = OPCODE_EX_PLAYMUSIC;      tmp_tok.number_to_destroy = 3; tmp_tok.arg_list = arg_list_iii;    tmp_tok.variable_type = VAR_INT; pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "UPDATEFILES");       tmp_tok.opcode = OPCODE_EX_UPDATEFILES;    tmp_tok.number_to_destroy = 1; tmp_tok.arg_list = arg_list_i;      tmp_tok.variable_type = VAR_VOID; pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "LOCALMESSAGE");      tmp_tok.opcode = OPCODE_EX_LOCALMESSAGE;   tmp_tok.number_to_destroy = 2; tmp_tok.arg_list = arg_list_ii;     tmp_tok.variable_type = VAR_INT; pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "NETWORKMESSAGE");    tmp_tok.opcode = OPCODE_EX_NETWORKMESSAGE; tmp_tok.number_to_destroy = 3; tmp_tok.arg_list = arg_list_iii;    tmp_tok.variable_type = VAR_INT; pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "LOGMESSAGE");        tmp_tok.opcode = OPCODE_EX_LOGMESSAGE;     tmp_tok.number_to_destroy = 1; tmp_tok.arg_list = arg_list_i;      tmp_tok.variable_type = VAR_INT; pxss_register_token(&tmp_tok); }

    { strcpy(tmp_tok.tag, "STRINGRANDOMNAME");   tmp_tok.opcode = OPCODE_EX_STRINGRANDOMNAME;   tmp_tok.number_to_destroy = 2; tmp_tok.arg_list = arg_list_ii;  tmp_tok.variable_type = VAR_INT; pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "STRINGSANITIZE");     tmp_tok.opcode = OPCODE_EX_STRINGSANITIZE;     tmp_tok.number_to_destroy = 1; tmp_tok.arg_list = arg_list_i;   tmp_tok.variable_type = VAR_INT; pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "STRINGLANGUAGE");     tmp_tok.opcode = OPCODE_EX_STRINGLANGUAGE;      tmp_tok.number_to_destroy = 1; tmp_tok.arg_list = arg_list_i;   tmp_tok.variable_type = VAR_INT; pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "STRINGUPPERCASE");    tmp_tok.opcode = OPCODE_LIB_STRINGUPPERCASE;    tmp_tok.number_to_destroy = 1; tmp_tok.arg_list = arg_list_i;   tmp_tok.variable_type = VAR_INT; pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "STRINGAPPENDNUMBER"); tmp_tok.opcode = OPCODE_LIB_STRINGAPPENDNUMBER; tmp_tok.number_to_destroy = 3; tmp_tok.arg_list = arg_list_iii; tmp_tok.variable_type = VAR_INT; pxss_register_token(&tmp_tok); }


    // Window functions
    tmp_tok.type     = TOKEN_FUNCTION;
    tmp_tok.fix_bits = FIX_PREFIX;
    { strcpy(tmp_tok.tag, "WINDOWBORDER");        tmp_tok.opcode = OPCODE_EX_WINDOWBORDER;      tmp_tok.number_to_destroy = 6; tmp_tok.arg_list = arg_list_iffiii;   tmp_tok.variable_type = VAR_INT; pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "WINDOWSTRING");        tmp_tok.opcode = OPCODE_EX_WINDOWSTRING;      tmp_tok.number_to_destroy = 4; tmp_tok.arg_list = arg_list_iffi;     tmp_tok.variable_type = VAR_INT; pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "WINDOWMINILIST");      tmp_tok.opcode = OPCODE_EX_WINDOWMINILIST;    tmp_tok.number_to_destroy = 6; tmp_tok.arg_list = arg_list_ffiiii;   tmp_tok.variable_type = VAR_INT; pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "WINDOWSLIDER");        tmp_tok.opcode = OPCODE_EX_WINDOWSLIDER;      tmp_tok.number_to_destroy = 5; tmp_tok.arg_list = arg_list_ffiii;    tmp_tok.variable_type = VAR_INT; pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "WINDOWIMAGE");         tmp_tok.opcode = OPCODE_EX_WINDOWIMAGE;       tmp_tok.number_to_destroy = 7; tmp_tok.arg_list = arg_list_ffffiii;  tmp_tok.variable_type = VAR_INT; pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "WINDOWTRACKER");       tmp_tok.opcode = OPCODE_EX_WINDOWTRACKER;     tmp_tok.number_to_destroy = 10; tmp_tok.arg_list = arg_list_ffffiiiiii; tmp_tok.variable_type = VAR_INT; pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "WINDOWBOOK");          tmp_tok.opcode = OPCODE_EX_WINDOWBOOK;        tmp_tok.number_to_destroy = 7; tmp_tok.arg_list = arg_list_ffiiiii;  tmp_tok.variable_type = VAR_INT; pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "WINDOWINPUT");         tmp_tok.opcode = OPCODE_EX_WINDOWINPUT;       tmp_tok.number_to_destroy = 5; tmp_tok.arg_list = arg_list_ffiii;    tmp_tok.variable_type = VAR_INT; pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "WINDOWEMACS");         tmp_tok.opcode = OPCODE_EX_WINDOWEMACS;       tmp_tok.number_to_destroy = 6; tmp_tok.arg_list = arg_list_ffiiii;   tmp_tok.variable_type = VAR_INT; pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "WINDOWMEGAIMAGE");     tmp_tok.opcode = OPCODE_EX_WINDOWMEGAIMAGE;   tmp_tok.number_to_destroy = 15; tmp_tok.arg_list = arg_list_ffffffffffffiii; tmp_tok.variable_type = VAR_INT; pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "WINDOW3DSTART");       tmp_tok.opcode = OPCODE_EX_WINDOW3DSTART;     tmp_tok.number_to_destroy = 5; tmp_tok.arg_list = arg_list_ffffi;    tmp_tok.variable_type = VAR_INT; pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "WINDOW3DEND");         tmp_tok.opcode = OPCODE_EX_WINDOW3DEND;       tmp_tok.number_to_destroy = 0; tmp_tok.arg_list = arg_list_none;     tmp_tok.variable_type = VAR_INT; pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "WINDOW3DPOSITION");    tmp_tok.opcode = OPCODE_EX_WINDOW3DPOSITION;  tmp_tok.number_to_destroy = 4; tmp_tok.arg_list = arg_list_fffi;     tmp_tok.variable_type = VAR_INT; pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "WINDOW3DMODEL");       tmp_tok.opcode = OPCODE_EX_WINDOW3DMODEL;     tmp_tok.number_to_destroy = 4; tmp_tok.arg_list = arg_list_iiii;     tmp_tok.variable_type = VAR_INT; pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "MODELASSIGN");         tmp_tok.opcode = OPCODE_EX_MODELASSIGN;       tmp_tok.number_to_destroy = 2; tmp_tok.arg_list = arg_list_ii; pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "PARTICLEBOUNCE");      tmp_tok.opcode = OPCODE_EX_PARTICLEBOUNCE;    tmp_tok.number_to_destroy = 0; tmp_tok.arg_list = arg_list_none;     tmp_tok.variable_type = VAR_INT; pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "WINDOWEDITKANJI");     tmp_tok.opcode = OPCODE_EX_WINDOWEDITKANJI;   tmp_tok.number_to_destroy = 5; tmp_tok.arg_list = arg_list_ifffi; pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "WINDOW3DROOM");        tmp_tok.opcode = OPCODE_EX_WINDOW3DROOM;      tmp_tok.number_to_destroy = 7; tmp_tok.arg_list = arg_list_fffiiii; pxss_register_token(&tmp_tok); }


    // Other functions...
    tmp_tok.type     = TOKEN_FUNCTION;
    tmp_tok.fix_bits = FIX_PREFIX;
    { strcpy(tmp_tok.tag, "INDEXISLOCALPLAYER");  tmp_tok.opcode = OPCODE_EX_INDEXISLOCALPLAYER; tmp_tok.number_to_destroy = 1; tmp_tok.arg_list = arg_list_i;      tmp_tok.variable_type = VAR_INT; pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "FINDBINDING");         tmp_tok.opcode = OPCODE_EX_FINDBINDING;       tmp_tok.number_to_destroy = 0; tmp_tok.arg_list = arg_list_none;     tmp_tok.variable_type = VAR_INT; pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "FINDSELF");            tmp_tok.opcode = OPCODE_EX_FINDSELF;          tmp_tok.number_to_destroy = 0; tmp_tok.arg_list = arg_list_none;     tmp_tok.variable_type = VAR_INT; pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "FINDTARGET");          tmp_tok.opcode = OPCODE_EX_FINDTARGET;        tmp_tok.number_to_destroy = 0; tmp_tok.arg_list = arg_list_none;     tmp_tok.variable_type = VAR_INT; pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "FINDOWNER");           tmp_tok.opcode = OPCODE_EX_FINDOWNER;         tmp_tok.number_to_destroy = 0; tmp_tok.arg_list = arg_list_none;     tmp_tok.variable_type = VAR_INT; pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "FINDINDEX");           tmp_tok.opcode = OPCODE_EX_FINDINDEX;         tmp_tok.number_to_destroy = 1; tmp_tok.arg_list = arg_list_i;      tmp_tok.variable_type = VAR_INT; pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "FINDBYINDEX");         tmp_tok.opcode = OPCODE_EX_FINDBYINDEX;       tmp_tok.number_to_destroy = 1; tmp_tok.arg_list = arg_list_i;      tmp_tok.variable_type = VAR_INT; pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "FINDWINDOW");          tmp_tok.opcode = OPCODE_EX_FINDWINDOW;        tmp_tok.number_to_destroy = 2; tmp_tok.arg_list = arg_list_ii;  tmp_tok.variable_type = VAR_INT; pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "FINDPARTICLE");        tmp_tok.opcode = OPCODE_EX_FINDPARTICLE;      tmp_tok.number_to_destroy = 1; tmp_tok.arg_list = arg_list_i;      tmp_tok.variable_type = VAR_INT; pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "ATTACHTOTARGET");      tmp_tok.opcode = OPCODE_EX_ATTACHTOTARGET;    tmp_tok.number_to_destroy = 1; tmp_tok.arg_list = arg_list_i;      tmp_tok.variable_type = VAR_INT; pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "GETDIRECTION");        tmp_tok.opcode = OPCODE_EX_GETDIRECTION;      tmp_tok.number_to_destroy = 2; tmp_tok.arg_list = arg_list_ff; tmp_tok.variable_type = VAR_INT; pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "DAMAGETARGET");        tmp_tok.opcode = OPCODE_EX_DAMAGETARGET;      tmp_tok.number_to_destroy = 3; tmp_tok.arg_list = arg_list_iii; pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "EXPERIENCEFUNCTION");  tmp_tok.opcode = OPCODE_EX_EXPERIENCEFUNCTION; tmp_tok.number_to_destroy = 4; tmp_tok.arg_list = arg_list_iiii;     tmp_tok.variable_type = VAR_INT; pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "ACQUIRETARGET");       tmp_tok.opcode = OPCODE_EX_ACQUIRETARGET;     tmp_tok.number_to_destroy = 3; tmp_tok.arg_list = arg_list_iif;      tmp_tok.variable_type = VAR_INT; pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "FINDPATH");            tmp_tok.opcode = OPCODE_EX_FINDPATH;          tmp_tok.number_to_destroy = 0; tmp_tok.arg_list = arg_list_none;     tmp_tok.variable_type = VAR_INT; pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "BUTTONPRESS");         tmp_tok.opcode = OPCODE_EX_BUTTONPRESS;       tmp_tok.number_to_destroy = 3; tmp_tok.arg_list = arg_list_iii; pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "AUTOAIM");             tmp_tok.opcode = OPCODE_EX_AUTOAIM;           tmp_tok.number_to_destroy = 7; tmp_tok.arg_list = arg_list_ffiiiii;  tmp_tok.variable_type = VAR_FLT; pxss_register_token(&tmp_tok); }
    { strcpy(tmp_tok.tag, "ROOMHEIGHTXY");        tmp_tok.opcode = OPCODE_EX_ROOMHEIGHTXY;      tmp_tok.number_to_destroy = 2; tmp_tok.arg_list = arg_list_ff;       tmp_tok.variable_type = VAR_FLT; pxss_register_token(&tmp_tok); }

};


#include "object.inl"

BASE_DATA * ex_spawn(PSoulfuScriptContext pss, Uint8 type, float x, float y, float z, Uint8* script_start)
{
    // ZZ > Creates a new object and returns a pointer to its data (for property access)

    BASE_DATA * retval;
    SPAWN_INFO loc_info;
    OBJECT_TYPE owner_type = OBJECT_UNKNOWN;

    memcpy(&loc_info, &(pss->spawn_info), sizeof(SPAWN_INFO));

    //???? make the owner THIS object ????
    if ( OBJECT_UNKNOWN == owner_type )
    {
        loc_info.owner = ptr_get_idx( &(pss->self.object), OBJECT_CHR );
        if( VALID_CHR(loc_info.owner) )
        {
            // the owner is a character
            owner_type = OBJECT_CHR;
        }
    }

    if ( OBJECT_UNKNOWN == owner_type )
    {
        loc_info.owner = ptr_get_idx( &(pss->self.object), OBJECT_PRT );
        if( VALID_PRT(loc_info.owner) )
        {
            // the owner is a particle
            owner_type = OBJECT_PRT;
        }
    }

    if ( OBJECT_UNKNOWN == owner_type )
    {
        loc_info.owner = ptr_get_idx( &(pss->self.object), OBJECT_WIN );
        if( VALID_WIN(loc_info.owner) )
        {
            // the owner is a window
            owner_type = OBJECT_WIN;
        }
    }

    if ( OBJECT_UNKNOWN == owner_type )
    {
        // there is no valid owner
        loc_info.owner = UINT16_MAX;
    }

    if (script_start == NULL)
    {
        // No .RUN file given, assume duplicate of current...  Makes spawning particles easier...
        script_start = pss->base.base.file_start;
    }

    retval = obj_spawn(pss, &loc_info, type, x, y, z, script_start, UINT16_MAX);

    if ( NULL != retval && type == OBJECT_CHR)
    {
        CHR_DATA * tmp_pchr = CAST(CHR_DATA *, retval);
        Uint8    * ddd_file, * ptmp_c;

        // Set the current frame to be the first frame of the current action...
        ddd_file = CAST(Uint8*, tmp_pchr->model.parts[MODEL_PART_BASE].file);
        if (NULL != ddd_file)
        {
            ptmp_c = ddd_file + 6 + ( tmp_pchr->action << 1);
            tmp_pchr->frame = DEREF( Uint16, ptmp_c );

            if (UINT16_MAX == tmp_pchr->frame)
            {
                // Desired action from Spawn() not found, so default to stand action...
                tmp_pchr->action = ACTION_STAND;
                tmp_pchr->daction = ACTION_STAND;
                ptmp_c = ddd_file + 6 + (tmp_pchr->action << 1);
                tmp_pchr->frame = DEREF( Uint16, ptmp_c );
            }
        }
    }


    return retval;
}

bool_t ex_gopoof(PSoulfuScriptContext pss, int type)
{
    int        i, j;
    bool_t     keepgoing = ktrue;
    OBJECT_PTR obj_ptr;
    Uint8    * ptmp_a;

    switch (type)
    {
        case POOF_SELF:
            obj_destroy( &(pss->self.object) );
            pxss_var_set_int( &(pss->base.base.return_var), kfalse);
            pss->base.base.running       = kfalse;
            break;

        case POOF_TARGET:
            i = pss->self.object.bas->target;

            if ( VALID_CHR_RANGE(i) )
            {
                obj_destroy( ptr_set_type(&obj_ptr, OBJECT_CHR, chr_data_get_ptr_raw( i )) );
            }

            break;

        case POOF_ALL_OTHER_WINDOWS:
            // Destroy all windows, except self (for menus)

            for (i = 0; i < main_used_window_count; i++)
            {
                j = main_used_window[i];
                ptmp_a = CAST(Uint8*, main_win_data + j);

                if (ptmp_a != pss->self.object.unk)
                {
                    obj_destroy(ptr_set_type(&obj_ptr, OBJECT_WIN, main_win_data + j));
                }
            }

            break;

        case WARN_ALL_OTHER_WINDOWS:
            // Give an Event() to all windows, except self (for menus)...
            // Since windows don't have an event byte, it's assumed that
            // the event function means that they're gonna be poof'd soon...
            repeat(i, main_used_window_count)
            {
                WIN_DATA * pwin;

                j = main_used_window[i];
                if( VALID_WIN_RANGE( j ) )
                {
                    pwin = main_win_data + j;

                    if (pwin->on && pwin != pss->self.object.win)
                    {
                        pss->window3d.order = MAX_WINDOW - ((Uint8) j);

                        current_win[current_win_idx++] = pwin;
                        sf_fast_rerun_script(&(pwin->script_ctxt), FAST_FUNCTION_EVENT);
                        assert(pwin == current_win[--current_win_idx] );
                        current_win[current_win_idx] = NULL;
                    }
                }
            }
            break;

        case POOF_STUCK_PARTICLES:
        case POOF_TARGET_STUCK_PARTICLES:

            // Find the character we're dealin' with...
            if (type == POOF_STUCK_PARTICLES)
            {
                i = ptr_get_idx(&(pss->self.object), OBJECT_CHR);
            }
            else
            {
                i = pss->self.object.bas->target;
            }

            if (UINT16_MAX != i)
            {
                // Destroy all of the particles that are stuck to this character...
                repeat(j, MAX_PARTICLE)
                {
                    PRT_DATA * pprt = main_prt_data + j;

                    if (!pprt->on) continue;

                    if ((pprt->flags) & PART_STUCK_ON)
                    {
                        if ((pprt->stuckto) == i)
                        {
                            pprt->delete_me = ktrue;
                            pprt->on        = kfalse;
                        }
                    }
                }
            }

            break;

        case POOF_ALL_PARTICLES:
            obj_poof_all(OBJECT_PRT);
            pss->enc_cursor.active = kfalse;
            break;

        case POOF_ALL_CHARACTERS:
            obj_poof_all(OBJECT_CHR);
            pss->enc_cursor.active = kfalse;
            break;
    }

    return keepgoing;
};

void ex_dismount(PSoulfuScriptContext pss)
{
    CHR_DATA * prider, * pmount;
    int irider, imount;
    int tmp_i;

    imount = ptr_get_idx( &(pss->self.object), OBJECT_CHR );

    if ( VALID_CHR_RANGE(imount) )
    {
        // Figger if mount is kickin' rider off, or if rider is jumpin' off...

        pmount = chr_data_get_ptr( imount );

        irider = pss->self.object.chr->rider;
        prider = chr_data_get_ptr( irider );

        if ( NULL == prider )
        {
            // Self.rider is invalid, so current char must be the rider jumpin' off the mount...
            irider = imount;
            prider = chr_data_get_ptr( irider );

            imount = pss->self.object.chr->mount;
            pmount = chr_data_get_ptr( imount );
        }

        // irider should be rider and imount should be mount...
        if ( NULL != prider && NULL != pmount )
        {
            OBJECT_PTR   tmp_obj_ptr;

            tmp_obj_ptr = pss->self.object;
            tmp_i = pss->self.item;
            {
                prider->event = EVENT_DISMOUNTED;
                sf_fast_rerun_script( &(prider->script_ctxt), FAST_FUNCTION_EVENT);

                pmount->event = EVENT_DISMOUNTED;
                sf_fast_rerun_script( &(pmount->script_ctxt), FAST_FUNCTION_EVENT);
            }

            focus_set( &(pss->self), &tmp_obj_ptr, tmp_i);

            prider->rider = UINT16_MAX;
            prider->mount = UINT16_MAX;
            pmount->rider = UINT16_MAX;
            pmount->mount = UINT16_MAX;

            // Set actions & velocities for rider...
            prider->action = ACTION_JUMP_BEGIN;
            prider->daction = ACTION_JUMP;

            if ( prider->flags & CHAR_IN_WATER)
            {
                // Rider is in water, so make sure we don't get stuck unable to dismount...
                prider->nctimer = 30;
            }

            prider->flags |= CHAR_FALL_ON;
            prider->vx = 0.0f;
            prider->vy = 0.0f;
            prider->vz = 0.65f;


            // Make sure rider isn't flag'd to stand on platform (in case mount is also a platform)
            prider->flags &= (~CHAR_ON_PLATFORM);
        }
    }
};

bool_t ex_playsound(PSoulfuScriptContext pss, Uint8 * raw_file, int pitch_skip, int volume)
{
    float  ftmp1, ftmp2, ftmp3;
    bool_t retval = kfalse;
    Uint8  pan = 128;

    // Setup volume and pan...
    if ( ptr_is_type(&(pss->self.object), OBJECT_CHR) )
    {
        // We're dealing with a character or particle, so do volume falloff with distance...
        ftmp1 = pss->self.object.chr->x - camera.target_xyz[XX];
        ftmp2 = pss->self.object.chr->y - camera.target_xyz[YY];
        ftmp3 = pss->self.object.chr->z - camera.target_xyz[ZZ];
        ftmp3 = ftmp1 * ftmp1 + ftmp2 * ftmp2 + ftmp3 * ftmp3;

        // Should be full volume at 50 feet...
        ftmp3 = (volume * 2500.0f) / ftmp3;
        CLIP(0.0f, ftmp3, ((float) volume));
        volume = (Uint8) ftmp3;

        // Now figger pan based on side dot...
        ftmp3 = 128.0f + ((camera.side_xyz[XX] * ftmp1 + camera.side_xyz[YY] * ftmp2) * 5.0f);
        CLIP(0.0f, ftmp3, 255.0f);
        pan = (Uint8) ftmp3;
    }

    volume = (Uint8) ((volume * master_sfx_volume) >> 8);

    if (volume > 0)
    {
        play_sound(0, raw_file, volume, pan, (Uint16) pitch_skip, NULL);
        retval = ktrue;
    }

    return retval;
}

bool_t ex_play_megasound(PSoulfuScriptContext pss, Uint8* raw_start, int pitch_skip, int volume, Uint8 pan, Uint8 * loop_data)
{
    int cur_bits = music_tempo;

    volume = (Uint8) ((((Uint8) volume) * master_sfx_volume) >> 8);

    music_tempo = 1378;                                     // Duration in 16th seconds...
    play_sound(0, raw_start,  volume, pan, (Uint16) pitch_skip, loop_data);
    music_tempo = cur_bits;

    return ktrue;
}

bool_t ex_distance_sound(PSoulfuScriptContext pss, int channel)
{
    // Changes the volume of an infinite looped sound, based on distance to camera

    int vol_left, vol_right;

    // !!!BAD!!!
    // !!!BAD!!!  Finish me...  Distance check for new_volume...  All new_volume stuff should be taken care of...  Just set channel and vol_right correctly depending on distance (instead of hacked into argument)...
    // !!!BAD!!!


    channel   %= MAX_CHANNEL;
    vol_left  = channel >> 6;
    vol_right = channel;

    if (channel_new_volume[channel][LEFT]  < vol_left )  channel_new_volume[channel][LEFT]  = vol_left;

    if (channel_new_volume[channel][RIGHT] < vol_right)  channel_new_volume[channel][RIGHT] = vol_right;

    return ktrue;
}

bool_t ex_update_files(PSoulfuScriptContext pss, Uint8 mode)
{
    SDF_PHEADER phdr_b;
    OBJECT_PTR  obj_ptr;
    Uint8     * ptmp_b;

    ptr_set_unk(&obj_ptr, mainbuffer);

    if (mode == UPDATE_END)
    {
        if (update_performed)
        {
            // Unload all of the textures from the graphics card
            display_unload_all_textures();

            // Stop playing any sounds
            sound_reset_channels();

            // Decompress any changed data files (ogg, jpg, pcx, etc...)  Overwrites old RAW and RGB and EKO files...
            decode_sdf_archive(SDF_FLAG_WAS_UPDATED, 0, 0);
            ddd_magic_update_thing(SDF_FLAG_WAS_UPDATED, 0, 0);
            render_crunch_all(SDF_FLAG_WAS_UPDATED, 0, 0);


            // Relink the RUN files...
            pxss_compiler_error = kfalse;

            while (sf_stage_compile(PXSS_FUNCTIONIZE, SDF_FLAG_ALL, 0.0f, 0.0f) == kfalse); // Keep linking until all of the errors go away...

            // Twiddle this and that
            update_active = kfalse;
            sdf_archive_flag_clear(SDF_FLAG_WAS_UPDATED);

            message_setup();


            // Update addresses so things don't explode...
            damage_setup();
            display_kanji_setup();


            // Find and run LANGUAGE.Setup() and ITEMREG.Setup()...
            phdr_b = sdf_archive_find_filetype("LANGUAGE", SDF_FILE_IS_RUN);

            if (phdr_b)
            {
                ptmp_b = sdf_file_get_data(phdr_b);
                _sf_fast_run_script_obj(pss->psgs, ptmp_b, FAST_FUNCTION_SETUP, &obj_ptr);
            }

            phdr_b = sdf_archive_find_filetype("ITEMREG", SDF_FILE_IS_RUN);

            if (phdr_b)
            {
                ptmp_b = sdf_file_get_data(phdr_b);
                _sf_fast_run_script_obj(pss->psgs, ptmp_b, FAST_FUNCTION_SETUP, &obj_ptr);
            }


            // Reload the new textures
            display_load_all_textures();
        }
    }
    else if (mode == UPDATE_RECOMPILE)
    {
        // Get script names for each object
        obj_recompile_start();

        // Recompile all of the SRC files that've been updated...
        ex_compile_setup();
        sf_stage_compile(PXSS_HEADERIZE, SDF_FLAG_WAS_UPDATED, 0.0f, 0.0f);
        sf_stage_compile(PXSS_COMPILERIZE, SDF_FLAG_WAS_UPDATED, 0.0f, 0.0f);

        // Relink the RUN files...
        while (sf_stage_compile(PXSS_FUNCTIONIZE, SDF_FLAG_ALL, 0.0f, 0.0f) == kfalse); // Keep linking until all of the errors go away...

        damage_setup();  // Need to keep track of PNUMBER.RUN...


        // Fill in the object script addresses based on the names...
        obj_recompile_end();

        // Find and run LANGUAGE.Setup() and ITEMREG.Setup()...
        phdr_b = sdf_archive_find_filetype("LANGUAGE", SDF_FILE_IS_RUN);

        if (phdr_b)
        {
            ptmp_b = sdf_file_get_data(phdr_b);
            _sf_fast_run_script_obj(pss->psgs, ptmp_b, FAST_FUNCTION_SETUP, &obj_ptr);
        }

        phdr_b = sdf_archive_find_filetype("ITEMREG", SDF_FILE_IS_RUN);

        if (phdr_b)
        {
            ptmp_b = sdf_file_get_data(phdr_b);
            _sf_fast_run_script_obj(pss->psgs, ptmp_b, FAST_FUNCTION_SETUP, &obj_ptr);
        }
    }
    else if (mode == UPDATE_SDFSAVE)
    {
        // Save the datafile
        sdf_archive_save("datafile.sdf");
    }
    else if (mode == UPDATE_SDFREORGANIZE)
    {
        // Reorganize the datafile
        sdf_archive_optimize();
    }
    else
    {
        // Reload all of the textures...
        display_unload_all_textures();
        display_load_all_textures();
    }

    return ktrue;
}

Uint16 ex_acquire_target(PSoulfuScriptContext pss, Uint32 flags, Uint8* script_file, float radius_max)
{
    Uint16 best_chr;
    float radius, best_radius, f;
    int i, tmp_j;
    CHR_DATA* pchr;

    best_chr = UINT16_MAX;                // The best character we've found...
    radius_max = radius_max * radius_max; // The original best_radius square'd...
    best_radius = radius_max;             // The best distance we've found...  Square'd actually...
    repeat(i, MAX_CHARACTER)
    {
        // Is this character on?
        pchr = chr_data_get_ptr(i);
        if (NULL == pchr) continue;


        // First check team...
        if ((pchr->team == TEAM_NEUTRAL && (flags & ACQUIRE_SEE_NEUTRAL)) || (pchr->team > TEAM_NEUTRAL && pchr->team != pss->self.object.bas->team && (flags & ACQUIRE_SEE_ENEMY)) || (pchr->team == pss->self.object.bas->team && (flags & ACQUIRE_SEE_FRIENDLY)))
        {
            // Team is valid...  Now check specified script...
            if (script_file == NULL || pchr->script_ctxt.base.base.file_start == script_file)
            {
                // Script is okay or not specified...  But let's check for some other miscellaneous stuff...
                if (pchr->action != ACTION_RIDE && (pchr->intimer < 1000) && ((flags & ACQUIRE_IGNORE_COLLISION) || !(pss->self.object.bas->flags & CHAR_NO_COLLISION)))
                {
                    // Check for mimic hiding...
                    if (!((pchr->vflags & VIRTUE_FLAG_STILL_HIDE) && pchr->action == ACTION_STAND) || (flags & ACQUIRE_SEE_INVISIBLE))
                    {
                        // Limit to open mounts if that's what we're after...
                        if (((pchr->flags & CHAR_CAN_BE_MOUNTED) && (pchr->pttimer == 0) && pchr->daction != ACTION_KNOCK_OUT && ((pchr->rider) == UINT16_MAX)) || !(flags & ACQUIRE_OPEN_MOUNT_ONLY))
                        {
                            // Limit to standing characters if that's what we're after...
                            if (pchr->action < ACTION_KNOCK_OUT_BEGIN || pchr->action > ACTION_KNOCK_OUT_END || !(flags & ACQUIRE_STANDING_ONLY))
                            {
                                // Looks like a valid character, but is it in front of us (or can we see behind us)?
                                radius = pchr->x - pss->self.object.bas->x;
                                f      = pchr->y - pss->self.object.bas->y;
                                tmp_j = (flags & ACQUIRE_SEE_BEHIND);

                                if ( ptr_is_type(&(pss->self.object), OBJECT_CHR) )
                                {
                                    // We're dealing with a character, so do a dot product with our matrix...
                                    tmp_j |= ((radius * pss->self.object.chr->frontx + f * pss->self.object.chr->fronty) > 0.0f) ? 1 : 0;

                                    // Also make sure we don't return our own mount or ourself...
                                    if (i == pss->self.object.chr->mount || pchr == pss->self.object.chr)
                                    {
                                        tmp_j = kfalse;
                                    }
                                }
                                else if ( ptr_is_type(&(pss->self.object), OBJECT_PRT) )
                                {
                                    // We're dealing with a particle, so dot with our velocity vector...
                                    tmp_j |= ((radius * pss->self.object.prt->vx + f * pss->self.object.prt->vy) > 0.0f) ? 0 : 1;
                                }

                                if (tmp_j)
                                {
                                    // It's either in front of us, or we've got wrap-around eyes...
                                    radius = (radius * radius) + (f * f);

                                    if (!(pchr->alpha > 100 || (flags & ACQUIRE_SEE_INVISIBLE)))
                                    {
                                        // Our target is invisible, and we can't see 'em...  But we can still smell 'em at close range...
                                        radius_max = 225.0f;  // 15 foot best_radius...
                                    }

                                    // But is it within the specified best_radius?
                                    if (radius < radius_max)
                                    {
                                        // Shorten the distance depending on max hits of opponent
                                        radius /= (((Uint16) pchr->hitsmax) + 1);


                                        // Is it our best distance yet?
                                        if (radius < best_radius)
                                        {
                                            // Looks like we've got a new best character...
                                            best_chr = i;
                                            best_radius = radius;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    return best_chr;

}

void ex_autoaim(PSoulfuScriptContext pss, float speed_horz, float speed_vert, int aim_spin, int aim_team, int aim_dex, int cone_angle, int mode)
{
    // Are we crunchin', or just returning values?
    if (mode < AUTOAIM_CRUNCH && mode > -1)
    {
        // Just returning values...
        if (mode == AUTOAIM_TARGET)
        {
            // I hope this works...  Converting a short to a float...
            // !!!BAD!!!
            // !!!BAD!!!  Check this...  May need a +0.5 or somethin'...
            // !!!BAD!!!
            push_flt_stack(&(pss->base.base), autoaim_target);
        }
        else
        {
            push_flt_stack(&(pss->base.base), autoaim_velocity_xyz[mode]);
        }
    }
    else
    {
        // Doin' the big bad crunch...  Or trying anyway...  Make sure we're dealin' with a character first...
        if ( ptr_is_type(&(pss->self.object), OBJECT_CHR) )
        {
            autoaim_helper(pss, speed_horz, speed_vert, (Uint16) aim_spin, (Uint8) aim_team, (Uint8) aim_dex, (Uint16) cone_angle, pss->self.object.chr->height, (Uint8) mode);
        }
        else
        {
            // Looks like we've got a particle...  0 for height...
            autoaim_helper(pss, speed_horz, speed_vert, (Uint16) aim_spin, (Uint8) aim_team, (Uint8) aim_dex, (Uint16) cone_angle, (Uint8) 0, (Uint8) mode);
        }

        push_flt_stack(&(pss->base.base), 0.0f);
    }
}

void ex_window_border(PSoulfuScriptContext pss, bool_t draggable, float x, float y, float w, float h, Uint32 flags)
{
    // sets the active window area AND window
    SOULFU_WINDOW loc_win;
    float x_min, x_max, y_min, y_max;

    assert(OBJECT_WIN == pss->self.object.type);

    pss->screen.x = x * pss->screen.scale + pss->self.object.win->x;
    pss->screen.y = y * pss->screen.scale + pss->self.object.win->y;
    pss->screen.w = w * pss->screen.scale;
    pss->screen.h = h * pss->screen.scale;

    x_min = pss->screen.x - pss->screen.scale;
    x_max = pss->screen.x + pss->screen.w + pss->screen.scale;

    y_min = pss->screen.y - pss->screen.scale;
    y_max = pss->screen.y + pss->screen.h + pss->screen.scale;

    // Allow the players to drag windows around...  Also check if the cursor is inside the window...
    if (draggable)
    {
        float dx, dy;
        dx = mouse.x - mouse.last_x;
        dy = mouse.y - mouse.last_y;

        if (mouse.down[BUTTON0] && ptr_equal(&(mouse.last.object), &(pss->self.object)) && mouse.last.item == DRAG_ITEM)
        {
            if (mouse.last_x > x_min && mouse.last_x < x_max)
            {
                if (mouse.last_y > y_min && mouse.last_y < y_max)
                {
#ifdef DEVTOOL

                    if (draggable != BORDER_DRAG)
                    {
                        // Modeler tools...
                        if (draggable == BORDER_SELECT)
                        {
                            // Selection box thing
                            if (!selection.box_on)
                            {
                                selection.box_on      = ktrue;
                                selection.box_tl[XX]  = mouse.x;
                                selection.box_tl[YY]  = mouse.y;
                                selection.box_min[XX] = x_min;
                                selection.box_min[YY] = y_min;
                                selection.box_max[XX] = x_max;
                                selection.box_max[YY] = y_max;

                                if (keyb.down[SDLK_LSHIFT] == kfalse && keyb.down[SDLK_RSHIFT] == kfalse)
                                {
                                    select_clear();
                                    room_select_clear();
                                }
                            }

                            selection.box_br[XX] = mouse.x;
                            selection.box_br[YY] = mouse.y;
                        }
                        else if (draggable == BORDER_CROSS_HAIRS)
                        {
                            selection.close_type = BORDER_CROSS_HAIRS;
                        }
                        else if (draggable == BORDER_POINT_PICK)
                        {
                            if (selection.pick_on == kfalse)
                            {
                                selection.close_type = BORDER_POINT_PICK;
                            }

                            selection.pick_on = ktrue;
                        }
                        else if (draggable == BORDER_MOVE)
                        {
                            // Movement or rotation or scaling...
                            if (selection.move_on == kfalse)
                            {
                                selection.close_type = BORDER_CROSS_HAIRS;
                                selection.move_on = ktrue;
                                rotation_view = selection.view;
                            }
                            else
                            {
                                if (rotation_view == selection.view)
                                {
                                    selection.close_type = BORDER_MOVE;
                                }
                            }
                        }
                    }
                    else
#endif
                    {
                        // Allow dragging
                        pss->self.object.win->x += dx;
                        pss->self.object.win->y += dy;
                        pss->screen.x += dx;
                        pss->screen.y += dy;
#ifdef DEVTOOL
                        selection.box_on = kfalse;
#endif
                    }
                }
            }
        }
    }


    if (mouse.x > x_min && mouse.x < x_max)
    {
        if (mouse.y > y_min && mouse.y < y_max)
        {
            focus_set(&(mouse.current), &(pss->self.object), NO_ITEM);

            if (mouse.pressed[BUTTON0] && ptr_equal(&(mouse.last.object), &(pss->self.object)) )
            {
                win_request_promotion( &(pss->self.object) );
            }

            if (draggable)
            {
                if (mouse.pressed[BUTTON0] || mouse.last.item == DRAG_ITEM)
                {
                    mouse.current.item = DRAG_ITEM;
                }
            }
        }
    }

    // Draw the window

    loc_win.scale = pss->screen.scale;
    loc_win.x = pss->screen.x;
    loc_win.y = pss->screen.y;
    loc_win.w = w;
    loc_win.h = h;

    display_window(&loc_win, (Uint16) flags);
};

bool_t ex_window_string(PSoulfuScriptContext pss, Uint32 color, float x, float y, char * string)
{
    SOULFU_WINDOW loc_win;

    // "pss->self.object.win" not used
    //assert(OBJECT_WIN == pss->self.object.type);

    loc_win.scale = pss->screen.scale;
    loc_win.x = x * loc_win.scale + pss->screen.x;
    loc_win.y = y * loc_win.scale + pss->screen.y;
    loc_win.w = 0;
    loc_win.h = 0;

    color_temp[0] = (color >> 16) & 0xFF;
    color_temp[1] = (color >> 8) & 0xFF;
    color_temp[2] = (color >> 0) & 0xFF;

    display_color(color_temp);

    if (color == 0)
    {
        // Black text should use the book texture...
        display_pick_texture(texture_bookfnt);
    }
    else
    {
        // Normal text
        display_pick_texture(texture_ascii);
    }

    display_string(string, loc_win.x, loc_win.y, loc_win.scale);

    return ktrue;
}


Uint16 ex_window_minilist(PSoulfuScriptContext pss, float x, float y, float w, float h, Uint16 state, char * sz_options)
{
    // Adds a mini list style input within a window border...
    int   minilist_count;
    Uint8 select_idx, expansion;
    float ftmp1, ftmp2, ftmp3;

    SOULFU_WINDOW loc_win;

    // "pss->self.object.win" not used
    //assert(OBJECT_WIN == pss->self.object.type);

    loc_win.scale = pss->screen.scale;
    loc_win.x = x * loc_win.scale + pss->screen.x;
    loc_win.y = y * loc_win.scale + pss->screen.y;
    loc_win.w = w * loc_win.scale + loc_win.x;
    loc_win.h = h * loc_win.scale + loc_win.y;

    select_idx = 0;
    expansion = 0;

    if (h > 0 && w > 0)
    {
        display_mini_list(sz_options, loc_win.x, loc_win.y, (Uint8)w, (Uint8)h, loc_win.scale, state);

        // Input handling...
        minilist_count = 0;             // Count of mice not over item
        expansion  = (state >> 0) & 0xFF; // The expansion information
        select_idx = (state >> 8) & 0xFF; // The selected list item...

        if ( focus_equal(&(mouse.last), &(pss->self)) )
        {
            // Able to handle input clicks...
            if (mouse.pressed[BUTTON0] && expansion < 129)
            {
                expansion = 129;
                // Delay window ordering effect until after all windows have been drawn...
                win_request_promotion( &(pss->self.object) );
            }
            else if (expansion > 128)
            {
                // List is expanding
                if (expansion < 239)
                {
                    expansion += 16;
                }
                else
                {
                    // List is fully expanded...  Check for selection...
                    ftmp2 = loc_win.y + (1.5f * loc_win.scale);
                    expansion = 255;

                    if (mouse.y > ftmp2)
                    {
                        expansion = (Uint8) ((mouse.y - ftmp2) / loc_win.scale);

                        if (expansion < h)
                        {
                            ftmp1 = loc_win.x + (loc_win.scale * w);
                            ftmp2 = ftmp2 + loc_win.scale * expansion;
                            ftmp3 = ftmp2 + loc_win.scale;
                            display_highlight(loc_win.x, ftmp2, ftmp1, ftmp3);
                        }
                    }

                    if (mouse.pressed[BUTTON0] || mouse.unpressed[BUTTON0])
                    {
                        // Did we pick something?
                        if (expansion < h)
                        {
                            // Remember the selection, and make it shrink again...
                            focus_copy(&(mouse.current), &(pss->self));
                            select_idx = expansion;
                            expansion   = 128;
                        }
                        else
                        {
                            if (mouse.pressed[BUTTON0])
                            {
                                // Make it shrink again...
                                expansion = 128;
                            }
                            else
                            {
                                // Unclick doesn't close on main entry...
                                expansion = 255;
                            }
                        }
                    }
                    else
                    {
                        // Stay expanded
                        expansion = 255;
                    }
                }
            }
            else
            {
                // Mouse is over button, so make it highlight...
                expansion = 128;
            }

        }
        else
        {
            // Mouse isn't over button, so make it shrink...
            minilist_count++;
        }


        // Is it expanded???
        if (expansion > 128)
        {
            // Yup...  Need to check even if cursor isn't inside window...
            if (mouse.x > loc_win.x && mouse.x < loc_win.w)
            {
                if (mouse.y > loc_win.y && mouse.y < loc_win.h + 2*loc_win.scale)
                {
                    focus_copy(&(mouse.current), &(pss->self));
                }
            }
        }
        else if ( ptr_equal(&(mouse.current.object), &(pss->self.object)) )
        {
            // Nope...  Only check if cursor is inside window...
            if (mouse.x > loc_win.x && mouse.x < loc_win.w)
            {
                if (mouse.y > loc_win.y && mouse.y < loc_win.y + loc_win.scale)
                {
                    mouse.current.item = pss->self.item;
                }
            }
        }

        // Make the button shrink
        if (minilist_count > 0)
        {
            if (expansion > 128)
            {
                expansion = 128;
            }

            expansion -= 4;

            if (expansion > 128)  expansion = 0;
        }
    }

    pss->self.item++;
    state = ((select_idx & 0xFF) << 8) | (expansion & 0xFF);

    return state;
}

int ex_window_slider(PSoulfuScriptContext pss, float x, float y, float w, float h, int pos)
{
    float ptmp1, ptmp2;
    SOULFU_WINDOW loc_win;

    // "pss->self.object.win" not used
    //assert(OBJECT_WIN == pss->self.object.type);

    loc_win.x = (x * pss->screen.scale) + pss->screen.x;
    loc_win.y = (y * pss->screen.scale) + pss->screen.y;
    loc_win.w = w;
    loc_win.h = h;
    loc_win.scale = pss->screen.scale;

    display_slider(loc_win.x, loc_win.y, (Uint8) loc_win.w, (Uint8) loc_win.h, loc_win.scale, ((float) pos) * INV_0xFF);

    // Input handling...
    if (loc_win.w > loc_win.h)
    {
        loc_win.x += 0.5f * loc_win.scale;
    }
    else
    {
        loc_win.y += 0.5f * loc_win.scale;
    }


    if ( focus_equal(&(mouse.last), &(pss->self)) )
    {
        if (mouse.down[BUTTON0])
        {
            focus_copy(&(mouse.current), &(pss->self));

            if (loc_win.w > loc_win.h)
            {
                // Horizontal...
                ptmp1 = ((loc_win.w - 1) * loc_win.scale);
                pos = (Sint32) (((mouse.x - loc_win.x) / ptmp1) * 255.0f);

                if (pos < 0) pos = 0;

                if (pos > 255) pos = 255;
            }
            else
            {
                // Vertical...
                ptmp2 = ((loc_win.h - 1) * loc_win.scale);
                pos = (Sint32) (((mouse.y - loc_win.y) / ptmp2) * 255.0f);

                if (pos < 0) pos = 0;

                if (pos > 255) pos = 255;
            }
        }
        else
        {
            mouse.last.item = NO_ITEM;
        }
    }
    else
    {
        // Is the mouse inside the slider?
        if (mouse.x > loc_win.x && mouse.y > loc_win.y)
        {
            if (loc_win.w > loc_win.h)
            {
                // Horizontal...
                ptmp1 = loc_win.x + ((loc_win.w - 1) * loc_win.scale);

                if (mouse.x < ptmp1 && mouse.y < (loc_win.y + loc_win.scale))
                {
                    if (mouse.pressed[BUTTON0])
                    {
                        focus_copy(&(mouse.current), &(pss->self));

                        // Delay window ordering effect until after all windows have been drawn...
                        win_request_promotion( &(pss->self.object) );
                    }
                }
            }
            else
            {
                // Vertical
                ptmp2 = loc_win.y + ((loc_win.h - 1) * loc_win.scale);

                if (mouse.x < (loc_win.x + loc_win.scale) && mouse.y < ptmp2)
                {
                    if (mouse.pressed[BUTTON0])
                    {
                        focus_copy(&(mouse.current), &(pss->self));

                        // Delay window ordering effect until after all windows have been drawn...
                        win_request_promotion( &(pss->self.object) );
                    }
                }
            }
        }
    }


    pss->self.item++;

    return pos;
}

int ex_window_image(PSoulfuScriptContext pss, float x, float y, float w, float h, Uint8* rgb_data, char* sz_alt_txt, int state)
{
    int cur_bits;
    SOULFU_WINDOW loc_win;
    SDF_PHEADER pheader;

    // "pss->self.object.win" not used
    //assert(OBJECT_WIN == pss->self.object.type);

    pheader = sdf_archive_find_header_by_data(rgb_data);

    loc_win.scale = pss->screen.scale;
    loc_win.x = x * loc_win.scale + pss->screen.x;   // Top left x
    loc_win.y = y * loc_win.scale + pss->screen.y;   // Top left y
    loc_win.w = w * loc_win.scale + loc_win.x;                 // Bottom right x
    loc_win.h = h * loc_win.scale + loc_win.y;                 // Bottom right y

    // Draw the image...  Can pass NULL instead of a filename to act as a hotspot...
    if (rgb_data != NULL)
    {
        if (((Uint32) rgb_data) == 1)
        {
            // Kanji image...
            if (sz_alt_txt)
            {
                display_color(black);
            }
            else
            {
                display_color(white);
            }

            display_kanji((Uint16) state, loc_win.x, loc_win.y, loc_win.w - loc_win.x, loc_win.h - loc_win.y);
            state = 0;
            sz_alt_txt = 0;
        }
        else
        {
            // Normal image...
            display_color(white);
            rgb_data += 2;
            display_image(loc_win.x, loc_win.y, loc_win.w, loc_win.h, DEREF( Uint32, rgb_data ));
        }
    }


    // Check mouse over...
    cur_bits = 0;

    if (mouse.x > loc_win.x && mouse.x < loc_win.w)
    {
        if (mouse.y > loc_win.y && mouse.y < loc_win.h)
        {
            focus_copy(&(mouse.current), &(pss->self));

            if ( focus_equal(&(mouse.last), &(pss->self)) )
            {
                // Cap the alt text timer at 50...
                if (state < 50)
                {
                    state += (main_frame_skip << 1);

                    if (state > 50)
                    {
                        state = 50;
                    }
                }

                cur_bits++;


                // Draw the alternate text...
                if (state >= 50)
                {
                    // Allow scripts to use NULL for alt text...
                    if (sz_alt_txt)
                    {
                        // Display it later...  With cursor drawings...
                        strcpy(mouse.text, sz_alt_txt);
                    }
                }


                // Handle clicks...
                if (mouse.pressed[BUTTON0])
                {
                    state = 255;
                    // Delay window ordering effect until after all windows have been drawn...
                    win_request_promotion( &(pss->self.object) );
                }
            }
        }
    }


    // Kill alt text timer if there aren't any mice over it...
    if (cur_bits == 0)
    {
        if (state > 250)
        {
            state--;
        }
        else
        {
            state = 0;
        }
    }


    pss->self.item++;

    return state;
}


bool_t ex_window_tracker(PSoulfuScriptContext pss, float x, float y, float w, float h, Uint8 * mus_file, int win_stt, int win_wid, int instrument, int volume, Uint8 pan)
{
    bool_t retval = kfalse;

#ifdef DEVTOOL
    SOULFU_WINDOW loc_win;

    // "pss->self.object.win" not used
    //assert(OBJECT_WIN == pss->self.object.type);

    loc_win.scale = pss->screen.scale;
    loc_win.x = (x * loc_win.scale) + pss->screen.x; // Top left x
    loc_win.y = (y * loc_win.scale) + pss->screen.y; // Top left y
    loc_win.w = (loc_win.scale * w) + loc_win.x;             // Bottom right x
    loc_win.h = (loc_win.scale * h) + loc_win.y;             // Bottom right y

    retval = tool_tracker(pss, loc_win.x, loc_win.y, loc_win.w, loc_win.h, mus_file, win_stt, win_wid, instrument, volume, pan);
#endif

    pss->self.item++;

    return retval;

};


bool_t ex_window_book(PSoulfuScriptContext pss, float x, float y, float w, float h, int num_pages, Uint8 page_turn, Uint8* page_data)
{
    float ftmp1;
    int itmp1;
    int page;

    SOULFU_WINDOW loc_win;

    // "pss->self.object.win" not used
    //assert(OBJECT_WIN == pss->self.object.type);

    loc_win.scale = pss->screen.scale;
    loc_win.x = (x * loc_win.scale) + pss->screen.x;
    loc_win.y = (y * loc_win.scale) + pss->screen.y;
    loc_win.w = w;
    loc_win.h = h;

    page = (Uint8) (page_turn >> 5);                               // The current page (must be less than number of pages...  0 if one page... )
    page = page << 1;                                              // Always an even number...  Page to display on right side...
    page_turn = (page_turn & 31);                                  // The current frame
    ftmp1 = loc_win.scale * .75f;
    itmp1 = (int)(loc_win.w * loc_win.h);                          // Page skip...

    // Filesize must be loc_win.w*loc_win.h*pages

    // Draw the pages...
    if (page_turn)
    {
        // Left page
        if (page != 0 && (page - 1) < num_pages) display_book_page(page_data + (itmp1*(page - 1)), loc_win.x, loc_win.y, page_xy[PAGE_FRAME-1], ftmp1, (int)loc_win.w, (int)loc_win.h, ktrue, page - 1);
        else                                   display_book_page(NULL, loc_win.x, loc_win.y, page_xy[PAGE_FRAME-1], ftmp1, (int)loc_win.w, (int)loc_win.h, ktrue, page - 1);

        // Right page
        if ((page + 2) < num_pages)  display_book_page(page_data + (itmp1*(page + 2)), loc_win.x, loc_win.y, page_xy[0], ftmp1, (int)loc_win.w, (int)loc_win.h, kfalse, page + 2);
        else                       display_book_page(NULL, loc_win.x, loc_win.y, page_xy[0], ftmp1, (int)loc_win.w, (int)loc_win.h, kfalse, page + 2);

        // Turning page
        if (page < num_pages)      display_book_page(page_data + (itmp1*page), loc_win.x, loc_win.y, page_xy[page_turn], ftmp1, (int)loc_win.w, (int)loc_win.h, kfalse, page);
        else                       display_book_page(NULL, loc_win.x, loc_win.y, page_xy[page_turn], ftmp1, (int)loc_win.w, (int)loc_win.h, kfalse, page);

        if ((page + 1) < num_pages)  display_book_page(page_data + (itmp1*(page + 1)), loc_win.x, loc_win.y, page_xy[page_turn], ftmp1, (int)loc_win.w, (int)loc_win.h, ktrue, page + 1);
        else                       display_book_page(NULL, loc_win.x, loc_win.y, page_xy[page_turn], ftmp1, (int)loc_win.w, (int)loc_win.h, ktrue, page + 1);
    }
    else
    {
        // Left page
        if (page != 0 && (page - 1) < num_pages) display_book_page(page_data + (itmp1*(page - 1)), loc_win.x, loc_win.y, page_xy[PAGE_FRAME-1], ftmp1, (int)loc_win.w, (int)loc_win.h, ktrue, page - 1);
        else                                   display_book_page(NULL, loc_win.x, loc_win.y, page_xy[PAGE_FRAME-1], ftmp1, (int)loc_win.w, (int)loc_win.h, ktrue, page - 1);

        // Right page
        if (page < num_pages)  display_book_page(page_data + (itmp1*page), loc_win.x, loc_win.y, page_xy[0], ftmp1, (int)loc_win.w, (int)loc_win.h, kfalse, page);
        else                   display_book_page(NULL, loc_win.x, loc_win.y, page_xy[0], ftmp1, (int)loc_win.w, (int)loc_win.h, kfalse, page);
    }

    return ktrue;
}


void ex_window_input(PSoulfuScriptContext pss, float x, float y, float w, char* sz_text, int offset)
{
    Uint8 ui8;
    PBaseScriptContext pbs = &(pss->base.base);

    SOULFU_WINDOW loc_win;

    // "pss->self.object.win" not used
    //assert(OBJECT_WIN == pss->self.object.type);

    // Draw the box...
    loc_win.scale = pss->screen.scale;
    loc_win.x = (x * loc_win.scale) + pss->screen.x;
    loc_win.y = (y * loc_win.scale) + pss->screen.y;
    loc_win.w = w;
    loc_win.h = 1;

    // Is this the active input box?
    if ( focus_equal(&(last_wi.input), &(pss->self)) )
    {
        // Yup...  Draw cursor and check keyboard...
        input_active = 3;
        display_input(sz_text, loc_win.x, loc_win.y, (Sint8) loc_win.w, loc_win.scale, (Sint8) last_wi.cursor_pos);

        if (last_wi.cursor_pos >= 0 && last_wi.cursor_pos < loc_win.w)
        {
            ui8 = input_read_key_buffer();

            if (ui8 != 0)
            {
                // We have input...
                if (ui8 == SDLK_BACKSPACE || ui8 == SDLK_DELETE)
                {
                    last_wi.cursor_pos--;

                    if (last_wi.cursor_pos < 0) last_wi.cursor_pos = 0;

                    sz_text[last_wi.cursor_pos] = 0;
                }
                else
                {
                    if (ui8 == SDLK_RETURN)
                    {
                        // Enter key...
                        last_wi.cursor_pos = 255;
                        last_wi.input.item += offset;  // Offset to next item
                    }
                    else if (last_wi.cursor_pos < (loc_win.w - 1))
                    {
                        // Some other key...
                        sz_text[last_wi.cursor_pos] = ui8;
                        last_wi.cursor_pos++;
                        sz_text[last_wi.cursor_pos] = 0;
                    }
                }
            }
        }

        push_int_stack(pbs, last_wi.cursor_pos);

        if (last_wi.cursor_pos == 255)
        {
            last_wi.cursor_pos = 0;
        }
    }
    else
    {
        // Nope... Don't draw cursor
        display_input(sz_text, loc_win.x, loc_win.y, (Sint8) loc_win.w, loc_win.scale, (Sint8) - 1);
        push_int_stack(pbs, -1);
    }


    // Check input handling...
    loc_win.w = loc_win.x + (w * loc_win.scale);
    loc_win.h = loc_win.y + (1 * loc_win.scale);

    if (mouse.x > loc_win.x && mouse.x < loc_win.w)
    {
        if (mouse.y > loc_win.y && mouse.y < loc_win.h)
        {
            focus_copy(&(mouse.current), &(pss->self));

            if (mouse.pressed[BUTTON0] || mouse.pressed[BUTTON1])
            {
                if ( focus_equal(&(mouse.last), &(pss->self)) )
                {
                    // Only allow one input box to take from the keyboard...
                    focus_copy(&(last_wi.input), &(pss->self));
                    input_quick_reset_key_buffer();

                    if (mouse.pressed[BUTTON0])
                    {
                        // Keep the current string...
                        last_wi.cursor_pos = strlen(sz_text);
                    }
                    else
                    {
                        // Kill the current string...
                        *sz_text = 0;
                        last_wi.cursor_pos = 0;
                    }


                    // Delay window ordering effect until after all windows have been drawn...
                    win_request_promotion( &(pss->self.object) );
                }
            }
        }
    }

    pss->self.item++;
}


int ex_window_emacs(PSoulfuScriptContext pss, float x, float y, float w, float h, Uint32 cur_bits, Uint8 * emacs_file)
{
    int ih = 0;

#ifdef DEVTOOL
    SOULFU_WINDOW loc_win;
    int key;
    Uint16 ui16;
    int iw, loc_iw;

    // "pss->self.object.win" not used
    //assert(OBJECT_WIN == pss->self.object.type);

    // Draw the text editor...
    x = (x * pss->screen.scale) + pss->screen.x;          // Top left x
    y = (y * pss->screen.scale) + pss->screen.y;          // Top left y
    display_emacs(x, y, (int)w, (int)h, (Uint8) ((cur_bits)&63), (Uint8) ((cur_bits >> 6)&63), (Uint8) (cur_bits >> 12), (Uint16) ((cur_bits >> 20)&4095), emacs_file, pss->screen.scale);


    // Handle cursor clicks
    loc_win.x = x + (pss->screen.scale * w);
    loc_win.y = y + (pss->screen.scale * h);
    loc_win.w = w * 1.5f;
    loc_win.h = h * 1.5f;

    h = (float)((cur_bits) & 63);     // curx
	h = (float)((cur_bits >> 6) & 63);    // cury


    if (mouse.x > x && mouse.x < loc_win.x)
    {
        if (mouse.y > y && mouse.y < loc_win.y)
        {
            focus_copy(&(mouse.current), &(pss->self));

            if (mouse.pressed[BUTTON0])
            {
                if ( focus_equal(&(mouse.last), &(pss->self)) )
                {
                    // Only allow one input box to take from the keyboard...
                    focus_copy(&(last_wi.input), &(pss->self));

                    input_quick_reset_key_buffer();
                    last_wi.cursor_pos = 0;

                    // Figure out the new cursor location...
                    h = ((mouse.x - x) / (loc_win.x - x)) * loc_win.w;
                    h = ((mouse.y - y) / (loc_win.y - y)) * loc_win.h;

                    // Delay window ordering effect until after all windows have been drawn...
                    win_request_promotion( &(pss->self.object) );
                }
            }
        }
    }

    w = (float)((cur_bits >> 12) & 255);    // xscroll
    //                cur_bits = (cur_bits>>20)&2047;   // yscroll
    cur_bits = (cur_bits >> 20) & 4095;   // yscroll


    // Handle typematic input
    if ( focus_equal(&(last_wi.input), &(pss->self)) )
    {
        // Emacs is active...  Check cursor movement
        input_active = 3;

        if (keyb.pressed[SDLK_UP] || keyb.pressed[SDLK_KP8])
        {
            if (h > 4)  h--;
            else if (cur_bits > 0)  cur_bits--;
            else if (h > 0)  h--;

            emacs_buffer_write = 0;
        }

        if (keyb.pressed[SDLK_DOWN] || keyb.pressed[SDLK_KP2])
        {
            if (h < loc_win.h - 4)  h++;
            //                        else if(cur_bits < 2047)  cur_bits++;
            else if (cur_bits < 4095)  cur_bits++;
            else if (h < loc_win.h)  h++;

            emacs_buffer_write = 0;
        }

        if (keyb.pressed[SDLK_LEFT] || keyb.pressed[SDLK_KP4])
        {
            if (h > 4)  h--;
            else if (w > 0)  w--;
            else if (h > 0)  h--;

            emacs_buffer_write = 0;
        }

        if (keyb.pressed[SDLK_RIGHT] || keyb.pressed[SDLK_KP6])
        {
            if (h < loc_win.w - 4)  h++;
            else if (w < 255)  w++;
            else if (h < loc_win.w)  h++;

            emacs_buffer_write = 0;
        }

        if (keyb.pressed[SDLK_HOME])
        {
            h = 0;
            w = 0;
            emacs_buffer_write = 0;
        }

        if (keyb.pressed[SDLK_PAGEUP])
        {
            cur_bits -= (Uint32)loc_win.h - 4;

            if (cur_bits < 0)
            {
                h = 0;
                cur_bits = 0;
            }

            emacs_buffer_write = 0;
        }

        if (keyb.pressed[SDLK_PAGEDOWN])
        {
			cur_bits += (Uint32)loc_win.h - 4;

            //                        if(cur_bits > 2047) cur_bits = 2047;
            if (cur_bits > 4095) cur_bits = 4095;

            emacs_buffer_write = 0;
        }


        // Figure out the current cursor position in the data
        word_temp[0] = (Uint8) loc_win.h;
        loc_win.h = cur_bits + h;
        ui16 = (Uint16)h;

        ih = 0;
        loc_iw = 1;

        while (loc_iw > 0 && ih < TEXT_SIZE)
        {
            loc_iw = strlen(emacs_file) + 1;
            ih += loc_iw;
            emacs_file += loc_iw;
            loc_iw--;
        }

        iw = (int)w;
        ih = ui16;
        loc_iw = strlen(emacs_file);
        ui16 = 0;

        if ((ih + iw) > loc_iw)
        {
            // Cursor is past end of text...
            ui16 = (ih + iw) - loc_iw;
            emacs_file += loc_iw;
        }
        else
        {
            emacs_file += ih + iw;
        }


        // End key...  More difficult cursor movement...
        if (keyb.pressed[SDLK_END])
        {
            if (ui16)
            {
                h += w;
                h -= ui16;
            }
            else
            {
                h += w;
                h += strlen(emacs_file);
            }

            w = 0;

            if (h > loc_win.w - 4)
            {
                w = h - (loc_win.w - 4);
                h = loc_win.w - 4;
            }

            emacs_buffer_write = 0;
        }


        // Check normal key presses and insert/delete data...
        key = input_read_key_buffer();

        if (key != 0)
        {
            // We have input...
            switch (key)
            {
                case SDLK_BACKSPACE:
                    emacs_buffer_write = 0;

                    if (w == 0 && h == 0)
                    {
                        // Go to end of last line...
                        h += key - 1;

                        if (h > loc_win.w - 4)
                        {
                            w = h - (loc_win.w - 4);
                            h = loc_win.w - 4;
                        }

                        // Move the cursor...
                        if (h > 4) { h--;  emacs_file--; }
                        else if (cur_bits > 0) { cur_bits--;  emacs_file--; }
                        else if (h > 0) { h--;  emacs_file--; }
                        else break;  // No data to left
                    }
                    else
                    {
                        // Move the cursor
                        if (h > 4)  { h--; emacs_file--; }
                        else if (w > 0)  { w--; emacs_file--; }
                        else { h--; emacs_file--; }
                    }

                    // Fall through to delete...
                case SDLK_DELETE:
                    // Delete the text
                    emacs_buffer_write = 0;

                    if (ui16 == 0)
                    {
                        sdf_insert_data(emacs_file, NULL, -1);
                    }

                    break;

                case SDLK_RETURN:
                    emacs_buffer_write = 0;
                    key = word_temp[0];
                    word_temp[0] = 0;
                    sdf_insert_data(emacs_file, word_temp, 1);
                    h = 0;
                    w = 0;

                    if (h < key - 4)  h++;
                    else if (cur_bits < 2047)  cur_bits++;
                    else if (h < key)  h++;

                    break;

                default:

                    if ((keyb.down[SDLK_RCTRL] || keyb.down[SDLK_LCTRL]) && (key == SDLK_x || key == SDLK_k || key == SDLK_c || key == SDLK_v || key == SDLK_h || key == SDLK_b))
                    {
                        if (key == SDLK_v)
                        {
                            emacs_paste(emacs_file);
                            emacs_buffer_write = 0;

                            while (emacs_buffer_write < emacs_return_count)
                            {
                                if (h < word_temp[0] - 4)  h++;
                                else if (cur_bits < 2047)  cur_bits++;
                                else if (h < word_temp[0])  h++;

                                emacs_buffer_write++;
                            }

                            emacs_buffer_write = 0;
                        }
                        else if (key == SDLK_c)
                        {
                            emacs_copy(emacs_file);
                        }
                        else if (key == SDLK_b || key == SDLK_h)
                        {
                            if (h < word_temp[0] - 4)  h++;
                            else if (cur_bits < 2047)  cur_bits++;
                            else if (h < word_temp[0])  h++;

                            emacs_buffer_write = 0;
                            word_temp[0] = ' ';

                            while (ui16 > 0)
                            {
                                sdf_insert_data(emacs_file, word_temp, 1);
                                emacs_file++;
                                ui16--;
                            }

                            if (key == SDLK_b)
                            {
                                sdf_insert_data(emacs_file, emacs_bad, EMACS_BAD_SIZE);
                            }
                            else
                            {
                                sdf_insert_data(emacs_file, emacs_horiz, EMACS_HORIZ_SIZE);
                            }
                        }
                        else
                        {
                            emacs_copy(emacs_file);
                            emacs_delete(emacs_file);
                        }
                    }
                    else
                    {
                        emacs_buffer_write = 0;
                        word_temp[0] = ' ';

                        while (ui16 > 0)
                        {
                            sdf_insert_data(emacs_file, word_temp, 1);
                            emacs_file++;
                            ui16--;
                        }

                        word_temp[0] = (Uint8) key;
                        sdf_insert_data(emacs_file, word_temp, 1);

                        if (h < loc_win.w - 4)  h++;
                        else if (w < 255)  w++;
                        else if (h < loc_win.w)  h++;
                    }

                    break;

            }
        }
    }
    else
    {
        // Emacs is not active...
        h = 63;
        h = 63;
    }

    pss->self.item++;


    // Throw back the new cursor data
    ih = (int)ih;
    iw = (int)iw;

    ih = ih & 63;
    ih |= ((ih & 63) << 6);
    ih |= ((iw & 255) << 12);
    //                ih|= ((cur_bits&2047)<<20);
    //                ih|= ((cur_bits&4095)<<20);
    ih = ((Uint32) ih)  |  (((Uint32) (cur_bits & 4095)) << 20);
#endif

    return ih;
}



bool_t ex_window_megaimage(PSoulfuScriptContext pss, Uint32 color, Uint32 alpha_blend, Uint8 * rgb_data)
{
    SDF_PHEADER pheader;

    // "pss->self.object.win" not used
    //assert(OBJECT_WIN == pss->self.object.type);

    pheader = sdf_archive_find_header_by_data(rgb_data);

    color_temp[0] = (color >> 16) & 0xFF;
    color_temp[1] = (color >> 8) & 0xFF;
    color_temp[2] = (color >> 0) & 0xFF;

    if (alpha_blend)
    {
        color_temp[3] = (Uint8) alpha_blend;
        display_color_alpha(color_temp);

        if (alpha_blend & ALPHA_LIGHT)
        {
            display_blend_light();
        }
    }
    else
    {
        display_color(color_temp);
    }

    if (NULL == rgb_data)
    {
        display_texture_off();

        // Draw the image
        display_start_fan();
        {
            display_vertex_xy(pss->point[0].v[XX], pss->point[0].v[YY]);
            display_vertex_xy(pss->point[1].v[XX], pss->point[1].v[YY]);
            display_vertex_xy(pss->point[2].v[XX], pss->point[2].v[YY]);
            display_vertex_xy(pss->point[3].v[XX], pss->point[3].v[YY]);
        }
        display_end();
        display_texture_on();
    }
    else
    {
        rgb_data += 2;
        display_pick_texture(DEREF( Uint32, rgb_data ))

        // Draw the image
        display_start_fan();
        {
            display_texpos_xy(pss->texpoint[0].v[XX], pss->texpoint[0].v[YY]);  display_vertex_xy(pss->point[0].v[XX], pss->point[0].v[YY]);
            display_texpos_xy(pss->texpoint[1].v[XX], pss->texpoint[1].v[YY]);  display_vertex_xy(pss->point[1].v[XX], pss->point[1].v[YY]);
            display_texpos_xy(pss->texpoint[2].v[XX], pss->texpoint[2].v[YY]);  display_vertex_xy(pss->point[2].v[XX], pss->point[2].v[YY]);
            display_texpos_xy(pss->texpoint[3].v[XX], pss->texpoint[3].v[YY]);  display_vertex_xy(pss->point[3].v[XX], pss->point[3].v[YY]);
        }
        display_end();
    }

    if (alpha_blend & ALPHA_LIGHT)
    {
        display_blend_trans();
    }

    return ktrue;
}

bool_t ex_window_3d_start(PSoulfuScriptContext pss, float x, float y, float w, float h, Uint8 detail_level)
{
    float ftmp;

    // "pss->self.object.win" not used
    //assert(OBJECT_WIN == pss->self.object.type);

    // set the position of the viewport...
    pss->window3d.x = x * pss->screen.scale + pss->screen.x;         // Top left x (0 - DEFAULT_W)
    pss->window3d.y = y * pss->screen.scale + pss->screen.y;         // Top left y (0 - DEFAULT_H)
    pss->window3d.w = w * pss->screen.scale;                         // W (0 - DEFAULT_W)
    pss->window3d.h = h * pss->screen.scale;                         // H (0 - DEFAULT_H)

    // Setup default lighting...
    global_render_light_color_rgb[0] = 255;
    global_render_light_color_rgb[1] = 255;
    global_render_light_color_rgb[2] = 255;
    global_render_light_offset_xy[XX] = 0.75f;  // 0.25f;
    global_render_light_offset_xy[YY] = 0.60f;  // 0.38f;


    // global_render_light_offset_xy[XX] = ((screen_x - mouse.x) * 0.00125f);
    // global_render_light_offset_xy[YY] = ((screen_y - mouse.y) * 0.0016666f);
    // sprintf(DEBUG_STRING, "%y, %y", global_render_light_offset_xy[XX], global_render_light_offset_xy[YY]);


    // DEFAULT_W x DEFAULT_H notation to true screen coordinates
    ftmp = screen_x / CAST(float, DEFAULT_W);
    x = pss->window3d.x * ftmp;
    w = pss->window3d.w * ftmp;

    ftmp = screen_y / CAST(float, DEFAULT_H);
    y = pss->window3d.y * ftmp;
    h = pss->window3d.h * ftmp;

    // GL Likes lower left corner...  Cartesian coordinates
    y = screen_y - y - h;

    // Set viewport
    display_viewport((int)x, (int)y, (int)w, (int)h);
    display_zbuffer_on();

    // Make depth work...
    display_depth_overlay(pss->window3d.order, pss->window3d.order + 1);

    return ktrue;
}

bool_t ex_window_3d_end(PSoulfuScriptContext pss)
{
    // "pss->self.object.win" not used
    //assert(OBJECT_WIN == pss->self.object.type);

    glLoadMatrixf(window_camera_matrix);
    display_viewport(0, 0, screen_x, screen_y);
    display_zbuffer_off();
    display_depth_scene();
    display_blend_trans();

    return ktrue;
};

bool_t ex_window_3d_position(PSoulfuScriptContext pss, float x, float y, float z, Uint8 type)
{
    bool_t retval = ktrue;

    // "pss->self.object.win" not used
    //assert(OBJECT_WIN == pss->self.object.type);

    switch (type)
    {
        case WIN_CAMERA:
            pss->window3d.camera_xyz[XX] = x;
            pss->window3d.camera_xyz[YY] = y;
            pss->window3d.camera_xyz[ZZ] = z;
            display_look_at(pss->window3d.camera_xyz, pss->window3d.target_xyz);
            break;

        case WIN_TARGET:
            pss->window3d.target_xyz[XX] = x;
            pss->window3d.target_xyz[YY] = y;
            pss->window3d.target_xyz[ZZ] = z;
            break;

        case WIN_LIGHT:
            // !!!BAD!!!
            // !!!BAD!!!
            // !!!BAD!!!
            // !!!BAD!!!
            // !!!BAD!!!
            // !!!BAD!!!
            // !!!BAD!!!
            break;

        case WIN_ORTHO:
            // Special for modeler...
            glMatrixMode(GL_PROJECTION);
            glLoadIdentity();
            glOrtho(-x, x, -y, y, ZNEAR, z);
            glMatrixMode(GL_MODELVIEW);
            break;

        case WIN_FRUSTUM:
            // Special for modeler...
            glMatrixMode(GL_PROJECTION);
            glLoadIdentity();
            glFrustum(-x, x, -y, y, ZNEAR, z);
            screen_frustum_x = x;
            screen_frustum_y = y;
            glMatrixMode(GL_MODELVIEW);
            break;

#ifdef DEVTOOL
        default:
            // Special for modeler...
            glScalef(x, y, z);
            break;
#else
        default:
            retval = kfalse;
#endif
    };

    return retval;
}


bool_t ex_window_3d_model(PSoulfuScriptContext pss, size_t rdy_offset, Sint32 frame, Uint32 alpha, Uint8 mode)
{
    bool_t retval = ktrue;
    Uint8 * tmp_data;
    int ipart;
    MODEL_DATA * ppart;

    ipart = (rdy_offset - MODEL_BASE_FILE) / 24;

    ppart = NULL;
    if( OBJECT_CHR == pss->self.object.type )
    {
        ppart = pss->self.object.chr->model.parts + ipart;
    }
    else if ( OBJECT_WIN == pss->self.object.type )
    {
        ppart = pss->self.object.win->model.parts + ipart;
    }

    if ( NULL == ppart || NULL == ppart->file ) return kfalse;

    switch (mode & 0xF0)
    {
        case WIN_3D_SHADOW:
            display_blend_trans();
            display_cull_off();
            render_rdy_shadow(pss, ppart->file, (Uint16) frame, 0.0f, 0.0f, 0.0f, mode);
            display_cull_on();
            break;

        case WIN_3D_MODEL:

            // Figure out the matrix to use...
            if (rdy_offset == MODEL_BASE_FILE)
            {
                // Clear the base matrix...
                matrix_clear(pss->matrix);
                render_generate_model_world_data(ppart->file, (Uint16) frame, pss->matrix, mainbuffer);  // Generate new bone frame in mainbuffer
                render_rdy(&(pss->screen), ppart->file, (Uint16) frame, mode, ppart->tex_lst, (Uint8) alpha, mainbuffer, 0, 0);
            }
            else if (rdy_offset == MODEL_RIDER_FILE)
            {
                // Set the base matrix to attach to the saddle bone...
                matrix_clear(pss->matrix);
                script_matrix_from_bone(pss, SADDLE_BONE);  // Uses data in mainbuffer from last base...
                render_generate_model_world_data(ppart->file, (Uint16) frame, pss->matrix, mainbuffer);  // Generate new bone frame in mainbuffer
                render_rdy(&(pss->screen), ppart->file, (Uint16) frame, mode, ppart->tex_lst, (Uint8) alpha, mainbuffer, 0, 0);
            }
            else if (rdy_offset >= MODEL_LEFT_FILE)
            {
                // Use a temporary matrix to attach to the desired weapon bone...

                int bone = 1 + ((rdy_offset - MODEL_LEFT_FILE) / 24);

                matrix_clear(pss->matrix);
                script_matrix_from_bone(pss, (Uint8)bone );  // Uses data in mainbuffer from last base...
                render_generate_model_world_data(ppart->file, (Uint16) frame, pss->matrix, subbuffer);  // Generate new bone frame in subbuffer
                rdy_offset = global_num_bone;
                tmp_data = global_bone_data;
                render_rdy(&(pss->screen), ppart->file, (Uint16) frame, mode, ppart->tex_lst, (Uint8) alpha, subbuffer, 0, 0);
                global_bone_data = tmp_data;
                global_num_bone = rdy_offset;
            }
            else
            {
                // Overlaps base bones...
                render_rdy(&(pss->screen), ppart->file, (Uint16) frame, mode, ppart->tex_lst, (Uint8) alpha, mainbuffer, 0, 0);
            }

            break;

#ifdef DEVTOOL
        case WIN_3D_AXIS:
            display_texture_off();
            render_axis();
            render_bounding_box();
            display_texture_on();
            break;

        case WIN_3D_BONE:
            render_rdy(&(pss->screen), ppart->file, (Uint16) frame, mode, ppart->tex_lst, (Uint8) alpha, NULL, 0, 0);
            break;

        case WIN_3D_VERTEX:
            render_rdy(&(pss->screen), ppart->file, (Uint16) frame, mode, ppart->tex_lst, (Uint8) alpha, NULL, 0, 0);
            break;

        case WIN_3D_BONE_UPDATE:
            render_rdy(&(pss->screen), ppart->file, (Uint16) frame, mode, ppart->tex_lst, (Uint8) alpha, NULL, 0, 0);
            break;

        case WIN_3D_TEXVERTEX:
            render_rdy(&(pss->screen), ppart->file, (Uint16) frame, mode, ppart->tex_lst, (Uint8) alpha, NULL, 0, 0);
            break;

        case WIN_3D_SKIN_FROM_CAMERA:
            render_rdy(&(pss->screen), ppart->file, (Uint16) frame, mode, ppart->tex_lst, (Uint8) alpha, NULL, 0, 0);
            break;

        case WIN_3D_MODEL_EDIT:
            matrix_clear(pss->matrix);
            render_generate_model_world_data(ppart->file, (Uint16) frame, pss->matrix, mainbuffer);  // Generate new bone frame in mainbuffer (so others attach correctly)
            render_rdy(&(pss->screen), ppart->file, (Uint16) frame, WIN_3D_MODEL, ppart->tex_lst, (Uint8) alpha, NULL, 0, 0);
            break;

#endif

        default:
            retval = kfalse;
            break;

    }

    return retval;
}

void ex_tool_kanjiedit(PSoulfuScriptContext pss, Sint32 mode, float x, float y, float scale, Sint32 font_num)
{

#ifdef DEVTOOL
    // "pss->self.object.win" not used
    //assert(OBJECT_WIN == pss->self.object.type);

    scale *= pss->screen.scale;
    x = (x * pss->screen.scale) + pss->screen.x;          // Top left x
    y = (y * pss->screen.scale) + pss->screen.y;          // Top left y
    tool_kanjiedit(pss, (Uint16) font_num, x, y, scale, (Uint16) mode);
#endif

}


bool_t ex_window_3d_room(PSoulfuScriptContext pss, float x, float y, float z, Uint8 * srf_file, Uint32 color, int rotation, Uint32 mode)
{
    // "pss->self.object.win" not used
    //assert(OBJECT_WIN == pss->self.object.type);

    color_temp[0] = (color >> 16) & 255;   // Red
    color_temp[1] = (color >> 8) & 255;   // Green
    color_temp[2] = (color >> 0) & 255;   // Blue
    color_temp[3] = 255;               // Alpha

    if (mode == ROOM_MODE_MINIMAP)
    {
        display_texture_off();
    }

    room_draw_srf(pss, x, y, z, srf_file, color_temp, (Uint16) rotation, (Uint8) mode);

    if (mode == ROOM_MODE_MINIMAP)
    {
        display_texture_on();
    }

    return ktrue;
}


bool_t ex_damage_target(PSoulfuScriptContext pss, int damage_type, int damage_amount, Uint32 wound_amount)
{
    Uint16 team, target;
    bool_t btmp;
    float ftmp1, ftmp2;
    CHR_DATA * ptarget, * pattack;

    team   = pss->self.object.bas->team;    // The team...
    target = pss->self.object.bas->target;  // The target

    ptarget = chr_data_get_ptr(target);
    if ( NULL == ptarget ) return kfalse;

    // Find out who the attacker is...
    attack_info.index = MAX_CHARACTER;
    attack_info.spin  = ptarget->spin + 32768;
    btmp = ktrue;

    if ( ptr_is_type( &(pss->self.object), OBJECT_PRT) )
    {
        // Thing doin' damage is a particle...
        attack_info.index = pss->self.object.prt->owner;  // particle's owner is doing attack...

        if (pss->self.object.prt->flags & PART_ATTACK_SPIN_ON)
        {
            // Set global attack spin from particle's location instead of owner's...
            btmp = kfalse;
        }
    }
    else if ( ptr_is_type( &(pss->self.object), OBJECT_CHR) )
    {
        // Thing doin' damage is a character...
        attack_info.index = ptr_get_idx(&(pss->self.object), OBJECT_CHR);  // character is doing attack...

        if (attack_info.index == target)
        {
            // Character is hurting itself...  We'll allow that as non-friendly fire...
            if (team == TEAM_GOOD)
            {
                team = TEAM_MONSTER;
            }
            else
            {
                team = TEAM_GOOD;
            }
        }
    }

    pattack = chr_data_get_ptr_raw(attack_info.index);
    if ( NULL != pattack )
    {
        if (btmp)
        {
            // Direction from attacker's position relative to target...
            ftmp1 = ptarget->x - pattack->x;
            ftmp2 = ptarget->y - pattack->y;
        }
        else
        {
            // Direction from particle's position relative to target...
            ftmp1 = ptarget->x - pss->self.object.bas->x;
            ftmp2 = ptarget->y - pss->self.object.bas->y;
        }

        attack_info.spin = (Uint16) (ATAN2(ftmp2, ftmp1) * RAD_TO_UINT16);
    }
    else
    {
        attack_info.index = UINT16_MAX;
    }

    damage_character(pss, (Uint16) target, (Uint8) damage_type, (Uint16) damage_amount, (Uint16) wound_amount, (Uint8)team);

    return ktrue;
}

Uint32 ex_experience_function(PSoulfuScriptContext pss, Uint16 target, Uint32 xp_type, int xp_amount, bool_t give_to_enemies)
{

    if (give_to_enemies)
    {

        // Give experience to all of target's enemies (characters on different team than target)...
        CHR_DATA * src_target = chr_data_get_ptr(target);
        if ( NULL != src_target )
        {
            repeat(target, MAX_CHARACTER)
            {
                CHR_DATA * dst_target = chr_data_get_ptr(target);
                if (NULL == dst_target) continue;

                if (dst_target->team != src_target->team)
                {
                    // Character is active and on different team than target...
                    experience_function_character((Uint16) target, (Uint8) xp_type, (Sint16) xp_amount, kfalse);
                }
            }
        }

        xp_type = 0;
    }
    else
    {
        // Give experience to just target...
        xp_type = experience_function_character((Uint16) target, (Uint8) xp_type, (Sint16) xp_amount, ktrue);
    }

    return xp_type;
}

//-----------------------------------------------------------------------------------------------
_INLINE PSoulfuScriptContext _sf_script_clone(PSoulfuScriptContext pold, PSoulfuScriptContext pnew)
{
    if (NULL == pold || NULL == pnew) return NULL;

    // copy everything
    memcpy(pnew, pold, sizeof(SoulfuScriptContext));

    return pnew;
};


//-----------------------------------------------------------------------------------------------
_INLINE PSoulfuScriptContext _sf_init_callfunction(PSoulfuScriptContext pold, PSoulfuScriptContext pnew)
{
    // spawn a script in the same global context as pold
    if (NULL == _sf_script_clone(pold, pnew)) return NULL;

    // grab the function parameters from the input stream
    if (NULL == _pxss_read_callfunction_params(&(pnew->base.base))) return NULL;

    return pnew;
}

_INLINE PSoulfuScriptContext _sf_finish_callfunction(PSoulfuScriptContext pold, PSoulfuScriptContext pnew)
{
    if (NULL == pold || NULL == pnew) return NULL;

    pold->self.item = pnew->self.item;
    memcpy( &(pold->enc_cursor), &(pnew->enc_cursor),  sizeof(ENCHANT_CURSOR));
    memcpy( &(pold->screen),     &(pnew->screen),      sizeof(SOULFU_WINDOW));
    memcpy( &(pold->point),      &(pnew->point),      8*sizeof(float));
    memcpy( &(pold->texpoint),   &(pnew->texpoint),   8*sizeof(float));
    memcpy( &(pold->spawn_info), &(pnew->spawn_info), sizeof(SPAWN_INFO));

    return pnew;
};



bool_t run_opcode_external(PSoulfuScriptContext pss, OPCODE_EXTENDED opcode)
{
    // Actually handle "custom" SoulFu opcodes.
    // Use SINT, UINT, and FLOT for "automatic" 64-bit compatibility

    bool_t handled = ktrue;

    SINT i;
    SINT j;
    SINT k;
    SINT m;
    FLOT f;
    FLOT e;

    SINT tmp_i;
    SINT tmp_j;
    FLOT tmp_f;
    FLOT tmp_e;

    Uint8 ui8;

    Uint8* ptmp_a;
    Uint8* ptmp_b;

    // for convenience
    //PSoulfuGlobalScriptContext psgs = pss->psgs;
    //PGlobalScriptContext       pgs  = pss->base.pgs;
    //PBaseGlobalScriptContext   pbgs = pss->base.base.pbgs;
    //PScriptContext             ps   = &(pss->base);
    PBaseScriptContext         pbs  = &(pss->base.base);


    switch (opcode)
    {
        case OPCODE_EX_CALLFUNCTION:
            {
                SoulfuScriptContext loc_sc;

                // Call function with information about the arguments it was given...
                if (NULL == _sf_init_callfunction(pss, &loc_sc))
                {
                    // Not the best way to handle an error...
                    push_int_stack(pbs, 0);
                }
                else
                {
                    sf_run_script(&loc_sc, ktrue);

                    switch (loc_sc.base.base.return_var.type)
                    {
                        case PXSS_VAR_INT:
                            push_int_stack(pbs, loc_sc.base.base.return_var.i);
                            break;

                        case PXSS_VAR_FLT:
                            push_flt_stack(pbs, loc_sc.base.base.return_var.f);
                            break;

                        case PXSS_VAR_PTR:
                            push_int_stack(pbs, loc_sc.base.base.return_var.i);
                            break;

                        default:
                            assert(kfalse);
                            break;
                    }

                    _sf_finish_callfunction(pss, &loc_sc);
                }

                // Now jump to our next opcode
                pbs->tokens.read = loc_sc.base.base.return_address;
            }
            break;

        case OPCODE_EX_STRINGRANDOMNAME:
            // Prints a random name from a datafile into a given string location
            ptmp_b = pop_int_stack_cast(pbs, Uint8*);        // File data
            ptmp_a = pop_int_stack_cast(pbs, Uint8*);       // Place to write
            ptmp_b = random_name(ptmp_b);

            // Copy the name into the string...  Should be safe to copy all 16 bytes...
            memcpy(ptmp_a, ptmp_b, 16);
            push_int_stack(pbs, ktrue);
            break;

        case OPCODE_EX_STRINGSANITIZE:
            // Removes naughty language from a string

            // !!!BAD!!!
            // !!!BAD!!!  Make this work (need for player names & chat text)...  (Chat text done automagically?)
            // !!!BAD!!!

            ptmp_a = pop_int_stack_cast(pbs, Uint8*);
            message_sanitize(ptmp_a);
            push_int_stack(pbs, ktrue);
            break;

        case OPCODE_EX_LOCALMESSAGE:
            // Spits a string into the message buffer...
            ptmp_a = pop_int_stack_cast(pbs, Uint8*);  // Message string
            ptmp_b = pop_int_stack_cast(pbs, Uint8*);  // Speaker name string

            message_add(ptmp_a, ptmp_b, kfalse);  // No sanitize...

            push_int_stack(pbs, ktrue);
            break;

        case OPCODE_EX_LOGMESSAGE:
            // Spits a string into the log file
            ptmp_a = pop_int_stack_cast(pbs, Uint8*);  // String

            log_message(0, "%s", ptmp_a);

            push_int_stack(pbs, ktrue);
            break;

        case OPCODE_EX_DEBUGMESSAGE:
            // Spits out a debug message...
            ptmp_a = pop_int_stack_cast(pbs, Uint8*);  // String

            message_add(ptmp_a, "DEBUG", kfalse);

            push_int_stack(pbs, ktrue);
            break;



        case OPCODE_EX_NETWORKMESSAGE:
            // Spits a string into the message buffer...
            i = pop_int_stack(pbs);  // Speaker's class
            ptmp_a = pop_int_stack_cast(pbs, Uint8*);  // Message string
            ptmp_b = pop_int_stack_cast(pbs, Uint8*);  // Speaker name string

            cl_transmit_chat((Uint8) i, ptmp_b, ptmp_a);

            push_int_stack(pbs, ktrue);
            break;


        case OPCODE_EX_STRINGLANGUAGE:
            // Gives a string from the current .LAN file...  Multilingual support...
            j = pop_int_stack(pbs);                                       // Index requested

            push_int_stack(pbs, (SINT)_ex_get_language_string(pss, j) );
            break;

        case OPCODE_EX_SPAWN:
            // Creates a new object and returns a pointer to its data (for property access)

            ptmp_a = pop_int_stack_cast(pbs, Uint8*);          // Script start
            tmp_e  = pop_flt_stack(pbs);                       // z
            f      = pop_flt_stack(pbs);                       // y
            e      = pop_flt_stack(pbs);                       // x
            ui8    = pop_int_stack_cast(pbs, Uint8);           // type

            push_int_stack(pbs, (SINT)ex_spawn(pss, ui8, e, f, tmp_e, ptmp_a));
            break;


        case OPCODE_EX_GOPOOF:
            // Ends the script like a return and destroys the current object...
            i = pop_int_stack(pbs);                                       // Type
            ex_gopoof(pss, i);
            break;


        case OPCODE_EX_DISMOUNT:
            // Knock THIS rider off its mount...

            ex_dismount(pss);
            break;

        case OPCODE_EX_ROLLDICE:
            // Return the result of a random dice roll...
            j = pop_int_stack(pbs);                                   // Sides on dice
            i = pop_int_stack(pbs);                                   // Number of dice

            push_int_stack(pbs, random_dice((Uint8) i, (Uint16) j) );
            break;

        case OPCODE_EX_PLAYSOUND:
            // Plays a sound
            j = pop_int_stack(pbs);                      // Volume
            i = pop_int_stack(pbs);                      // Pitch skip
            ptmp_a = pop_int_stack_cast(pbs, Uint8*);    // Raw file start

            push_int_stack(pbs, ex_playsound(pss, ptmp_a, i, j) );
            break;

        case OPCODE_EX_PLAYMEGASOUND:
            // Plays a sound in a fully specified way
            ptmp_b = pop_int_stack_cast(pbs, Uint8*);       // Loop data
            ui8 = pop_int_stack_cast(pbs, Uint8);           // Pan
            j = pop_int_stack(pbs);                         // Volume
            i = pop_int_stack(pbs);                         // Pitch skip
            ptmp_a = pop_int_stack_cast(pbs, Uint8*);       // Raw file start

            push_int_stack(pbs, ex_play_megasound(pss, ptmp_a, i, j, ui8, ptmp_b) );
            break;

        case OPCODE_EX_DISTANCESOUND:
            // Changes the volume of an infinite looped sound, based on distance to camera
            i = pop_int_stack(pbs);                                       // Channel
            push_int_stack(pbs, ex_distance_sound(pss, i));
            break;

        case OPCODE_EX_PLAYMUSIC:
            // Plays a music file
            ui8 = pop_int_stack_cast(pbs, Uint8);              // Mode
            i = pop_int_stack(pbs);                                       // Start time
            ptmp_a = pop_int_stack_cast(pbs, Uint8*);       // Mus file start

            play_music(ptmp_a, (Uint16) i, ui8);

            push_int_stack(pbs, ktrue);
            break;

        case OPCODE_EX_UPDATEFILES:
            // Begins the file update process...
            ui8 = pop_int_stack_cast(pbs, Uint8);              // Mode

            ex_update_files(pss, ui8);
            break;

        case OPCODE_EX_ACQUIRETARGET:
            // Should only be called by particles or characters...
            tmp_e = pop_flt_stack(pbs);                     // Radius...
            ptmp_a = pop_int_stack_cast(pbs, Uint8*);       // Script File pointer...
            j = pop_int_stack(pbs);                         // Flags...
            // Return the best character we found...
            push_int_stack(pbs, ex_acquire_target(pss, j, ptmp_a, tmp_e));
            break;

        case OPCODE_EX_FINDPATH:
            // Modify gotoxy to be better...
            // Make sure we're dealin' with a character...

            m = kfalse; // return code...

            if ( ptr_is_type(&(pss->self.object), OBJECT_CHR) )
            {
                m = room_findpath(roombuffer, &(pss->self.object.chr->x), &(pss->self.object.chr->gotox), &(pss->self.object.chr->gotox), pss->self.object.chr);
            }

            push_int_stack(pbs, m);
            break;

        case OPCODE_EX_BUTTONPRESS:
            k = pop_int_stack(pbs);                                       // axis (or time)
            j = pop_int_stack(pbs);                                       // button
            i = pop_int_stack(pbs);                                       // code...

            // Make sure we're dealin' with a character...
            tmp_i = ptr_get_idx(&(pss->self.object), OBJECT_CHR);

            if (tmp_i != UINT16_MAX)
            {
                character_button_function((Uint16) tmp_i, (Uint8) i, (Uint8) j, (Uint8) k);
            }

            break;

        case OPCODE_EX_AUTOAIM:
            k = pop_int_stack(pbs);                                       // function return type (X, Y, Z, or CRUNCH...  Must do CRUNCH first)
            j = pop_int_stack(pbs);                                       // cone angle (0-UINT16_MAX...  16384 is 90'...)
            i = pop_int_stack(pbs);                                       // aimer's dexterity (dexterity of 0 is completely random angle, dexterity 50 is aimed precisely at a target enemy, dexterity 25 is a 50-50 mix)
            tmp_j = pop_int_stack(pbs);                           // aimer's team
            tmp_i = pop_int_stack(pbs);                           // aimer's spin
            f = pop_flt_stack(pbs);                                     // speed z
            e = pop_flt_stack(pbs);                                     // speed xy

            ex_autoaim(pss, e, f, tmp_i, tmp_j, i, j, k);

            break;

        case OPCODE_EX_ROOMHEIGHTXY:
            f = pop_flt_stack(pbs);                                     // y position
            e = pop_flt_stack(pbs);                                     // x position

            push_flt_stack(pbs, room_heightmap_height(roombuffer, e, f));
            break;

        case OPCODE_EX_WINDOWBORDER:
            {
                // Draws a window border on the screen AND
                // sets the size of the active area of the window

                float x, y;
                int   w, h;

                assert( ptr_is_type( &(pss->self.object), OBJECT_WIN) );

                // Draws a window border on the screen...
                k = pop_int_stack(pbs);                       // border flags
                h = pop_int_stack(pbs);                       // h
                w = pop_int_stack(pbs);                       // w
                y = pop_flt_stack(pbs);                       // y
                x = pop_flt_stack(pbs);                       // x
                ui8 = peek_int_stack(pbs);                    // draggable...

                ex_window_border(pss, ui8, x, y, (float)w, (float)h, k);
            }
            break;

        case OPCODE_EX_WINDOWSTRING:
            // Draws a string within a window border...

            assert( ptr_is_type( &(pss->self.object), OBJECT_WIN) );

            ptmp_b = pop_int_stack_cast(pbs, Uint8*);        // string
            f = pop_flt_stack(pbs);                          // y
            e = pop_flt_stack(pbs);                          // x
            i = pop_int_stack(pbs);                          // color

            push_int_stack(pbs, ex_window_string(pss, i, e, f, ptmp_b));
            break;

        case OPCODE_EX_WINDOWMINILIST:

            assert( ptr_is_type( &(pss->self.object), OBJECT_WIN) );

            ptmp_b = pop_int_stack_cast(pbs, Uint8*);        // option string
            k = pop_int_stack(pbs);                          // expansion state
            j = pop_int_stack(pbs);                          // h
            i = pop_int_stack(pbs);                          // w
            f = pop_flt_stack(pbs);                          // y
            e = pop_flt_stack(pbs);                          // x

            push_int_stack(pbs, ex_window_minilist(pss, e, f, (float)i, (float)j, k, ptmp_b));
            break;

        case OPCODE_EX_WINDOWSLIDER:
            // Adds a slider style input within a window border...

            assert( ptr_is_type( &(pss->self.object), OBJECT_WIN) );

            k = pop_int_stack(pbs);                                       // dinglethorpe position
            j = pop_int_stack(pbs);                                       // h
            i = pop_int_stack(pbs);                                       // w
            f = pop_flt_stack(pbs);                                     // y
            e = pop_flt_stack(pbs);                                     // x

            push_int_stack(pbs, ex_window_slider(pss, e, f, (float)i, (float)j, k));
            break;

        case OPCODE_EX_WINDOWIMAGE:

            assert( ptr_is_type( &(pss->self.object), OBJECT_WIN) );

            k      = pop_int_stack(pbs);                    // current state
            ptmp_b = pop_int_stack_cast(pbs, Uint8*);       // alt text string
            ptmp_a = pop_int_stack_cast(pbs, Uint8*);       // image file
            tmp_f  = pop_flt_stack(pbs);                    // h
            tmp_e  = pop_flt_stack(pbs);                    // w
            f      = pop_flt_stack(pbs);                    // y
            e      = pop_flt_stack(pbs);                    // x

            push_int_stack(pbs, ex_window_image(pss, e, f, tmp_e, tmp_f, ptmp_a, ptmp_b, k));
            break;

        case OPCODE_EX_WINDOWTRACKER:
            // The tracker tool for making .MUS files...

            assert( ptr_is_type( &(pss->self.object), OBJECT_WIN) );

            ui8 = pop_int_stack_cast(pbs, Uint8);                     // pan
            m = pop_int_stack(pbs);                                   // volume
            k = pop_int_stack(pbs);                                   // instrument
            j = pop_int_stack(pbs);                                   // seconds to display
            i = pop_int_stack(pbs);                                   // time offset

            ptmp_a = pop_int_stack_cast(pbs, Uint8*);                 // MUS file data
            tmp_f = pop_flt_stack(pbs);                         // h
            tmp_e = pop_flt_stack(pbs);                         // w
            f = pop_flt_stack(pbs);                                   // y
            e = pop_flt_stack(pbs);                                   // x

            push_int_stack(pbs, ex_window_tracker(pss, e, f, tmp_e, tmp_f, ptmp_a, i, j, k, m, ui8));
            break;

        case OPCODE_EX_WINDOWBOOK:
            // Draws the top 2/3 pages of a book and the associated text...

            assert( ptr_is_type( &(pss->self.object), OBJECT_WIN) );

            ptmp_a = pop_int_stack_cast(pbs, Uint8*);       // filedata
            m = pop_int_stack(pbs);                         // pageturn
            k = pop_int_stack(pbs);                         // number of pages
            j = pop_int_stack(pbs);                         // h (15)
            i = pop_int_stack(pbs);                         // w (25)
            f = pop_flt_stack(pbs);                         // y
            e = pop_flt_stack(pbs);                         // x

            push_int_stack(pbs, ex_window_book(pss, e, f, (float)i, (float)j, k, m, ptmp_a));
            break;

        case OPCODE_EX_WINDOWINPUT:

            assert( ptr_is_type( &(pss->self.object), OBJECT_WIN) );

            k = pop_int_stack(pbs);                         // next item offset
            ptmp_a = pop_int_stack_cast(pbs, Uint8*);       // string
            i = pop_int_stack(pbs);                         // w
            f = pop_flt_stack(pbs);                         // y
            e = pop_flt_stack(pbs);                         // x

            ex_window_input(pss, e, f, (float)i, ptmp_a, k);
            break;

        case OPCODE_EX_WINDOWEMACS:

            assert( ptr_is_type( &(pss->self.object), OBJECT_WIN) );

            ptmp_a = pop_int_stack_cast(pbs, Uint8*);       // file
            m = pop_int_stack(pbs);                                       // cursorx,cursory,scrolldown
            k = pop_int_stack(pbs);                                       // h
            i = pop_int_stack(pbs);                                       // w
            f = pop_flt_stack(pbs);                                     // y
            e = pop_flt_stack(pbs);                                     // x


            push_int_stack(pbs, (SINT)ex_window_emacs(pss, e, f, (float)i, (float)k, m, ptmp_a));
            break;

        case OPCODE_EX_WINDOWMEGAIMAGE:
            {
                // Draw a deformable, blendable, colorable image...
                assert( ptr_is_type( &(pss->self.object), OBJECT_WIN) );

                ptmp_a = pop_int_stack_cast(pbs, Uint8*);       // Image file
                k = pop_int_stack(pbs);                         // Alpha + Blend mode
                i = pop_int_stack(pbs);                         // Color

                // Texture coordinates
                f = pop_flt_stack(pbs);                                // ty
                e = pop_flt_stack(pbs);                                // tx
                pss->texpoint[2].v[YY] = f;                              // t2y
                pss->texpoint[2].v[XX] = e;                              // t2x
                pss->texpoint[3].v[YY] = f;                              // t3y
                pss->texpoint[1].v[XX] = e;                              // t1x

                f = pop_flt_stack(pbs);                                // ty
                e = pop_flt_stack(pbs);                                // tx
                pss->texpoint[0].v[YY] = f;                              // t0y
                pss->texpoint[0].v[XX] = e;                              // t0x
                pss->texpoint[1].v[YY] = f;                              // t1y
                pss->texpoint[3].v[XX] = e;                              // t3x

                // Shape coordinates
                f = pop_flt_stack(pbs);                                     // p3y
                e = pop_flt_stack(pbs);                                     // p3x
                pss->point[3].v[XX] = (e * pss->screen.scale) + pss->screen.x;
                pss->point[3].v[YY] = (f * pss->screen.scale) + pss->screen.y;

                f = pop_flt_stack(pbs);                                     // p3y
                e = pop_flt_stack(pbs);                                     // p3x
                pss->point[2].v[XX] = (e * pss->screen.scale) + pss->screen.x;
                pss->point[2].v[YY] = (f * pss->screen.scale) + pss->screen.y;

                f = pop_flt_stack(pbs);                                     // p3y
                e = pop_flt_stack(pbs);                                     // p3x
                pss->point[1].v[XX] = (e * pss->screen.scale) + pss->screen.x;
                pss->point[1].v[YY] = (f * pss->screen.scale) + pss->screen.y;

                f = pop_flt_stack(pbs);                                     // p3y
                e = pop_flt_stack(pbs);                                     // p3x
                pss->point[0].v[XX] = (e * pss->screen.scale) + pss->screen.x;
                pss->point[0].v[YY] = (f * pss->screen.scale) + pss->screen.y;

                push_int_stack(pbs, ex_window_megaimage(pss, i, k, ptmp_a) );
            }
            break;

        case OPCODE_EX_WINDOW3DSTART:
            // sets up a 3d window

            assert( ptr_is_type( &(pss->self.object), OBJECT_WIN) );

            ui8 = pop_int_stack_cast(pbs, Uint8);   // Detail level override  !!!BAD!!!  Unused...
            tmp_f = pop_flt_stack(pbs);             // H
            tmp_e = pop_flt_stack(pbs);             // W
            f = pop_flt_stack(pbs);                 // Y
            e = pop_flt_stack(pbs);                 // X

            push_int_stack(pbs, ex_window_3d_start(pss, e, f, tmp_e, tmp_f, ui8));
            break;

        case OPCODE_EX_WINDOW3DEND:
            assert( ptr_is_type( &(pss->self.object), OBJECT_WIN) );

            push_int_stack(pbs, ex_window_3d_end(pss));
            break;

        case OPCODE_EX_WINDOW3DPOSITION:
            assert( ptr_is_type( &(pss->self.object), OBJECT_WIN) );

            ui8 = pop_int_stack_cast(pbs, Uint8);      // Position type
            tmp_e = pop_flt_stack(pbs);          // Z
            f = pop_flt_stack(pbs);                    // Y
            e = pop_flt_stack(pbs);                    // X

            push_int_stack(pbs, ex_window_3d_position(pss, e, f, tmp_e, ui8));
            break;

        case OPCODE_EX_WINDOW3DMODEL:
            assert( ptr_is_type( &(pss->self.object), OBJECT_WIN) );

            k = pop_int_stack(pbs);                                                           // Main alpha
            j = pop_int_stack(pbs);                                                           // Current frame
            i = pop_int_stack(pbs);                                                           // Character model data block offset...  First entry is RDY filestart...
            ui8 = pop_int_stack_cast(pbs, Uint8);                                             // Mode

            push_int_stack(pbs, ex_window_3d_model(pss, i, j, k, ui8));
            break;

        case OPCODE_EX_MODELASSIGN:
            // Set the color and textures for a given character or window...
            j = pop_int_stack(pbs);                               // Color or Texture filestart
            i = pop_int_stack(pbs);                               // Location to stick the color or texture...

            ex_model_assign( CAST(Uint8 *, i) , j );
            break;

        case OPCODE_EX_PARTICLEBOUNCE:
            // Reflects a particle off collision_normal_xyz, changing its velocities...
            // !!!BAD!!!
            // !!!BAD!!!  Should probably axe this...
            // !!!BAD!!!
            //                incoming_xyz = ((float*) (pss->self.object+24));
            //                e = 2.0f * dot_product(incoming_xyz, collision_normal_xyz);
            //                incoming_xyz[XX] -= collision_normal_xyz[XX]*e;
            //                incoming_xyz[YY] -= collision_normal_xyz[YY]*e;
            //                incoming_xyz[ZZ] -= collision_normal_xyz[ZZ]*e;
            //                push_int_stack(collision_normal_xyz[ZZ] > INV_SQRT_2)  // Return landability...
            push_int_stack(pbs, ktrue);
            break;

        case OPCODE_EX_WINDOWEDITKANJI:
            // Scripted vector font tool...

            assert( ptr_is_type( &(pss->self.object), OBJECT_WIN) );

            j = pop_int_stack(pbs);                                       // Font to edit
            tmp_f = pop_flt_stack(pbs);                         // Scale
            f = pop_flt_stack(pbs);                                     // y
            e = pop_flt_stack(pbs);                                     // x
            i = pop_int_stack(pbs);                                       // Edit mode

            ex_tool_kanjiedit(pss, i, e, f, tmp_f, j);

            break;

        case OPCODE_EX_WINDOW3DROOM:

            assert( ptr_is_type( &(pss->self.object), OBJECT_WIN) );

            k = pop_int_stack(pbs);               // The ROOM_MODE_??? thing
            j = pop_int_stack(pbs);               // The rotation (0-UINT16_MAX)
            i = pop_int_stack(pbs);               // The RGB color
            ptmp_a = pop_int_stack_cast(pbs, Uint8*);  // The start of the SRF file...
            tmp_e = pop_flt_stack(pbs);         // z center position
            f = pop_flt_stack(pbs);             // y center position
            e = pop_flt_stack(pbs);             // x center position

            ex_window_3d_room(pss, e, f, tmp_e, ptmp_a, i, j, k);
            break;

        case OPCODE_EX_INDEXISLOCALPLAYER:
            // Return ktrue if argument is one of the player characters
            j = pop_int_stack(pbs);                                       // Character index

            i = kfalse;

            if ( VALID_CHR_RANGE(j) )
            {
                repeat(k, MAX_LOCAL_PLAYER)
                {
                    if (player_device[k].type)
                    {
                        if (local_player_character[k] == (Uint16) j)
                        {
                            i = ktrue;
                            k = MAX_LOCAL_PLAYER;
                        }
                    }
                }
            }

            push_int_stack(pbs, i);
            break;

        case OPCODE_EX_FINDSELF:
            // Returns a pointer to the current object...  For property addressing...
            push_int_stack(pbs, (SINT) pss->self.object.unk);
            break;

        case OPCODE_EX_FINDBINDING:
            i = 0;

            if( OBJECT_WIN == pss->self.object.type )
            {
                i = pss->self.object.win->binding;
                i = CAST(SINT, chr_data_get_ptr(i));
            }

            push_int_stack(pbs, i);
            break;

        case OPCODE_EX_FINDTARGET:
            i = pss->self.object.bas->target;
            i = CAST(SINT, chr_data_get_ptr(i));

            push_int_stack(pbs, i);
            break;

        case OPCODE_EX_FINDOWNER:
            i = pss->self.object.bas->owner;
            i = CAST(SINT, chr_data_get_ptr(i));

            push_int_stack(pbs, i);
            break;

        case OPCODE_EX_FINDINDEX:
            // Returns 0-MAX_CHARACTER, given a pointer to character data...
            // Returns 0-MAX_PARTICLE, given a pointer to particle data...
            // Returns UINT16_MAX if bad...
            ptmp_a = pop_int_stack_cast(pbs, Uint8*);

            i = _get_chr_index((CHR_DATA *)ptmp_a);

            if (UINT16_MAX == i)
            {
                i = _get_prt_index((PRT_DATA*)ptmp_a);
            }

            push_int_stack(pbs, i);
            break;

        case OPCODE_EX_FINDBYINDEX:
            // Returns a pointer to a character, given the character's index number
            i = pop_int_stack(pbs);

            i = CAST(SINT, chr_data_get_ptr(i));

            push_int_stack(pbs, i);
            break;

        case OPCODE_EX_FINDWINDOW:
            // Returns a pointer to a window's data, given either (or both) the window's binding character index,
            // or the window's script data start...  Returns first match, or NULL if none was found...
            ptmp_a = pop_int_stack_cast(pbs, Uint8*);   // Window script data start...
            k      = pop_int_stack(pbs);                // Window's binding character index...


            log_info(1, "FindWindow(binding == %d, script == %d)", k, (SINT) ptmp_a);
            i = 0;
            repeat(tmp_i, main_used_window_count)
            {
                WIN_DATA * pwin;

                j = main_used_window[tmp_i];
                pwin = main_win_data + j;

                ptmp_b = pwin->script_ctxt.base.base.file_start;

                log_info(1, "Checking window %d (binding == %d, script == %d)", j, pwin->binding, (SINT) ptmp_b);

                if (ptmp_a == ptmp_b || ptmp_a == NULL)
                {
                    log_info(1, "Passed script");

                    if (k == pwin->binding || k == UINT16_MAX)
                    {
                        log_info(1, "Passed binding");

                        i = CAST(SINT, pwin);
                        tmp_i = main_used_window_count;
                    }
                }
            }
            push_int_stack(pbs, i);
            break;

        case OPCODE_EX_FINDPARTICLE:
            // Returns a pointer to a particle, given the particle's index number
            i = pop_int_stack(pbs);

            if ( VALID_PRT(i) )
            {
                i = CAST(SINT, main_prt_data + i);
            }
            else
            {
                i = 0;
            }

            push_int_stack(pbs, i);
            break;

        case OPCODE_EX_ATTACHTOTARGET:
            // Attaches a particle to its target (for stickin' arrows in characters...)
            // If m is ktrue, the particle attaches to the best non-standard bone of the
            // character (for stickin' attack particles to a weapon)
            m = pop_int_stack(pbs);       // The bone type to attach to (0 for standard bones, 1 or 2 for left/right weapons, 255 for special attach-to-vertex thing...)


            i = kfalse;

            if ( ptr_is_type(&(pss->self.object), OBJECT_PRT) )
            {
                j = pss->self.object.prt->target;

                if ( VALID_CHR_RANGE(j) )
                {
                    k = ptr_get_idx( &(pss->self.object), OBJECT_PRT); // Should give particle...
                    i = particle_attach_to_character((Uint16) k, (Uint16) j, (Uint8) m);
                }
            }

            push_int_stack(pbs, i);
            break;

        case OPCODE_EX_GETDIRECTION:
            // Returns the direction from a character to an XY spot...  Returns 0 - 3,
            // For Front, Back, Left, Right...
            f = pop_flt_stack(pbs);         // Y
            e = pop_flt_stack(pbs);         // X


            // Make sure we're dealin' with a character...
            i = 0;

            if ( ptr_is_type(&(pss->self.object), OBJECT_CHR) )
            {
                // XY Vector to spot...
                e -= pss->self.object.chr->x;
                f -= pss->self.object.chr->y;


                // Dot vector with front and side normals...
                tmp_e = (e * pss->self.object.chr->frontx) + (f * pss->self.object.chr->fronty);  // Front dot...
                tmp_f = (e * pss->self.object.chr->sidex ) + (f * pss->self.object.chr->sidey );  // Side dot...
                e = tmp_e;
                f = tmp_f;
                ABS(e);
                ABS(f);

                if (e > f)
                {
                    // Forward or backwards...
                    i = (tmp_e > 0.0f) ? DIRECTION_FRONT : DIRECTION_BACK;
                }
                else
                {
                    // Left or right...
                    i = (tmp_f > 0.0f) ? DIRECTION_LEFT : DIRECTION_RIGHT;
                }
            }

            push_int_stack(pbs, i);
            break;

        case OPCODE_EX_DAMAGETARGET:
            // Does some damage to the target character...
            k = pop_int_stack(pbs);           // wound_amount
            j = pop_int_stack(pbs);           // damage_amount
            i = pop_int_stack(pbs);           // damage_type

            ex_damage_target(pss, i, j, k);

            break;

        case OPCODE_EX_EXPERIENCEFUNCTION:
            // Gives some experience to a character...
            k = pop_int_stack(pbs);           // affect entire team
            j = pop_int_stack(pbs);           // experience_amount
            i = pop_int_stack(pbs);           // experience_type
            m = pop_int_stack(pbs);           // target of function (character index number)

            push_int_stack(pbs, ex_experience_function(pss, m, i, j, k) );
            break;

        default:
            handled = kfalse;
    }

    return handled;
}



//-----------------------------------------------------------------------------------------------
void script_matrix_from_bone(PSoulfuScriptContext pss, Uint8 bone_name)
{
    // <ZZ> This is a yicky function that gets a grip matrix from a given bone...  The bone frame
    //      data is assumed to be in mainbuffer...
    Uint16 i, bone;
    Uint8* data;
    Uint16 joint[2];
    Uint8* joint_data_start;

    //PSoulfuGlobalScriptContext psgs = pss->psgs;


    data = global_bone_data;
    bone = global_num_bone;
    repeat(i, global_num_bone)
    {
        if (*data == bone_name)
        {
            bone = i;
            joint[0] = DEREF( Uint16, data + 1 );
            joint[1] = DEREF( Uint16, data + 3 );
            i = global_num_bone;
        }

        data += 9;
    }

    if (bone < global_num_bone)
    {
        data = mainbuffer + (bone << 4) + (bone << 3);


        // Front normal...
        pss->matrix[3] = DEREF( float, data );  data += 4;
        pss->matrix[4] = DEREF( float, data );  data += 4;
        pss->matrix[5] = DEREF( float, data );  data += 4;


        // Side normal...
        pss->matrix[0] =  DEREF( float, data );  data += 4;
        pss->matrix[1] =  DEREF( float, data );  data += 4;
        pss->matrix[2] =  DEREF( float, data );  data += 4;


        // Up normal...  Calculated from joint positions...
        joint_data_start = mainbuffer + (global_num_bone << 4) + (global_num_bone << 3);
        data = joint_data_start + (joint[1] << 3) + (joint[1] << 2);
        pss->matrix[6] =  DEREF( float, data );  data += 4;
        pss->matrix[7] =  DEREF( float, data );  data += 4;
        pss->matrix[8] =  DEREF( float, data );  data += 4;
        data = joint_data_start + (joint[0] << 3) + (joint[0] << 2);
        pss->matrix[9] =  DEREF( float, data );  data += 4;
        pss->matrix[10] =  DEREF( float, data );  data += 4;
        pss->matrix[11] =  DEREF( float, data );  data += 4;
        pss->matrix[6] -= pss->matrix[9];
        pss->matrix[7] -= pss->matrix[10];
        pss->matrix[8] -= pss->matrix[11];
    }
}



//-----------------------------------------------------------------------------------------------
Uint8* pxss_find_data(const char * filename)
{
    Uint8 * pdata   = NULL;

    SDF_PHEADER pheader = sdf_archive_find_header(filename);

    if (NULL != pheader)
    {
        pdata = sdf_file_get_data(pheader);
    };

    return pdata;
};

//-----------------------------------------------------------------------------------------------
Uint8* pxss_find_run_data(const char * filename)
{
    SDF_PHEADER pheader = NULL;
    Uint8 * pdata       = NULL;
    char *  dotpointer  = NULL;
    char    loc_fname[256];

    // strip any existing extension
    strncpy(loc_fname, filename, sizeof(loc_fname) - 1);
    loc_fname[255] = '\0';
    dotpointer = strrchr(loc_fname, '.');

    if (NULL != dotpointer) *dotpointer = '\0';

    pheader = sdf_archive_find_filetype(loc_fname, SDF_FILE_IS_RUN);

    if (NULL != pheader)
    {
        pdata = sdf_file_get_data(pheader);
    };

    return pdata;
};

//-----------------------------------------------------------------------------------------------
Uint8* pxss_find_int_data(const char * filename)
{
    SDF_PHEADER pheader = NULL;
    Uint8 * pdata       = NULL;
    char *  dotpointer  = NULL;
    char    loc_fname[256];


    // strip any existing extension
    strncpy(loc_fname, filename, sizeof(loc_fname) - 1);
    loc_fname[255] = '\0';
    dotpointer = strrchr(loc_fname, '.');

    if (NULL != dotpointer) *dotpointer = '\0';


    pheader = sdf_archive_find_filetype(loc_fname, SDF_FILE_IS_INT);

    if (NULL != pheader)
    {
        pdata = sdf_file_get_data(pheader);
    };

    return pdata;
};

//-----------------------------------------------------------------------------------------------
Uint8* pxss_find_src_data(const char * filename)
{
    SDF_PHEADER pheader = NULL;
    Uint8 * pdata       = NULL;
    char *  dotpointer  = NULL;
    char    loc_fname[256];

    // strip any existing extension
    strncpy(loc_fname, filename, sizeof(loc_fname) - 1);
    loc_fname[255] = '\0';
    dotpointer = strrchr(loc_fname, '.');

    if (NULL != dotpointer) *dotpointer = '\0';


    pheader = sdf_archive_find_filetype(loc_fname, SDF_FILE_IS_SRC);

    if (NULL != pheader)
    {
        pdata = sdf_file_get_data(pheader);
    };

    return pdata;
};

//-----------------------------------------------------------------------------------------------
Uint8* pxss_find_txt_data(const char * filename)
{
    SDF_PHEADER pheader = NULL;
    Uint8 * pdata       = NULL;
    char *  dotpointer  = NULL;
    char    loc_fname[256];

    // strip any existing extension
    strncpy(loc_fname, filename, sizeof(loc_fname) - 1);
    loc_fname[255] = '\0';
    dotpointer = strrchr(loc_fname, '.');

    if (NULL != dotpointer) *dotpointer = '\0';


    pheader = sdf_archive_find_filetype(loc_fname, SDF_FILE_IS_TXT);

    if (NULL != pheader)
    {
        pdata = sdf_file_get_data(pheader);
    };

    return pdata;
};

//-----------------------------------------------------------------------------------------------
char * pxss_find_file_name(Uint8 * data_start)
{
    char * loc_fname = NULL;
    SDF_PHEADER loc_ph = NULL;

    loc_ph = sdf_archive_find_header_by_data(data_start);

    if (NULL != loc_ph)
    {
        loc_fname = sdf_file_get_name(loc_ph);
    }

    return loc_fname;
}

//-----------------------------------------------------------------------------------------------
//bool_t src_extended_find_function(TOKEN * ptok)
//{
//    if(NULL == ptok)               return kfalse;
//    if(OPCODE_NIL != ptok->opcode) return ktrue;
//
//
//
//    else if (0 == strcmp(ptok->tag, "DEBUGMESSAGE"))     { ptok->opcode = OPCODE_EX_DEBUGMESSAGE;   ptok->number_to_destroy = 1; ptok->arg_list = arg_list_i;      ptok->variable_type = VAR_INT; }
//    else if (0 == strcmp(ptok->tag, "SPAWN"))            { ptok->opcode = OPCODE_EX_SPAWN;          ptok->number_to_destroy = 5; ptok->arg_list = arg_list_ifffi;    ptok->variable_type = VAR_INT; }
//    else if (0 == strcmp(ptok->tag, "GOPOOF"))           { ptok->opcode = OPCODE_EX_GOPOOF;         ptok->number_to_destroy = 1; ptok->arg_list = arg_list_i; }
//    else if (0 == strcmp(ptok->tag, "DISMOUNT"))         { ptok->opcode = OPCODE_EX_DISMOUNT;       ptok->number_to_destroy = 0; ptok->arg_list = arg_list_none; }
//    else if (0 == strcmp(ptok->tag, "ROLLDICE"))         { ptok->opcode = OPCODE_EX_ROLLDICE;       ptok->number_to_destroy = 2; ptok->arg_list = arg_list_ii;  ptok->variable_type = VAR_INT; }
//    else if (0 == strcmp(ptok->tag, "PLAYSOUND"))        { ptok->opcode = OPCODE_EX_PLAYSOUND;      ptok->number_to_destroy = 3; ptok->arg_list = arg_list_iii;      ptok->variable_type = VAR_INT; }
//    else if (0 == strcmp(ptok->tag, "PLAYMEGASOUND"))    { ptok->opcode = OPCODE_EX_PLAYMEGASOUND;  ptok->number_to_destroy = 5; ptok->arg_list = arg_list_iiiii;    ptok->variable_type = VAR_INT; }
//    else if (0 == strcmp(ptok->tag, "DISTANCESOUND"))    { ptok->opcode = OPCODE_EX_DISTANCESOUND;  ptok->number_to_destroy = 1; ptok->arg_list = arg_list_i;      ptok->variable_type = VAR_INT; }
//    else if (0 == strcmp(ptok->tag, "PLAYMUSIC"))        { ptok->opcode = OPCODE_EX_PLAYMUSIC;      ptok->number_to_destroy = 3; ptok->arg_list = arg_list_iii;      ptok->variable_type = VAR_INT; }
//    else if (0 == strcmp(ptok->tag, "UPDATEFILES"))      { ptok->opcode = OPCODE_EX_UPDATEFILES;    ptok->number_to_destroy = 1; ptok->arg_list = arg_list_i;}
//    else if (0 == strcmp(ptok->tag, "LOCALMESSAGE"))     { ptok->opcode = OPCODE_EX_LOCALMESSAGE;   ptok->number_to_destroy = 2; ptok->arg_list = arg_list_ii;  ptok->variable_type = VAR_INT; }
//    else if (0 == strcmp(ptok->tag, "NETWORKMESSAGE"))   { ptok->opcode = OPCODE_EX_NETWORKMESSAGE; ptok->number_to_destroy = 3; ptok->arg_list = arg_list_iii;    ptok->variable_type = VAR_INT; }
//    else if (0 == strcmp(ptok->tag, "LOGMESSAGE"))       { ptok->opcode = OPCODE_EX_LOGMESSAGE;     ptok->number_to_destroy = 1; ptok->arg_list = arg_list_i;      ptok->variable_type = VAR_INT; }
//
//    else if (0 == strcmp(ptok->tag, "STRINGRANDOMNAME"))  { ptok->opcode = OPCODE_EX_STRINGRANDOMNAME;   ptok->number_to_destroy = 2; ptok->arg_list = arg_list_ii;  ptok->variable_type = VAR_INT; }
//    else if (0 == strcmp(ptok->tag, "STRINGSANITIZE"))    { ptok->opcode = OPCODE_EX_STRINGSANITIZE;     ptok->number_to_destroy = 1; ptok->arg_list = arg_list_i;      ptok->variable_type = VAR_INT; }
//    else if (0 == strcmp(ptok->tag, "STRINGLANGUAGE"))    { ptok->opcode = OPCODE_EX_STRINGLANGUAGE;     ptok->number_to_destroy = 1; ptok->arg_list = arg_list_i;      ptok->variable_type = VAR_INT; }
//    else if (0 == strcmp(ptok->tag, "STRINGUPPERCASE"))   { ptok->opcode = OPCODE_LIB_STRINGUPPERCASE;    ptok->number_to_destroy = 1; ptok->arg_list = arg_list_i;      ptok->variable_type = VAR_INT; }
//    else if (0 == strcmp(ptok->tag, "STRINGAPPENDNUMBER")){ ptok->opcode = OPCODE_LIB_STRINGAPPENDNUMBER; ptok->number_to_destroy = 3; ptok->arg_list = arg_list_iii;      ptok->variable_type = VAR_INT; }
//
//
//    // Window functions
//    else if (0 == strcmp(ptok->tag, "WINDOWBORDER"))       { ptok->opcode = OPCODE_EX_WINDOWBORDER;      ptok->number_to_destroy = 6; ptok->arg_list = arg_list_iffiii;   ptok->variable_type = VAR_INT; }
//    else if (0 == strcmp(ptok->tag, "WINDOWSTRING"))       { ptok->opcode = OPCODE_EX_WINDOWSTRING;      ptok->number_to_destroy = 4; ptok->arg_list = arg_list_iffi;     ptok->variable_type = VAR_INT; }
//    else if (0 == strcmp(ptok->tag, "WINDOWMINILIST"))     { ptok->opcode = OPCODE_EX_WINDOWMINILIST;    ptok->number_to_destroy = 6; ptok->arg_list = arg_list_ffiiii;   ptok->variable_type = VAR_INT; }
//    else if (0 == strcmp(ptok->tag, "WINDOWSLIDER"))       { ptok->opcode = OPCODE_EX_WINDOWSLIDER;      ptok->number_to_destroy = 5; ptok->arg_list = arg_list_ffiii;    ptok->variable_type = VAR_INT; }
//    else if (0 == strcmp(ptok->tag, "WINDOWIMAGE"))        { ptok->opcode = OPCODE_EX_WINDOWIMAGE;       ptok->number_to_destroy = 7; ptok->arg_list = arg_list_ffffiii;  ptok->variable_type = VAR_INT; }
//    else if (0 == strcmp(ptok->tag, "WINDOWTRACKER"))      { ptok->opcode = OPCODE_EX_WINDOWTRACKER;     ptok->number_to_destroy = 10;ptok->arg_list = arg_list_ffffiiiiii;ptok->variable_type= VAR_INT; }
//    else if (0 == strcmp(ptok->tag, "WINDOWBOOK"))         { ptok->opcode = OPCODE_EX_WINDOWBOOK;        ptok->number_to_destroy = 7; ptok->arg_list = arg_list_ffiiiii;  ptok->variable_type= VAR_INT; }
//    else if (0 == strcmp(ptok->tag, "WINDOWINPUT"))        { ptok->opcode = OPCODE_EX_WINDOWINPUT;       ptok->number_to_destroy = 5; ptok->arg_list = arg_list_ffiii;    ptok->variable_type= VAR_INT; }
//    else if (0 == strcmp(ptok->tag, "WINDOWEMACS"))        { ptok->opcode = OPCODE_EX_WINDOWEMACS;       ptok->number_to_destroy = 6; ptok->arg_list = arg_list_ffiiii;   ptok->variable_type= VAR_INT; }
//    else if (0 == strcmp(ptok->tag, "WINDOWMEGAIMAGE"))    { ptok->opcode = OPCODE_EX_WINDOWMEGAIMAGE;   ptok->number_to_destroy = 15;ptok->arg_list = arg_list_ffffffffffffiii; ptok->variable_type= VAR_INT; }
//    else if (0 == strcmp(ptok->tag, "WINDOW3DSTART"))      { ptok->opcode = OPCODE_EX_WINDOW3DSTART;     ptok->number_to_destroy = 5; ptok->arg_list = arg_list_ffffi;    ptok->variable_type= VAR_INT; }
//    else if (0 == strcmp(ptok->tag, "WINDOW3DEND"))        { ptok->opcode = OPCODE_EX_WINDOW3DEND;       ptok->number_to_destroy = 0; ptok->arg_list = arg_list_none;     ptok->variable_type= VAR_INT; }
//    else if (0 == strcmp(ptok->tag, "WINDOW3DPOSITION"))   { ptok->opcode = OPCODE_EX_WINDOW3DPOSITION;  ptok->number_to_destroy = 4; ptok->arg_list = arg_list_fffi;     ptok->variable_type= VAR_INT; }
//    else if (0 == strcmp(ptok->tag, "WINDOW3DMODEL"))      { ptok->opcode = OPCODE_EX_WINDOW3DMODEL;     ptok->number_to_destroy = 4; ptok->arg_list = arg_list_iiii;     ptok->variable_type= VAR_INT; }
//    else if (0 == strcmp(ptok->tag, "MODELASSIGN"))        { ptok->opcode = OPCODE_EX_MODELASSIGN;       ptok->number_to_destroy = 2; ptok->arg_list = arg_list_ii; }
//    else if (0 == strcmp(ptok->tag, "PARTICLEBOUNCE"))     { ptok->opcode = OPCODE_EX_PARTICLEBOUNCE;    ptok->number_to_destroy = 0; ptok->arg_list = arg_list_none;     ptok->variable_type= VAR_INT; }
//    else if (0 == strcmp(ptok->tag, "WINDOWEDITKANJI"))    { ptok->opcode = OPCODE_EX_WINDOWEDITKANJI;   ptok->number_to_destroy = 5; ptok->arg_list = arg_list_ifffi; }
//    else if (0 == strcmp(ptok->tag, "WINDOW3DROOM"))       { ptok->opcode = OPCODE_EX_WINDOW3DROOM;      ptok->number_to_destroy = 7; ptok->arg_list = arg_list_fffiiii; }
//
//
//    // Other functions...
//    else if (0 == strcmp(ptok->tag, "INDEXISLOCALPLAYER")) { ptok->opcode = OPCODE_EX_INDEXISLOCALPLAYER;ptok->number_to_destroy = 1; ptok->arg_list = arg_list_i;      ptok->variable_type= VAR_INT; }
//    else if (0 == strcmp(ptok->tag, "FINDBINDING"))        { ptok->opcode = OPCODE_EX_FINDBINDING;       ptok->number_to_destroy = 0; ptok->arg_list = arg_list_none;     ptok->variable_type= VAR_INT; }
//    else if (0 == strcmp(ptok->tag, "FINDSELF"))         { ptok->opcode = OPCODE_SF_FINDSELF;     ptok->number_to_destroy = 0; ptok->arg_list = arg_list_none;     ptok->variable_type = VAR_INT; }
//    else if (0 == strcmp(ptok->tag, "FINDTARGET"))         { ptok->opcode = OPCODE_EX_FINDTARGET;        ptok->number_to_destroy = 0; ptok->arg_list = arg_list_none;     ptok->variable_type= VAR_INT; }
//    else if (0 == strcmp(ptok->tag, "FINDOWNER"))          { ptok->opcode = OPCODE_EX_FINDOWNER;         ptok->number_to_destroy = 0; ptok->arg_list = arg_list_none;     ptok->variable_type= VAR_INT; }
//    else if (0 == strcmp(ptok->tag, "FINDINDEX"))          { ptok->opcode = OPCODE_EX_FINDINDEX;         ptok->number_to_destroy = 1; ptok->arg_list = arg_list_i;      ptok->variable_type= VAR_INT; }
//    else if (0 == strcmp(ptok->tag, "FINDBYINDEX"))        { ptok->opcode = OPCODE_EX_FINDBYINDEX;       ptok->number_to_destroy = 1; ptok->arg_list = arg_list_i;      ptok->variable_type= VAR_INT; }
//    else if (0 == strcmp(ptok->tag, "FINDWINDOW"))         { ptok->opcode = OPCODE_EX_FINDWINDOW;        ptok->number_to_destroy = 2; ptok->arg_list = arg_list_ii;  ptok->variable_type= VAR_INT; }
//    else if (0 == strcmp(ptok->tag, "FINDPARTICLE"))       { ptok->opcode = OPCODE_EX_FINDPARTICLE;      ptok->number_to_destroy = 1; ptok->arg_list = arg_list_i;      ptok->variable_type= VAR_INT; }
//    else if (0 == strcmp(ptok->tag, "ATTACHTOTARGET"))     { ptok->opcode = OPCODE_EX_ATTACHTOTARGET;    ptok->number_to_destroy = 1; ptok->arg_list = arg_list_i;      ptok->variable_type= VAR_INT; }
//    else if (0 == strcmp(ptok->tag, "GETDIRECTION"))       { ptok->opcode = OPCODE_EX_GETDIRECTION;      ptok->number_to_destroy = 2; ptok->arg_list = arg_list_ff; ptok->variable_type= VAR_INT; }
//    else if (0 == strcmp(ptok->tag, "DAMAGETARGET"))       { ptok->opcode = OPCODE_EX_DAMAGETARGET;      ptok->number_to_destroy = 3; ptok->arg_list = arg_list_iii; }
//    else if (0 == strcmp(ptok->tag, "EXPERIENCEFUNCTION")) { ptok->opcode = OPCODE_EX_EXPERIENCEFUNCTION;ptok->number_to_destroy = 4; ptok->arg_list = arg_list_iiii;     ptok->variable_type= VAR_INT; }
//    else if (0 == strcmp(ptok->tag, "ACQUIRETARGET"))      { ptok->opcode = OPCODE_EX_ACQUIRETARGET;     ptok->number_to_destroy = 3; ptok->arg_list = arg_list_iif;      ptok->variable_type= VAR_INT; }
//    else if (0 == strcmp(ptok->tag, "FINDPATH"))           { ptok->opcode = OPCODE_EX_FINDPATH;          ptok->number_to_destroy = 0; ptok->arg_list = arg_list_none;     ptok->variable_type= VAR_INT; }
//    else if (0 == strcmp(ptok->tag, "BUTTONPRESS"))        { ptok->opcode = OPCODE_EX_BUTTONPRESS;       ptok->number_to_destroy = 3; ptok->arg_list = arg_list_iii; }
//    else if (0 == strcmp(ptok->tag, "AUTOAIM"))            { ptok->opcode = OPCODE_EX_AUTOAIM;           ptok->number_to_destroy = 7; ptok->arg_list = arg_list_ffiiiii;  ptok->variable_type= VAR_FLT; }
//    else if (0 == strcmp(ptok->tag, "ROOMHEIGHTXY"))       { ptok->opcode = OPCODE_EX_ROOMHEIGHTXY;      ptok->number_to_destroy = 2; ptok->arg_list = arg_list_ff;  ptok->variable_type= VAR_FLT; }
//
//    return (OPCODE_NIL != ptok->opcode);
//};
//




SINT system_set(PScriptContext ps, int type, int number, int sub_number, SINT new_value)
{
    SINT retval = 0;

    // !!!! a bit of trickery that should work !!!!
    PSoulfuScriptContext pss = (PSoulfuScriptContext)ps;

    switch (type)
    {
        case SYS_INVALID: retval = _ex_set_INVALID(pss, number, sub_number, new_value); break;
        case SYS_PLAYERDEVICE: retval = _ex_set_PLAYERDEVICE(pss, number, sub_number, new_value); break;
        case SYS_WINDOWSCALE: retval = _ex_set_WINDOWSCALE(pss, number, sub_number, new_value); break;
        case SYS_TOPWINDOW: retval = _ex_set_TOPWINDOW(pss, number, sub_number, new_value); break;
        case SYS_SFXVOLUME: retval = _ex_set_SFXVOLUME(pss, number, sub_number, new_value); break;
        case SYS_MUSICVOLUME: retval = _ex_set_MUSICVOLUME(pss, number, sub_number, new_value); break;
        case SYS_USERLANGUAGE: retval = _ex_set_USERLANGUAGE(pss, number, sub_number, new_value); break;
        case SYS_LANGUAGEFILE: retval = _ex_set_LANGUAGEFILE(pss, number, sub_number, new_value); break;
        case SYS_QUITGAME: retval = _ex_set_QUITGAME(pss, number, sub_number, new_value); break;
        case SYS_FILEFTPFLAG: retval = _ex_set_FILEFTPFLAG(pss, number, sub_number, new_value); break;
        case SYS_CURSORDRAW: retval = _ex_set_CURSORDRAW(pss, number, sub_number, new_value); break;
#ifdef DEVTOOL
        case SYS_MODELVIEW: retval = _ex_set_MODELVIEW(pss, number, sub_number, new_value); break;
        case SYS_MODELMOVE: retval = _ex_set_MODELMOVE(pss, number, sub_number, new_value); break;
        case SYS_MODELPLOP: retval = _ex_set_MODELPLOP(pss, number, sub_number, new_value); break;
        case SYS_MODELSELECT: retval = _ex_set_MODELSELECT(pss, number, sub_number, new_value); break;
        case SYS_MODELDELETE: retval = _ex_set_MODELDELETE(pss, number, sub_number, new_value); break;
        case SYS_MODELPLOPTRIANGLE: retval = _ex_set_MODELPLOPTRIANGLE(pss, number, sub_number, new_value); break;
        case SYS_MODELDELETETRIANGLE: retval = _ex_set_MODELDELETETRIANGLE(pss, number, sub_number, new_value); break;
        case SYS_MODELREGENERATE: retval = _ex_set_MODELREGENERATE(pss, number, sub_number, new_value); break;
        case SYS_MODELPLOPJOINT: retval = _ex_set_MODELPLOPJOINT(pss, number, sub_number, new_value); break;
        case SYS_MODELPLOPBONE: retval = _ex_set_MODELPLOPBONE(pss, number, sub_number, new_value); break;
        case SYS_MODELDELETEBONE: retval = _ex_set_MODELDELETEBONE(pss, number, sub_number, new_value); break;
        case SYS_MODELDELETEJOINT: retval = _ex_set_MODELDELETEJOINT(pss, number, sub_number, new_value); break;
        case SYS_MODELJOINTSIZE: retval = _ex_set_MODELJOINTSIZE(pss, number, sub_number, new_value); break;
        case SYS_MODELBONEID: retval = _ex_set_MODELBONEID(pss, number, sub_number, new_value); break;
        case SYS_MODELCRUNCH: retval = _ex_set_MODELCRUNCH(pss, number, sub_number, new_value); break;
        case SYS_MODELROTATEBONES: retval = _ex_set_MODELROTATEBONES(pss, number, sub_number, new_value); break;
        case SYS_MODELUNROTATEBONES: retval = _ex_set_MODELUNROTATEBONES(pss, number, sub_number, new_value); break;
        case SYS_MODELHIDE: retval = _ex_set_MODELHIDE(pss, number, sub_number, new_value); break;
        case SYS_MODELTRIANGLELINES: retval = _ex_set_MODELTRIANGLELINES(pss, number, sub_number, new_value); break;
        case SYS_MODELCOPYPASTE: retval = _ex_set_MODELCOPYPASTE(pss, number, sub_number, new_value); break;
        case SYS_MODELWELDVERTICES: retval = _ex_set_MODELWELDVERTICES(pss, number, sub_number, new_value); break;
        case SYS_MODELWELDTEXVERTICES: retval = _ex_set_MODELWELDTEXVERTICES(pss, number, sub_number, new_value); break;
        case SYS_MODELFLIP: retval = _ex_set_MODELFLIP(pss, number, sub_number, new_value); break;
        case SYS_MODELSCALE: retval = _ex_set_MODELSCALE(pss, number, sub_number, new_value); break;
        case SYS_MODELTEXSCALE: retval = _ex_set_MODELTEXSCALE(pss, number, sub_number, new_value); break;
        case SYS_MODELANCHOR: retval = _ex_set_MODELANCHOR(pss, number, sub_number, new_value); break;
        case SYS_MODELTEXFLAGSALPHA: retval = _ex_set_MODELTEXFLAGSALPHA(pss, number, sub_number, new_value); break;
        case SYS_MODELADDFRAME: retval = _ex_set_MODELADDFRAME(pss, number, sub_number, new_value); break;
        case SYS_MODELADDBASEMODEL: retval = _ex_set_MODELADDBASEMODEL(pss, number, sub_number, new_value); break;
        case SYS_MODELFRAMEBASEMODEL: retval = _ex_set_MODELFRAMEBASEMODEL(pss, number, sub_number, new_value); break;
        case SYS_MODELEXTERNALFILENAME: retval = _ex_set_MODELEXTERNALFILENAME(pss, number, sub_number, new_value); break;
        case SYS_MODELSHADOWTEXTURE: retval = _ex_set_MODELSHADOWTEXTURE(pss, number, sub_number, new_value); break;
        case SYS_MODELSHADOWALPHA: retval = _ex_set_MODELSHADOWALPHA(pss, number, sub_number, new_value); break;
        case SYS_MODELINTERPOLATE: retval = _ex_set_MODELINTERPOLATE(pss, number, sub_number, new_value); break;
        case SYS_MODELCENTER: retval = _ex_set_MODELCENTER(pss, number, sub_number, new_value); break;
        case SYS_MODELDETEXTURE: retval = _ex_set_MODELDETEXTURE(pss, number, sub_number, new_value); break;
        case SYS_MODELPLOPATSTRING: retval = _ex_set_MODELPLOPATSTRING(pss, number, sub_number, new_value); break;
        case SYS_MODELMARKFRAME: retval = _ex_set_MODELMARKFRAME(pss, number, sub_number, new_value); break;
        case SYS_MODELCOPYFRAME: retval = _ex_set_MODELCOPYFRAME(pss, number, sub_number, new_value); break;
        case SYS_MODELAUTOSHADOW: retval = _ex_set_MODELAUTOSHADOW(pss, number, sub_number, new_value); break;
#endif
        case SYS_MODELFRAMEACTIONNAME: retval = _ex_set_MODELFRAMEACTIONNAME(pss, number, sub_number, new_value); break;
        case SYS_MODELFRAMEFLAGS: retval = _ex_set_MODELFRAMEFLAGS(pss, number, sub_number, new_value); break;
        case SYS_WATERLAYERSACTIVE: retval = _ex_set_WATERLAYERSACTIVE(pss, number, sub_number, new_value); break;
        case SYS_CARTOONMODE: retval = _ex_set_CARTOONMODE(pss, number, sub_number, new_value); break;
        case SYS_KANJICOPY: retval = _ex_set_KANJICOPY(pss, number, sub_number, new_value); break;
        case SYS_KANJIPASTE: retval = _ex_set_KANJIPASTE(pss, number, sub_number, new_value); break;
        case SYS_KANJIDELETE: retval = _ex_set_KANJIDELETE(pss, number, sub_number, new_value); break;
        case SYS_BLOCKKEYBOARD: retval = _ex_set_BLOCKKEYBOARD(pss, number, sub_number, new_value); break;
#ifdef DEVTOOL
        case SYS_BILLBOARDACTIVE: retval = _ex_set_BILLBOARDACTIVE(pss, number, sub_number, new_value); break;
#endif
        case SYS_JOINGAME: retval = _ex_set_JOINGAME(pss, number, sub_number, new_value); break;
        case SYS_STARTGAME: retval = _ex_set_STARTGAME(pss, number, sub_number, new_value); break;
        case SYS_LEAVEGAME: retval = _ex_set_LEAVEGAME(pss, number, sub_number, new_value); break;
        case SYS_LOCALPLAYER: retval = _ex_set_LOCALPLAYER(pss, number, sub_number, new_value); break;
        case SYS_SCREENSHAKE: retval = _ex_set_SCREENSHAKE(pss, number, sub_number, new_value); break;
        case SYS_INCLUDEPASSWORD: retval = _ex_set_INCLUDEPASSWORD(pss, number, sub_number, new_value); break;
        case SYS_RANDOMSEED: retval = _ex_set_RANDOMSEED(pss, number, sub_number, new_value); break;
        case SYS_MIPMAPACTIVE: retval = _ex_set_MIPMAPACTIVE(pss, number, sub_number, new_value); break;
        case SYS_WATERTEXTURE: retval = _ex_set_WATERTEXTURE(pss, number, sub_number, new_value); break;
        case SYS_PLAYERCONTROLHANDLED: retval = _ex_set_PLAYERCONTROLHANDLED(pss, number, sub_number, new_value); break;
        case SYS_GLOBALSPAWN: retval = _ex_set_GLOBALSPAWN(pss, number, sub_number, new_value); break;
        case SYS_GLOBALATTACKSPIN: retval = _ex_set_GLOBALATTACKSPIN(pss, number, sub_number, new_value); break;
        case SYS_GLOBALATTACKER: retval = _ex_set_GLOBALATTACKER(pss, number, sub_number, new_value); break;
        case SYS_CURRENTITEM: retval = _ex_set_CURRENTITEM(pss, number, sub_number, new_value); break;
        case SYS_ITEMREGISTRYCLEAR: retval = _ex_set_ITEMREGISTRYCLEAR(pss, number, sub_number, new_value); break;
        case SYS_ITEMREGISTRYSCRIPT: retval = _ex_set_ITEMREGISTRYSCRIPT(pss, number, sub_number, new_value); break;
        case SYS_ITEMREGISTRYICON: retval = _ex_set_ITEMREGISTRYICON(pss, number, sub_number, new_value); break;
        case SYS_ITEMREGISTRYOVERLAY: retval = _ex_set_ITEMREGISTRYOVERLAY(pss, number, sub_number, new_value); break;
        case SYS_ITEMREGISTRYPRICE: retval = _ex_set_ITEMREGISTRYPRICE(pss, number, sub_number, new_value); break;
        case SYS_ITEMREGISTRYFLAGS: retval = _ex_set_ITEMREGISTRYFLAGS(pss, number, sub_number, new_value); break;
        case SYS_ITEMREGISTRYSTR: retval = _ex_set_ITEMREGISTRYSTR(pss, number, sub_number, new_value); break;
        case SYS_ITEMREGISTRYDEX: retval = _ex_set_ITEMREGISTRYDEX(pss, number, sub_number, new_value); break;
        case SYS_ITEMREGISTRYINT: retval = _ex_set_ITEMREGISTRYINT(pss, number, sub_number, new_value); break;
        case SYS_ITEMREGISTRYMANA: retval = _ex_set_ITEMREGISTRYMANA(pss, number, sub_number, new_value); break;
        case SYS_ITEMREGISTRYAMMO: retval = _ex_set_ITEMREGISTRYAMMO(pss, number, sub_number, new_value); break;
        case SYS_WEAPONGRIP: retval = _ex_set_WEAPONGRIP(pss, number, sub_number, new_value); break;
        case SYS_WEAPONMODELSETUP: retval = _ex_set_WEAPONMODELSETUP(pss, number, sub_number, new_value); break;
        case SYS_WEAPONEVENT: retval = _ex_set_WEAPONEVENT(pss, number, sub_number, new_value); break;
        case SYS_WEAPONFRAMEEVENT: retval = _ex_set_WEAPONFRAMEEVENT(pss, number, sub_number, new_value); break;
        case SYS_WEAPONUNPRESSED: retval = _ex_set_WEAPONUNPRESSED(pss, number, sub_number, new_value); break;
        case SYS_CHARFASTFUNCTION: retval = _ex_set_CHARFASTFUNCTION(pss, number, sub_number, new_value); break;
        case SYS_FASTANDUGLY: retval = _ex_set_FASTANDUGLY(pss, number, sub_number, new_value); break;
        case SYS_DEFENSERATING: retval = _ex_set_DEFENSERATING(pss, number, sub_number, new_value); break;
        case SYS_CLEARDEFENSERATING: retval = _ex_set_CLEARDEFENSERATING(pss, number, sub_number, new_value); break;
        case SYS_ITEMDEFENSERATING: retval = _ex_set_ITEMDEFENSERATING(pss, number, sub_number, new_value); break;
        case SYS_CAMERAANGLE: retval = _ex_set_CAMERAANGLE(pss, number, sub_number, new_value); break;
        case SYS_STARTFADE: retval = _ex_set_STARTFADE(pss, number, sub_number, new_value); break;
        case SYS_ROOMUNCOMPRESS: retval = _ex_set_ROOMUNCOMPRESS(pss, number, sub_number, new_value); break;
#ifdef DEVTOOL
        case SYS_ROOMPLOPATSTRING: retval = _ex_set_ROOMPLOPATSTRING(pss, number, sub_number, new_value); break;
        case SYS_ROOMSELECT: retval = _ex_set_ROOMSELECT(pss, number, sub_number, new_value); break;
        case SYS_ROOMPLOPVERTEX: retval = _ex_set_ROOMPLOPVERTEX(pss, number, sub_number, new_value); break;
        case SYS_ROOMDELETEVERTEX: retval = _ex_set_ROOMDELETEVERTEX(pss, number, sub_number, new_value); break;
        case SYS_ROOMWELDVERTICES: retval = _ex_set_ROOMWELDVERTICES(pss, number, sub_number, new_value); break;
        case SYS_ROOMCLEAREXTERIORWALL: retval = _ex_set_ROOMCLEAREXTERIORWALL(pss, number, sub_number, new_value); break;
        case SYS_ROOMPLOPEXTERIORWALL: retval = _ex_set_ROOMPLOPEXTERIORWALL(pss, number, sub_number, new_value); break;
        case SYS_ROOMEXTERIORWALLFLAGS: retval = _ex_set_ROOMEXTERIORWALLFLAGS(pss, number, sub_number, new_value); break;
        case SYS_ROOMPLOPWAYPOINT: retval = _ex_set_ROOMPLOPWAYPOINT(pss, number, sub_number, new_value); break;
        case SYS_ROOMDELETEWAYPOINT: retval = _ex_set_ROOMDELETEWAYPOINT(pss, number, sub_number, new_value); break;
        case SYS_ROOMLINKWAYPOINT: retval = _ex_set_ROOMLINKWAYPOINT(pss, number, sub_number, new_value); break;
        case SYS_ROOMUNLINKWAYPOINT: retval = _ex_set_ROOMUNLINKWAYPOINT(pss, number, sub_number, new_value); break;
        case SYS_ROOMDELETEBRIDGE: retval = _ex_set_ROOMDELETEBRIDGE(pss, number, sub_number, new_value); break;
        case SYS_ROOMPLOPTRIANGLE: retval = _ex_set_ROOMPLOPTRIANGLE(pss, number, sub_number, new_value); break;
        case SYS_ROOMPLOPFAN: retval = _ex_set_ROOMPLOPFAN(pss, number, sub_number, new_value); break;
        case SYS_ROOMDELETETRIANGLE: retval = _ex_set_ROOMDELETETRIANGLE(pss, number, sub_number, new_value); break;
        case SYS_ROOMGROUP: retval = _ex_set_ROOMGROUP(pss, number, sub_number, new_value); break;
        case SYS_ROOMOBJECT: retval = _ex_set_ROOMOBJECT(pss, number, sub_number, new_value); break;
        case SYS_ROOMAUTOTEXTURE: retval = _ex_set_ROOMAUTOTEXTURE(pss, number, sub_number, new_value); break;
        case SYS_ROOMAUTOTRIM: retval = _ex_set_ROOMAUTOTRIM(pss, number, sub_number, new_value); break;
        case SYS_ROOMTEXTUREFLAGS: retval = _ex_set_ROOMTEXTUREFLAGS(pss, number, sub_number, new_value); break;
        case SYS_ROOMHARDPLOPPER: retval = _ex_set_ROOMHARDPLOPPER(pss, number, sub_number, new_value); break;
        case SYS_ROOMCOPYPASTE: retval = _ex_set_ROOMCOPYPASTE(pss, number, sub_number, new_value); break;
#endif
        case SYS_MOUSETEXT: retval = _ex_set_MOUSETEXT(pss, number, sub_number, new_value); break;
        case SYS_BUMPABORT: retval = _ex_set_BUMPABORT(pss, number, sub_number, new_value); break;
#ifdef DEVTOOL
        case SYS_MODELAUTOVERTEX: retval = _ex_set_MODELAUTOVERTEX(pss, number, sub_number, new_value); break;
#endif
        case SYS_ITEMINDEX: retval = _ex_set_ITEMINDEX(pss, number, sub_number, new_value); break;
        case SYS_WEAPONREFRESHXYZ: retval = _ex_set_WEAPONREFRESHXYZ(pss, number, sub_number, new_value); break;
        case SYS_WEAPONREFRESHFLASH: retval = _ex_set_WEAPONREFRESHFLASH(pss, number, sub_number, new_value); break;
        case SYS_FASTFUNCTION: retval = _ex_set_FASTFUNCTION(pss, number, sub_number, new_value); break;
        case SYS_KEEPITEM: retval = _ex_set_KEEPITEM(pss, number, sub_number, new_value); break;
        case SYS_MAKEINPUTACTIVE: retval = _ex_set_MAKEINPUTACTIVE(pss, number, sub_number, new_value); break;
#ifdef DEVTOOL
        case SYS_GNOMIFYVECTOR: retval = _ex_set_GNOMIFYVECTOR(pss, number, sub_number, new_value); break;
        case SYS_GNOMIFYJOINT: retval = _ex_set_GNOMIFYJOINT(pss, number, sub_number, new_value); break;
        case SYS_JOINTFROMVERTEX: retval = _ex_set_JOINTFROMVERTEX(pss, number, sub_number, new_value); break;
#endif
        case SYS_MESSAGESIZE: retval = _ex_set_MESSAGESIZE(pss, number, sub_number, new_value); break;
        case SYS_LASTINPUTCURSORPOS: retval = _ex_set_LASTINPUTCURSORPOS(pss, number, sub_number, new_value); break;
        case SYS_MESSAGERESET: retval = _ex_set_MESSAGERESET(pss, number, sub_number, new_value); break;
        case SYS_FASTFUNCTIONFOUND: retval = _ex_set_FASTFUNCTIONFOUND(pss, number, sub_number, new_value); break;
        case SYS_INPUTACTIVE: retval = _ex_set_INPUTACTIVE(pss, number, sub_number, new_value); break;
        case SYS_LOCALPLAYERINPUT: retval = _ex_set_LOCALPLAYERINPUT(pss, number, sub_number, new_value); break;
        case SYS_SANDTEXTURE: retval = _ex_set_SANDTEXTURE(pss, number, sub_number, new_value); break;
        case SYS_PAYINGCUSTOMER: retval = _ex_set_PAYINGCUSTOMER(pss, number, sub_number, new_value); break;
        case SYS_FILESETFLAG: retval = _ex_set_FILESETFLAG(pss, number, sub_number, new_value); break;
        case SYS_ENCHANTCURSOR: retval = _ex_set_ENCHANTCURSOR(pss, number, sub_number, new_value); break;
        case SYS_FLIPPAN: retval = _ex_set_FLIPPAN(pss, number, sub_number, new_value); break;
        case SYS_MAPCLEAR: retval = _ex_set_MAPCLEAR(pss, number, sub_number, new_value); break;
        case SYS_MAPROOM: retval = _ex_set_MAPROOM(pss, number, sub_number, new_value); break;
        case SYS_MAPAUTOMAPPRIME: retval = _ex_set_MAPAUTOMAPPRIME(pss, number, sub_number, new_value); break;
        case SYS_MAPAUTOMAPDRAW: retval = _ex_set_MAPAUTOMAPDRAW(pss, number, sub_number, new_value); break;
        case SYS_MAPOBJECTRECORD: retval = _ex_set_MAPOBJECTRECORD(pss, number, sub_number, new_value); break;
        case SYS_MAPOBJECTDEFEATED: retval = _ex_set_MAPOBJECTDEFEATED(pss, number, sub_number, new_value); break;
        case SYS_MAPDOOROPEN: retval = _ex_set_MAPDOOROPEN(pss, number, sub_number, new_value); break;
        case SYS_CAMERARESET: retval = _ex_set_CAMERARESET(pss, number, sub_number, new_value); break;
        case SYS_RESPAWNCHARACTER: retval = _ex_set_RESPAWNCHARACTER(pss, number, sub_number, new_value); break;
        case SYS_RESERVECHARACTER: retval = _ex_set_RESERVECHARACTER(pss, number, sub_number, new_value); break;
        case SYS_SWAPCHARACTERS: retval = _ex_set_SWAPCHARACTERS(pss, number, sub_number, new_value); break;
        case SYS_LUCK: retval = _ex_set_LUCK(pss, number, sub_number, new_value); break;
        case SYS_ROOMTEXTURE: retval = _ex_set_ROOMTEXTURE(pss, number, sub_number, new_value); break;
        case SYS_DAMAGECHARACTER: retval = _ex_set_DAMAGECHARACTER(pss, number, sub_number, new_value); break;
        case SYS_CAMERAZOOM: retval = _ex_set_CAMERAZOOM(pss, number, sub_number, new_value); break;
        case SYS_CAMERASPIN: retval = _ex_set_CAMERASPIN(pss, number, sub_number, new_value); break;
        case SYS_ROOMRESTOCK: retval = _ex_set_ROOMRESTOCK(pss, number, sub_number, new_value); break;
#ifdef DEVTOOL
        case SYS_MODELCHECKHACK: retval = _ex_set_MODELCHECKHACK(pss, number, sub_number, new_value); break;
#endif
        default:
            log_error(0, "OPCODE MISS: system_set %d %d %d\n", type, number, sub_number);
            break;
    }

    return retval;
}



SINT _ex_set_INVALID(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    return kfalse;
};

SINT _ex_set_PLAYERDEVICE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // sub_number is the local player number, number is the request type (DEVICE_NUMBER, DEVICE_LEFT_BUTTON, etc.)
    // new_value is the new value (DEVICE_JOYSTICK_0, DEVICE_BUTTON_0, etc.)
    // Returns kfalse if the change wasn't accepted...

    bool_t is_used = kfalse, retval = kfalse;
    int i;
    int tmp_j, tmp_k;

    number %= MAX_LOCAL_PLAYER;

    if (sub_number == PLAYER_DEVICE_TYPE)
    {
        // Setting the main player device type...
        if (new_value >= PLAYER_DEVICE_JOYSTICK_1)
        {
            // Need to check if the joystick is already used...
            repeat(i, MAX_LOCAL_PLAYER)
            {
                if (i != number && player_device[i].type == new_value)
                {
                    is_used = ktrue;
                }
            }

            // Also check if joystick exists...
            if ((new_value - 1) > joy_list_count)
            {
                // Trying to use an unconnected joystick...
                is_used = ktrue;
            }
        }

        if (!is_used)
        {
            // No conflicts...
            player_device[number].type = new_value;
            retval = ktrue;
        }
    }
    else
    {
        // Setting one of the button bindings...
        if (sub_number >= 0 && sub_number < MAX_PLAYER_DEVICE_BUTTON)
        {
            // Check for key conflicts for keyboard...
            if (player_device[number].type == PLAYER_DEVICE_KEYBOARD)
            {
                repeat(tmp_j, MAX_LOCAL_PLAYER)
                {
                    if (tmp_j != number && player_device[tmp_j].type == PLAYER_DEVICE_KEYBOARD)
                    {
                        repeat(tmp_k, MAX_PLAYER_DEVICE_BUTTON)
                        {
                            if (player_device[tmp_j].button[tmp_k] == new_value)
                            {
                                is_used = ktrue;
                                tmp_j = MAX_LOCAL_PLAYER;
                                tmp_k = MAX_PLAYER_DEVICE_BUTTON;
                            }
                        }
                    }
                }
            }

            if (!is_used)
            {
                // No conflicts...
                player_device[number].button[sub_number] = new_value;
                retval = ktrue;
            }
        }
    }

    return retval;
};

SINT _ex_set_WINDOWSCALE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // "pss->self.object.win" not used
    //assert(OBJECT_WIN == pss->self.object.type);

    pss->screen.scale = window_scale = (new_value / 600.0f) + 6.0f;

    return (SINT)pss->screen.scale;
};

SINT _ex_set_TOPWINDOW(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // Delay window ordering effect until after all windows have been drawn...
    win_request_promotion( &(pss->self.object) );

    return ktrue;
};

SINT _ex_set_SFXVOLUME(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    master_sfx_volume = new_value;

    return master_sfx_volume;
};

SINT _ex_set_MUSICVOLUME(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    master_music_volume = new_value;

    return master_music_volume;
};

SINT _ex_set_USERLANGUAGE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    if (new_value < LANGUAGE_MAX) user_language = new_value;

    if (NULL == language_file[user_language]) user_language = 0;

    if (language_file[user_language] != NULL) user_language_phrases = (SINT) endian_read_mem_int32(language_file[user_language]);
    else user_language_phrases = 0;

    return user_language;
};

SINT _ex_set_LANGUAGEFILE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    Uint8 * retval = NULL;

    if (number < LANGUAGE_MAX)
    {
        language_file[number] = (Uint8*) new_value;
        retval = CAST(Uint8*, new_value);
    }

    return CAST(SINT, retval);
};

SINT _ex_set_QUITGAME(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    quitgame = (Uint8) new_value;

    return quitgame;
};

SINT _ex_set_FILEFTPFLAG(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    SDF_PHEADER pheader  = sdf_archive_get_header(number);

    if (NULL != pheader)
    {
        if (new_value)
        {
            sdf_file_add_flags(pheader, SDF_FLAG_NO_UPDATE);
        }
        else
        {
            sdf_file_remove_flags(pheader,  SDF_FLAG_NO_UPDATE);
        }
    }

    return (NULL != pheader);
};

SINT _ex_set_CURSORDRAW(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    mouse.draw = (Uint8) new_value;

    return mouse.draw;
};

SINT _ex_set_MODELFRAMEFLAGS(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    bool_t retval = kfalse;
    Uint8 *ptmp_a;

    ptmp_a = get_start_of_frame((Uint8*) number, (Uint16) sub_number);

    if (ptmp_a)
    {
        *(ptmp_a + 1) = (Uint8) new_value;
        retval = ktrue;
    }

    return retval;
};

SINT _ex_set_MODELFRAMEACTIONNAME(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    bool_t retval = kfalse;
    Uint8 *ptmp_a;

    ptmp_a = get_start_of_frame((Uint8*) number, (Uint16) sub_number);

    if (ptmp_a)
    {
        *(ptmp_a) = (Uint8) new_value;
        ddd_generate_model_action_list((Uint8*) number);
        retval = ktrue;
    }

    return retval;
};

#ifdef DEVTOOL
SINT _ex_set_MODELVIEW(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    selection.view = (Uint8) new_value;

    return selection.view;
};

SINT _ex_set_MODELMOVE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    selection.move = (Uint8) new_value;

    return selection.move;
};

SINT _ex_set_MODELPLOP(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    if (keyb.down[SDLK_LSHIFT] || keyb.down[SDLK_RSHIFT])
    {
        // Snapped to nearest quarter foot plop...
        selection.center_xyz[XX] = ((SINT) (selection.center_xyz[XX] * 4.0f)) * 0.25f;
        selection.center_xyz[YY] = ((SINT) (selection.center_xyz[YY] * 4.0f)) * 0.25f;
        selection.center_xyz[ZZ] = ((SINT) (selection.center_xyz[ZZ] * 4.0f)) * 0.25f;
        render_insert_vertex((Uint8*) number, (Uint16) sub_number, selection.center_xyz, 0, MAX_VERTEX);
    }
    else
    {
        // Normal plop vertex...
        render_insert_vertex((Uint8*) number, (Uint16) sub_number, selection.center_xyz, 0, MAX_VERTEX);
    }

    return ktrue;
};

SINT _ex_set_MODELSELECT(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    bool_t retval = ktrue;
    FLOT e, f, tmp_e;
    Uint8* ptmp_a;

    switch (number)
    {
        case SELECT_CONNECTED:
            selection.close_type = BORDER_SPECIAL_SELECT_CONNECTED;
            break;

        case SELECT_INVERT:
            selection.close_type = BORDER_SPECIAL_SELECT_INVERT;
            break;

        case SELECT_SWAP:

            if (new_value < select_num && sub_number < select_num)
            {
                number = select_list[new_value];
                e = select_xyz[new_value][XX];
                f = select_xyz[new_value][YY];
                tmp_e = select_xyz[new_value][ZZ];
                ptmp_a = (Uint8*) select_data[new_value];

                select_list[new_value] = select_list[sub_number];
                select_xyz[new_value][XX] = select_xyz[sub_number][XX];
                select_xyz[new_value][YY] = select_xyz[sub_number][YY];
                select_xyz[new_value][ZZ] = select_xyz[sub_number][ZZ];
                select_data[new_value] = select_data[sub_number];

                select_list[sub_number] = number;
                select_xyz[sub_number][XX] = e;
                select_xyz[sub_number][XX] = f;
                select_xyz[sub_number][XX] = tmp_e;
                select_data[sub_number] = (FLOT*) ptmp_a;
            }

            break;

        case SELECT_REMOVE:
            select_remove((Uint16) new_value);
            break;

        case SELECT_ALL:

            if (new_value)
            {
                selection.close_type = BORDER_SPECIAL_SELECT_ALL;
            }
            else
            {
                select_clear();
            }

            break;

        default:
            retval = kfalse;
    }


    return retval;
};

SINT _ex_set_MODELDELETE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    int i;

    if (select_num > 0)
    {
        i = 50000;

        while (i > 0)
        {
            i--;

            if ( select_inlist((Uint16) i) )
            {
                render_insert_vertex((Uint8*) number, (Uint16) sub_number, NULL, (Uint16) i, MAX_VERTEX);
            }
        }

        select_num = 0;
    }

    return ktrue;
};

SINT _ex_set_MODELPLOPTRIANGLE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    return render_insert_triangle((Uint8*) number, (Uint16) sub_number, ktrue, (Uint8) new_value);
};

SINT _ex_set_MODELDELETETRIANGLE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    return render_insert_triangle((Uint8*) number, (Uint16) sub_number, kfalse, (Uint8) new_value);
};

SINT _ex_set_MODELREGENERATE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    render_generate_bone_normals((Uint8*) number, (Uint16) sub_number);
    return ktrue;
};

SINT _ex_set_MODELPLOPJOINT(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    bool_t retval;

    retval = render_insert_joint((Uint8*) number, (Uint16) sub_number, selection.center_xyz, 0, (Uint8) new_value);

    if (keyb.down[SDLK_x])
    {
        selection.center_xyz[XX] = -selection.center_xyz[XX];
        render_insert_joint((Uint8*) number, (Uint16) sub_number, selection.center_xyz, 0, (Uint8) new_value);
        selection.center_xyz[XX] = -selection.center_xyz[XX];
    }

    return retval;
};

SINT _ex_set_MODELPLOPBONE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    return render_insert_bone((Uint8*) number, (Uint16) sub_number, select_list[0], select_list[1], ktrue, (Uint8) new_value);
};

SINT _ex_set_MODELDELETEBONE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    return render_insert_bone((Uint8*) number, (Uint16) sub_number, select_list[0], select_list[1], kfalse, 0);
};

SINT _ex_set_MODELDELETEJOINT(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    return render_insert_joint((Uint8*) number, (Uint16) sub_number, NULL, select_list[0], 0);
};

SINT _ex_set_MODELJOINTSIZE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    int i;

    repeat(i, select_num)
    {
        render_joint_size((Uint8*) number, (Uint16) sub_number, select_list[i], (Uint8) new_value);
    }

    return ktrue;
};

SINT _ex_set_MODELBONEID(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    return render_bone_id((Uint8*) number, (Uint16) sub_number, select_list[0], select_list[1], (Uint8) new_value);
};

SINT _ex_set_MODELCRUNCH(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    render_crunch_rdy((Uint8*) number);

    return ktrue;
};

SINT _ex_set_MODELROTATEBONES(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    render_rotate_bones((Uint8*) number, (Uint16) sub_number, (Sint8) new_value, kfalse);

    return ktrue;
};

SINT _ex_set_MODELUNROTATEBONES(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    render_rotate_bones((Uint8*) number, (Uint16) sub_number, (Sint8) 0, ktrue);

    return ktrue;
};

SINT _ex_set_MODELHIDE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    hide_vertices((Uint8*) number, (Uint16) sub_number, (Uint8) new_value);

    return ktrue;
};

SINT _ex_set_MODELTRIANGLELINES(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    triangle_lines = (triangle_lines + 1) & 1;

    return triangle_lines;
};

SINT _ex_set_MODELCOPYPASTE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    if (new_value)
    {
        render_paste_selected((Uint8*) number, (Uint16) sub_number, (Uint8) (new_value - 1));
    }
    else
    {
        render_copy_selected((Uint8*) number, (Uint16) sub_number);
    }

    return ktrue;
};

SINT _ex_set_MODELWELDVERTICES(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    weld_selected_vertices((Uint8*) number, (Uint16) sub_number, (Uint8) new_value);

    return ktrue;
};

SINT _ex_set_MODELWELDTEXVERTICES(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    weld_selected_tex_vertices((Uint8*) number, (Uint16) sub_number);

    return ktrue;
};

SINT _ex_set_MODELFLIP(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    flip_selected_vertices((Uint8*) number, (Uint16) sub_number, (Uint8) new_value);

    return ktrue;
};

SINT _ex_set_MODELSCALE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    //scale_selected_vertices((Uint8*) number, (Uint16) sub_number, (Uint8) new_value, 0.95f);
    //scale_selected_vertices((Uint8*) number, (Uint16) sub_number, (Uint8) new_value, 0.75f);

    // Temporary tools...
    //set_selected_vertices((Uint8*) number, (Uint16) sub_number, (Uint8) new_value, -1.25f);
    if (new_value == ZZ)
    {
        //move_selected_vertices((Uint8*) number, (Uint16) sub_number, (Uint8) new_value, 0.0625f);
        //move_selected_vertices((Uint8*) number, (Uint16) sub_number, (Uint8) new_value, -0.25f);
        //break_anim_selected_vertices((Uint8*) number, (Uint16) sub_number);
        break_anim_joints((Uint8*) number, (Uint16) sub_number);


        //scale_selected_vertices((Uint8*) number, (Uint16) sub_number, (Uint8) new_value, 0.86666666666666f);
        // scale_selected_vertices_centrid((Uint8*) number, (Uint16) sub_number, 0.86666666666666f);
    }
    else if (new_value == XX)
    {
        rotate_selected_vertices((Uint8*) number, (Uint16) sub_number);
    }
    else
    {
        scale_all_joints_and_vertices((Uint8*) number, 1.25f);
        // scale_selected_vertices((Uint8*) number, (Uint16) sub_number, X, 0.9f);
        // scale_selected_vertices((Uint8*) number, (Uint16) sub_number, Y, 0.9f);
        // scale_selected_vertices((Uint8*) number, (Uint16) sub_number, Z, 0.9f);
    }

    //                                if(new_value == X)
    //
    //{
    //                                    tree_rotate_selected_vertices((Uint8*) number, (Uint16) sub_number, 0.24f);
    //                                }
    //                                else
    //
    //{
    //                                    tree_rotate_selected_vertices((Uint8*) number, (Uint16) sub_number, -0.24f);
    //                                }
    //}

    return ktrue;
};

SINT _ex_set_MODELTEXSCALE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    scale_selected_tex_vertices((Uint8*) number, (Uint16) sub_number, (Uint8) new_value, 0.984375f);

    return ktrue;
};

SINT _ex_set_MODELANCHOR(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    do_anchor_swap = ktrue;

    return do_anchor_swap;
};

SINT _ex_set_MODELTEXFLAGSALPHA(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    bool_t retval = kfalse;
    Uint8* ptmp_a;

    ptmp_a = get_start_of_triangles((Uint8*) number, (Uint16) sub_number, (Uint8) (new_value >> 16));

    if (ptmp_a)
    {
        if (*ptmp_a)
        {
            ptmp_a++;
            *ptmp_a = (Uint8) (new_value >> 8);  // Flags
            ptmp_a++;
            *ptmp_a = (Uint8) (new_value);  // Alpha

            retval = ktrue;
        }
    }

    return retval;
};

SINT _ex_set_MODELADDFRAME(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    bool_t retval;

    retval = render_insert_frame((Uint8*) number, (Uint16) sub_number, (Uint8) new_value);
    ddd_generate_model_action_list((Uint8*) number);

    return retval;
};

SINT _ex_set_MODELADDBASEMODEL(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    return render_insert_base_model((Uint8*) number, (Uint16) sub_number, (Uint8) new_value);
};

SINT _ex_set_MODELFRAMEBASEMODEL(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    return render_change_frame_base_model((Uint8*) number, (Uint16) sub_number, (Uint16) new_value);
};

SINT _ex_set_MODELEXTERNALFILENAME(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    return (SINT)render_get_set_external_linkage((Uint8*) number, (Uint8*) sub_number);
};

SINT _ex_set_MODELSHADOWTEXTURE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    Uint8 *ptmp_a;

    ptmp_a = (Uint8*) number;
    ptmp_a += 6 + (ACTION_MAX << 1) + (sub_number & 3);
    *ptmp_a = (Uint8) new_value;

    return new_value;
};

SINT _ex_set_MODELSHADOWALPHA(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    Uint8* ptmp_a;
    int tmp_i, tmp_j, tmp_k;

    ptmp_a = get_start_of_frame_shadows((Uint8*) number, (Uint16) sub_number);
    tmp_j = number;
    tmp_k = sub_number;

    if (ptmp_a)
    {
        tmp_i = 0;  // Number of bytes added to bone frame...
        repeat(number, 4)
        {
            sub_number = ((new_value >> (number << 2)) & 15) << 4;  // sub_number is the new alpha for the current shadow...  0 to 240

            if (*ptmp_a)
            {
                // Shadow is turned on...  Are we turning it off, or just modifying the opacity?
                *ptmp_a = sub_number;  ptmp_a++;

                if (sub_number)
                {
                    // Just modifying opacity...
                    ptmp_a += 32;
                }
                else
                {
                    // Turning it off...
                    sdf_insert_data(ptmp_a, NULL, -32);
                    select_clear();
                    tmp_i -= 32;
                }
            }
            else
            {
                // Shadow is turned off...  Are we turning it on?
                *ptmp_a = sub_number;  ptmp_a++;

                if (sub_number)
                {
                    // Turning it on...
                    sdf_insert_data(ptmp_a, NULL, 32);
                    DEREF( FLOT, ptmp_a ) = -1.3f;  ptmp_a += 4;
                    DEREF( FLOT, ptmp_a ) = -1.3f;  ptmp_a += 4;

                    DEREF( FLOT, ptmp_a ) =  1.3f;  ptmp_a += 4;
                    DEREF( FLOT, ptmp_a ) = -1.3f;  ptmp_a += 4;

                    DEREF( FLOT, ptmp_a ) =  1.3f;  ptmp_a += 4;
                    DEREF( FLOT, ptmp_a ) =  1.3f;  ptmp_a += 4;

                    DEREF( FLOT, ptmp_a ) = -1.3f;  ptmp_a += 4;
                    DEREF( FLOT, ptmp_a ) =  1.3f;  ptmp_a += 4;
                    select_clear();
                    sub_number = (number << 4);
                    select_add((Uint16) sub_number, (FLOT*) (ptmp_a - 32)); sub_number++;
                    select_add((Uint16) sub_number, (FLOT*) (ptmp_a - 24)); sub_number++;
                    select_add((Uint16) sub_number, (FLOT*) (ptmp_a - 16)); sub_number++;
                    select_add((Uint16) sub_number, (FLOT*) (ptmp_a - 8));
                    tmp_i += 32;
                }
            }
        }

        // Push around later bone frames...
        if (tmp_i != 0)
        {
            sub_number = tmp_k;
            number = tmp_j;
            sub_number++;
            ptmp_a = get_frame_pointer((Uint8*) number, (Uint16) sub_number);

            while (ptmp_a)
            {
                (DEREF( UINT, ptmp_a )) = (DEREF( UINT, ptmp_a )) + tmp_i;
                sub_number++;
                ptmp_a = get_frame_pointer((Uint8*) number, (Uint16) sub_number);
            }
        }
    }

    return ktrue;
};

SINT _ex_set_MODELINTERPOLATE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    if (new_value)
    {
        render_shadow_reset((Uint8*) number, (Uint16) sub_number);
    }
    else
    {
        render_interpolate((Uint8*) number, (Uint16) sub_number);
    }

    return ktrue;
};

SINT _ex_set_MODELCENTER(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    render_center_frame((Uint8*) number, (Uint16) sub_number, (Uint8) new_value);

    return ktrue;
};

SINT _ex_set_MODELDETEXTURE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    remove_all_tex_vertex((Uint8*) number, (Uint16) sub_number);

    return ktrue;
};

SINT _ex_set_MODELPLOPATSTRING(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    sprintf(DEBUG_STRING, "SPACE=Plop %2.2f, %2.2f, %2.2f SHIFT=Snap", selection.center_xyz[XX], selection.center_xyz[YY], selection.center_xyz[ZZ]);

    return ktrue;
};

SINT _ex_set_MODELMARKFRAME(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    render_marked_frame = (Uint16) sub_number;

    return render_marked_frame;
};

SINT _ex_set_MODELCOPYFRAME(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    render_copy_frame((Uint8*) number, (Uint16) sub_number);

    return ktrue;
};

SINT _ex_set_MODELAUTOSHADOW(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    render_auto_shadow((Uint8*) number, (Uint16) sub_number, (Uint8) new_value);

    return ktrue;
};
#endif


SINT _ex_set_WATERLAYERSACTIVE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    water_layers_active = (Uint8) new_value;

    return water_layers_active;
};

SINT _ex_set_CARTOONMODE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    line_mode = (Uint8) new_value;

    return line_mode;
};

SINT _ex_set_KANJICOPY(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
#ifdef DEVTOOL
    kanji_copy_from = number;
#endif

    return kanji_copy_from;
};

SINT _ex_set_KANJIPASTE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
#ifdef DEVTOOL
    Uint8 *ptmp_a, *ptmp_b;
    int    i;
    int    tmp_i, tmp_j;

    if (kanji_data)
    {
        i = endian_read_mem_int16(kanji_data);

        if (kanji_copy_from < i && number < i)
        {
            // Figure out where copy from is...
            ptmp_a = kanji_data + endian_read_mem_int32(kanji_data + 2 + (kanji_copy_from << 2));

            // Figure out where new one goes...  One we're going to write after...
            ptmp_b =  kanji_data + endian_read_mem_int32(kanji_data + 2 + (number << 2));

            // Determine size of copy from...
            i = *ptmp_a;
            tmp_i = *(ptmp_a + (i << 1) + 1);
            i = (i << 1) + 1 + (tmp_i * 3) + 1;


            // Figure out where to write...
            sub_number = *ptmp_b;
            ptmp_b += (sub_number << 1) + 1;
            sub_number = *ptmp_b;
            ptmp_b += (sub_number * 3) + 1;


            // Add a new offset block...
            number++;

            if (sdf_insert_data(kanji_data + 2 + (number << 2), NULL, 4))
            {
                // Bump back the pointers...
                ptmp_a += 4;
                ptmp_b += 4;

                if (kanji_copy_from >= number)
                {
                    kanji_copy_from++;
                }


                // Fill in the offset...
                sub_number = ptmp_b - kanji_data;
                endian_write_mem_int32(kanji_data + 2 + (number << 2), (UINT) sub_number);

                // Add the new kanji block...
                if (sdf_insert_data(ptmp_b, NULL, i))
                {
                    // Go through all offsets, bumping them back as needed...
                    tmp_i = endian_read_mem_int16(kanji_data) + 1;
                    repeat(sub_number, tmp_i)
                    {
                        // Find offset for kurrent kanji...
                        tmp_j = endian_read_mem_int32(kanji_data + 2 + (sub_number << 2));

                        if (sub_number < number)
                        {
                            // Comes before added kanji...  Still need to bump back because we added an offset...
                            tmp_j += 4;
                        }
                        else if (sub_number > number)
                        {
                            // Comes after added kanji...  Have to bump back for offset and data...
                            tmp_j += 4 + i;
                        }

                        if (sub_number == kanji_copy_from)
                        {
                            ptmp_a = kanji_data + tmp_j;
                        }

                        endian_write_mem_int32(kanji_data + 2 + (sub_number << 2), (UINT) tmp_j);
                    }


                    // Fill in the new kanji block...
                    memcpy(ptmp_b, ptmp_a, i);


                    // Increase the kanji count...
                    endian_write_mem_int16(kanji_data, (Uint16) tmp_i);
                }
            }
        }
    }

#endif

    return NULL != kanji_data;
};

SINT _ex_set_KANJIDELETE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
#ifdef DEVTOOL
    int i;
    int tmp_j, tmp_k;

    Uint8 *ptmp_b;

    if (kanji_data)
    {
        i = endian_read_mem_int16(kanji_data);

        if (number < i && i > 1)
        {
            // Figure out which one we're deleting...
            ptmp_b =  kanji_data + endian_read_mem_int32(kanji_data + 2 + (number << 2));

            // Determine size of deletion...
            sub_number = *ptmp_b;
            tmp_k = *(ptmp_b + (sub_number << 1) + 1);
            sub_number = (sub_number << 1) + 1 + (tmp_k * 3) + 1;


            // Delete the kanji block...
            if (sdf_insert_data(ptmp_b, NULL, -sub_number))
            {
                // Delete the offset block...
                if (sdf_insert_data(kanji_data + 2 + (number << 2), NULL, -4))
                {
                    // Update the number of kanji...
                    i--;
                    endian_write_mem_int16(kanji_data, (Uint16) i);


                    // Go through all offsets, bumping them as needed...
                    tmp_k = sub_number;
                    repeat(sub_number, i)
                    {
                        // Find offset for kurrent kanji...
                        tmp_j = endian_read_mem_int32(kanji_data + 2 + (sub_number << 2));

                        if (sub_number < number)
                        {
                            // Comes before deleted kanji...  Still need to bump because we deleted an offset...
                            tmp_j -= 4;
                        }
                        else
                        {
                            // Comes after deleted kanji...  Have to bump for offset and data...
                            tmp_j -= 4 + tmp_k;
                        }

                        endian_write_mem_int32(kanji_data + 2 + (sub_number << 2), (UINT) tmp_j);
                    }
                }
            }
        }
    }

#endif

    return NULL != kanji_data;
};

SINT _ex_set_BLOCKKEYBOARD(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    global_block_keyboard_timer = (Uint16) new_value;

    return global_block_keyboard_timer;
};

#ifdef DEVTOOL
SINT _ex_set_BILLBOARDACTIVE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    global_billboard_active = (Uint8) new_value;

    return global_billboard_active;
};
#endif

SINT _ex_set_JOINGAME(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // number is the continent, sub_number is the direction, new_value is the letter...

    cl_transmit_join();

    return ktrue;
};

SINT _ex_set_STARTGAME(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    int i;

    play_game_active = ktrue;
    log_info(0, "Starting game");
    repeat(i, MAX_LOCAL_PLAYER)
    {
        repeat(number, 4)
        {
            player_device[i].button_pressed[number] = kfalse;
            player_device[i].button_unpressed[number] = kfalse;
        }
    }

    return play_game_active;
};

SINT _ex_set_LEAVEGAME(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    play_game_active = kfalse;
    main_game_active = kfalse;

    cl_transmit_quit();

    return ktrue;
};

SINT _ex_set_LOCALPLAYER(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // number is the local player number (0-3), new_value is the character controlled by that player...

    bool_t retval = kfalse;

    number %= MAX_LOCAL_PLAYER;
    local_player_character[number] = new_value;

    if ( VALID_CHR_RANGE(new_value) )
    {
        chr_data_get_ptr_raw( new_value )->reserve_on = ktrue;
        retval = ktrue;
    }

    return retval;
};

SINT _ex_set_SCREENSHAKE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // sub_number is the shake amount (1 - 1000), new_value is the shake timer (60 is 1 second)...
    int   i;
    FLOT e;

    number = kfalse;  // Did we find any local players still playin'?
    repeat(i, MAX_LOCAL_PLAYER)
    {
        // Is this player active?
        if (!player_device[i].type) continue;

        // Find this player's character
        if ( NULL != chr_data_get_ptr(local_player_character[i]) )
        {
            number = ktrue;
            i = MAX_LOCAL_PLAYER;
        }
    }

    if (number)
    {
        e = sub_number * 0.001f;

        if (e >= screen_shake_amount || screen_shake_timer < 1)
        {
            screen_shake_amount = e;
            screen_shake_timer = new_value;
        }
    }

    return ktrue;
};

SINT _ex_set_INCLUDEPASSWORD(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // !!!BAD!!!
    // !!!BAD!!!  Stupid...
    // !!!BAD!!!

    return kfalse;
};

SINT _ex_set_RANDOMSEED(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // new_value is the new random seed...
    g_rand.next = (Uint16) new_value;

    return g_rand.next;
};

SINT _ex_set_MIPMAPACTIVE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // new_value is either ktrue or kfalse
    mip_map_active = ((Uint8) new_value) & 1;

    return mip_map_active;
};

SINT _ex_set_WATERTEXTURE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // number is ktrue for shimmer, kfalse for water...
    // sub_number is the texture to set (0 to MAX_WATER_FRAME-1)
    // new_value is the RGB file

    bool_t retval = kfalse;

    if ( (GLuint)sub_number != (~(GLuint)0) && sub_number < MAX_WATER_FRAME)
    {
        if (number)
        {
            texture_shimmer[sub_number] = DEREF( UINT, new_value + 2 );
        }
        else
        {
            texture_water[sub_number] = DEREF( UINT, new_value + 2 );
        }

        retval = ktrue;
    }

    return retval;
};

SINT _ex_set_PLAYERCONTROLHANDLED(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    log_error(0, "SYS_PLAYERCONTROLHANDLED Called...");

    return kfalse;
};

SINT _ex_set_GLOBALSPAWN(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // Sets one of the global_spawn variables...
    // sub_number is the variable to set...
    // new_value is the new value...

    bool_t retval = ktrue;

    switch (sub_number)
    {
        case GLOBAL_SPAWN_OWNER:   pss->spawn_info.owner   = (Uint16) new_value; break;
        case GLOBAL_SPAWN_TARGET:  pss->spawn_info.target  = (Uint16) new_value; break;
        case GLOBAL_SPAWN_TEAM:    pss->spawn_info.team    = (Uint8)  new_value; break;
        case GLOBAL_SPAWN_SUBTYPE: pss->spawn_info.subtype = (Uint8)  new_value; break;
        case GLOBAL_SPAWN_CLASS:   pss->spawn_info.cclass   = (Uint8)  new_value; break;
        default: retval = kfalse; break;
    }

    return retval;
};

SINT _ex_set_GLOBALATTACKSPIN(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // Sets the global attack spin...
    // new_value is the new value...
    attack_info.spin = (Uint16) new_value;

    return attack_info.spin;
};

SINT _ex_set_GLOBALATTACKER(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // Sets the global attacker...
    // new_value is the new value...
    attack_info.index = (Uint16) new_value;

    return attack_info.index;
};

SINT _ex_set_CURRENTITEM(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // Sets the current item to a specific value...  Used to prevent conditional item problem in
    // windows (click items get renumbered if something like a button is only present sometimes,
    // can be weird...)
    // new_value is the new value...
    pss->self.item = new_value;

    return pss->self.item;
};

SINT _ex_set_ITEMREGISTRYCLEAR(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // Clears all item types from the item registry...
    item_type_setup();

    return ktrue;
};

SINT _ex_set_ITEMREGISTRYSCRIPT(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // Registers a new item's script...
    // number is the item type index number...
    // new_value is the script file (.RUN)...
    number %= MAX_ITEM_TYPE;
    item_list[number].script = (Uint8*) new_value;

    return CAST(SINT, item_list[number].script);
};

SINT _ex_set_ITEMREGISTRYICON(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // Registers a new item's icon...
    // number is the item type index number...
    // new_value is the image file (.RGB)...
    number %= MAX_ITEM_TYPE;
    item_list[number].icon = (Uint8*) new_value;

    return CAST(SINT, item_list[number].icon);
};

SINT _ex_set_ITEMREGISTRYOVERLAY(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // Registers a new item's icon overlay...
    // number is the item type index number...
    // new_value is the image file (.RGB)...
    number %= MAX_ITEM_TYPE;
    item_list[number].overlay = (Uint8*) new_value;

    return CAST(SINT, item_list[number].overlay);
};

SINT _ex_set_ITEMREGISTRYPRICE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // Registers a new item's shop price...
    // number is the item type index number...
    // new_value is the price (Sint16)
    number %= MAX_ITEM_TYPE;
    item_list[number].price = (Sint16) new_value;

    return item_list[number].price;
};

SINT _ex_set_ITEMREGISTRYFLAGS(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // Registers a new item's flags...
    // number is the item type index number...
    // new_value is the flags (16 bits)...
    number %= MAX_ITEM_TYPE;
    item_list[number].flags = (Uint16) new_value;

    return item_list[number].flags;
};

SINT _ex_set_ITEMREGISTRYSTR(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // Registers a new item's minimum strength requirement...
    // number is the item type index number...
    // new_value is the requirement (8 bits)...
    number %= MAX_ITEM_TYPE;
    item_list[number].istr = (Uint8) new_value;

    return item_list[number].istr;
};

SINT _ex_set_ITEMREGISTRYDEX(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // Registers a new item's minimum dexterity requirement...
    // number is the item type index number...
    // new_value is the requirement (8 bits)...
    number %= MAX_ITEM_TYPE;
    item_list[number].idex = (Uint8) new_value;

    return item_list[number].idex;
};

SINT _ex_set_ITEMREGISTRYINT(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // Registers a new item's minimum intelligence requirement...
    // number is the item type index number...
    // new_value is the requirement (8 bits)...
    number %= MAX_ITEM_TYPE;
    item_list[number].iint = (Uint8) new_value;

    return item_list[number].iint;
};

SINT _ex_set_ITEMREGISTRYMANA(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // Registers a new item's minimum manamax requirement...
    // number is the item type index number...
    // new_value is the requirement (8 bits)...
    number %= MAX_ITEM_TYPE;
    item_list[number].imana = (Uint8) new_value;

    return item_list[number].imana;
};

SINT _ex_set_ITEMREGISTRYAMMO(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // Registers a new item's ammo number (displayed over top the item's icon)...
    // number is the item type index number...
    // new_value is the number to display (8 bits)...
    number %= MAX_ITEM_TYPE;
    item_list[number].ammo = (Uint8) new_value;

    return item_list[number].ammo;
};

SINT _ex_set_WEAPONGRIP(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // Sets the grip (left or right) for the weapon setup call (& weaponframeevent)...
    // new_value is the grip (MODEL_LEFT_FILE or MODEL_RIGHT_FILE or MODEL_LEFT2_FILE or MODEL_RIGHT2_FILE)
    weapon_setup_grip = (Uint16) new_value;

    return weapon_setup_grip;
};

SINT _ex_set_WEAPONMODELSETUP(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // Calls the ModelSetup callback for the given weapon...

    PSoulfuGlobalScriptContext psgs = pss->psgs;
    int i, j;

    j = (weapon_setup_grip - MODEL_LEFT_FILE) / 24;

    i = -1;
    if( OBJECT_CHR == pss->self.object.type )
    {
        i = pss->self.object.chr->eqslot[j & 3];
    }
    else if ( OBJECT_WIN == pss->self.object.type )
    {
        i = pss->self.object.win->eqslot[j & 3];
    }
    if( -1 == i ) return CAST(SINT, NULL);

    psgs->item_index = i;

    // Clear out the current ModelAssign() thing...
    if( OBJECT_CHR == pss->self.object.type )
    {
        pss->self.object.chr->model.parts[MODEL_PART_LEFT + j].file = NULL;
    }
    else if ( OBJECT_WIN == pss->self.object.type )
    {
        pss->self.object.win->model.parts[MODEL_PART_LEFT + j].file = NULL;
    }

    // Now call that item i's ModelSetup() function...
    if (NULL != item_list[i].script)
    {
        _sf_fast_run_script_obj(pss->psgs, item_list[i].script, FAST_FUNCTION_MODELSETUP, &(pss->self.object));
    }

    return CAST(SINT, item_list[i].script);
};

SINT _ex_set_WEAPONEVENT(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // Calls the Event() callback for the given weapon...

    PSoulfuGlobalScriptContext psgs = pss->psgs;
    int i, j;

    j = (weapon_setup_grip - MODEL_LEFT_FILE) / 24;

    i = -1;
    if( OBJECT_CHR == pss->self.object.type )
    {
        i = pss->self.object.chr->eqslot[j & 3];
    }
    else if ( OBJECT_WIN == pss->self.object.type )
    {
        i = pss->self.object.win->eqslot[j & 3];
    }
    if( -1 == i ) return CAST(SINT, NULL);

    // Now call that item i's Event() function...
    if (NULL != item_list[i].script)
    {
        psgs->item_index = (Uint16) i;
        _sf_fast_run_script_obj(pss->psgs, item_list[i].script, FAST_FUNCTION_EVENT, &(pss->self.object));
    }

    return CAST(SINT, item_list[i].script);
};

SINT _ex_set_WEAPONFRAMEEVENT(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // Calls the FrameEvent() callback for the given weapon...

    PSoulfuGlobalScriptContext psgs = pss->psgs;
    int i, j;

    j = (weapon_setup_grip - MODEL_LEFT_FILE) / 24;

    i = -1;
    if( OBJECT_CHR == pss->self.object.type )
    {
        i = pss->self.object.chr->eqslot[j & 3];
    }
    else if ( OBJECT_WIN == pss->self.object.type )
    {
        i = pss->self.object.win->eqslot[j & 3];
    }
    if( -1 == i ) return CAST(SINT, NULL);


    // Now call that item i's FrameEvent() function...
    if (NULL != item_list[i].script)
    {
        psgs->item_index = (Uint16) i;
        _sf_fast_run_script_obj(pss->psgs, item_list[i].script, FAST_FUNCTION_FRAMEEVENT, &(pss->self.object));
    }

    return CAST(SINT, item_list[i].script);
};

SINT _ex_set_WEAPONUNPRESSED(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // Calls the Unpressed() callback for the given weapon...

    PSoulfuGlobalScriptContext psgs = pss->psgs;
    int i, j;

    j = (weapon_setup_grip - MODEL_LEFT_FILE) / 24;

    i = -1;
    if( OBJECT_CHR == pss->self.object.type )
    {
        i = pss->self.object.chr->eqslot[j & 3];
    }
    else if ( OBJECT_WIN == pss->self.object.type )
    {
        i = pss->self.object.win->eqslot[j & 3];
    }
    if( -1 == i ) return CAST(SINT, NULL);


    // Now call that item i's Unpressed() function...
    if (NULL != item_list[i].script)
    {
        psgs->item_index = (Uint16) i;
        _sf_fast_run_script_obj(pss->psgs, item_list[i].script, FAST_FUNCTION_UNPRESSED, &(pss->self.object));
    }

    return CAST(SINT, item_list[i].script);
};

SINT _ex_set_CHARFASTFUNCTION(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // A generic way of calling any of the FAST_FUNCTION_ type of things, for any character...
    // number is the character index (0-MAX_CHARACTER-1)
    // new_value is the FAST_FUNCTION_ number thing

    bool_t retval = kfalse;

    pxss_fast_run_found = kfalse;

    if ( VALID_CHR_RANGE(number) )
    {
        new_value = new_value & 254;

        if (new_value < (MAX_FAST_FUNCTION << 1))
        {
            pxss_looking_for_fast_run = ktrue;

            if( chr_data_get_ptr_raw( number )->on || chr_data_get_ptr_raw( number )->reserve_on )
            {
                sf_fast_rerun_script( &(chr_data_get_ptr_raw( number )->script_ctxt), new_value );
                retval = ktrue;
            }
        }
    }

    return retval;
};

SINT _ex_set_FASTANDUGLY(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // Turns on/off bilinear filtering and stuff...
    fast_and_ugly_active = (Uint8) new_value;

    return fast_and_ugly_active;
};

SINT _ex_set_DEFENSERATING(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // Accumulates a global defense rating in one of several damage type categories...
    // sub_number is the damage type
    // new_value is the defense amount

    attack_info.defense[sub_number%MAX_DAMAGE_TYPE] += (Sint8) new_value;

    return ktrue;
};

SINT _ex_set_CLEARDEFENSERATING(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // Clears out the accumulated defense ratings...
    repeat(new_value, MAX_DAMAGE_TYPE)
    {
        attack_info.defense[new_value] = 0;
    }

    return ktrue;
};

SINT _ex_set_ITEMDEFENSERATING(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // Calls the DefenseRating() function for a given item type...
    // new_value is the item type

    bool_t retval = kfalse;
    PSoulfuGlobalScriptContext psgs = pss->psgs;

    if (NULL != item_list[new_value&255].script)
    {
        psgs->item_index = (Uint16) new_value;
        _sf_fast_run_script_obj(pss->psgs, item_list[new_value&255].script, FAST_FUNCTION_DEFENSERATING, &(pss->self.object));
        retval = ktrue;
    }

    return retval;
};

SINT _ex_set_CAMERAANGLE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // !!!BAD!!!
    // !!!BAD!!!
    // !!!BAD!!!  Make movie record/playback function instead...
    // !!!BAD!!!
    // !!!BAD!!!
    // Sets the x rotation for the camera...
    // new_value is the new angle...
    //                        camera.to_rotation_xy[XX] = (Uint16) new_value;
    // !!!BAD!!!
    // !!!BAD!!!
    // !!!BAD!!!

    return kfalse;
};

SINT _ex_set_STARTFADE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // Starts one of the special screen fades...
    // sub_number is the fade type...
    // new_value is the color
    color_temp[0] = (new_value >> 16) & 255;
    color_temp[1] = (new_value >> 8) & 255;
    color_temp[2] = (new_value) & 255;
    color_temp[3] = 255;

    if (sub_number == 255)
    {
        // Special for camera...
        display_start_fade(FADE_TYPE_FULL, FADE_OUT*16, 0.5f * DEFAULT_W, 0.5f * DEFAULT_H, color_temp);
    }
    else
    {
        display_start_fade((Uint8) sub_number, FADE_OUT, 0.5f * DEFAULT_W, 0.5f * DEFAULT_H, color_temp);
    }

    return 0;
};

SINT _ex_set_ROOMUNCOMPRESS(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // Loads the specified room into the roombuffer...
    // number is the SRF file for the room layout
    // sub_number is the DDD file for walls & pillars
    // new_value is the map room number (or -1 to force all doors to spawn for test room)...

    OBJECT_PTR obj_ptr;

    if (new_value > -1 && new_value < num_map_room)
    {
        // Actual room...
        room_uncompress((Uint8*) number, roombuffer, (Uint8*) sub_number, DEREF( Uint16, map_room_data[new_value] + 8 ), (map_room_data[new_value] + 24), (map_room_data[new_value] + 32), map_room_data[new_value][30], map_room_data[new_value][10], map_room_data[new_value][10]);
    }
    else
    {
        // Test room...
        room_uncompress((Uint8*) number, roombuffer, (Uint8*) sub_number, 0, NULL, NULL, 0, 0, global_room_active_group);
    }

    obj_ptr = pss->self.object;                      // Backup...
    number  = pss->self.item;                        // Backup...
    {
        character_update_all(pss);
    }
    focus_set( &(pss->self), &obj_ptr, number);

    global_room_changed = ktrue;
    main_timer_length = 32;  // Reset the timer to do a few frames...

    return ktrue;
};

#ifdef DEVTOOL
SINT _ex_set_ROOMPLOPATSTRING(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    sprintf(NAME_STRING, "%3.2f, %3.2f, %3.2f", selection.center_xyz[XX], selection.center_xyz[YY], selection.center_xyz[ZZ]);

    return ktrue;
};

SINT _ex_set_ROOMSELECT(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    bool_t retval = kfalse;

    if (number == SELECT_CONNECTED)
    {
        selection.close_type = BORDER_SPECIAL_SELECT_CONNECTED;
    }
    else if (number == SELECT_INVERT)
    {
        selection.close_type = BORDER_SPECIAL_SELECT_INVERT;
    }
    else if (number == SELECT_ALL)
    {
        if (new_value)
        {
            selection.close_type = BORDER_SPECIAL_SELECT_ALL;
        }
        else
        {
            room_select_clear();
        }
    }
    else
    {
        retval = kfalse;
    };

    return retval;
};

SINT _ex_set_ROOMPLOPVERTEX(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // number is the file, new_value is ktrue if you want the vertex to be selected after being plop'd...
    return room_srf_add_vertex((Uint8*) number, selection.center_xyz[XX], selection.center_xyz[YY], selection.center_xyz[ZZ], (Uint8) new_value);
};

SINT _ex_set_ROOMDELETEVERTEX(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    bool_t retval = ktrue;
    int i;

    if (room_select_num > 0)
    {
        i = 50000;

        while (i > 0)
        {
            i--;

            if (room_select_inlist((Uint16) i))
            {
                room_srf_delete_vertex((Uint8*) number, (Uint16) i);
            }
        }

        room_select_num = 0;
        room_srf_autotexture((Uint8*) number);
    }
    else
    {
        retval = kfalse;
    };

    return retval;
};

SINT _ex_set_ROOMWELDVERTICES(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    room_srf_weld_selected_vertices((Uint8*) number);

    return ktrue;
};

SINT _ex_set_ROOMCLEAREXTERIORWALL(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    room_srf_clear_exterior((Uint8*) number, 0, ktrue);

    return ktrue;
};

SINT _ex_set_ROOMPLOPEXTERIORWALL(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    bool_t retval = ktrue;

    if (room_select_num > 0)
    {
        sub_number = room_select_list[room_select_num-1];
        room_srf_add_next_exterior_wall((Uint8*) number, (Uint16) sub_number);
        room_srf_minimap_build((Uint8*) number);
    }
    else
    {
        retval = kfalse;
    };

    return retval;
};

SINT _ex_set_ROOMEXTERIORWALLFLAGS(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    bool_t retval = ktrue;
    int i;

    // number is pointer to srf data, new_value is new flag data...
    if (room_select_num > 1)
    {
        i = room_select_list[room_select_num-2];
        sub_number = room_select_list[room_select_num-1];
        room_srf_exterior_wall_flags((Uint8*) number, (Uint16) i, (Uint16) sub_number, ktrue, (Uint8) new_value);
    }
    else
    {
        retval = kfalse;
    };

    return retval;
};

SINT _ex_set_ROOMPLOPWAYPOINT(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    return room_srf_add_waypoint((Uint8*) number, selection.center_xyz[XX], selection.center_xyz[YY]);

};

SINT _ex_set_ROOMDELETEWAYPOINT(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    bool_t retval = ktrue;
    int i;

    if (room_select_num > 0)
    {
        i = 50000;

        while (i > 0)
        {
            i--;

            if (room_select_inlist((Uint16) i))
            {
                room_srf_delete_waypoint((Uint8*) number, (Uint8) i);
            }
        }

        room_select_num = 0;
    }
    else
    {
        retval = kfalse;
    };

    return retval;
};

SINT _ex_set_ROOMLINKWAYPOINT(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    bool_t retval = ktrue;

    if (room_select_num == 2)
    {
        room_srf_link_waypoint((Uint8*) number, (Uint8) room_select_list[0], (Uint8) room_select_list[1]);
        room_select_num = 0;
    }
    else
    {
        retval = kfalse;
    };

    return retval;
};

SINT _ex_set_ROOMUNLINKWAYPOINT(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    bool_t retval = ktrue;

    if (room_select_num == 2)
    {
        room_srf_unlink_waypoint((Uint8*) number, (Uint8) room_select_list[0], (Uint8) room_select_list[1]);
        room_select_num = 0;
    }
    else
    {
        retval = kfalse;
    };

    return retval;
};

SINT _ex_set_ROOMDELETEBRIDGE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    bool_t retval = ktrue;

    if (room_select_num > 0)
    {
        room_srf_delete_bridge((Uint8*) number, (Uint8) (room_select_list[0] >> 1));
        room_select_num = 0;
    }
    else
    {
        retval = kfalse;
    };

    return retval;
};

SINT _ex_set_ROOMPLOPTRIANGLE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    bool_t retval = ktrue;

    if (room_select_num > 2)
    {
        room_srf_add_texture_triangle((Uint8*) number, (Uint8) sub_number, room_select_list[0], room_select_list[1], room_select_list[2], NULL, NULL, NULL);
        room_select_num = 0;
        room_srf_autotexture((Uint8*) number);
    }
    else
    {
        retval = kfalse;
    };

    return retval;
};

SINT _ex_set_ROOMPLOPFAN(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    bool_t retval = ktrue;
    int i;
    int tmp_i, tmp_k;

    if (room_select_num > 2)
    {
        room_srf_add_texture_triangle((Uint8*) number, (Uint8) sub_number, room_select_list[0], room_select_list[1], room_select_list[2], NULL, NULL, NULL);
        i = room_select_list[0];
        sub_number = (SINT) room_select_data[0];

        tmp_i = room_select_list[2];
        tmp_k = (SINT) room_select_data[2];

        room_select_num = 0;
        room_srf_autotexture((Uint8*) number);
        room_select_add((Uint16) i, (Uint8*) sub_number, 3);
        room_select_add((Uint16) tmp_i, (Uint8*) tmp_k, 3);
    }
    else
    {
        retval = kfalse;
    };

    return retval;
};

SINT _ex_set_ROOMDELETETRIANGLE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    bool_t retval = ktrue;

    if (room_select_num > 2)
    {
        room_srf_delete_triangle((Uint8*) number, room_select_list[0], room_select_list[1], room_select_list[2]);
        room_select_num = 0;
        room_srf_autotexture((Uint8*) number);
    }
    else
    {
        retval = kfalse;
    };

    return retval;
};

SINT _ex_set_ROOMGROUP(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    bool_t retval = ktrue;

    if (sub_number == 0)
    {
        room_group_set((Uint8*) number, (Uint16) new_value);
    }
    else if (sub_number == 1)
    {
        room_group_add((Uint8*) number, kfalse);
    }
    else if (sub_number == 2)
    {
        room_group_add((Uint8*) number, ktrue);
    }
    else
    {
        retval = kfalse;
    };

    return retval;

};

SINT _ex_set_ROOMOBJECT(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    bool_t retval = ktrue;

    if (sub_number == 0)
    {
        room_object_set((Uint8*) number, (Uint16) new_value);
    }
    else if (sub_number == 1)
    {
        room_object_add((Uint8*) number, kfalse);
    }
    else if (sub_number == 2)
    {
        room_object_add((Uint8*) number, ktrue);
    }
    else
    {
        retval = kfalse;
    };

    return retval;
};

SINT _ex_set_ROOMAUTOTEXTURE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    room_srf_autotexture((Uint8*) number);

    return ktrue;
};

SINT _ex_set_ROOMAUTOTRIM(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    FLOT e, f;

    if (new_value == 1)
    {
        // 1 is used to define the length of the wall...
        autotrim_length = 0.0f;
        autotrim_offset = 0.0f;
        autotrim_scaling = WALL_TEXTURE_SCALE;

        if (room_select_num > 1)
        {
            repeat(sub_number, room_select_num)
            {
                number = (sub_number + 1) % room_select_num;
                e = room_select_xyz[number][XX] - room_select_xyz[sub_number][XX];
                f = room_select_xyz[number][YY] - room_select_xyz[sub_number][YY];
                autotrim_length += SQRT(e * e + f * f);
            }

            if (autotrim_length > 0.0f)
            {
                autotrim_scaling = ((SINT) (autotrim_length * WALL_TEXTURE_SCALE)) / (autotrim_length * WALL_TEXTURE_SCALE);
                autotrim_scaling *= WALL_TEXTURE_SCALE;
            }
        }

        sprintf(DEBUG_STRING, "Autotrim length == %f", autotrim_length);
    }
    else if (new_value == 5)
    {
        // 5 is used to do the autotrim...
        room_srf_autotrim((Uint8*) number);
    }
    else if (new_value > 1)
    {
        // 2, 3 and 4 are used to set markers...
        room_autotrim_select_count[new_value-2] = room_select_num;
    }

    return ktrue;
};

SINT _ex_set_ROOMTEXTUREFLAGS(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // number is the srf file, sub_number is the texture (0-31), new_value is the new value for the flags...
    return room_srf_textureflags((Uint8*) number, (Uint8) sub_number, ktrue, (Uint8) new_value);
};

SINT _ex_set_ROOMHARDPLOPPER(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // number is the srf file, sub_number is the width/normalization/other info (size in lower 8 bits) (normalization flag in bit 8 (256)) (base vertex ledge flag in bit 9 (512)) (randomize ledge flag in bit 10 (1024)) (ledge height in bits 11,12,13,14,15), new_value is the hard plopper type (0==fence, 1==stairs)
    room_srf_hardplopper((Uint8*) number, (Uint8) (sub_number&255), (Uint8) ((sub_number >> 8)&1), (Uint8) new_value, (Uint8) ((sub_number >> 9)&1), (Uint8) ((sub_number >> 10)&1), (Uint8) (((sub_number >> 11)&31) + 2));

    return ktrue;
};

SINT _ex_set_ROOMCOPYPASTE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // number is the srf file, new_value is ktrue for paste (copy otherwise)...
    if (new_value)
    {
        room_srf_paste((Uint8*) number);
    }
    else
    {
        room_srf_copy((Uint8*) number);
    }

    return ktrue;
};
#endif

SINT _ex_set_MOUSETEXT(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // new_value is a pointer to a text string...
    strcpy(mouse.text, (char *) new_value);

    return (SINT)mouse.text;
};

SINT _ex_set_BUMPABORT(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // Stops the hard coded collision stuff after a collision event...
    global_bump_abort = ktrue;

    return global_bump_abort;
};

#ifdef DEVTOOL
SINT _ex_set_MODELAUTOVERTEX(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // Sets the shadow vertex to apply the auto shadow for...  (for modeler...)
    // new_value is the vertex (255 for all)...
    global_autoshadow_vertex = (Uint8) new_value;

    return global_autoshadow_vertex;
};
#endif

SINT _ex_set_ITEMINDEX(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // Sets the index of the item that the current script is for...
    // Used to figure out if weapons & armor are enchanted variants of main type...

    PSoulfuGlobalScriptContext psgs = pss->psgs;

    psgs->item_index = (Uint8) new_value;

    return psgs->item_index;
};

SINT _ex_set_WEAPONREFRESHXYZ(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // Finds a random XYZ location at which to spawn an enchantment effect
    // particle.  Should only be called during a weapon item's Refresh() function...

    PSoulfuGlobalScriptContext psgs = pss->psgs;
    int i;

    weapon_refresh_xyz[XX] = 0.0f;
    weapon_refresh_xyz[YY] = 0.0f;
    weapon_refresh_xyz[ZZ] = -500.0f;

    i = ptr_get_idx(&(pss->self.object), OBJECT_CHR);

    if (i != UINT16_MAX)
    {
        item_find_random_xyz(pss, (Uint16) i, psgs->item_bone_name);
    }

    return i != UINT16_MAX;
};

SINT _ex_set_WEAPONREFRESHFLASH(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // Alters the color of the current character's weapon slot, so that it flashes...
    // Should only be called during a weapon item's Refresh() function...
    // number is the first color...
    // sub_number is the second color...
    // new_value is the number of clock ticks for a full color cycle...

    bool_t retval = kfalse;
    PSoulfuGlobalScriptContext psgs = pss->psgs;
    Uint8 *ptmp_b;
    FLOT e, f;

    if (psgs->item_bone_name == LEFT_BONE || psgs->item_bone_name == RIGHT_BONE)
    {
        ptmp_b = CAST(Uint8*, pss->self.object.bas) + (psgs->item_bone_name * 24) + 516;

        if (new_value > 0)
        {
            // Flashing color
            e = sine_table[(((main_game_frame%new_value)<<12)/new_value) & 4095];
            e = (e + 1.0f) * 0.5f;  // Should now range from 0.0 to 1.0...
            f = 1.0f - e;
            new_value = ((Uint8) ((number & 255) * e)) + ((Uint8) ((sub_number & 255) * f));
            new_value |= (((Uint8) (((number >> 8) & 255) * e)) + ((Uint8) (((sub_number >> 8) & 255) * f))) << 8;
            new_value |= (((Uint8) (((number >> 16) & 255) * e)) + ((Uint8) (((sub_number >> 16) & 255) * f))) << 16;
            DEREF( int, ptmp_b ) = new_value;
        }
        else
        {
            // Single color
            DEREF( int, ptmp_b ) = number;
        }

        retval = ktrue;
    }

    return retval;
};

SINT _ex_set_FASTFUNCTION(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // A generic way of calling any of the FAST_FUNCTION_ type of things, for any script...
    // number is the object data (ie. self, target, etc.)
    // sub_number is the script file start (ie. "FILE:MAPGEN.RUN")
    // new_value is the FAST_FUNCTION_ number thing (ie. FAST_FUNCTION_SETUP)

    new_value = new_value & 254;
    pxss_fast_run_found = kfalse;

    if (new_value < (MAX_FAST_FUNCTION << 1))
    {
        if (((Uint8*) number) != NULL && ((Uint8*) sub_number) != NULL)
        {
            pxss_looking_for_fast_run = ktrue;
            sf_fast_run_script(((Uint8*) sub_number), new_value, (Uint8*)number);
        }
    }

    return pxss_fast_run_found;
};

SINT _ex_set_KEEPITEM(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // new_value is the new value of the global variable...

    PSoulfuGlobalScriptContext psgs = pss->psgs;

    psgs->item_keep = new_value;

    return psgs->item_keep;
};

SINT _ex_set_MAKEINPUTACTIVE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // Makes the last WindowInput the active one...
    focus_set(&(last_wi.input), &(pss->self.object), pss->self.item - 1);
    input_quick_reset_key_buffer();

    return ktrue;
};


#ifdef DEVTOOL
SINT _ex_set_GNOMIFYVECTOR(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // Sets the g'nomify working direction between two joints on a model...  new_value has both joints stacked in it...
    int tmp_i, tmp_j;

    tmp_i = (Uint16) (((UINT)new_value) & UINT16_MAX);  // joint one (start of vector)
    tmp_j = (Uint16) (((UINT)new_value) >> 16);  // joint two (end of vector)
    render_gnomify_working_direction((Uint8*) number, (Uint16) sub_number, (Uint16) tmp_i, (Uint16) tmp_j);

    return ktrue;
};

SINT _ex_set_GNOMIFYJOINT(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // Moves a joint around by a specified amount...  new_value has joint number and percentage stacked in it...

    int tmp_i, tmp_j;

    tmp_i = (Uint16) (((UINT)new_value) & UINT16_MAX);  // joint number
    tmp_j = (Uint16) (((UINT)new_value) >> 16);  // percentage
    render_gnomify_affect_joint((Uint8*) number, (Uint16) sub_number, (Uint16) tmp_i, (Uint8) tmp_j);

    return ktrue;
};

SINT _ex_set_JOINTFROMVERTEX(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // Moves a joint to the location of a vertex...  Assumes last joint and last vertex are used...
    render_joint_from_vertex_location((Uint8*) number, (Uint16) sub_number);

    return ktrue;
};
#endif

SINT _ex_set_MESSAGESIZE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // Sets the number of characters across that should be printed in the message window...
    message_size = (Uint16) new_value;

    return message_size;
};

SINT _ex_set_LASTINPUTCURSORPOS(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // Funky thing to manually adjust the cursor position for an input window (for scrolling typer-inner of message window)
    last_wi.cursor_pos = (Sint8) new_value;

    return last_wi.cursor_pos;
};

SINT _ex_set_MESSAGERESET(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // Clears out all of the messages in the message window...
    message_reset();

    return ktrue;
};

SINT _ex_set_FASTFUNCTIONFOUND(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // new_value is either ktrue or kfalse...
    pxss_fast_run_found = new_value;

    return pxss_fast_run_found;
};

SINT _ex_set_INPUTACTIVE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // Sets the input active timer...
    input_active = (Uint8) new_value;

    return input_active;
};

SINT _ex_set_LOCALPLAYERINPUT(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // Turns local player input on or off temporarily...  Used for joystick inventory access...
    // number is the local player number (0-3), new_value is ktrue to turn input on, kfalse to turn it off...
    number %= MAX_LOCAL_PLAYER;
    player_device[number].controls_active = new_value;

    return new_value;
};

SINT _ex_set_SANDTEXTURE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // new_value is the RGB file
    texture_sand = DEREF( UINT, new_value + 2 );

    return texture_sand;
};

SINT _ex_set_PAYINGCUSTOMER(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // new_value is either ktrue or kfalse
    paying_customer = (Uint8) new_value;

    return paying_customer;
};

SINT _ex_set_FILESETFLAG(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // number is the file name string (like "WROOMER.SRC" or something)
    // new_value is the flag to set (like SDF_FLAG_WAS_UPDATED or something)
    SDF_PHEADER parchive = sdf_archive_find_header((char*) number);
    sdf_file_add_flags(parchive, (Uint8) new_value);

    return NULL != parchive;
};

SINT _ex_set_ENCHANTCURSOR(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // This function is called by an enchantment item script to setup the mouse
    // cursor for picking the enchantment's target...  If the character using
    // this script is an AI, then the character's self.target is used as the
    // target for the spell and it is cast right away...  new_value should be the
    // pointer to the casting character's data...  number and sub_number are usually 0 and 0,
    // but if they are set, number specifies the target's index and sub_number specifies the
    // target's item and the spell is cast right away (used that way in WSTATUS.SRC)...

    PSoulfuGlobalScriptContext psgs = pss->psgs;
    int i;
    int tmp_i, tmp_j;
    OBJECT_PTR otmp;
    CHR_DATA * new_chr;

    i = UINT16_MAX;

    // is new_value a character pointer?
    ptr_set_unk( &otmp, CAST(void*,new_value) );

    new_chr = (CHR_DATA*)ptr_get_ptr(&otmp, OBJECT_CHR);
    if( NULL == new_chr ) return kfalse;

    if (number == 0 && sub_number == 0)
    {
        // Using normal call when spell is first used...
        repeat(sub_number, MAX_LOCAL_PLAYER)
        {
            if (player_device[sub_number].type && VALID_CHR_RANGE(local_player_character[sub_number]) )
            {
                if ( chr_data_get_ptr_raw(local_player_character[sub_number]) == new_chr)
                {
                    if ( new_chr->aitimer == 0)
                    {
                        i = local_player_character[sub_number];
                        sub_number = MAX_LOCAL_PLAYER;
                    }
                }
                else
                {
                    // Check for local helper...
                    if ( new_chr->owner == local_player_character[sub_number])
                    {
                        // The given character data is owned by this player (sub_number)...
                        i = _get_chr_index(new_chr);
                        sub_number = MAX_LOCAL_PLAYER;
                    }
                }
            }
        }

        if (i != UINT16_MAX)
        {
            // The given pointer is a reference to a local player's character data...
            if (pss->enc_cursor.active && (pss->enc_cursor.character == (Uint16) i) && pss->enc_cursor.item == psgs->item_index)
            {
                // Enchant mode was active and same player tried to do it again...  That means they changed
                // their mind and don't really want an enchantment after all...
                pss->enc_cursor.active = kfalse;
            }
            else
            {
                // Remember who's calling for the enchantment...
                pss->enc_cursor.active = ktrue;
                pss->enc_cursor.character = (Uint16) i;
                pss->enc_cursor.item = psgs->item_index;
            }
        }
        else
        {
            // The given pointer is a reference to a non-player-character's data...
            // Use the AI's self.target as the target and use right away...

            Uint16 idx_tmp  = _get_chr_index(new_chr);

            if (idx_tmp != UINT16_MAX)
            {
                tmp_i = pss->enc_cursor.character;
                tmp_j = pss->enc_cursor.item;
                pss->enc_cursor.character = idx_tmp;
                pss->enc_cursor.item = psgs->item_index;
                pss->enc_cursor.target = new_chr->target;
                pss->enc_cursor.target_item = 0;
                call_enchantment_function(pss);
                pss->enc_cursor.item = tmp_j;
                pss->enc_cursor.character = tmp_i;
            }
        }
    }
    else
    {
        // WSTATUS style call to select an inventory item...
        if (pss->enc_cursor.active)
        {
            pss->enc_cursor.target = number;
            pss->enc_cursor.target_item = sub_number;
            call_enchantment_function(pss);

            if ( kfalse != pxss_var_get_int(&(pss->base.base.return_var)) )
            {
                // Revert to normal cursor if EnchantUsage() returns ktrue
                pss->enc_cursor.active = kfalse;
            }

            mouse.pressed[BUTTON0] = kfalse;
        }
    }


    return ktrue;
};

SINT _ex_set_FLIPPAN(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // Used to tell the sound routines whether the left & right speakers are
    // backwards or not...  If new_value is ktrue, they're backwards...
    sound_flip_pan = (Uint8) new_value;

    return sound_flip_pan;
};

SINT _ex_set_MAPCLEAR(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // Clears the map of all rooms...
    map_clear();

    return ktrue;
};

MAP_CREATE_INFO map_create;

SINT _ex_set_MAPROOM(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // Used to add a room to the map...  Called several times to specify all the parameters...
    // sub_number is the parameter to set, new_value is its value...

    bool_t retval = ktrue;

    switch (sub_number)
    {
        case MAP_ROOM_SRF:
            map_create.srf_file = (Uint8*) new_value;
            break;

        case MAP_ROOM_X:
            map_create.x = (FLOT) new_value;
            break;

        case MAP_ROOM_Y:
            map_create.y = (FLOT) new_value;
            break;

        case MAP_ROOM_ROTATION:
            map_create.rotation = (Uint16) new_value;
            break;

        case MAP_ROOM_SEED:
            map_create.seed = (Uint8) new_value;
            break;

        case MAP_ROOM_TWSET:
            map_create.twset = (Uint8) new_value;
            break;

        case MAP_ROOM_LEVEL:
            map_create.level = (Uint8) new_value;
            break;

        case MAP_ROOM_FLAGS:
            map_create.flags = (Uint8) new_value;
            break;

        case MAP_ROOM_DIFFICULTY:
            map_create.difficulty = (Uint8) new_value;
            break;

        case MAP_ROOM_AREA:
            map_create.area = (Uint8) new_value;
            break;

        case MAP_ROOM_FROM_ROOM:
            map_create.from_room = (Uint16) new_value;
            break;

        case MAP_ROOM_MULTI_CONNECT:
            map_create.multi_connect = (Uint8) new_value;
            break;

        case MAP_ROOM_CURRENT:
            map_current_room = (Uint16) new_value;
            break;

        case MAP_ROOM_DOOR_PUSHBACK:
            map_room_door_pushback = new_value * 0.01f;
            break;

        case MAP_ROOM_LAST_TOWN:
            map_last_town_room = (Uint16) new_value;
            break;

        case MAP_ROOM_REMOVE:
            // Remove the last room added...
            map_remove_room();
            break;

        case MAP_ROOM_UPDATE_FLAGS:

            // Hrmmm...  Here we're actually modifying room data, instead of
            // buffering it for later...  number is our room number...
            if (number < num_map_room)
            {
                map_room_data[number][13] = (Uint8) new_value;
            }

            break;

        case MAP_ROOM_ADD:
            map_create_room(&map_create);
            break;

        default:
            retval = kfalse;
    }

    return retval;
};

SINT _ex_set_MAPAUTOMAPPRIME(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // Generates a list of rooms to draw for the automap...  number is the camera x position, sub_number is
    // the camera y position, new_value is the current level...

    map_automap_prime((FLOT) number, (FLOT) sub_number, (Uint8) new_value, 800.0f);

    return ktrue;
};

SINT _ex_set_MAPAUTOMAPDRAW(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // Draws all of the rooms for the automap...  Equivalent to calling Window3DRoom() many times,
    // so make sure camera location & everything are specified first...

    map_automap_draw(pss);

    return ktrue;
};

SINT _ex_set_MAPOBJECTRECORD(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // Records which room objects have been poof'd
    bool_t retval = kfalse;

    if (map_current_room < num_map_room)
    {
        repeat(number, 8)
        {
            map_room_data[map_current_room][32+number] = 255;
        }
        log_info(1, "Object Record list...");
        repeat(number, MAX_CHARACTER)
        {
            CHR_DATA * num_chr;

            num_chr = chr_data_get_ptr(number);
            if (NULL == num_chr) continue;


            sub_number = num_chr->rm_ob_num;
            log_info(1, "Object %d is on", sub_number);

            if (sub_number < 64)
            {
                retval = kfalse;
                repeat(new_value, MAX_LOCAL_PLAYER)
                {
                    retval = retval || (num_chr->owner == local_player_character[new_value]);
                }

                if (num_chr->team == TEAM_GOOD && retval)
                {
                    log_info(1, "Object %d was recorded as off, because it was a TEAM_GOOD helper...", sub_number);
                }
                else
                {
                    map_room_data[map_current_room][32+(sub_number>>3)] = map_room_data[map_current_room][32+(sub_number>>3)] & (255 - (1 << (sub_number & 7)));
                    num_chr->rm_ob_num = 255;
                }
            }
        }

        log_info(1, "Object Record list final...");
        repeat(number, 8)
        {
            log_info(1, "%d", map_room_data[map_current_room][32+number]);
        }
    }

    return ktrue;
};

SINT _ex_set_MAPOBJECTDEFEATED(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // new_value is the character to remove from map...  Usually not needed, unless it's to be removed without poofing (or poof comes later)...

    bool_t retval = kfalse;

    if ( VALID_CHR_RANGE(new_value) )
    {
        chr_data_get_ptr_raw( new_value )->rm_ob_num = 255;

        retval = ktrue;
    }

    return retval;
};

SINT _ex_set_MAPDOOROPEN(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // Used to make the map remember that a door is open...
    // new_value is the door number...

    bool_t retval = kfalse;

    if (map_current_room < num_map_room)
    {
        if (new_value < 5 && new_value >= 0)
        {
            map_room_data[map_current_room][29] |= (1 << new_value);
            number = DEREF( Uint16, map_room_data[map_current_room] + 14 + (new_value << 1) );  // The next room...

            if (number < num_map_room)
            {
                repeat(sub_number, 5)
                {
                    if (DEREF( Uint16, map_room_data[number] + 14 + (sub_number << 1) ) == map_current_room)
                    {
                        map_room_data[number][29] |= (1 << sub_number);
                        sub_number = 5;
                    }
                }
            }

            retval = ktrue;
        }
    }

    return retval;
};

SINT _ex_set_CAMERARESET(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // Makes the camera recenter on the players...

    camera.target_xyz[XX] = 0.0f;
    camera.target_xyz[YY] = 0.0f;
    camera.target_xyz[ZZ] = 5.0f;
    display_camera_position(1, 0.0f, 0.0f);

    return ktrue;
};

SINT _ex_set_RESPAWNCHARACTER(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // Respawns the given character, if it was reserved...
    // new_value is the character index...

    bool_t retval = kfalse;

    if ( VALID_CHR_RANGE(new_value) )
    {
        if ( chr_data_get_ptr_raw( new_value )->reserve_on )
        {
            chr_data_get_ptr_raw( new_value )->on = ktrue;
            retval = ktrue;
        }
    }

    return retval;
};

SINT _ex_set_RESERVECHARACTER(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // Makes a character able to be respawned...
    // new_value is the character index...
    // number is ktrue to reserve the character, kfalse to unreserve it...

    bool_t retval = kfalse;

    if ( VALID_CHR_RANGE(new_value) )
    {
        chr_data_get_ptr_raw( new_value )->reserve_on = (Uint8) number;
    }

    return retval;
};

SINT _ex_set_SWAPCHARACTERS(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // Switches one character's data with another, basically changing
    // their slot numbers...  Used in the morph code to ensure that
    // owners & targets stay the same when the new character is
    // spawn'd...
    // sub_number and new_value are the character indices...

    bool_t retval = kfalse;

    if ( VALID_CHR_RANGE(sub_number) && VALID_CHR_RANGE(new_value) )
    {
        // Swap the data...
        CHR_DATA    tmp_data;

        memcpy(&tmp_data, chr_data_get_ptr_raw( sub_number ), sizeof(CHR_DATA));
        memcpy(chr_data_get_ptr_raw( sub_number ), chr_data_get_ptr_raw( number ), sizeof(CHR_DATA));
        memcpy(chr_data_get_ptr_raw( number ), &tmp_data, sizeof(CHR_DATA));

        retval = ktrue;
    }

    return retval;
};

SINT _ex_set_LUCK(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // Sets the global luck timer...
    global_luck_timer = (Uint16) new_value;

    return global_luck_timer;
};

SINT _ex_set_ROOMTEXTURE(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // Sets one of the 32 textures being used by the currently running room...  Can change textures while running, but that'd be weird...
    // sub_number is the texture to set, new_value is a pointer to the RGB file thing...
    // If number is ktrue it means we're doing textures for WROOMER...  DEVTOOL stuff...

    //bool_t retval = kfalse;
    Uint8 *ptmp_a, *ptmp_b;

    sub_number = sub_number & 31;
    ptmp_a = (Uint8*) new_value;

    if (ptmp_a != NULL)
    {
        ptmp_a += 2;

        if (number)
        {
            // Working with WROOMER stuff...
#ifdef DEVTOOL
            room_editor_texture[sub_number] = DEREF( UINT, ptmp_a );
#endif
        }
        else
        {
            // Normal room texture fill in...
            ptmp_b = roombuffer + (DEREF( UINT, roombuffer + SRF_TEXTURE_OFFSET ));
            ptmp_b += (sub_number << 3);
            DEREF( UINT, ptmp_b ) = DEREF( UINT, ptmp_a );
        }
    }

    return ktrue;
};

SINT _ex_set_DAMAGECHARACTER(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // number is the index of the character to damage...
    // sub_number is (damage type<<16), (damage amount<<8), and (wound amount)
    // new_value is the index of the character who's doing the damage...

    SINT retval = TEAM_NEUTRAL;
    CHR_DATA * pattacker;

    attack_info.index = new_value;

    pattacker = chr_data_get_ptr(new_value);
    if ( NULL != pattacker )
    {
        retval = pattacker->team;  // The attacker's team;

        if (new_value == number)
        {
            // character is attacking itself...
            if (retval == TEAM_GOOD)
            {
                retval = TEAM_MONSTER;
            }
            else
            {
                retval = TEAM_GOOD;
            }
        }
    }

    damage_character(pss, (Uint16) number, (Uint8) (sub_number >> 16), (Uint16) ((sub_number >> 8)&255), (Uint16) (sub_number&255), retval);

    return ktrue;
};

SINT _ex_set_CAMERAZOOM(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // new_value is the new zoom distance...
    camera.to_distance = (FLOT) new_value;

    if (camera.to_distance < MIN_CAMERA_DISTANCE)
    {
        camera.to_distance = MIN_CAMERA_DISTANCE;
    }

    if (camera.to_distance > MAX_CAMERA_DISTANCE)
    {
        camera.to_distance = MAX_CAMERA_DISTANCE;
    }

    return ktrue;
};

SINT _ex_set_CAMERASPIN(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // new_value is the new spin...
    camera.rotation_xy[XX] = (Uint16) new_value;

    return ktrue;
};

SINT _ex_set_ROOMRESTOCK(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
    // Restocks a random object for the given room...
    // new_value is the map room number
    if (new_value > -1 && new_value < num_map_room)
    {
        if (!(map_room_data[new_value][13] & MAP_ROOM_FLAG_BOSS))
        {
            room_restock_monsters(DEREF(Uint8*, map_room_data[new_value]), (map_room_data[new_value] + 32), map_room_data[new_value][30], map_room_data[new_value][10], map_room_data[new_value][10]);
        }
    }

    return ktrue;
};

SINT _ex_set_MODELCHECKHACK(PSoulfuScriptContext pss, int number, int sub_number, SINT new_value)
{
#ifdef DEVTOOL
    int i;

    log_info(0, "Model Check Hack...");
    number = 256;
    repeat(i, 15)
    {
        sub_number = DEREF( int, CAST(Uint8*, pss->self.object.bas) + number );

        if (NULL == sdf_archive_find_header_by_data((Uint8*) sub_number))
        {
            log_error(0, "No match found for RDY slot %d (%x == %u)", i, CAST(Uint8*, pss->self.object.bas) + number, sub_number);
        }

        number += 24;
    }
#endif

    return ktrue;
};

SINT system_get(PScriptContext ps, int type, int number, int sub_number)
{
    SINT retval = 0;

    // ???? tricky stuff ????
    PSoulfuScriptContext pss = (PSoulfuScriptContext)ps;

    switch (type)
    {
        case SYS_INVALID: retval = _ex_get_INVALID(pss, number, sub_number); break;
        case SYS_PLAYERDEVICE: retval = _ex_get_PLAYERDEVICE(pss, number, sub_number); break;
        case SYS_NUMJOYSTICK: retval = _ex_get_NUMJOYSTICK(pss, number, sub_number); break;
        case SYS_WINDOWSCALE: retval = _ex_get_WINDOWSCALE(pss, number, sub_number); break;
        case SYS_KEYPRESSED: retval = _ex_get_KEYPRESSED(pss, number, sub_number); break;
        case SYS_KEYDOWN: retval = _ex_get_KEYDOWN(pss, number, sub_number); break;
        case SYS_TOPWINDOW: retval = _ex_get_TOPWINDOW(pss, number, sub_number); break;
        case SYS_SFXVOLUME: retval = _ex_get_SFXVOLUME(pss, number, sub_number); break;
        case SYS_MUSICVOLUME: retval = _ex_get_MUSICVOLUME(pss, number, sub_number); break;
        case SYS_MESSAGE: retval = _ex_get_MESSAGE(pss, number, sub_number); break;
        case SYS_USERLANGUAGE: retval = _ex_get_USERLANGUAGE(pss, number, sub_number); break;
        case SYS_FILENAME: retval = _ex_get_FILENAME(pss, number, sub_number); break;
        case SYS_FILESIZE: retval = _ex_get_FILESIZE(pss, number, sub_number); break;
        case SYS_FILEFTPFLAG: retval = _ex_get_FILEFTPFLAG(pss, number, sub_number); break;
        case SYS_FILECOUNT: retval = _ex_get_FILECOUNT(pss, number, sub_number); break;
        case SYS_FILEFREE: retval = _ex_get_FILEFREE(pss, number, sub_number); break;
        case SYS_DEVTOOL: retval = _ex_get_DEVTOOL(pss, number, sub_number); break;
        case SYS_CURSORPOS: retval = _ex_get_CURSORPOS(pss, number, sub_number); break;
        case SYS_CURSORBUTTONDOWN: retval = _ex_get_CURSORBUTTONDOWN(pss, number, sub_number); break;
#ifdef DEVTOOL
        case SYS_MODELSELECT: retval = _ex_get_MODELSELECT(pss, number, sub_number); break;
        case SYS_MODELTEXFLAGSALPHA: retval = _ex_get_MODELTEXFLAGSALPHA(pss, number, sub_number); break;
        case SYS_MODELFRAMEBASEMODEL: retval = _ex_get_MODELFRAMEBASEMODEL(pss, number, sub_number); break;
        case SYS_MODELFRAMEOFFSETX: retval = _ex_get_MODELFRAMEOFFSETX(pss, number, sub_number); break;
        case SYS_MODELFRAMEOFFSETY: retval = _ex_get_MODELFRAMEOFFSETY(pss, number, sub_number); break;
        case SYS_MODELEXTERNALFILENAME: retval = _ex_get_MODELEXTERNALFILENAME(pss, number, sub_number); break;
        case SYS_MODELSHADOWTEXTURE: retval = _ex_get_MODELSHADOWTEXTURE(pss, number, sub_number); break;
        case SYS_MODELSHADOWALPHA: retval = _ex_get_MODELSHADOWALPHA(pss, number, sub_number); break;
        case SYS_MODELNUMTRIANGLE: retval = _ex_get_MODELNUMTRIANGLE(pss, number, sub_number); break;
        case SYS_MODELNUMCARTOONLINE: retval = _ex_get_MODELNUMCARTOONLINE(pss, number, sub_number); break;
        case SYS_MODELSARELINKABLE: retval = _ex_get_MODELSARELINKABLE(pss, number, sub_number); break;
#endif
        case SYS_MODELFRAMEACTIONNAME: retval = _ex_get_MODELFRAMEACTIONNAME(pss, number, sub_number); break;
        case SYS_MODELFRAMEFLAGS: retval = _ex_get_MODELFRAMEFLAGS(pss, number, sub_number); break;
        case SYS_MODELMAXFRAME: retval = _ex_get_MODELMAXFRAME(pss, number, sub_number); break;
        case SYS_MODELACTIONSTART: retval = _ex_get_MODELACTIONSTART(pss, number, sub_number); break;
        case SYS_LASTKEYPRESSED: retval = _ex_get_LASTKEYPRESSED(pss, number, sub_number); break;
        case SYS_CURSORLASTPOS: retval = _ex_get_CURSORLASTPOS(pss, number, sub_number); break;
        case SYS_CURSORINOBJECT: retval = _ex_get_CURSORINOBJECT(pss, number, sub_number); break;
        case SYS_NUMKANJI: retval = _ex_get_NUMKANJI(pss, number, sub_number); break;
        case SYS_NUMKANJITRIANGLE: retval = _ex_get_NUMKANJITRIANGLE(pss, number, sub_number); break;
        case SYS_CURSORSCREENPOS: retval = _ex_get_CURSORSCREENPOS(pss, number, sub_number); break;
        case SYS_DAMAGEAMOUNT: retval = _ex_get_DAMAGEAMOUNT(pss, number, sub_number); break;
        case SYS_COLLISIONCHAR: retval = _ex_get_COLLISIONCHAR(pss, number, sub_number); break;
        case SYS_MAINVIDEOFRAME: retval = _ex_get_MAINVIDEOFRAME(pss, number, sub_number); break;
        case SYS_MAINGAMEFRAME: retval = _ex_get_MAINGAMEFRAME(pss, number, sub_number); break;
        case SYS_MAINFRAMESKIP: retval = _ex_get_MAINFRAMESKIP(pss, number, sub_number); break;
        case SYS_FINDDATASIZE: retval = _ex_get_FINDDATASIZE(pss, number, sub_number); break;
        case SYS_ARCTANANGLE: retval = _ex_get_ARCTANANGLE(pss, number, sub_number); break;
        case SYS_MAINSERVERLOCATED: retval = _ex_get_MAINSERVERLOCATED(pss, number, sub_number); break;
        case SYS_SHARDLIST: retval = _ex_get_SHARDLIST(pss, number, sub_number); break;
        case SYS_SHARDLISTPING: retval = _ex_get_SHARDLISTPING(pss, number, sub_number); break;
        case SYS_SHARDLISTPLAYERS: retval = _ex_get_SHARDLISTPLAYERS(pss, number, sub_number); break;
        case SYS_VERSIONERROR: retval = _ex_get_VERSIONERROR(pss, number, sub_number); break;
        case SYS_STARTGAME: retval = _ex_get_STARTGAME(pss, number, sub_number); break;
        case SYS_NETWORKON: retval = _ex_get_NETWORKON(pss, number, sub_number); break;
        case SYS_MAINGAMEACTIVE: retval = _ex_get_MAINGAMEACTIVE(pss, number, sub_number); break;
        case SYS_NETWORKGAMEACTIVE: retval = _ex_get_NETWORKGAMEACTIVE(pss, number, sub_number); break;
        case SYS_TRYINGTOJOINGAME: retval = _ex_get_TRYINGTOJOINGAME(pss, number, sub_number); break;
        case SYS_JOINPROGRESS: retval = _ex_get_JOINPROGRESS(pss, number, sub_number); break;
        case SYS_GAMESEED: retval = _ex_get_GAMESEED(pss, number, sub_number); break;
        case SYS_LOCALPASSWORDCODE: retval = _ex_get_LOCALPASSWORDCODE(pss, number, sub_number); break;
        case SYS_NUMNETWORKPLAYER: retval = _ex_get_NUMNETWORKPLAYER(pss, number, sub_number); break;
        case SYS_NETWORKFINISHED: retval = _ex_get_NETWORKFINISHED(pss, number, sub_number); break;
        case SYS_SERVERSTATISTICS: retval = _ex_get_SERVERSTATISTICS(pss, number, sub_number); break;
        case SYS_LOCALPLAYER: retval = _ex_get_LOCALPLAYER(pss, number, sub_number); break;
        case SYS_FPS: retval = _ex_get_FPS(pss, number, sub_number); break;
        case SYS_RANDOMSEED: retval = _ex_get_RANDOMSEED(pss, number, sub_number); break;
        case SYS_GLOBALATTACKSPIN: retval = _ex_get_GLOBALATTACKSPIN(pss, number, sub_number); break;
        case SYS_GLOBALATTACKER: retval = _ex_get_GLOBALATTACKER(pss, number, sub_number); break;
        case SYS_DEBUGACTIVE: retval = _ex_get_DEBUGACTIVE(pss, number, sub_number); break;
        case SYS_DAMAGECOLOR: retval = _ex_get_DAMAGECOLOR(pss, number, sub_number); break;
        case SYS_ITEMREGISTRYSCRIPT: retval = _ex_get_ITEMREGISTRYSCRIPT(pss, number, sub_number); break;
        case SYS_ITEMREGISTRYICON: retval = _ex_get_ITEMREGISTRYICON(pss, number, sub_number); break;
        case SYS_ITEMREGISTRYOVERLAY: retval = _ex_get_ITEMREGISTRYOVERLAY(pss, number, sub_number); break;
        case SYS_ITEMREGISTRYPRICE: retval = _ex_get_ITEMREGISTRYPRICE(pss, number, sub_number); break;
        case SYS_ITEMREGISTRYFLAGS: retval = _ex_get_ITEMREGISTRYFLAGS(pss, number, sub_number); break;
        case SYS_ITEMREGISTRYSTR: retval = _ex_get_ITEMREGISTRYSTR(pss, number, sub_number); break;
        case SYS_ITEMREGISTRYDEX: retval = _ex_get_ITEMREGISTRYDEX(pss, number, sub_number); break;
        case SYS_ITEMREGISTRYINT: retval = _ex_get_ITEMREGISTRYINT(pss, number, sub_number); break;
        case SYS_ITEMREGISTRYMANA: retval = _ex_get_ITEMREGISTRYMANA(pss, number, sub_number); break;
        case SYS_ITEMREGISTRYAMMO: retval = _ex_get_ITEMREGISTRYAMMO(pss, number, sub_number); break;
        case SYS_ITEMREGISTRYNAME: retval = _ex_get_ITEMREGISTRYNAME(pss, number, sub_number); break;
        case SYS_WEAPONGRIP: retval = _ex_get_WEAPONGRIP(pss, number, sub_number); break;
        case SYS_DEFENSERATING: retval = _ex_get_DEFENSERATING(pss, number, sub_number); break;
        case SYS_CURSORBUTTONPRESSED: retval = _ex_get_CURSORBUTTONPRESSED(pss, number, sub_number); break;
        case SYS_MEMBUFFER: retval = _ex_get_MEMBUFFER(pss, number, sub_number); break;
#ifdef DEVTOOL
        case SYS_ROOMSELECT: retval = _ex_get_ROOMSELECT(pss, number, sub_number); break;
        case SYS_ROOMEXTERIORWALLFLAGS: retval = _ex_get_ROOMEXTERIORWALLFLAGS(pss, number, sub_number); break;
        case SYS_ROOMGROUP: retval = _ex_get_ROOMGROUP(pss, number, sub_number); break;
        case SYS_ROOMOBJECT: retval = _ex_get_ROOMOBJECT(pss, number, sub_number); break;
        case SYS_ROOMTEXTUREFLAGS: retval = _ex_get_ROOMTEXTUREFLAGS(pss, number, sub_number); break;
        case SYS_ROOMMETALBOXITEM: retval = _ex_get_ROOMMETALBOXITEM(pss, number, sub_number); break;
#endif
        case SYS_ROOMMONSTERTYPE: retval = _ex_get_ROOMMONSTERTYPE(pss, number, sub_number); break;
        case SYS_ROOMWATERLEVEL: retval = _ex_get_ROOMWATERLEVEL(pss, number, sub_number); break;
        case SYS_ROOMWATERTYPE: retval = _ex_get_ROOMWATERTYPE(pss, number, sub_number); break;
        case SYS_MOUSELASTOBJECT: retval = _ex_get_MOUSELASTOBJECT(pss, number, sub_number); break;
        case SYS_MOUSELASTITEM: retval = _ex_get_MOUSELASTITEM(pss, number, sub_number); break;
        case SYS_MOUSELASTSCRIPT: retval = _ex_get_MOUSELASTSCRIPT(pss, number, sub_number); break;
        case SYS_ITEMINDEX: retval = _ex_get_ITEMINDEX(pss, number, sub_number); break;
        case SYS_WEAPONREFRESHXYZ: retval = _ex_get_WEAPONREFRESHXYZ(pss, number, sub_number); break;
        case SYS_WEAPONREFRESHBONENAME: retval = _ex_get_WEAPONREFRESHBONENAME(pss, number, sub_number); break;
        case SYS_KEEPITEM: retval = _ex_get_KEEPITEM(pss, number, sub_number); break;
        case SYS_FREEPARTICLE: retval = _ex_get_FREEPARTICLE(pss, number, sub_number); break;
        case SYS_MAPSIDENORMAL: retval = _ex_get_MAPSIDENORMAL(pss, number, sub_number); break;
        case SYS_MESSAGESIZE: retval = _ex_get_MESSAGESIZE(pss, number, sub_number); break;
        case SYS_FASTFUNCTIONFOUND: retval = _ex_get_FASTFUNCTIONFOUND(pss, number, sub_number); break;
        case SYS_INPUTACTIVE: retval = _ex_get_INPUTACTIVE(pss, number, sub_number); break;
        case SYS_LOCALPLAYERINPUT: retval = _ex_get_LOCALPLAYERINPUT(pss, number, sub_number); break;
        case SYS_PAYINGCUSTOMER: retval = _ex_get_PAYINGCUSTOMER(pss, number, sub_number); break;
        case SYS_ENCHANTCURSOR: retval = _ex_get_ENCHANTCURSOR(pss, number, sub_number); break;
        case SYS_CHARACTERSCRIPTFILE: retval = _ex_get_CHARACTERSCRIPTFILE(pss, number, sub_number); break;
        case SYS_PARTICLESCRIPTFILE: retval = _ex_get_PARTICLESCRIPTFILE(pss, number, sub_number); break;
        case SYS_MAPROOM: retval = _ex_get_MAPROOM(pss, number, sub_number); break;
        case SYS_MAPDOOROPEN: retval = _ex_get_MAPDOOROPEN(pss, number, sub_number); break;
        case SYS_LOCALPLAYERZ: retval = _ex_get_LOCALPLAYERZ(pss, number, sub_number); break;
        case SYS_RESERVECHARACTER: retval = _ex_get_RESERVECHARACTER(pss, number, sub_number); break;
        case SYS_LUCK: retval = _ex_get_LUCK(pss, number, sub_number); break;
        case SYS_NETWORKSCRIPT: retval = _ex_get_NETWORKSCRIPT(pss, number, sub_number); break;
        case SYS_CAMERAZOOM: retval = _ex_get_CAMERAZOOM(pss, number, sub_number); break;
        case SYS_CAMERASPIN: retval = _ex_get_CAMERASPIN(pss, number, sub_number); break;
        default:
            log_error(0, "OPCODE MISS: system_get %d %d %d\n", type, number, sub_number);
            break;
    }

    return retval;
}

char * _ex_get_language_string(PSoulfuScriptContext pss, int j)
{
    // ZZ> Gives a string from the current .LAN file...  Multilingual support...

    char  * retval;
    Uint8 * temp_file;

    if (j <= user_language_phrases && j > 0)
    {
        temp_file = language_file[user_language] + (j << 2) + 1;

        j  = (*temp_file);  j <<= 8;  temp_file++;
        j += (*temp_file);  j <<= 8;  temp_file++;
        j += (*temp_file);
        retval = j + language_file[user_language];
    }
    else
    {
        retval = bad_string;
    }

    return retval;
}


SINT _ex_get_INVALID(PSoulfuScriptContext pss, int number, int sub_number)
{
    return ktrue;
};

SINT _ex_get_PLAYERDEVICE(PSoulfuScriptContext pss, int number, int sub_number)
{
    // sub_number is the local player number, number is the request type (DEVICE_NUMBER, DEVICE_LEFT_BUTTON, etc.)

    SINT retval = kfalse;

    number %= MAX_LOCAL_PLAYER;

    if (sub_number == PLAYER_DEVICE_TYPE)
    {
        // Current device...
        retval = player_device[number].type;
    }
    else
    {
        // Current button bindings...
        if (sub_number >= 0 && sub_number < MAX_PLAYER_DEVICE_BUTTON)
        {
            retval = player_device[number].button[sub_number];
        }
    }

    return retval;
};

SINT _ex_get_NUMJOYSTICK(PSoulfuScriptContext pss, int number, int sub_number)
{
    return joy_list_count;
};

SINT _ex_get_WINDOWSCALE(PSoulfuScriptContext pss, int number, int sub_number)
{
    // "pss->self.object.win" not used
    //assert(OBJECT_WIN == pss->self.object.type);

    return (SINT) ((pss->screen.scale - 6.0f) * 600.0f);
};

SINT _ex_get_KEYPRESSED(PSoulfuScriptContext pss, int number, int sub_number)
{
    SINT retval = kfalse;

    if (number < MAX_KEY && input_active == 0)
    {
        retval = keyb.win_pressed[number];
    }

    return retval;
};

SINT _ex_get_KEYDOWN(PSoulfuScriptContext pss, int number, int sub_number)
{
    SINT retval = kfalse;

    if (number < MAX_KEY && input_active == 0)
    {
        retval = keyb.down[number];
    }

    return retval;
};

SINT _ex_get_TOPWINDOW(PSoulfuScriptContext pss, int number, int sub_number)
{
    SINT retval = main_used_window_count;

    if (retval > 0)
    {
        retval = main_used_window[main_used_window_count-1];
        retval = ((main_win_data + retval) == pss->self.object.win);
    }

    return retval;
};

SINT _ex_get_SFXVOLUME(PSoulfuScriptContext pss, int number, int sub_number)
{
    return master_sfx_volume;
};

SINT _ex_get_MUSICVOLUME(PSoulfuScriptContext pss, int number, int sub_number)
{
    return master_music_volume;
};

SINT _ex_get_MESSAGE(PSoulfuScriptContext pss, int number, int sub_number)
{
    return (SINT) (message_get(number));
};

SINT _ex_get_USERLANGUAGE(PSoulfuScriptContext pss, int number, int sub_number)
{
    return user_language;
};

SINT _ex_get_FILENAME(PSoulfuScriptContext pss, int number, int sub_number)
{
    SINT retval = kfalse;
    Uint8 ui8;

    if (number < sdf_archive_get_num_files())
    {
        SDF_PHEADER pheader  = sdf_archive_get_header(number);
        char * fname = sdf_file_get_name(pheader);
        ui8       = sdf_file_get_type(pheader);

        if (ui8 != SDF_FILE_IS_UNUSED)
        {
            char * name      = sdf_file_get_name(pheader);
            const char * extension = sdf_file_get_extension(pheader);

            retval = 0;

            while (retval < 8 && *(fname + retval) != 0)
            {
                system_file_name[retval] = *(fname + retval);
                retval++;
            }

            system_file_name[retval] = '.';
            retval++;
            repeat(sub_number, 4)
            {
                system_file_name[retval] = extension[sub_number];
                retval++;
            }
            retval = (SINT) system_file_name;
        }
        else
        {
            retval = (SINT) unused_file_name;
        }
    }
    else
    {
        system_file_name[0] = 0;
        retval = (SINT) system_file_name;
    }

    return retval;
};

SINT _ex_get_FILESIZE(PSoulfuScriptContext pss, int number, int sub_number)
{
    SINT retval = kfalse;
    Uint8 ui8;

    if (number < sdf_archive_get_num_files())
    {
        SDF_PHEADER pheader  = sdf_archive_get_header(number);
        ui8               = sdf_file_get_type(pheader);

        if (ui8 != SDF_FILE_IS_UNUSED)
        {
            sub_number = sdf_file_get_size(pheader);
            sprintf(system_file_name, "%8d", sub_number);
            retval = (SINT) system_file_name;
        }
        else
        {
            sprintf(system_file_name, "%8d", 0);
            retval = (SINT) system_file_name;
        }
    }
    else
    {
        system_file_name[0] = 0;
        retval = (SINT) system_file_name;
    }

    return retval;
};

SINT _ex_get_FILEFTPFLAG(PSoulfuScriptContext pss, int number, int sub_number)
{
    SINT retval = kfalse;
    Uint8 ui8;

    if (number < sdf_archive_get_num_files())
    {
        SDF_PHEADER pheader  = sdf_archive_get_header(number);
        ui8               = sdf_file_get_type(pheader);

        if ((ui8&15) != SDF_FILE_IS_UNUSED)
        {
            retval = ((ui8 & SDF_FLAG_NO_UPDATE) == SDF_FLAG_NO_UPDATE);
        }
    }

    return retval;
};

SINT _ex_get_FILECOUNT(PSoulfuScriptContext pss, int number, int sub_number)
{
    return sdf_archive_get_num_files();
};

SINT _ex_get_FILEFREE(PSoulfuScriptContext pss, int number, int sub_number)
{
    return sdf_archive_free_file_count();
};

SINT _ex_get_DEVTOOL(PSoulfuScriptContext pss, int number, int sub_number)
{
#ifdef DEVTOOL
    return ktrue;
#else
    return kfalse;
#endif
};

SINT _ex_get_CURSORPOS(PSoulfuScriptContext pss, int number, int sub_number)
{
    SINT retval = 0;
    FLOT e;

    if (sub_number == XX)
    {
        e = mouse.x - pss->screen.x;
    }
    else
    {
        e = mouse.y - pss->screen.y;
    }

    if (pss->screen.scale > 0)
    {
        retval = (SINT) (e * 100.0f / pss->screen.scale);
    }
    else
    {
        retval = 0;
    }

    return retval;
};

SINT _ex_get_CURSORBUTTONDOWN(PSoulfuScriptContext pss, int number, int sub_number)
{
    sub_number %= MAX_MOUSE_BUTTON;

    return mouse.down[sub_number];
};

#ifdef DEVTOOL
SINT _ex_get_MODELSELECT(PSoulfuScriptContext pss, int number, int sub_number)
{
    return select_num;
};

SINT _ex_get_MODELTEXFLAGSALPHA(PSoulfuScriptContext pss, int number, int sub_number)
{
    SINT retval = 0;
    Uint8 *ptmp_a;

    ptmp_a = get_start_of_triangles((Uint8*) number, (Uint16) sub_number, (Uint8) (sub_number >> 16));

    if (ptmp_a)
    {
        if (*ptmp_a)
        {
            ptmp_a++;
            retval = *ptmp_a;  // Flags
            retval = retval << 8;
            ptmp_a++;
            retval += *ptmp_a;  // Alpha
        }
    }

    return retval;
};
#endif

SINT _ex_get_MODELMAXFRAME(PSoulfuScriptContext pss, int number, int sub_number)
{
    return DEREF( Uint16, number + 4 );
};

SINT _ex_get_MODELFRAMEFLAGS(PSoulfuScriptContext pss, int number, int sub_number)
{
    SINT retval = 0;
    Uint8 *ptmp_a;

    ptmp_a = get_start_of_frame((Uint8*) number, (Uint16) sub_number);

    if (ptmp_a)
    {
        retval = *(ptmp_a + 1);
    }

    return retval;
};

SINT _ex_get_MODELFRAMEACTIONNAME(PSoulfuScriptContext pss, int number, int sub_number)
{
    SINT retval = 0;
    Uint8 *ptmp_a;

    ptmp_a = get_start_of_frame((Uint8*) number, (Uint16) sub_number);
    retval = 0;

    if (ptmp_a)
    {
        retval = *(ptmp_a);
    }

    return retval;
};

SINT _ex_get_MODELACTIONSTART(PSoulfuScriptContext pss, int number, int sub_number)
{
    SINT retval = UINT16_MAX;
    Uint8 *mdl_file;

    sub_number &= 255;

    if (sub_number < ACTION_MAX)
    {
        mdl_file = CAST(Uint8*, number);

        if (mdl_file == NULL)
        {
            mdl_file = pss->self.object.chr->model.parts[MODEL_PART_BASE].file;
        }

        if (NULL != mdl_file)
        {
            mdl_file += 6;
            retval = ((Uint16*)mdl_file)[sub_number];
        }
    }

    return retval;
};

#ifdef DEVTOOL
SINT _ex_get_MODELFRAMEBASEMODEL(PSoulfuScriptContext pss, int number, int sub_number)
{
    return render_change_frame_base_model((Uint8*) number, (Uint16) sub_number, UINT16_MAX);
};
#endif

SINT _ex_get_MODELFRAMEOFFSETX(PSoulfuScriptContext pss, int number, int sub_number)
{
    SINT retval = 0;
    Uint8 *ptmp_a;

    ptmp_a = get_start_of_frame((Uint8*) number, (Uint16) sub_number);

    if (ptmp_a)
    {
        retval = (SINT) (DEREF( FLOT, ptmp_a + 3 ) * 256.0f);
    }

    return retval;
};

SINT _ex_get_MODELFRAMEOFFSETY(PSoulfuScriptContext pss, int number, int sub_number)
{
    SINT retval = 0;
    Uint8 *ptmp_a;

    ptmp_a = get_start_of_frame((Uint8*) number, (Uint16) sub_number);

    if (ptmp_a)
    {
        retval = (SINT) (DEREF( FLOT, ptmp_a + 7 ) * 256.0f);
    }

    return retval;
};

#ifdef DEVTOOL
SINT _ex_get_MODELEXTERNALFILENAME(PSoulfuScriptContext pss, int number, int sub_number)
{
    return (SINT)render_get_set_external_linkage((Uint8*) number, NULL);
};
#endif

SINT _ex_get_MODELSHADOWTEXTURE(PSoulfuScriptContext pss, int number, int sub_number)
{
    SINT retval = 0;
    Uint8 *ptmp_a;

    ptmp_a = (Uint8*) number;
    ptmp_a += 6 + (ACTION_MAX << 1) + (sub_number & 3);
    retval = *ptmp_a;

    return retval;
};

#ifdef DEVTOOL
SINT _ex_get_MODELSHADOWALPHA(PSoulfuScriptContext pss, int number, int sub_number)
{
    SINT retval = 0;
    Uint8 *ptmp_a;

    ptmp_a = get_start_of_frame_shadows((Uint8*) number, (Uint16) sub_number);

    if (ptmp_a)
    {
        repeat(number, 4)
        {
            if (*ptmp_a)
            {
                retval |= (*ptmp_a >> 4) << (number << 2);
                ptmp_a += 33;
            }
            else
            {
                ptmp_a++;
            }
        }
    }

    return retval;
};

SINT _ex_get_MODELNUMTRIANGLE(PSoulfuScriptContext pss, int number, int sub_number)
{
    SINT retval = 0;
    Uint8 *ptmp_a;
    int m;

    repeat(m, 4)
    {
        ptmp_a = get_start_of_triangles((Uint8*) number, (Uint16) sub_number, (Uint8) m);

        if (ptmp_a)
        {
            if (*ptmp_a)
            {
                ptmp_a += 3;
                retval += DEREF( Uint16, ptmp_a );
            }
        }
    }

    return retval;
};

SINT _ex_get_MODELNUMCARTOONLINE(PSoulfuScriptContext pss, int number, int sub_number)
{
    SINT retval = 0;
    Uint8 *ptmp_a;

    ptmp_a = get_start_of_triangles((Uint8*) number, (Uint16) sub_number, (Uint8) 4);

    if (ptmp_a)
    {
        retval = DEREF( Uint16, ptmp_a );
    }

    return retval;
};

SINT _ex_get_MODELSARELINKABLE(PSoulfuScriptContext pss, int number, int sub_number)
{
    return (get_number_of_bones((Uint8*) number) == get_number_of_bones((Uint8*) sub_number));
};
#endif

SINT _ex_get_LASTKEYPRESSED(PSoulfuScriptContext pss, int number, int sub_number)
{
    SINT retval = keyb.last_pressed;
    keyb.last_pressed = 0;

    return retval;
};

SINT _ex_get_CURSORLASTPOS(PSoulfuScriptContext pss, int number, int sub_number)
{
    SINT retval;
    FLOT e;

    if (sub_number == XX)
    {
        e = mouse.last_x - pss->screen.x;
    }
    else
    {
        e = mouse.last_y - pss->screen.y;
    }

    if (pss->screen.scale > 0)
    {
        retval = (SINT) (e * 100.0f / pss->screen.scale);
    }
    else
    {
        retval = 0;
    }

    return retval;
};

SINT _ex_get_CURSORINOBJECT(PSoulfuScriptContext pss, int number, int sub_number)
{
    return ptr_equal(&(mouse.last.object), &(pss->self.object));
};

SINT _ex_get_NUMKANJI(PSoulfuScriptContext pss, int number, int sub_number)
{
    SINT retval = 0;

    if (kanji_data)
    {
        retval = kanji_data[0];  retval <<= 8;
        retval += kanji_data[1];
    }

    return retval;
};

SINT _ex_get_NUMKANJITRIANGLE(PSoulfuScriptContext pss, int number, int sub_number)
{
    SINT retval = 0;
    Uint8 *ptmp_a;

    if (kanji_data)
    {
        retval = endian_read_mem_int16(kanji_data);

        if (number < retval)
        {
            ptmp_a = kanji_data + 2 + (number << 2);
            number = endian_read_mem_int32(ptmp_a);
            ptmp_a = kanji_data + number;
            number = *ptmp_a;  ptmp_a++;
            ptmp_a += number << 1;
            retval = *ptmp_a;
        }
        else
        {
            retval = 0;
        }
    }

    return retval;
};

SINT _ex_get_CURSORSCREENPOS(PSoulfuScriptContext pss, int number, int sub_number)
{
    return (SINT)((sub_number == XX) ? mouse.x : mouse.y);
};

SINT _ex_get_DAMAGEAMOUNT(PSoulfuScriptContext pss, int number, int sub_number)
{
    return attack_info.amount;
};

SINT _ex_get_COLLISIONCHAR(PSoulfuScriptContext pss, int number, int sub_number)
{
    return global_bump_char;
};

SINT _ex_get_MAINVIDEOFRAME(PSoulfuScriptContext pss, int number, int sub_number)
{
    return main_video_frame;
};

SINT _ex_get_MAINGAMEFRAME(PSoulfuScriptContext pss, int number, int sub_number)
{
    return main_game_frame;
};

SINT _ex_get_MAINFRAMESKIP(PSoulfuScriptContext pss, int number, int sub_number)
{
    return main_frame_skip;
};

SINT _ex_get_FINDDATASIZE(PSoulfuScriptContext pss, int number, int sub_number)
{
    // sub_number is the start of a file's data...

    SINT retval = kfalse;

    Uint8 *ptmp_a;

    ptmp_a = (Uint8*) sub_number;
    retval = 0;
    repeat(number, sdf_archive_get_num_files())
    {
        SDF_PHEADER pheader = sdf_archive_get_header(number);

        if ( sdf_file_get_type(pheader) != SDF_FILE_IS_UNUSED)
        {
            if (ptmp_a == sdf_file_get_data(pheader))
            {
                number = sdf_archive_get_num_files();
                retval = sdf_file_get_size(pheader);
            }
        }
    }

    return retval;
};

SINT _ex_get_ARCTANANGLE(PSoulfuScriptContext pss, int number, int sub_number)
{
    // Return the arctangent angle of the given x, y coordinates

    // !!!BAD!!!
    // !!!BAD!!!  Better way of doin' this...
    // !!!BAD!!!

    return (SINT)((ATAN2(-number, sub_number) + PI) * RAD_TO_DEG);
};

SINT _ex_get_MAINSERVERLOCATED(PSoulfuScriptContext pss, int number, int sub_number)
{
#if defined(SOULFU_SERVER) || defined(SOULFU_CLIENT)
    return (NULL != local_socket_set);
#else
    return kfalse;
#endif
};

SINT _ex_get_SHARDLIST(PSoulfuScriptContext pss, int number, int sub_number)
{
    // number is continent, sub_number is direction

    // !!!BAD!!!
    // !!!BAD!!!  Stupid...
    // !!!BAD!!!

    return kfalse;
};

SINT _ex_get_SHARDLISTPING(PSoulfuScriptContext pss, int number, int sub_number)
{
    // sub_number is the letter...

    // !!!BAD!!!
    // !!!BAD!!!  Stupid...
    // !!!BAD!!!

    return 128;
};

SINT _ex_get_SHARDLISTPLAYERS(PSoulfuScriptContext pss, int number, int sub_number)
{
    // sub_number is the letter...

    // !!!BAD!!!
    // !!!BAD!!!  Stupid...
    // !!!BAD!!!

    return kfalse;
};

SINT _ex_get_VERSIONERROR(PSoulfuScriptContext pss, int number, int sub_number)
{
    // sub_number is the subrequest...

    SINT retval = kfalse;

    //   0 is do we have an error?
    //   1 is what executable version is required?
    //   2 is what data version is required?
    retval = global_version_error;

    if (sub_number == 1)
    {
        retval = required_executable_version;
    }

    if (sub_number == 2)
    {
        retval = required_data_version;
    }

    return retval;
};

SINT _ex_get_STARTGAME(PSoulfuScriptContext pss, int number, int sub_number)
{
    return play_game_active;
};

SINT _ex_get_NETWORKON(PSoulfuScriptContext pss, int number, int sub_number)
{
    return network_on;
};

SINT _ex_get_MAINGAMEACTIVE(PSoulfuScriptContext pss, int number, int sub_number)
{
    return main_game_active;
};

SINT _ex_get_NETWORKGAMEACTIVE(PSoulfuScriptContext pss, int number, int sub_number)
{
    // !!!BAD!!!
    // !!!BAD!!!  Stupid
    // !!!BAD!!!

    return network_on && main_game_active;
};

SINT _ex_get_TRYINGTOJOINGAME(PSoulfuScriptContext pss, int number, int sub_number)
{
    // !!!BAD!!!
    // !!!BAD!!!  Stupid
    // !!!BAD!!!

    return kfalse;
};

SINT _ex_get_JOINPROGRESS(PSoulfuScriptContext pss, int number, int sub_number)
{
    // !!!BAD!!!
    // !!!BAD!!!  Stupid
    // !!!BAD!!!

    return kfalse;
};

SINT _ex_get_GAMESEED(PSoulfuScriptContext pss, int number, int sub_number)
{
    // !!!BAD!!!
    // !!!BAD!!!  Stupid...  Change to map_seed...
    // !!!BAD!!!

    return game_seed;
};

SINT _ex_get_LOCALPASSWORDCODE(PSoulfuScriptContext pss, int number, int sub_number)
{
    // !!!BAD!!!
    // !!!BAD!!!  Stupid...
    // !!!BAD!!!

    return kfalse;
};

SINT _ex_get_NUMNETWORKPLAYER(PSoulfuScriptContext pss, int number, int sub_number)
{
    return remote_list_count;
};

SINT _ex_get_NETWORKFINISHED(PSoulfuScriptContext pss, int number, int sub_number)
{
    // Only returns ktrue if all the network handling is done...

    // !!!BAD!!!
    // !!!BAD!!!  Stupid...
    // !!!BAD!!!

    return ktrue;
};

SINT _ex_get_SERVERSTATISTICS(PSoulfuScriptContext pss, int number, int sub_number)
{
    // !!!BAD!!!
    // !!!BAD!!!  Stupid...
    // !!!BAD!!!

    return kfalse;
};

SINT _ex_get_LOCALPLAYER(PSoulfuScriptContext pss, int number, int sub_number)
{
    // number is the local player (0-3)...  Return value needn't be a valid character...

    number %= MAX_LOCAL_PLAYER;

    return local_player_character[number];
};

SINT _ex_get_FPS(PSoulfuScriptContext pss, int number, int sub_number)
{
    // Return the Frames per second...
    return (SINT)main_timer_fps;
};

SINT _ex_get_RANDOMSEED(PSoulfuScriptContext pss, int number, int sub_number)
{
    // Return the random seed...

    return g_rand.next;
};

SINT _ex_get_GLOBALATTACKSPIN(PSoulfuScriptContext pss, int number, int sub_number)
{
    // Return the global attack spin...

    return attack_info.spin;
};

SINT _ex_get_GLOBALATTACKER(PSoulfuScriptContext pss, int number, int sub_number)
{
    // Return whoever did damage...

    return attack_info.index;
};

SINT _ex_get_DEBUGACTIVE(PSoulfuScriptContext pss, int number, int sub_number)
{
    // Returns whether debug stuff is on or off...

#ifdef DEVTOOL
    return debug_active;
#else
    return kfalse;
#endif

};

SINT _ex_get_DAMAGECOLOR(PSoulfuScriptContext pss, int number, int sub_number)
{
    // Returns the color of a specific damage type (for the little numbers that pop up)...
    // sub_number is the damage type...

    sub_number = sub_number % MAX_DAMAGE_TYPE;
    return (damage_color_rgb[sub_number][0] << 16) + (damage_color_rgb[sub_number][1] << 8) + (damage_color_rgb[sub_number][2]);
};

SINT _ex_get_ITEMREGISTRYSCRIPT(PSoulfuScriptContext pss, int number, int sub_number)
{
    // Returns an item's script...

    // number is the item type index number...
    number %= MAX_ITEM_TYPE;
    return CAST(SINT, item_list[number].script);
};

SINT _ex_get_ITEMREGISTRYICON(PSoulfuScriptContext pss, int number, int sub_number)
{
    // Returns an item's icon image...

    // number is the item type index number...
    number %= MAX_ITEM_TYPE;
    return CAST(SINT, item_list[number].icon);
};

SINT _ex_get_ITEMREGISTRYOVERLAY(PSoulfuScriptContext pss, int number, int sub_number)
{
    // Returns an item's overlay image...

    // number is the item type index number...
    number %= MAX_ITEM_TYPE;
    return CAST(SINT, item_list[number].overlay);
};

SINT _ex_get_ITEMREGISTRYPRICE(PSoulfuScriptContext pss, int number, int sub_number)
{
    // Returns an item's shop price...

    // number is the item type index number...

    number %= MAX_ITEM_TYPE;
    return item_list[number].price;
};

SINT _ex_get_ITEMREGISTRYFLAGS(PSoulfuScriptContext pss, int number, int sub_number)
{
    // Returns an item's flags...

    // number is the item type index number...
    number %= MAX_ITEM_TYPE;
    return item_list[number].flags;
};

SINT _ex_get_ITEMREGISTRYSTR(PSoulfuScriptContext pss, int number, int sub_number)
{
    // Returns an item's minimum strength requirement...

    // number is the item type index number...
    number %= MAX_ITEM_TYPE;
    return item_list[number].istr;
};

SINT _ex_get_ITEMREGISTRYDEX(PSoulfuScriptContext pss, int number, int sub_number)
{
    // Returns an item's minimum dexterity requirement...

    // number is the item type index number...
    number %= MAX_ITEM_TYPE;
    return item_list[number].idex;
};

SINT _ex_get_ITEMREGISTRYINT(PSoulfuScriptContext pss, int number, int sub_number)
{
    // Returns an item's minimum intelligence requirement...

    // number is the item type index number...
    number %= MAX_ITEM_TYPE;
    return item_list[number].iint;
};

SINT _ex_get_ITEMREGISTRYMANA(PSoulfuScriptContext pss, int number, int sub_number)
{
    // Returns an item's minimum manamax requirement...

    // number is the item type index number...
    number %= MAX_ITEM_TYPE;
    return item_list[number].imana;
};

SINT _ex_get_ITEMREGISTRYAMMO(PSoulfuScriptContext pss, int number, int sub_number)
{
    // Returns an item's ammo number (to be displayed over the item's icon)...

    // number is the item type index number...
    number %= MAX_ITEM_TYPE;
    return item_list[number].ammo;
};

SINT _ex_get_ITEMREGISTRYNAME(PSoulfuScriptContext pss, int number, int sub_number)
{
    // Runs the GetName() function for the given item type...

    // number is the item type index number...

    PSoulfuGlobalScriptContext psgs = pss->psgs;

    psgs->item_index = (Uint16) number;

    return item_get_type_name((Uint16) number, pss->self.object.bas);
};

SINT _ex_get_WEAPONGRIP(PSoulfuScriptContext pss, int number, int sub_number)
{
    // Gets the grip (left or right) for the weapon setup call...

    return weapon_setup_grip;
};

SINT _ex_get_DEFENSERATING(PSoulfuScriptContext pss, int number, int sub_number)
{
    // Returns the accumulated amount for a given damage type...

    // sub_number is the damage type
    sub_number %= MAX_DAMAGE_TYPE;

    return attack_info.defense[sub_number%MAX_DAMAGE_TYPE];
};

SINT _ex_get_CURSORBUTTONPRESSED(PSoulfuScriptContext pss, int number, int sub_number)
{
    // Returns the cursor button pressed state for a given button

    // sub_number is the button...
    sub_number %= MAX_MOUSE_BUTTON;

    return mouse.pressed[sub_number];
};

SINT _ex_get_MEMBUFFER(PSoulfuScriptContext pss, int number, int sub_number)
{
    // Returns a pointer to the given buffer...

    SINT retval = kfalse;

    // sub_number is the buffer number...
    if (sub_number < 2)
    {
        retval = (UINT) mainbuffer;
    }

    if (sub_number == 2)
    {
        retval = (UINT) subbuffer;
    }

    if (sub_number == 3)
    {
        retval = (UINT) thirdbuffer;
    }

    if (sub_number == 4)
    {
        retval = (UINT) fourthbuffer;
    }

    if (sub_number > 4)
    {
        // !!!BAD!!!
        // !!!BAD!!!
        // !!!BAD!!!
        log_error(0, "Membuffer MAPBUFFER requested...");
        retval = (UINT) 0;
        // !!!BAD!!!
        // !!!BAD!!!
        // !!!BAD!!!
    }

    return retval;
};

#ifdef DEVTOOL
SINT _ex_get_ROOMSELECT(PSoulfuScriptContext pss, int number, int sub_number)
{
    return room_select_num;
};

SINT _ex_get_ROOMEXTERIORWALLFLAGS(PSoulfuScriptContext pss, int number, int sub_number)
{
    // number is pointer to srf data...

    SINT retval = kfalse;

    retval = 0;

    if (room_select_num > 1)
    {
        retval = room_select_list[room_select_num-2];
        sub_number = room_select_list[room_select_num-1];
        retval = room_srf_exterior_wall_flags((Uint8*) number, (Uint16) retval, (Uint16) sub_number, kfalse, 0);
    }

    return retval;
};

SINT _ex_get_ROOMGROUP(PSoulfuScriptContext pss, int number, int sub_number)
{
    return global_room_active_group;
};

SINT _ex_get_ROOMOBJECT(PSoulfuScriptContext pss, int number, int sub_number)
{
    SINT retval;

    if (sub_number == 0)
    {
        retval = global_room_active_object;
    }
    else
    {
        retval = CAST(SINT, global_room_active_object_data);
    }

    return retval;
};

SINT _ex_get_ROOMTEXTUREFLAGS(PSoulfuScriptContext pss, int number, int sub_number)
{
    // number is the srf file, sub_number is the texture (0-31)...

    return room_srf_textureflags((Uint8*) number, (Uint8) sub_number, kfalse, 0);
};
#endif

SINT _ex_get_MOUSELASTOBJECT(PSoulfuScriptContext pss, int number, int sub_number)
{
    return CAST(SINT, mouse.last.object.unk);
};

SINT _ex_get_MOUSELASTITEM(PSoulfuScriptContext pss, int number, int sub_number)
{
    return mouse.last.item;
};

SINT _ex_get_MOUSELASTSCRIPT(PSoulfuScriptContext pss, int number, int sub_number)
{
    SINT retval = 0;

    if ( ptr_is_type(&(mouse.last.object), OBJECT_WIN) )
    {
        retval = ptr_get_idx(&(mouse.last.object), OBJECT_WIN);
        retval = (SINT) main_win_data[retval].script_ctxt.base.base.file_start;
    }

    return retval;
};

SINT _ex_get_ITEMINDEX(PSoulfuScriptContext pss, int number, int sub_number)
{
    // Returns the index of the item that the current script is for...

    // Used to figure out if weapons & armor are enchanted variants of main type...
    PSoulfuGlobalScriptContext psgs = pss->psgs;

    return psgs->item_index;
};

SINT _ex_get_WEAPONREFRESHXYZ(PSoulfuScriptContext pss, int number, int sub_number)
{
    // Returns one of the XYZ coordinates we generated earlier...

    SINT retval = kfalse;

    // Should only be called during a weapon item's Refresh() function,
    // and only after a set call to SYS_WEAPONREFRESHXYZ...
    // sub_number is the axis...
    // Return value is scaled by 1000...

    retval = 0;

    if (sub_number >= XX && sub_number <= ZZ)
    {
        retval = (SINT) (weapon_refresh_xyz[sub_number] * 1000.0f);
    }

    return retval;
};

SINT _ex_get_WEAPONREFRESHBONENAME(PSoulfuScriptContext pss, int number, int sub_number)
{
    // Returns the bone name used by the weapon refresh functions...  So you can tell
    // which weapon slot you're dealing with...
    PSoulfuGlobalScriptContext psgs = pss->psgs;

    return psgs->item_bone_name;
};

SINT _ex_get_KEEPITEM(PSoulfuScriptContext pss, int number, int sub_number)
{
    // Returns the value of the global variable...

    PSoulfuGlobalScriptContext psgs = pss->psgs;

    return psgs->item_keep;
};

SINT _ex_get_FREEPARTICLE(PSoulfuScriptContext pss, int number, int sub_number)
{
    // Returns number of unused particles...

    return main_unused_particle_count;
};

SINT _ex_get_ROOMWATERLEVEL(PSoulfuScriptContext pss, int number, int sub_number)
{
    // Returns the room_water_level...  Multiplied by 100 to fit to int...

    return (SINT)(room_water_level * 100.0f);
};

SINT _ex_get_ROOMWATERTYPE(PSoulfuScriptContext pss, int number, int sub_number)
{
    // Returns the room_water_type...

    return room_water_type;
};

SINT _ex_get_MAPSIDENORMAL(PSoulfuScriptContext pss, int number, int sub_number)
{
    // Returns the vector components that tell which way the camera is facing...  Multiplied by 100 to fit to int...

    // number is the axis...
    return (SINT)(map_side_xy[number&1] * 100.0f);
};

SINT _ex_get_MESSAGESIZE(PSoulfuScriptContext pss, int number, int sub_number)
{
    // Returns the number of characters across that should be printed in the message window...

    return message_size;
};

SINT _ex_get_FASTFUNCTIONFOUND(PSoulfuScriptContext pss, int number, int sub_number)
{
    // Returns ktrue if the last SYS_FASTFUNCTION or SYS_CHARFASTFUNCTION found a corresponding script function to run...

    return pxss_fast_run_found;
};

SINT _ex_get_INPUTACTIVE(PSoulfuScriptContext pss, int number, int sub_number)
{
    // Returns the current input active timer...

    return input_active;
};

SINT _ex_get_LOCALPLAYERINPUT(PSoulfuScriptContext pss, int number, int sub_number)
{
    // Used by WSTATUS window to read player inputs for inventory control via joystick/keyboard...

    SINT retval = kfalse;

    // number is the local player number (0-3), sub_number is the input button to read...
    number %= MAX_LOCAL_PLAYER;
    retval = 0;

    if (sub_number >= 0)
    {
        if (sub_number < 4)
        {
            retval = player_device[number].button_pressed_copy[sub_number];
        }
        else
        {
            if (sub_number == PLAYER_DEVICE_BUTTON_ITEMS)
            {
                retval = player_device[number].inventory_toggle;
            }

            if (sub_number == PLAYER_DEVICE_BUTTON_MOVE_UP || sub_number == PLAYER_DEVICE_BUTTON_MOVE_DOWN)
            {
                retval = (SINT) (player_device[number].xy[YY] * 100.0f);
            }

            if (sub_number == PLAYER_DEVICE_BUTTON_MOVE_LEFT || sub_number == PLAYER_DEVICE_BUTTON_MOVE_RIGHT)
            {
                retval = (SINT) (player_device[number].xy[XX] * 100.0f);
            }

            if (sub_number == PLAYER_DEVICE_BUTTON_ITEMS_DOWN)
            {
                retval = player_device[number].inventory_down;
            }
        }
    }

    return retval;
};

SINT _ex_get_ROOMMONSTERTYPE(PSoulfuScriptContext pss, int number, int sub_number)
{
    // sub_number is the subtype to poll (0-3)...  Used for CPORTAL and CRANDOM to
    // get the proper monster types...  Subtype refers to the portal's subtype...

    return roombuffer[36 + (sub_number&3)];
};

SINT _ex_get_PAYINGCUSTOMER(PSoulfuScriptContext pss, int number, int sub_number)
{
    // Returns either ktrue or kfalse

    return paying_customer;
};

SINT _ex_get_ENCHANTCURSOR(PSoulfuScriptContext pss, int number, int sub_number)
{
    // Returns a variety of pss->enc_cursor info...  sub_number is the subtype...

    SINT retval = kfalse;

    if (sub_number == ENCHANT_CURSOR_TARGET)
    {
        retval = pss->enc_cursor.target;
    }
    else if (sub_number == ENCHANT_CURSOR_TARGET_ITEM)
    {
        retval = pss->enc_cursor.target_item;
    }
    else
    {
        retval = pss->enc_cursor.active;
    }

    return retval;
};

SINT _ex_get_CHARACTERSCRIPTFILE(PSoulfuScriptContext pss, int number, int sub_number)
{
    // Returns a pointer to the .RUN file for the given character index...

    SINT retval = 0;

    // sub_number is the character index...

    if ( VALID_CHR_RANGE( sub_number) )
    {
        if( chr_data_get_ptr_raw( sub_number )->on || chr_data_get_ptr_raw( sub_number )->reserve_on )
        {
            retval = (SINT) chr_data_get_ptr_raw( sub_number )->script_ctxt.base.base.file_start;
        }
    };

    return retval;
};

SINT _ex_get_PARTICLESCRIPTFILE(PSoulfuScriptContext pss, int number, int sub_number)
{
    // Returns a pointer to the .RUN file for the given particle index...

    // sub_number is the particle index...
    sub_number %= MAX_PARTICLE;
    return CAST(SINT, main_prt_data[sub_number].script_ctxt.base.base.file_start);
};

SINT _ex_get_MAPROOM(PSoulfuScriptContext pss, int number, int sub_number)
{
    // Used to get map info...  sub_number is the parameter to read...

    SINT retval = kfalse;

    // number is the room number (only needed for some...)
    // Top 16 bits of number is sometimes used as additional parameter...

    //PSoulfuGlobalScriptContext psgs = pss->psgs;
    int m;

    m = number >> 16;
    number = number & UINT16_MAX;

    if (number < num_map_room)
    {
        if (sub_number == MAP_ROOM_SRF)
        {
            retval = (SINT) (DEREF( Uint8*, map_room_data[number] ));
        }

        if (sub_number == MAP_ROOM_X)
        {
            retval = ((DEREF( Uint16, map_room_data[number] + 4 )) - 32768) * 10;
        }

        if (sub_number == MAP_ROOM_Y)
        {
            retval = ((DEREF( Uint16, map_room_data[number] + 6 )) - 32768) * 10;
        }

        if (sub_number == MAP_ROOM_DOOR_X || sub_number == MAP_ROOM_DOOR_Y || sub_number == MAP_ROOM_DOOR_Z)
        {
            pss->matrix[0] = 0.0f;
            pss->matrix[1] = 0.0f;
            pss->matrix[2] = 0.0f;

            pss->matrix[3] = 0.0f;
            pss->matrix[4] = 0.0f;
            pss->matrix[5] = 0.0f;

            if (m < 5)
            {
                room_find_wall_center(DEREF( Uint8*, map_room_data[number] ), DEREF( Uint16, map_room_data[number] + 8 ), map_room_data[number][24+m], pss->matrix, pss->matrix + 3, map_room_door_pushback);
            }

            if (sub_number == MAP_ROOM_DOOR_X)
            {
                retval = (SINT) (pss->matrix[0] * 100.0f);
            }

            if (sub_number == MAP_ROOM_DOOR_Y)
            {
                retval = (SINT) (pss->matrix[1] * 100.0f);
            }

            if (sub_number == MAP_ROOM_DOOR_Z)
            {
                retval = (SINT) (pss->matrix[2] * 100.0f);
            }
        }

        if (sub_number == MAP_ROOM_ROTATION)
        {
            retval = DEREF( Uint16, map_room_data[number] + 8 );
        }

        if (sub_number == MAP_ROOM_SEED)
        {
            retval = map_room_data[number][10];
        }

        if (sub_number == MAP_ROOM_TWSET)
        {
            retval = map_room_data[number][11];
        }

        if (sub_number == MAP_ROOM_LEVEL)
        {
            retval = map_room_data[number][12];
        }

        if (sub_number == MAP_ROOM_FLAGS || sub_number == MAP_ROOM_UPDATE_FLAGS)
        {
            retval = map_room_data[number][13];
        }

        if (sub_number == MAP_ROOM_DIFFICULTY)
        {
            retval = map_room_data[number][30];
        }

        if (sub_number == MAP_ROOM_AREA)
        {
            retval = map_room_data[number][31];
        }

        if (sub_number == MAP_ROOM_NEXT_ROOM)
        {
            m = m % 5;
            retval = DEREF( Uint16, map_room_data[number] + 14 + (m << 1) );
        }
    }

    if (sub_number == MAP_ROOM_NUMBER)
    {
        retval = num_map_room;
    }

    if (sub_number == MAP_ROOM_CURRENT)
    {
        retval = map_current_room;
    }

    if (sub_number == MAP_ROOM_DOOR_SPIN)
    {
        // This is set on a call to room_find_wall_center() with pushback set...
        retval = map_room_door_spin;
    }

    if (sub_number == MAP_ROOM_LAST_TOWN)
    {
        retval = map_last_town_room;
    }

    return retval;
};

SINT _ex_get_MAPDOOROPEN(PSoulfuScriptContext pss, int number, int sub_number)
{
    // Used to find if a door is open in the current room...

    SINT retval = kfalse;

    // sub_number is the door number...
    retval = 0;

    if (map_current_room < num_map_room)
    {
        if (sub_number < 5 && sub_number >= 0)
        {
            retval = (map_room_data[map_current_room][29] >> sub_number) & 1;
        }
    }

    return retval;
};

SINT _ex_get_LOCALPLAYERZ(PSoulfuScriptContext pss, int number, int sub_number)
{
    // Returns the z level that the camera is lookin' at...

    return (SINT)camera.target_xyz[ZZ];
};

SINT _ex_get_RESERVECHARACTER(PSoulfuScriptContext pss, int number, int sub_number)
{
    // Returns main_character_reserve_on for a given character...

    SINT retval = kfalse;

    // number is the character index...
    retval = kfalse;

    if ( VALID_CHR_RANGE(number) )
    {
        retval = chr_data_get_ptr_raw( number )->reserve_on;
    }

    return retval;
};

SINT _ex_get_LUCK(PSoulfuScriptContext pss, int number, int sub_number)
{
    // Returns the global luck timer...

    return global_luck_timer;
};

SINT _ex_get_NETWORKSCRIPT(PSoulfuScriptContext pss, int number, int sub_number)
{
    // Returns the value of the desired netscript. variable...

    SINT retval = kfalse;

    // sub_number is the variable number...

    retval = 0;

    switch (sub_number)
    {
        case NETWORK_SCRIPT_NEWLY_SPAWNED: retval = netscript.newly_spawned;  break;
        case NETWORK_SCRIPT_EXTRA_DATA:    retval = netscript.extra_data;  break;
        case NETWORK_SCRIPT_REMOTE_INDEX:  retval = netscript.remote_index;  break;
        case NETWORK_SCRIPT_NETLIST_INDEX: retval = netscript.netlist_index; break;
        case NETWORK_SCRIPT_X:             retval = netscript.x; break;
        case NETWORK_SCRIPT_Y:             retval = netscript.y; break;
        case NETWORK_SCRIPT_Z:             retval = netscript.z; break;
        case NETWORK_SCRIPT_FACING:        retval = netscript.facing; break;
        case NETWORK_SCRIPT_ACTION:        retval = netscript.action; break;
        case NETWORK_SCRIPT_TEAM:          retval = netscript.team; break;
        case NETWORK_SCRIPT_POISON:        retval = netscript.poison; break;
        case NETWORK_SCRIPT_PETRIFY:       retval = netscript.petrify; break;
        case NETWORK_SCRIPT_ALPHA:         retval = netscript.alpha; break;
        case NETWORK_SCRIPT_DEFLECT:       retval = netscript.deflect; break;
        case NETWORK_SCRIPT_HASTE:         retval = netscript.haste; break;
        case NETWORK_SCRIPT_OTHER_ENCHANT: retval = netscript.other_enchant; break;
        case NETWORK_SCRIPT_EQLEFT:        retval = netscript.eqleft; break;
        case NETWORK_SCRIPT_EQRIGHT:       retval = netscript.eqright; break;
        case NETWORK_SCRIPT_EQCOL01:       retval = netscript.eqcol01; break;
        case NETWORK_SCRIPT_EQCOL23:       retval = netscript.eqcol23; break;
        case NETWORK_SCRIPT_EQSPEC1:       retval = netscript.eqspec1; break;
        case NETWORK_SCRIPT_EQSPEC2:       retval = netscript.eqspec2; break;
        case NETWORK_SCRIPT_EQHELM:        retval = netscript.eqhelm; break;
        case NETWORK_SCRIPT_EQBODY:        retval = netscript.eqbody; break;
        case NETWORK_SCRIPT_EQLEGS:        retval = netscript.eqlegs; break;
        case NETWORK_SCRIPT_CLASS:         retval = netscript.cclass; break;
        case NETWORK_SCRIPT_MOUNT_INDEX:   retval = netscript.mount_index; break;
    }

    return retval;
};

SINT _ex_get_ROOMMETALBOXITEM(PSoulfuScriptContext pss, int number, int sub_number)
{
    // Returns the room_metal_box_item...

    return room_metal_box_item;
};

SINT _ex_get_CAMERAZOOM(PSoulfuScriptContext pss, int number, int sub_number)
{
    // Returns the current camera zoom level...

    return (SINT)camera.to_distance;
};

SINT _ex_get_CAMERASPIN(PSoulfuScriptContext pss, int number, int sub_number)
{
    // Returns the current camera spin...

    return camera.rotation_xy[XX];
};

bool_t set_part_value( MODEL_DATA * part, int index, size_t val )
{
    bool_t retval = kfalse;

    if(NULL == part) return kfalse;

    retval = ktrue;
    switch(index)
    {
        case 0: part->file       = CAST(Uint8*,val); break;
        case 1: part->tex_lst[0] = CAST(Uint8*,val); break;
        case 2: part->tex_lst[1] = CAST(Uint8*,val); break;
        case 3: part->tex_lst[2] = CAST(Uint8*,val); break;
        case 4: part->tex_lst[3] = CAST(Uint8*,val); break;
        case 5: part->color      = CAST(Uint32,val); break;
        default: retval = kfalse;                    break;
    }

    return retval;
}

bool_t ex_model_assign( Uint8 * unk_ptr, size_t part_data )
{
    OBJECT_PTR   obj;
    MODEL_DATA * part_list = NULL;
    Uint8      * base_ptr;
    bool_t       retval = kfalse;

    // use this to determine what kind of object we are referencing
    ptr_set_unk ( &obj, unk_ptr);

    if( NULL == part_list )
    {
        base_ptr = (Uint8 *)ptr_get_ptr( &obj, OBJECT_CHR );
        if( NULL != base_ptr )
        {
            part_list = CAST(CHR_DATA *, base_ptr)->model.parts;
        }
    }

    if( NULL == part_list )
    {
        base_ptr = (Uint8 *)ptr_get_ptr( &obj, OBJECT_WIN );
        if( NULL != base_ptr )
        {
            part_list = CAST(WIN_DATA *, base_ptr)->model.parts;
        }
    }

    if( NULL != part_list )
    {
        size_t offset = unk_ptr - base_ptr;
        int    ipart  = (offset - MODEL_BASE_FILE) / 24;

        // make sure that the part number is valid!
        if(ipart < MODEL_PART_COUNT)
        {
            int ipart_offset = (offset - MODEL_BASE_FILE) - ipart*24;
            int ipart_index  = ipart_offset >> 2;

            retval = set_part_value( part_list + ipart, ipart_index, part_data );
        }
    }

    return retval;
}

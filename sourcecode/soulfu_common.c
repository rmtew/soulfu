// <ZZ> This file contains a bunch of common functions
//  **  make_uppercase          - Changes a string to be all uppercase letters
//  **  free_mainbuffer         - Frees up the memory used by the general purpose buffer
//  **  buffer_setup          - Allocates memory for a general purpose buffer
//  **  count_indentation       - Returns the number of spaces at the start of a string
//  **  datadump                - Spits out some data into a file on disk (for debuggin')

#include <SDL.h>
#include "soulfu_common.h"
#include "sdf_archive.h"
#include "logfile.h"

Uint8* mainbuffer = NULL;       // A General purpose buffer
Uint8* subbuffer = NULL;        // A subset of the mainbuffer
Uint8* thirdbuffer = NULL;      // A subset of the mainbuffer
Uint8* fourthbuffer = NULL;     // A subset of the mainbuffer
Uint8* mapbuffer = NULL;        // A subset of the mainbuffer
Uint8* roombuffer = NULL;       // A subset of the mainbuffer

//-----------------------------------------------------------------------------------------------
Uint32 timer_start_time = 0;
Uint32 timer_end_time;
Uint32 timer_end_length;
void timer_start()
{
    // <ZZ> This function is for figuring out how slow things are...
    timer_start_time = SDL_GetTicks();
}

//-----------------------------------------------------------------------------------------------
void timer_end()
{
    // <ZZ> This function is for figuring out how slow things are...
    timer_end_time = SDL_GetTicks();
    timer_end_length = timer_end_time - timer_start_time;
#ifdef DEVTOOL
    log_info(0, "Timed function took %d msecs to run", timer_end_length);
#endif
}

//-----------------------------------------------------------------------------------------------
Uint32 main_timer_length = 0;
Uint32 main_timer_start_time;
void main_timer_start()
{
    // <ZZ> This function is for figuring out how many frames to update...
    main_timer_start_time = SDL_GetTicks();
}

//-----------------------------------------------------------------------------------------------
Uint32 main_timer_end_time;
float main_timer_fps = 0;
void main_timer_end()
{
    // <ZZ> This function is for figuring out how slow things are...
    main_timer_end_time = SDL_GetTicks();
    timer_end_length = (main_timer_end_time - main_timer_start_time);
    main_timer_length += timer_end_length;


    // Do FPS calculations
    if (timer_end_length > 0)
    {
        main_timer_fps = (((1000.0f / timer_end_length) + 0.5f) * 0.05f) + (main_timer_fps * 0.95f);
    }
}

//-----------------------------------------------------------------------------------------------
void make_uppercase(char *string)
{
    // <ZZ> This function changes all lowercase letters in string to be uppercase.
    int i;

    i = 0;

    while (string[i] != 0)
    {
        string[i] = toupper(string[i]);
        i++;
    }
}

//-----------------------------------------------------------------------------------------------
void free_mainbuffer(void)
{
    // <ZZ> This function frees up the memory allocated by buffer_setup(), and should be
    //      called automatically atexit().
    if (mainbuffer)
    {
        free(mainbuffer);
    }
}

//-----------------------------------------------------------------------------------------------
Sint8 buffer_setup(void)
{
    // <ZZ> This function reserves memory for a general purporse buffer used throughout the
    //      program.  If it fails, it returns kfalse.  If it works, it returns ktrue and registers
    //      free_mainbuffer() to be called atexit().

    mainbuffer = (Uint8*)malloc(MAINBUFFERSIZE);

    if (mainbuffer)
    {
        log_info(0, "Allocated %d bytes (%d Meg) for the mainbuffer", MAINBUFFERSIZE, (MAINBUFFERSIZE / MEG));
        subbuffer    = mainbuffer   + BUFFERSIZE;
        thirdbuffer  = subbuffer    + BUFFERSIZE;
        fourthbuffer = thirdbuffer  + BUFFERSIZE;
        mapbuffer    = fourthbuffer + BUFFERSIZE;
        roombuffer   = mapbuffer    + BUFFERSIZE;
        atexit(free_mainbuffer);
        return ktrue;
    }

    log_error(0, "Could not allocate memory for the mainbuffer");
    return kfalse;
}

//-----------------------------------------------------------------------------------------------
int count_indentation(SDF_PSTREAM pstream)
{
    // <ZZ> This function returns the number of spaces at the start of string
    int count = 0;

    while (' ' == *(pstream->read) && !sdf_stream_eos(pstream))
    {
        pstream->read++;
        count++;
    }

    return count;
}

//-----------------------------------------------------------------------------------------------
void datadump(Uint8* location, int size, Sint8 append)
{
    // <ZZ> This function copies the data at location to a file on disk named DATADUMP.DAT.
    //      Size is the number of bytes to write...  If append is ktrue, data is appended,
    //      otherwise the file is overwritten...
    FILE* openfile;

    if (append) openfile = fopen("DATADUMP.DAT", "ab");
    else openfile = fopen("DATADUMP.DAT", "wb");

    if (openfile)
    {
        fwrite(location, 1, size, openfile);
        fclose(openfile);
        log_info(0, "Wrote %d bytes to DATADUMP.DAT from location %d", size, location);
    }
    else
    {
        log_error(0, "Couldn't open DATADUMP.DAT");
    }
}

////-----------------------------------------------------------------------------------------------
//void cross_product(float* A_xyz, float* B_xyz, float* C_xyz)
//{
//    // <ZZ> This function crosses two vectors, A and B, and gives the result in C.  C should be
//    //      a different location than either A or B...
//    C_xyz[XX] = (A_xyz[YY]*B_xyz[ZZ]) - (A_xyz[ZZ]*B_xyz[YY]);
//    C_xyz[YY] = (A_xyz[ZZ]*B_xyz[XX]) - (A_xyz[XX]*B_xyz[ZZ]);
//    C_xyz[ZZ] = (A_xyz[XX]*B_xyz[YY]) - (A_xyz[YY]*B_xyz[XX]);
//// !!!BAD!!!
//// !!!BAD!!!  Should macroize...
//// !!!BAD!!!
//}
//
////-----------------------------------------------------------------------------------------------
//#define dot_product(A, B) (A[XX]*B[XX] + A[YY]*B[YY] + A[ZZ]*B[ZZ])
////float dot_product(float* A_xyz, float* B_xyz)
////{
////  // <ZZ> This function dots two vectors, A and B, and gives the result.
////    return (A_xyz[XX]*B_xyz[XX] + A_xyz[YY]*B_xyz[YY] + A_xyz[ZZ]*B_xyz[ZZ]);
////}
//
////-----------------------------------------------------------------------------------------------
//float vector_length(float* A_xyz)
//{
//    // <ZZ> This function returns the length of a vector.
//    return (SQRT(A_xyz[XX]*A_xyz[XX] + A_xyz[YY]*A_xyz[YY] + A_xyz[ZZ]*A_xyz[ZZ]));
//// !!!BAD!!!
//// !!!BAD!!!  Should macroize...
//// !!!BAD!!!
//}



//-----------------------------------------------------------------------------------------------

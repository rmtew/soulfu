#include "runsrc.h"

#include "object.h"
#include "soulfu.h"

#include "pxss_run.inl"
#include "script_extensions.inl"


static int sdf_run_script_depth = 0;

//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------

bool_t sf_run_setup( PSoulfuGlobalScriptContext psgs );
PSoulfuScriptContext sf_script_spawn(PSoulfuGlobalScriptContext psgs, PSoulfuScriptContext pss);
void _sf_update_global(PSoulfuGlobalScriptContext psgs, PSoulfuScriptContext pss);

//-----------------------------------------------------------------------------------------------
bool_t sf_run_setup( PSoulfuGlobalScriptContext psgs )
{
    if ( !pxss_run_setup( &(psgs->base) ) )
    {
        return kfalse;
    };

    // initialize the enchant cursor
    psgs->enc_cursor.active = kfalse;

    // other stuff
    psgs->item_keep      = NO_ITEM;

    psgs->scale          = 6.0f;

    psgs->item_index     = NO_ITEM;

    psgs->item_bone_name = 0;

    return ktrue;
};


//-----------------------------------------------------------------------------------------------
PSoulfuScriptContext sf_script_init(PSoulfuScriptContext pss, Uint8 *file_start, Uint8 * pdata)
{
    OBJECT_PTR obj_ptr;

    if (NULL == _sf_script_init_obj(pss, file_start, ptr_set_unk(&obj_ptr, pdata))) return NULL;

    return pss;
};

//-----------------------------------------------------------------------------------------------
bool_t sf_fast_run_script(Uint8 *file_start, Uint16 fast_function, Uint8 * pdata)
{
    OBJECT_PTR obj_ptr;

    return _sf_fast_run_script_obj(&g_sf_scr, file_start, fast_function, ptr_set_unk(&obj_ptr, pdata));
};


//-----------------------------------------------------------------------------------------------
PSoulfuScriptContext sf_script_spawn(PSoulfuGlobalScriptContext psgs, PSoulfuScriptContext pss)
{
    if (NULL == pxss_script_spawn(&(psgs->base), &(pss->base))) return NULL;

    pss->psgs = psgs;

    // initialize the spawn info
    pss->spawn_info.owner   = UINT16_MAX;
    pss->spawn_info.target  = UINT16_MAX;
    pss->spawn_info.team    = 0;
    pss->spawn_info.subtype = 0;
    pss->spawn_info.cclass   = 255;

    //---- download the basic info ----
    //memcpy(&(pss->self),       (&psgs->focus),      sizeof(FOCUS_INFO));
    memcpy(&(pss->enc_cursor), (&psgs->enc_cursor), sizeof(ENCHANT_CURSOR));
    //memcpy(&(pss->screen),     (&psgs->screen),     sizeof(SOULFU_WINDOW));

    return pss;
};

//-----------------------------------------------------------------------------------------------
void _sf_update_global(PSoulfuGlobalScriptContext psgs, PSoulfuScriptContext pss)
{
    //---- upload the SHARED INFO ----
    memcpy( &(psgs->enc_cursor), &(pss->enc_cursor), sizeof(ENCHANT_CURSOR) );

    _update_global( &(psgs->base), &(pss->base) );
}


//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------
// <ZZ> Little function wrapper for doing an enchantment...  Assumes that all of the global
//      g_scr.enc_cursor values are set properly...
//-----------------------------------------------------------------------------------------------
void call_enchantment_function(PSoulfuScriptContext pss)
{
    CHR_DATA * pchr, *ptarget;

    pchr    = chr_data_get_ptr( pss->enc_cursor.character );
    ptarget = chr_data_get_ptr( pss->enc_cursor.target );

    if ( NULL != pchr && NULL != ptarget )
    {
        pss->psgs->item_index = pss->enc_cursor.item;
        sf_fast_run_script(item_list[pss->enc_cursor.item].script, FAST_FUNCTION_ENCHANTUSAGE, CAST(Uint8*, pchr) );
    }
}

//-----------------------------------------------------------------------------------------------
static void sf_run_script_begin(PSoulfuScriptContext pss, bool_t share_itemid_pool)
{
    sdf_run_script_depth++;
    pxss_run_script_begin( &(pss->base) );

    if (kfalse == share_itemid_pool)
    {
        // reset pss->self.item every time through the script so that
        // each "item" has a unique identifier
        pss->self.item = 0;
    }
}

//-----------------------------------------------------------------------------------------------

static OPCODE sf_run_script_loop_begin(PSoulfuScriptContext pss)
{
    return pxss_run_script_loop_begin( &(pss->base) );
};

//-----------------------------------------------------------------------------------------------
static bool_t sf_run_script_loop_end(PSoulfuScriptContext pss, OPCODE opcode)
{
    bool_t handled = kfalse;

    // ---- handle the opcodes
    // ---- do it in "reverse" order so that you can override
    // ---- default behavior of the opcodes

    // handle sf_script functions
    handled = run_opcode_external(pss, opcode);

    if (!handled)
    {
        // handle sf_script functions
        handled = pxss_run_script_loop_end( &(pss->base) , opcode);
    }

    return handled;
}

//-----------------------------------------------------------------------------------------------
static void sf_run_script_end(PSoulfuScriptContext pss)
{
    pxss_run_script_end( &(pss->base) );

    sdf_run_script_depth--;
}

//-----------------------------------------------------------------------------------------------
bool_t sf_run_script(PSoulfuScriptContext pss, bool_t share_itemid_pool)
{
    // <ZZ> This function runs a script, starting with whatever opcode is at pbs->function_start.
    OPCODE             opcode;
    PScriptContext     ps;
    PBaseScriptContext pbs;

    // for convenience
    ps   = &(pss->base);
    pbs  = &(ps->base);

    sf_run_script_begin(pss, share_itemid_pool);

    /*
        if( pss->self.object.unk == current_win[0] )
        {
            pxss_debug_level = 2;
        }
        else
        {
            pxss_debug_level = 2;
        }
        */

    while ( pbs->running )
    {
        opcode = sf_run_script_loop_begin(pss);

        sf_run_script_loop_end(pss, opcode);
    };

    sf_run_script_end(pss);

    // upload changes to the shared base global variables
    _sf_update_global(pss->psgs, pss);

    return ktrue;
};

//-----------------------------------------------------------------------------------------------
//static bool_t sf_run_script(PSoulfuScriptContext pss)
//{
//    // <ZZ> This function runs a script, starting with whatever opcode is at pss->base.base.function_start.
//    OPCODE opcode;
//    size_t offset;
//    bool_t handled = kfalse;
//
//    Sint32 i;
//    Sint32 j;
//    Sint32 k;
//    Sint32 m;
//    float  f;
//    float  e;
//
//    OBJECT_PTR obj_ptr;
//
//    Uint8 ui8;
//
//    Uint8* ptmp_a;
//    Uint8* ptmp_b;
//    Uint8* ptmp_c;
//
//    SDF_PHEADER phdr_a;
//    SDF_PHEADER phdr_b;
//    SDF_PHEADER loc_ph;
//
//    PSoulfuGlobalScriptContext psgs = pss->psgs;
//    PGlobalScriptContext       pgs  = pss->base.pgs;
//    PBaseGlobalScriptContext   pbgs = pss->base.base.pbgs;
//    PScriptContext             ps   = &(pss->base);
//    PBaseScriptContext         pbs  = &(pss->base.base);
//
//
//    // for convenience
//    pgs = ps->pgs;
//    pbs->int_link = pbs->pbgs->int_stack_head;
//    pbs->flt_link = pbs->pbgs->flt_stack_head;
//
//    // debugging info
//    loc_ph = sdf_archive_find_header_by_data(pbs->file_start);
//    pbs->file_name = sdf_file_get_name(loc_ph);
//    fprintf(stdout, "_run_script() - %s.%s()\n", pbs->file_name, pbs->function_name);
//
//    // initialize the token stream
//    sdf_stream_open_mem(&pbs->tokens, pbs->function_start, 0);
//
//    // Keep going until we hit a return...
//    pbs->running = ktrue;
//    while (pbs->running)
//    {
//        // Read the opcode
//        offset = pbs->tokens.read - pbs->file_start;
//        opcode = _receive_uint08(&pbs->tokens);
//
//        // Do these two opcodes outside the switch so
//        // I don't have to look at them when I'm debugging...
//        if(OPCODE_DEBUG_LINE_NUMBER == opcode)
//        {
//            pbgs->int_stack_head = pbs->int_link;
//            pbgs->flt_stack_head = pbs->flt_link;
//
//            pbs->line_number = _receive_uint16(&pbs->tokens);
//            continue;
//        }
//        else if( OPCODE_DEBUG_LINE_POINTER ==  opcode)
//        {
//            pbs->line_pointer = (char *)_receive_uint32(&pbs->tokens);
//            //fprintf(stdout, "%s(%d) - %s\n", pbs->file_name, pbs->line_number, pss->line_pointer);
//            continue;
//        };
//
//
//        // Show debug info
//        // show_int_stack(pgs);
//        // show_flt_stack(pgs);
//        log_info(1, "\t0x%06d: (0x%02x) %s ", offset, opcode, opcode_name[opcode]);
//
//        // ---- handle the opcodes
//        // ---- do it in "reverse" order so that you can override
//        // ---- default behavior of the opcodes
//
//
//        // handle external sf_script functions
//        handled = run_opcode_external(pss, opcode);
//
//        if(!handled)
//        {
//            // handle sf_script functions
//            handled = _pxss_run_opcode_soulfu(ps, opcode);
//        }
//
//        if(!handled)
//        {
//            // handle sf_script functions
//            handled = _pxss_run_opcode_library(ps, opcode);
//        }
//
//        if(!handled)
//        {
//            handled = _run_opcode_basic(pbs, opcode);
//        }
//
//
//        if(!handled)
//        {
//            // Invalid opcode
//            log_error(0, "Invalid opcode at 0x%04x", opcode, pbs->tokens.read - pbs->file_start);
//            pbs->running = kfalse;
//        }
//
//    }
//
//    fprintf(stdout, "_run_script() - EXIT %s.%s()\n", pbs->file_name, pbs->function_name);
//
//
//    // upload changes to the shared global variables
//    _update_global(psgs, pss);
//
//
//    // Reset the stacks...
//    assert(pbgs->int_stack_head >= pbs->int_link);
//    pbgs->int_stack_head = pbs->int_link;
//
//    assert(pbgs->flt_stack_head >= pbs->flt_link);
//    pbgs->flt_stack_head = pbs->flt_link;
//
//    // Return ktrue if pss->return_int is set, kfalse if pss->return_flt is set
//    return pbs->return_is_int;
//}

//-----------------------------------------------------------------------------------------------

//static bool_t _soulfu_run_script(PSoulfuScriptContext pss)
//{
//    // <ZZ> This function runs a script, starting with whatever opcode is at pss->base.base.function_start.
//    OPCODE opcode;
//    size_t offset;
//    bool_t handled = kfalse;
//
//    Sint32 i;
//    Sint32 j;
//    Sint32 k;
//    Sint32 m;
//    float  f;
//    float  e;
//
//    OBJECT_PTR obj_ptr;
//
//    Uint8 ui8;
//
//    Uint8* ptmp_a;
//    Uint8* ptmp_b;
//    Uint8* ptmp_c;
//
//    SDF_PHEADER phdr_a;
//    SDF_PHEADER phdr_b;
//    SDF_PHEADER loc_ph;
//
//    PBaseGlobalScriptContext pgs;
//
//    // for convenience
//    pgs = ps->pgs;
//    pss->base.base.int_link = pss->base.base.pbgs->int_stack_head;
//    pss->base.base.flt_link = pss->base.base.pbgs->flt_stack_head;
//
//    // debugging info
//    loc_ph = sdf_archive_find_header_by_data(pss->base.base.file_start);
//    pss->base.base.file_name = sdf_file_get_name(loc_ph);
//    fprintf(stdout, "_run_script() - %s.%s()\n", pss->base.base.file_name, pss->base.base.function_name);
//
//    // initialize the token stream
//    sdf_stream_open_mem(&pss->base.base.tokens, pss->base.base.function_start, 0);
//
//    // Keep going until we hit a return...
//    pss->base.base.running = ktrue;
//    while (pss->base.base.running)
//    {
//        // Read the opcode
//        offset = pss->base.base.tokens.read - pss->base.base.file_start;
//        opcode = _receive_uint08(&pss->base.base.tokens);
//
//        // Show debug info
//        // show_int_stack(pgs);
//        // show_flt_stack(pgs);
//        log_info(1, "\t0x%06d: (0x%02x) %s ", offset, opcode, opcode_name[opcode]);
//
//        handled = _run_opcode_basic(ps, opcode);
//
//        if(!handled)
//        {
//            // handle sf_script functions
//            handled = _run_opcode_soulfu(ps, opcode);
//        }
//
//        if(!handled)
//        {
//            // handle soulfu_script_library functions
//            handled = run_opcode_external(ps, opcode);
//        }
//
//    }
//
//    fprintf(stdout, "_run_script() - EXIT %s.%s()\n", pss->base.base.file_name, pss->base.base.function_name);
//
//    // upload the shared info to the GlobalScriptContext
//    if(NULL != ps->pgs)
//    {
//        pgs->return_is_int = pss->base.base.return_int;
//        pgs->return_int    = pss->base.base.return_int;
//        pgs->return_flt    = pss->base.base.return_flt;
//
//        //// download the spawn info
//        //memcpy(&(pgs->spawn_info), &(ps->spawn_info), sizeof(SPAWN_INFO));
//
//        //// download the enchant cursor
//        //memcpy(&(pgs->enc_cursor), &(ps->enc_cursor), sizeof(ENCHANT_CURSOR));
//    }
//
//
//    // Reset the stacks...
//    assert(pgs->int_stack_head >= pss->base.base.int_link);
//    pgs->int_stack_head = pss->base.base.int_link;
//
//    assert(pgs->flt_stack_head >= pss->base.base.flt_link);
//    pgs->flt_stack_head = pss->base.base.flt_link;
//
//    // Return ktrue if pss->base.base.return_int is set, kfalse if pss->base.base.return_flt is set
//    return pss->base.base.return_is_int;
//}
//

//-----------------------------------------------------------------------------------------------
bool_t sf_fast_rerun_script(PSoulfuScriptContext pss, Uint16 fast_function)
{
    if (NULL == pxss_prepare_fast_run( &(pss->base), fast_function) )
    {
        return kfalse;
    };

    return sf_run_script(pss, kfalse);
};

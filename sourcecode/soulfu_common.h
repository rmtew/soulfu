#pragma once

#include "soulfu_types.h"

struct sdf_stream_t;
typedef struct sdf_stream_t * SDF_PSTREAM;

extern float main_timer_fps;
extern Uint32 main_timer_length;
void main_timer_start();
void main_timer_end();

//-Macros----------------------------------------------------------------------------------------

#define repeat(AA, BB) for(AA=0;  AA<BB;  AA++)
#define CAST(AA,BB)  ( (AA) (BB) )
#define DEREF(AA,BB) ( *CAST(AA *,BB) )

#define MEG            0x00100000
#define BUFFERSIZE     (4 * MEG)
#define MAINBUFFERSIZE (8 * BUFFERSIZE) // 16 Meg for temporary buffers, 4 Meg for map, 12 Meg for room...

#define HAS_ALL_FLAGS (TEST, FLAGS) ( (FLAGS) == ((TEST) & (FLAGS)) )
#define HAS_SOME_FLAGS(TEST, FLAGS) ( 0       != ((TEST) & (FLAGS)) )
#define HAS_NO_FLAGS  (TEST, FLAGS) ( 0       == ((TEST) & (FLAGS)) )

#define FLAGS_ADD(VAL, FLAGS)     (VAL) |= (FLAGS)
#define FLAGS_REMOVE(VAL, FLAGS)  (VAL) &= (~(FLAGS))


//-----------------------------------------------------------------------------------------------

extern Uint8* mainbuffer;       // A General purpose buffer
extern Uint8* subbuffer;        // A subset of the mainbuffer
extern Uint8* thirdbuffer;      // A subset of the mainbuffer
extern Uint8* fourthbuffer;     // A subset of the mainbuffer
extern Uint8* mapbuffer;        // A subset of the mainbuffer
extern Uint8* roombuffer;       // A subset of the mainbuffer

#define MAX_STRING 16                           // Global string variables for string functions...
#define MAX_STRING_SIZE 256

void make_uppercase(char *string);
void free_mainbuffer(void);
Sint8 buffer_setup(void);
Sint32 count_indentation(SDF_PSTREAM pstream);
void datadump(Uint8* location, Sint32 size, Sint8 append);

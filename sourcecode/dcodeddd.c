// <ZZ> This file contains functions to convert DDD files to RDY
//      decode_ddd              - The main function to do a DDD conversion

#include "sdf_archive.h"
#include "display.h"
#include "object.h"
#include "logfile.h"

#include "soulfu_math.inl"
#include "soulfu_endian.inl"


#define NO_LINE_FLAG  64

//-----------------------------------------------------------------------------------------------
// Variables for seeing how well/poorly the fanning/stripping is working/not working
Uint8* global_base_old_vertex_start;
Uint8* global_base_old_tex_vertex_start;
Uint8* global_base_old_texture_start;
Uint8* global_base_old_joint_start;
Uint16 global_base_num_vertex;
Uint16 global_base_num_tex_vertex;
Uint16 global_base_num_triangle;
Uint32 global_base_data_size;

//-----------------------------------------------------------------------------------------------
void ddd_generate_model_action_list(Uint8* data)
{
    // <ZZ> This function generates an action list for a given RDY model.  Action list is a
    //      list of the first frame for each defined action, or FFFF if action unnavailable.
    Uint8* frame_data;
    Uint8** frame_data_start;
    Uint16* action_list;
    Uint16 num_bone_frame;
    Uint16 i, j;
    Uint8 num_base_model;


    data += 3;
    num_base_model = *data;  data++;
    num_bone_frame = DEREF( Uint16, data );  data += 2;
    action_list = (Uint16*) data;
    data += (ACTION_MAX << 1);
    data += (MAX_DDD_SHADOW_TEXTURE);
    frame_data_start =  (Uint8**) (data + (num_base_model * 20 * DETAIL_LEVEL_MAX));


    // Go through each action...
    repeat(i, ACTION_MAX)
    {
        // Go through each frame looking for first instance...
        action_list[i] = UINT16_MAX;
        repeat(j, num_bone_frame)
        {
            frame_data = frame_data_start[j];

            if (*frame_data == i)
            {
                action_list[i] = j;
                j = num_bone_frame;
            }
        }
    }
}

//-----------------------------------------------------------------------------------------------
void ddd_remove_data(Uint8* data_block_start, Uint32 data_size, Uint8* data_to_remove, Uint16 bytes_to_remove)
{
    // <ZZ> This function removes several bytes from a block of data, and scooches everything after
    //      back to fit...
    Sint32 bytes_to_move;
    bytes_to_move = ((Uint32) data_block_start) + data_size - ((Uint32) data_to_remove) - bytes_to_remove;

    if (bytes_to_move > 0)
    {
        memmove(data_to_remove, data_to_remove + bytes_to_remove, bytes_to_move);
    }

    memset(data_block_start + data_size - bytes_to_remove, 0, bytes_to_remove);
}

//-----------------------------------------------------------------------------------------------
void ddd_simplify_two_vertices(Uint16* num_vertex_spot, Uint16* num_tex_vertex_spot, Uint16 vertex_to_keep, Uint16 vertex_to_remove, Uint16 tex_vertex_to_keep, Uint16 tex_vertex_to_remove, Uint8* data, Uint32 data_size, Uint8 num_texture)
{
    // <ZZ> This combines two vertices into one, and deletes any associated triangle.
    Uint16 temp;
    Uint8* triangle_data;
    Uint8* strip_data_start;
    float* first_xyz;
    float* second_xyz;
    Uint16 i, j, k;
    Uint16 num_triangle;
    Uint16 matches;



    // Figure out which one to keep...
    if (vertex_to_keep > vertex_to_remove)
    {
        // Swap 'em, so the lower indexed vertex is kept...
        temp = vertex_to_keep;
        vertex_to_keep = vertex_to_remove;
        vertex_to_remove = temp;
    }


    // Do the same for tex vertices...
    if (tex_vertex_to_keep > tex_vertex_to_remove)
    {
        // Swap 'em, so the lower indexed tex_vertex is kept...
        temp = tex_vertex_to_keep;
        tex_vertex_to_keep = tex_vertex_to_remove;
        tex_vertex_to_remove = temp;
    }


    // Go through triangles...  Delete any that contains both of the vertices...  Renumber any
    // instance of the removed vertex...  Handle tex vertices too.
    triangle_data = data + (num_vertex_spot[0] << 5) + (num_tex_vertex_spot[0] << 3);
    repeat(i, num_texture)
    {
        triangle_data++;

        if (*(triangle_data - 1) != 0)  // Mode
        {
            strip_data_start = triangle_data;
            num_triangle = DEREF( Uint16, triangle_data );  triangle_data += 2;
            repeat(j, num_triangle)
            {
                triangle_data += 2;  // Skip number of points in strip
                matches = 0;
                repeat(k, 3)
                {
                    if (DEREF( Uint16, triangle_data ) == vertex_to_remove)
                        DEREF( Uint16, triangle_data ) = vertex_to_keep;

                    if (DEREF( Uint16, triangle_data ) == vertex_to_keep)
                        matches++;

                    if (DEREF( Uint16, triangle_data ) > vertex_to_remove)
                        DEREF( Uint16, triangle_data ) -= 1;

                    triangle_data += 2;

                    if (tex_vertex_to_keep != tex_vertex_to_remove)
                    {
                        if (DEREF( Uint16, triangle_data ) == tex_vertex_to_remove)
                            DEREF( Uint16, triangle_data ) = tex_vertex_to_keep;

                        if (DEREF( Uint16, triangle_data ) > tex_vertex_to_remove)
                            DEREF( Uint16, triangle_data ) -= 1;
                    }

                    triangle_data += 2;
                }

                // Delete the triangle if need be...
                if (matches > 1)
                {
                    if (num_triangle > 1)
                    {
                        // Just remove the triangle...
                        ddd_remove_data(data, data_size, triangle_data - 14, 14);
                        num_triangle--;
                        DEREF( Uint16, strip_data_start ) = num_triangle;
                        triangle_data -= 14;
                        j--;
                    }
                    else
                    {
                        // Remove the entire texture...
                        ddd_remove_data(data, data_size, strip_data_start - 1, 18);
                        *(strip_data_start - 1) = 0;
                        triangle_data = strip_data_start - 2;
                    }
                }
            }
            triangle_data += 2;  // Skip number of fans
        }
    }


    // Average the positions of the points...
    first_xyz = (float*) (data + (vertex_to_keep << 5));
    second_xyz = (float*) (data + (vertex_to_remove << 5));
    first_xyz[XX] = (first_xyz[XX] + second_xyz[XX]) * 0.5f;
    first_xyz[YY] = (first_xyz[YY] + second_xyz[YY]) * 0.5f;
    first_xyz[ZZ] = (first_xyz[ZZ] + second_xyz[ZZ]) * 0.5f;
    // Increase the weight, making it harder to remove in the future...
    first_xyz[6] += 0.5f;

    if (tex_vertex_to_keep != tex_vertex_to_remove)
    {
        first_xyz = (float*) (data + (num_vertex_spot[0] << 5) + (tex_vertex_to_keep << 3));
        second_xyz = (float*) (data + (num_vertex_spot[0] << 5) + (tex_vertex_to_remove << 3));
        first_xyz[XX] = (first_xyz[XX] + second_xyz[XX]) * 0.5f;
        first_xyz[YY] = (first_xyz[YY] + second_xyz[YY]) * 0.5f;
    }


    // Now delete the vertex and the texture vertex...  Higher numbered references should have
    // been renumbered...
    num_vertex_spot[0]--;
    ddd_remove_data(data, data_size, data + (vertex_to_remove << 5), 32);

    if (tex_vertex_to_keep != tex_vertex_to_remove)
    {
        num_tex_vertex_spot[0]--;
        ddd_remove_data(data, data_size, data + (num_vertex_spot[0] << 5) + (tex_vertex_to_remove << 3), 8);
    }
}

//-----------------------------------------------------------------------------------------------
Uint8 ddd_simplify_once(Uint16* num_vertex_spot, Uint16* num_tex_vertex_spot, Uint8* data, Uint32 data_size, Uint8 num_texture)
{
    // <ZZ> This function determines the best pair of vertices to get rid of, then gets rid of
    //      'em.
    Uint16 i, j, k;
    Uint16 best_vertex[2];
    Uint16 best_tex_vertex[2];
    float best_size;
    float size;
    Uint8* triangle_data;
    Uint16 num_triangle;
    Uint16 triangle_vertex[3];
    Uint16 triangle_tex_vertex[3];
    float* vertexa_xyz;
    float* vertexb_xyz;
    float* vertexc_xyz;
    float side_xyz[3];

    best_size = 999999.0f;
    triangle_data = data + (num_vertex_spot[0] << 5) + (num_tex_vertex_spot[0] << 3);
    repeat(i, num_texture)
    {
        triangle_data++;

        if (*(triangle_data - 1) != 0)  // Mode
        {
            num_triangle = DEREF( Uint16, triangle_data );  triangle_data += 2;
            repeat(j, num_triangle)
            {
                triangle_data += 2;  // Skip number of points in strip...  Assumed to be 3...
                repeat(k, 3)
                {
                    triangle_vertex[k] = DEREF( Uint16, triangle_data );  triangle_data += 2;
                    triangle_tex_vertex[k] = DEREF( Uint16, triangle_data );  triangle_data += 2;
                }
                vertexa_xyz = (float*) (data + (triangle_vertex[0] << 5));
                vertexb_xyz = (float*) (data + (triangle_vertex[1] << 5));
                vertexc_xyz = (float*) (data + (triangle_vertex[2] << 5));
                // Find the shortest edge of triangle...  Side AB
                side_xyz[XX] = vertexb_xyz[XX] - vertexa_xyz[XX];
                side_xyz[YY] = vertexb_xyz[YY] - vertexa_xyz[YY];
                side_xyz[ZZ] = vertexb_xyz[ZZ] - vertexa_xyz[ZZ];
                size = (side_xyz[XX] * side_xyz[XX]) + (side_xyz[YY] * side_xyz[YY]) + (side_xyz[ZZ] * side_xyz[ZZ]);
                size = size * (vertexb_xyz[6] + vertexa_xyz[6]);  // Factor vertex weight into it...

                if (size < best_size)
                {
                    best_vertex[0] = triangle_vertex[0];
                    best_vertex[1] = triangle_vertex[1];
                    best_tex_vertex[0] = triangle_tex_vertex[0];
                    best_tex_vertex[1] = triangle_tex_vertex[1];
                    best_size = size;
                }

                // Find the shortest edge of triangle...  Side BC
                side_xyz[XX] = vertexc_xyz[XX] - vertexb_xyz[XX];
                side_xyz[YY] = vertexc_xyz[YY] - vertexb_xyz[YY];
                side_xyz[ZZ] = vertexc_xyz[ZZ] - vertexb_xyz[ZZ];
                size = (side_xyz[XX] * side_xyz[XX]) + (side_xyz[YY] * side_xyz[YY]) + (side_xyz[ZZ] * side_xyz[ZZ]);
                size = size * (vertexc_xyz[6] + vertexb_xyz[6]);  // Factor vertex weight into it...

                if (size < best_size)
                {
                    best_vertex[0] = triangle_vertex[1];
                    best_vertex[1] = triangle_vertex[2];
                    best_tex_vertex[0] = triangle_tex_vertex[1];
                    best_tex_vertex[1] = triangle_tex_vertex[2];
                    best_size = size;
                }

                // Find the shortest edge of triangle...  Side CA
                side_xyz[XX] = vertexa_xyz[XX] - vertexc_xyz[XX];
                side_xyz[YY] = vertexa_xyz[YY] - vertexc_xyz[YY];
                side_xyz[ZZ] = vertexa_xyz[ZZ] - vertexc_xyz[ZZ];
                size = (side_xyz[XX] * side_xyz[XX]) + (side_xyz[YY] * side_xyz[YY]) + (side_xyz[ZZ] * side_xyz[ZZ]);
                size = size * (vertexa_xyz[6] + vertexc_xyz[6]);  // Factor vertex weight into it...

                if (size < best_size)
                {
                    best_vertex[0] = triangle_vertex[2];
                    best_vertex[1] = triangle_vertex[0];
                    best_tex_vertex[0] = triangle_tex_vertex[2];
                    best_tex_vertex[1] = triangle_tex_vertex[0];
                    best_size = size;
                }
            }
            triangle_data += 2;  // Skip number of fans
        }
    }


    // Did we find a triangle to axe?
    if (best_size < 999990.0f)
    {
        ddd_simplify_two_vertices(num_vertex_spot, num_tex_vertex_spot, best_vertex[0], best_vertex[1], best_tex_vertex[0], best_tex_vertex[1], data, data_size, num_texture);
        return ktrue;
    }

    return kfalse;
}

//-----------------------------------------------------------------------------------------------
void ddd_simplify_geometry(Uint16* num_vertex_spot, Uint16* num_tex_vertex_spot, Uint8* data, Uint8 percent, Uint32 data_size, Uint8 num_texture)
{
    // <ZZ> Given a block of vertex/texture vertex/texture data, this function spits out a simpler
    //      version of that data (depending on detail percent).  The values at num_vertex_spot and
    //      num_tex_vertex_spot are changed by the function to show the new number of vertices and
    //      texture vertices.
    Uint16 num_vertex_wanted;
    Uint8 can_remove_more;
    Uint8* vertex_data;
    Uint8* triangle_data;
    Uint16 num_triangle;
    Uint16 triangle_vertex[3];
    float start_xyz[3];
    float end_xyz[3];
    float side_xyz[3];
    float front_xyz[3];
    float* normal_xyz;
    float length;
    Uint16 i, j, k;



    // Clear out the extra vertex data we've been given...
    vertex_data = data;
    repeat(i, num_vertex_spot[0])
    {
        DEREF( float, vertex_data + 12 ) = 0;         // Normal X
        DEREF( float, vertex_data + 16 ) = 0;         // Normal Y
        DEREF( float, vertex_data + 20 ) = 0;         // Normal Z
        DEREF( float, vertex_data + 24 ) = 0;         // Weight
        DEREF( Uint16, vertex_data + 28 ) = 0;  // Neighbor count
        DEREF( Uint16, vertex_data + 30 ) = i;  // Original vertex
        vertex_data += 32;
    }


    // Figure out normal and neighbor count for each vertex...
    vertex_data = data;
    triangle_data = data + (num_vertex_spot[0] << 5) + (num_tex_vertex_spot[0] << 3);
    repeat(i, num_texture)
    {
        triangle_data++;

        if (*(triangle_data - 1) != 0)  // Mode
        {
            num_triangle = DEREF( Uint16, triangle_data );  triangle_data += 2;
            repeat(j, num_triangle)
            {
                triangle_data += 2;  // Skip number of points in strip, assume to be 3
                repeat(k, 3)
                {
                    triangle_vertex[k] = DEREF( Uint16, triangle_data );  triangle_data += 4;
                    // Increase the neighbor count for each vertex...
                    DEREF( Uint16, vertex_data + 28 + (triangle_vertex[k] << 5) ) += 1;
                }
                // Figure out the normal of the triangle
                start_xyz[XX] = DEREF( float, vertex_data + (triangle_vertex[0] << 6) );
                start_xyz[YY] = DEREF( float, vertex_data + 4 + (triangle_vertex[0] << 6) );
                start_xyz[ZZ] = DEREF( float, vertex_data + 8 + (triangle_vertex[0] << 6) );
                end_xyz[XX] = DEREF( float, vertex_data + (triangle_vertex[1] << 6) );
                end_xyz[YY] = DEREF( float, vertex_data + 4 + (triangle_vertex[1] << 6) );
                end_xyz[ZZ] = DEREF( float, vertex_data + 8 + (triangle_vertex[1] << 6) );
                end_xyz[XX] -= start_xyz[XX];
                end_xyz[YY] -= start_xyz[YY];
                end_xyz[ZZ] -= start_xyz[ZZ];
                side_xyz[XX] = DEREF( float, vertex_data + (triangle_vertex[2] << 6) );
                side_xyz[YY] = DEREF( float, vertex_data + 4 + (triangle_vertex[2] << 6) );
                side_xyz[ZZ] = DEREF( float, vertex_data + 8 + (triangle_vertex[2] << 6) );
                side_xyz[XX] -= start_xyz[XX];
                side_xyz[YY] -= start_xyz[YY];
                side_xyz[ZZ] -= start_xyz[ZZ];
                cross_product(end_xyz, side_xyz, front_xyz);
                // Normalize the normal
                length = vector_length(front_xyz);

                if (length > 0.01f)
                {
                    front_xyz[XX] /= length;
                    front_xyz[YY] /= length;
                    front_xyz[ZZ] /= length;
                }

                // Add the normal to each of the vertices
                repeat(k, 3)
                {
                    DEREF( float, vertex_data + 12 + (triangle_vertex[k] << 5) ) += front_xyz[XX];
                    DEREF( float, vertex_data + 16 + (triangle_vertex[k] << 5) ) += front_xyz[YY];
                    DEREF( float, vertex_data + 20 + (triangle_vertex[k] << 5) ) += front_xyz[ZZ];
                }
            }
            triangle_data += 2;  // Skip number of fans
        }
    }


    // Calculate vertex weights...
    repeat(i, num_vertex_spot[0])
    {
        normal_xyz = ((float*) (vertex_data + 12 + (i << 5)));
        length = vector_length(normal_xyz);

        if (DEREF( Uint16, vertex_data + 28 + (i << 5) ) > 0)
        {
            // Weight is the similarity of normals...
            DEREF( float, vertex_data + 24 + (i << 5) ) = 1.0f - (0.5f * (length / (DEREF( Uint16, vertex_data + 28 + (i << 5) ))));
        }
    }


    // Start removing vertices...
    num_vertex_wanted = (num_vertex_spot[0] * percent) >> 8;

    if (num_vertex_wanted < 16)
    {
        // So eyes and other little details come through okay...
        num_vertex_wanted = 16;
    }

    can_remove_more = ktrue;

    while (can_remove_more && num_vertex_spot[0] > num_vertex_wanted)
    {
        can_remove_more = ddd_simplify_once(num_vertex_spot, num_tex_vertex_spot, data, data_size, num_texture);
    }
}

//-----------------------------------------------------------------------------------------------
void ddd_continue_simplify(Uint16* num_vertex_spot, Uint16* num_tex_vertex_spot, Uint8* data, Uint8 percent, Uint32 data_size, Uint8 num_texture, Uint16 num_vertex)
{
    // <ZZ> Optimization (read hack) to ddd_simplify_geometry that allows a lower detail level's
    //      simplification process to start with the last detail level's data...
    Uint16 num_vertex_wanted;
    Uint8 can_remove_more;


    // Start removing vertices...
    num_vertex_wanted = (num_vertex * percent) >> 8;
    log_info(1, "Continue simplify");
    log_info(1, "Have %d vertices, Want %d", num_vertex_spot[0], num_vertex_wanted);

    if (num_vertex_wanted < 8)
    {
        // So eyes and other little details come through okay...
        num_vertex_wanted = 8;
    }

    can_remove_more = ktrue;

    while (can_remove_more && num_vertex_spot[0] > num_vertex_wanted)
    {
        can_remove_more = ddd_simplify_once(num_vertex_spot, num_tex_vertex_spot, data, data_size, num_texture);

        log_info(1, "Have %d vertices, Want %d", num_vertex_spot[0], num_vertex_wanted);
    }
}

//-----------------------------------------------------------------------------------------------
void add_cartoon_line(Uint16 vertex_a, Uint16 vertex_b, Uint16 check, Uint8 force_line, Uint16* cartoon_data)
{
    // <ZZ> This function adds a line between two vertices (if there isn't already one between 'em).
    //      The check points are points on nearby triangles that are used to determine if the line
    //      is visible or not (visible if both check points are on same side of line, when seen
    //      from camera position).
    //      If called once for a given vertex pair, the line is assumed to be always on (if force_line is ktrue)...
    //      If called twice, the two check points are used to figure if the line is on or not...
    int num_cartoon_line;
    Uint16* data;
    Uint8 found;
    Uint16 i;

    num_cartoon_line = *cartoon_data;
    data = cartoon_data + 1;
    found = kfalse;
    repeat(i, num_cartoon_line)
    {
        if (*data == vertex_a || *data == vertex_b)
        {
            if (*(data + 1) == vertex_a || *(data + 1) == vertex_b)
            {
                if (*(data + 2) != check)
                {
                    found = ktrue;
                    i = num_cartoon_line;
                    // Fill in the second check point...
                    *(data + 3) = check;
                }
            }
        }

        data += 4;
    }

    if (found == kfalse)
    {
        // Line doesn't exist yet, so add it...
        *data = vertex_a;  data++;
        *data = vertex_b;  data++;
        *data = check;  data++;

        if (force_line)
        {
            *data = check;
        }
        else
        {
            *data = UINT16_MAX;  // Go back and remove these entries later...
        }

        num_cartoon_line++;
        *cartoon_data = num_cartoon_line;
    }
}

//-----------------------------------------------------------------------------------------------
void remove_cartoon_lines(Uint16* cartoon_data)
{
    // <ZZ> This function deletes any cartoon line with UINT16_MAX as its second check point...
    //      Bare edges of culled textures...
    int num_cartoon_line;
    Uint16* data;
    Uint16* copy_data;
    Uint16 i, j;

    num_cartoon_line = *cartoon_data;
    data = cartoon_data + 1;
    repeat(i, num_cartoon_line)
    {
        if (*(data + 3) == UINT16_MAX)
        {
            // We should delete this one...  Bump all others back...
            copy_data = data;
            j = i + 1;

            while (j < num_cartoon_line)
            {
                *(copy_data)   = *(copy_data + 4);
                *(copy_data + 1) = *(copy_data + 5);
                *(copy_data + 2) = *(copy_data + 6);
                *(copy_data + 3) = *(copy_data + 7);
                copy_data += 4;
                j++;
            }

            // Remember that it's been deleted...
            num_cartoon_line--;
            *cartoon_data = num_cartoon_line;
            i--;
        }
        else
        {
            data += 4;
        }
    }
}

//-----------------------------------------------------------------------------------------------
Uint8* ddd_create_strip(Uint8* helper_data, Uint8* new_data, Uint16 main_triangle, Uint16 num_triangle)
{
    // <ZZ> This function adds a strip for the strip_fan_geometry function...  Returns next primitive
    //      position...  Checks all 3 directions for largest strip possible, and uses that one...
    Uint8 texture;
    Uint16 look_triangle;
    Uint16 test_triangle;
    Uint16 last_triangle[3];
    Uint16 num_found_triangle[3];
    Uint16 vertex[3];
    Uint16 tex_vertex[3];
    Uint16 starting_vertex[3];
    Uint16 vertex_edge[2];
    Uint16 tex_vertex_edge[2];
    Uint16 i, j, k;
    Uint8* new_data_start;
    Uint8* temp_data;
    Uint16 set_count;
    Uint8 keep_looking;
    Uint8 looking_for_neighbor;
    Uint16 num_found_vertex;
    Uint16 order, second;

    texture = *(helper_data + 30 + (main_triangle << 5));
    new_data_start = new_data;

    log_info(1, "Making triangle strip that includes triangle %d", main_triangle);

    // Check all 3 directions for strip size...  Direction depends on the starting vertex...
    repeat(i, 3)
    {
        log_info(1, "Stripping attempt %d", i);

        // Clear out all temporary used flags
        temp_data = helper_data;
        repeat(j, num_triangle)
        {
            *(temp_data + 15) &= USED_PERMANENT;
            temp_data += 16;
        }


        // Figure out which vertices are in the triangle...
        repeat(j, 3)
        {
            k = (i + j) % 3;
            vertex[j] = DEREF( Uint16, helper_data + (main_triangle << 5) + (k << 1) + 18 );
            tex_vertex[j] = DEREF( Uint16, helper_data + (main_triangle << 5) + (k << 1) + 24 );
        }


        // Mark main triangle as being used...
        log_info(1, "Added triangle %d (main)", main_triangle);

        *(helper_data + (main_triangle << 5) + 31) |= USED_TEMPORARY;
        num_found_triangle[i] = 1;


        // Go forwards...  Connected triangle should have points 1 and 2...
        starting_vertex[i] = vertex[0];
        look_triangle = main_triangle;
        vertex_edge[0] = vertex[1];
        vertex_edge[1] = vertex[2];
        tex_vertex_edge[0] = tex_vertex[1];
        tex_vertex_edge[1] = tex_vertex[2];
        keep_looking = ktrue;

        while (keep_looking)
        {
            // Find a neighbor who uses both vertices in the edge, and make it our current triangle...
            looking_for_neighbor = ktrue;
            j = 0;

            while (looking_for_neighbor && j < DEREF( Uint16, helper_data + (look_triangle << 5) ) && j < 8)
            {
                test_triangle = DEREF( Uint16, helper_data + (look_triangle << 5) + 2 + (j << 1) );

                if (*(helper_data + (test_triangle << 5) + 31) == 0 && *(helper_data + (test_triangle << 5) + 30) == texture)
                {
                    // Hasn't been used yet and is of the proper texture...  Does it contain both of the vertices?
                    if ((DEREF( Uint16, helper_data + (test_triangle << 5) + 18 ) == vertex_edge[0]) || (DEREF( Uint16, helper_data + (test_triangle << 5) + 20 ) == vertex_edge[0]) || (DEREF( Uint16, helper_data + (test_triangle << 5) + 22 ) == vertex_edge[0]))
                    {
                        // Contains the first vertex...  How bout the second?
                        if ((DEREF( Uint16, helper_data + (test_triangle << 5) + 18 ) == vertex_edge[1]) || (DEREF( Uint16, helper_data + (test_triangle << 5) + 20 ) == vertex_edge[1]) || (DEREF( Uint16, helper_data + (test_triangle << 5) + 22 ) == vertex_edge[1]))
                        {
                            // Contains both vertices...  How bout tex vertices?
                            if ((DEREF( Uint16, helper_data + (test_triangle << 5) + 24 ) == tex_vertex_edge[0]) || (DEREF( Uint16, helper_data + (test_triangle << 5) + 26 ) == tex_vertex_edge[0]) || (DEREF( Uint16, helper_data + (test_triangle << 5) + 28 ) == tex_vertex_edge[0]))
                            {
                                // First okay...  Second?
                                if ((DEREF( Uint16, helper_data + (test_triangle << 5) + 24 ) == tex_vertex_edge[1]) || (DEREF( Uint16, helper_data + (test_triangle << 5) + 26 ) == tex_vertex_edge[1]) || (DEREF( Uint16, helper_data + (test_triangle << 5) + 28 ) == tex_vertex_edge[1]))
                                {
                                    // Use it as our next triangle...
                                    look_triangle = test_triangle;
                                    looking_for_neighbor = kfalse;
                                }
                                else
                                {
                                    // !!!BAD!!!
                                    // !!!BAD!!!
                                    // !!!BAD!!!  Get these errors a lot...  Think I need to check if texture mode is on...
                                    // !!!BAD!!!
                                    // !!!BAD!!!

                                    log_error(1, "Fore Tex Vertex 1 %d", tex_vertex_edge[1]);
                                }
                            }
                            else
                            {
                                log_error(1, "Fore Tex Vertex 0 %d", tex_vertex_edge[0]);
                            }
                        }
                    }
                }

                j++;
            }

            if (looking_for_neighbor)
            {
                // Didn't find a valid neighbor...  Must be all done in this direction...
                keep_looking = kfalse;
            }
            else
            {
                log_info(1, "Added triangle %d (forward)", look_triangle);

                num_found_triangle[i]++;
                *(helper_data + (look_triangle << 5) + 31) = USED_TEMPORARY;


                // Figure out next pair of vertices...
                repeat(j, 3)
                {
                    if (DEREF( Uint16, helper_data + 18 + (j << 1) + (look_triangle << 5) ) != vertex_edge[0] && DEREF( Uint16, helper_data + 18 + (j << 1) + (look_triangle << 5) ) != vertex_edge[1])
                    {
                        vertex_edge[0] = vertex_edge[1];
                        vertex_edge[1] = DEREF( Uint16, helper_data + 18 + (j << 1) + (look_triangle << 5) );
                        tex_vertex_edge[0] = tex_vertex_edge[1];
                        tex_vertex_edge[1] = DEREF( Uint16, helper_data + 24 + (j << 1) + (look_triangle << 5) );
                        j = 3;
                    }
                }
            }
        }




        // Go backwards...  Count in sets of two...  Must have points 0 and 1....
        look_triangle = main_triangle;
        vertex_edge[0] = vertex[1];
        vertex_edge[1] = vertex[0];
        tex_vertex_edge[0] = tex_vertex[1];
        tex_vertex_edge[1] = tex_vertex[0];
        set_count = 0;
        keep_looking = ktrue;

        while (keep_looking)
        {
            // Find a neighbor who uses both vertices in the edge, and make it our current triangle...
            looking_for_neighbor = ktrue;
            j = 0;

            while (looking_for_neighbor && j < DEREF( Uint16, helper_data + (look_triangle << 5) ) && j < 8)
            {
                test_triangle = DEREF( Uint16, helper_data + (look_triangle << 5) + 2 + (j << 1) );

                if (*(helper_data + (test_triangle << 5) + 31) == 0 && *(helper_data + (test_triangle << 5) + 30) == texture)
                {
                    // Hasn't been used yet and is of the proper texture...  Does it contain both of the vertices?
                    if ((DEREF( Uint16, helper_data + (test_triangle << 5) + 18 ) == vertex_edge[0]) || (DEREF( Uint16, helper_data + (test_triangle << 5) + 20 ) == vertex_edge[0]) || (DEREF( Uint16, helper_data + (test_triangle << 5) + 22 ) == vertex_edge[0]))
                    {
                        // Contains the first vertex...  How bout the second?
                        if ((DEREF( Uint16, helper_data + (test_triangle << 5) + 18 ) == vertex_edge[1]) || (DEREF( Uint16, helper_data + (test_triangle << 5) + 20 ) == vertex_edge[1]) || (DEREF( Uint16, helper_data + (test_triangle << 5) + 22 ) == vertex_edge[1]))
                        {
                            // Contains both vertices...  How bout tex vertices?
                            if ((DEREF( Uint16, helper_data + (test_triangle << 5) + 24 ) == tex_vertex_edge[0]) || (DEREF( Uint16, helper_data + (test_triangle << 5) + 26 ) == tex_vertex_edge[0]) || (DEREF( Uint16, helper_data + (test_triangle << 5) + 28 ) == tex_vertex_edge[0]))
                            {
                                // First okay...  Second?
                                if ((DEREF( Uint16, helper_data + (test_triangle << 5) + 24 ) == tex_vertex_edge[1]) || (DEREF( Uint16, helper_data + (test_triangle << 5) + 26 ) == tex_vertex_edge[1]) || (DEREF( Uint16, helper_data + (test_triangle << 5) + 28 ) == tex_vertex_edge[1]))
                                {
                                    // If this one ends up being our last triangle, we'll need to remember a starting vertex...
                                    log_info(1, "Should we set start vertex???");

                                    if (set_count == 1 && look_triangle != main_triangle)
                                    {
                                        log_info(1, "Setting the start vertex for direction %d", i);

                                        if ((DEREF( Uint16, helper_data + (test_triangle << 5) + 18 ) != vertex_edge[0]) && (DEREF( Uint16, helper_data + (test_triangle << 5) + 18 ) != vertex_edge[1]))
                                        {
                                            starting_vertex[i] = DEREF( Uint16, helper_data + (test_triangle << 5) + 18 );
                                            log_info(1, "Start vertex is %d (first)", starting_vertex[i]);
                                        }
                                        else if ((DEREF( Uint16, helper_data + (test_triangle << 5) + 20 ) != vertex_edge[0]) && (DEREF( Uint16, helper_data + (test_triangle << 5) + 20 ) != vertex_edge[1]))
                                        {
                                            starting_vertex[i] = DEREF( Uint16, helper_data + (test_triangle << 5) + 20 );
                                            log_info(1, "Start vertex is %d (second)", starting_vertex[i]);
                                        }
                                        else if ((DEREF( Uint16, helper_data + (test_triangle << 5) + 22 ) != vertex_edge[0]) && (DEREF( Uint16, helper_data + (test_triangle << 5) + 22 ) != vertex_edge[1]))
                                        {
                                            starting_vertex[i] = DEREF( Uint16, helper_data + (test_triangle << 5) + 22 );
                                            log_info(1, "Start vertex is %d (third)", starting_vertex[i]);
                                        }
                                    }


                                    // Contains both vertices...  Use it as our next triangle...
                                    last_triangle[i] = look_triangle;
                                    look_triangle = test_triangle;
                                    looking_for_neighbor = kfalse;
                                    set_count = (set_count + 1) & 1;
                                }
                                else
                                {
                                    log_error(1, "Back Tex Vertex 1 %d", tex_vertex_edge[1]);
                                }
                            }
                            else
                            {
                                log_error(1, "Back Tex Vertex 0 %d", tex_vertex_edge[0]);
                            }
                        }
                    }
                }

                j++;
            }

            if (looking_for_neighbor)
            {
                // Didn't find a valid neighbor...  Must be all done in this direction...
                keep_looking = kfalse;
            }
            else
            {
                log_info(1, "Added triangle %d (backward)", look_triangle);
                num_found_triangle[i]++;
                *(helper_data + (look_triangle << 5) + 31) = USED_TEMPORARY;


                // Figure out next pair of vertices...
                repeat(j, 3)
                {
                    if (DEREF( Uint16, helper_data + 18 + (j << 1) + (look_triangle << 5) ) != vertex_edge[0] && DEREF( Uint16, helper_data + 18 + (j << 1) + (look_triangle << 5) ) != vertex_edge[1])
                    {
                        vertex_edge[0] = vertex_edge[1];
                        vertex_edge[1] = DEREF( Uint16, helper_data + 18 + (j << 1) + (look_triangle << 5) );
                        tex_vertex_edge[0] = tex_vertex_edge[1];
                        tex_vertex_edge[1] = DEREF( Uint16, helper_data + 24 + (j << 1) + (look_triangle << 5) );
                        j = 3;
                    }
                }
            }
        }

        // Going backwards 2 at a time...
        if (set_count == 0)
        {
            last_triangle[i] = look_triangle;
        }

        num_found_triangle[i] -= set_count;
        log_info(1, "Found %d triangles, strip should start on %d, ignoring %d triangle", num_found_triangle[i], last_triangle[i], set_count);
    }


    // Figure out which triangle to start from (depending on whichever direction had the longest strip)
    i = 0;

    if (num_found_triangle[1] > num_found_triangle[i])  i = 1;

    if (num_found_triangle[2] > num_found_triangle[i])  i = 2;

    main_triangle = last_triangle[i];

    log_info(1, "Best strip was from direction %d", i);
    log_info(1, "Starting at triangle %d, Vertex %d", main_triangle, starting_vertex[i]);
    log_info(1, "First three vertices are %d, %d, %d...", DEREF( Uint16, helper_data + 18 + (main_triangle << 5) ), DEREF( Uint16, helper_data + 20 + (main_triangle << 5) ), DEREF( Uint16, helper_data + 22 + (main_triangle << 5) ));

    // Find the starting edge...
    repeat(j, 3)
    {
        if (DEREF( Uint16, helper_data + (main_triangle << 5) + (j << 1) + 18 ) == starting_vertex[i])
        {
            tex_vertex[0] = DEREF( Uint16, helper_data + (main_triangle << 5) + (j << 1) + 24 );
            j = (j + 1) % 3;
            vertex_edge[0] = DEREF( Uint16, helper_data + (main_triangle << 5) + (j << 1) + 18 );
            tex_vertex[1] = DEREF( Uint16, helper_data + (main_triangle << 5) + (j << 1) + 24 );
            j = (j + 1) % 3;
            vertex_edge[1] = DEREF( Uint16, helper_data + (main_triangle << 5) + (j << 1) + 18 );
            tex_vertex[2] = DEREF( Uint16, helper_data + (main_triangle << 5) + (j << 1) + 24 );
            j = 3;
        }
    }


    // Clear out all temporary used flags
    temp_data = helper_data;
    repeat(j, num_triangle)
    {
        *(temp_data + 31) &= USED_PERMANENT;
        temp_data += 32;
    }
    *(helper_data + (main_triangle << 5) + 31) = USED_PERMANENT;


    // Start writing our output data...

    log_info(1, "Output start...");
    log_info(1, "(char)  'S'");
    log_info(1, "(char)  %d", texture);
    log_info(1, "(word)  %d (may change)", num_found_triangle[i] + 2);

    *new_data = 'S';  new_data++;
    *new_data = texture;  new_data += 3;
    num_found_vertex = 3;

    log_info(1, "(word)  %d", starting_vertex[i]);
    log_info(1, "(word)  %d (tex)", tex_vertex[0]);

    DEREF( Uint16, new_data ) = starting_vertex[i];  new_data += 2;
    DEREF( Uint16, new_data ) = tex_vertex[0];  new_data += 2;

    log_info(1, "(word)  %d", vertex_edge[0]);
    log_info(1, "(word)  %d (tex)", tex_vertex[1]);

    DEREF( Uint16, new_data ) = vertex_edge[0];  new_data += 2;
    DEREF( Uint16, new_data ) = tex_vertex[1];  new_data += 2;

    log_info(1, "(word)  %d", vertex_edge[1]);
    log_info(1, "(word)  %d (tex)", tex_vertex[2]);

    DEREF( Uint16, new_data ) = vertex_edge[1];  new_data += 2;
    DEREF( Uint16, new_data ) = tex_vertex[2];  new_data += 2;
    tex_vertex_edge[0] = tex_vertex[1];
    tex_vertex_edge[1] = tex_vertex[2];


    // Go through triangles again, this time with a known starting triangle and vertex...
    // Write output vertices as we go...
    starting_vertex[i] = vertex[0];
    look_triangle = main_triangle;
    set_count = 0;
    keep_looking = ktrue;

    while (keep_looking)
    {
        // Find a neighbor who uses both vertices in the edge, and make it our current triangle...
        log_info(1, "Triangle %d has been added, now looking for a neighbor...", look_triangle);
        log_info(1, "Looking on edge %d to %d", vertex_edge[0], vertex_edge[1]);

        looking_for_neighbor = ktrue;
        j = 0;

        while (looking_for_neighbor && j < DEREF( Uint16, helper_data + (look_triangle << 5) ) && j < 8)
        {
            test_triangle = DEREF( Uint16, helper_data + (look_triangle << 5) + 2 + (j << 1) );
            log_info(1, "Testing neighbor %d", test_triangle);

            if (*(helper_data + (test_triangle << 5) + 31) == 0 && *(helper_data + (test_triangle << 5) + 30) == texture)
            {
                log_info(1, "Hasn't been used and is of proper texture");
                // Hasn't been used yet and is of the proper texture...  Does it contain both of the vertices?




                // !!!BAD!!!
                // !!!BAD!!!  Order test...
                // !!!BAD!!!
                order = 5;

                if (DEREF( Uint16, helper_data + (test_triangle << 5) + 18 ) == vertex_edge[0])  { order = 0; }
                else if (DEREF( Uint16, helper_data + (test_triangle << 5) + 20 ) == vertex_edge[0])  { order = 1; }
                else if (DEREF( Uint16, helper_data + (test_triangle << 5) + 22 ) == vertex_edge[0])  { order = 2; }

                if (order < 3)
                {
                    if (set_count) second = (order + 1) % 3;
                    else          second = (order + 2) % 3;

                    if (DEREF( Uint16, helper_data + (test_triangle << 5) + 18 + (second << 1) ) == vertex_edge[1])
                    {
                        if (DEREF( Uint16, helper_data + (test_triangle << 5) + 24 + (order << 1) ) == tex_vertex_edge[0])
                        {
                            if (DEREF( Uint16, helper_data + (test_triangle << 5) + 24 + (second << 1) ) == tex_vertex_edge[1])
                            {







                                //                if((DEREF( Uint16, helper_data + (test_triangle<<5) + 18 ) == vertex_edge[0]) || (DEREF( Uint16, helper_data + (test_triangle<<5) + 20 ) == vertex_edge[0]) || (DEREF( Uint16, helper_data + (test_triangle<<5) + 22 ) == vertex_edge[0]))
                                //                {
                                log_info(1, "Contains first vertex (%d)", vertex_edge[0]);
                                //                    // Contains the first vertex...  How bout the second?
                                //                    if((DEREF( Uint16, helper_data + (test_triangle<<5) + 18 ) == vertex_edge[1]) || (DEREF( Uint16, helper_data + (test_triangle<<5) + 20 ) == vertex_edge[1]) || (DEREF( Uint16, helper_data + (test_triangle<<5) + 22 ) == vertex_edge[1]))
                                //                    {
                                //                        // Contains both vertices...  How bout tex vertices?
                                //                        if((DEREF( Uint16, helper_data + (test_triangle<<5) + 24 ) == tex_vertex_edge[0]) || (DEREF( Uint16, helper_data + (test_triangle<<5) + 26 ) == tex_vertex_edge[0]) || (DEREF( Uint16, helper_data + (test_triangle<<5) + 28 ) == tex_vertex_edge[0]))
                                //                        {
                                //                            // First okay...  Second?
                                //                            if((DEREF( Uint16, helper_data + (test_triangle<<5) + 24 ) == tex_vertex_edge[1]) || (DEREF( Uint16, helper_data + (test_triangle<<5) + 26 ) == tex_vertex_edge[1]) || (DEREF( Uint16, helper_data + (test_triangle<<5) + 28 ) == tex_vertex_edge[1]))
                                //                            {
                                log_info(1, "Contains second vertex (%d)", vertex_edge[1]);
                                log_info(1, "Looks like a good neighbor");
                                // Contains all vertices...  Use it as our next triangle...
                                look_triangle = test_triangle;
                                looking_for_neighbor = kfalse;
                                set_count = (set_count + 1) & 1;
                            }
                        }
                    }
                }
            }

            j++;
        }

        if (looking_for_neighbor)
        {
            // Didn't find a valid neighbor...  Must be all done in this direction...
            keep_looking = kfalse;
        }
        else
        {
            *(helper_data + (look_triangle << 5) + 31) = USED_PERMANENT;


            // Figure out next pair of vertices...  And which one to add to output
            repeat(j, 3)
            {
                if (DEREF( Uint16, helper_data + 18 + (j << 1) + (look_triangle << 5) ) != vertex_edge[0] && DEREF( Uint16, helper_data + 18 + (j << 1) + (look_triangle << 5) ) != vertex_edge[1])
                {
                    vertex_edge[0] = vertex_edge[1];
                    vertex_edge[1] = DEREF( Uint16, helper_data + 18 + (j << 1) + (look_triangle << 5) );
                    DEREF( Uint16, new_data ) = vertex_edge[1];  new_data += 2;
                    tex_vertex_edge[0] = tex_vertex_edge[1];
                    tex_vertex_edge[1] = DEREF( Uint16, helper_data + 24 + (j << 1) + (look_triangle << 5) );
                    DEREF( Uint16, new_data ) = tex_vertex_edge[1];  new_data += 2;
                    num_found_vertex++;

                    log_info(1, "(word)  %d", vertex_edge[1]);
                    log_info(1, "(word)  %d (tex)", tex_vertex_edge[1]);

                    j = 3;
                }
            }
        }
    }

    log_info(1, "Final vertex count was %d (written back in up above...)", num_found_vertex);
    DEREF( Uint16, new_data_start + 2 ) = num_found_vertex;


    *new_data = 0;  // To prevent a random 'S' from bein' there...  Can cause trouble...
    return new_data;
}

//-----------------------------------------------------------------------------------------------
void ddd_strip_fan_geometry(Uint16 num_vertex, Uint16 num_tex_vertex, Uint8* old_data, Uint8* helper_data, Uint8* new_data, Uint8 num_texture)
{
    // <ZZ> This function converts a list of triangles into a list of strips and fans...  Also
    //      generates connection info, which is useful for other things (cartoon lines)...  No longer
    //      handles fan generation, as it generally wasn't as efficient as just having strips...
    Uint16 num_triangle, num_primitive, triangle, first_triangle;
    Uint16 i, j, k, m;
    Uint8 mode;
    Uint8* data;
    Uint16 vertex[3];
    Uint8 match;


    log_info(1, "Stripping, Fanning");


    // Skip over useless stuff...
    old_data += (num_vertex << 5) + (num_tex_vertex << 3);


    // Count the number of triangles
    num_triangle = 0;
    data = old_data;
    repeat(i, num_texture)
    {
        mode = *(data);  data++;

        if (mode)
        {
            num_primitive = DEREF( Uint16, data );  data += 2;
            num_triangle += num_primitive;
            data += (num_primitive * 14);
            data += 2;  // Skip number of fans (should be 0)
        }
    }


    // Start building triangle connection table...
    data = helper_data;
    repeat(i, num_triangle)
    {
        DEREF( Uint16, data ) = 0;  data += 2;        //  0  Number of neighboring triangles
        DEREF( Uint16, data ) = UINT16_MAX;  data += 2;    //  2  Neighbor 0
        DEREF( Uint16, data ) = UINT16_MAX;  data += 2;    //  4  Neighbor 1
        DEREF( Uint16, data ) = UINT16_MAX;  data += 2;    //  6  Neighbor 2
        DEREF( Uint16, data ) = UINT16_MAX;  data += 2;    //  8  Neighbor 3
        DEREF( Uint16, data ) = UINT16_MAX;  data += 2;    // 10  Neighbor 4
        DEREF( Uint16, data ) = UINT16_MAX;  data += 2;    // 12  Neighbor 5
        DEREF( Uint16, data ) = UINT16_MAX;  data += 2;    // 14  Neighbor 6
        DEREF( Uint16, data ) = UINT16_MAX;  data += 2;    // 16  Neighbor 7
        DEREF( Uint16, data ) = 0;  data += 2;        // 18  Vertex 0
        DEREF( Uint16, data ) = 0;  data += 2;        // 20  Vertex 1
        DEREF( Uint16, data ) = 0;  data += 2;        // 22  Vertex 2
        DEREF( Uint16, data ) = 0;  data += 2;        // 24  Tex Vertex 0
        DEREF( Uint16, data ) = 0;  data += 2;        // 26  Tex Vertex 1
        DEREF( Uint16, data ) = 0;  data += 2;        // 28  Tex Vertex 2
        *data = 0;  data += 1;                            // 30  Texture triangle is in
        *data = 0;  data += 1;                            // 31  Triangle used somewhere
    }
    global_base_num_triangle = num_triangle;
    // Fill in the table with triangle data...
    data = old_data;
    triangle = 0;
    repeat(i, num_texture)
    {
        mode = *(data);  data++;

        if (mode)
        {
            num_primitive = DEREF( Uint16, data );  data += 2;
            first_triangle = triangle;
            repeat(j, num_primitive)
            {
                data += 2;  // Skip number of vertices (should be 3)
                repeat(k, 3)
                {
                    vertex[k] = DEREF( Uint16, data );  data += 2;
                    DEREF( Uint16, helper_data + (triangle << 5) + 18 + (k << 1) ) = vertex[k];  // Vertex
                    DEREF( Uint16, helper_data + (triangle << 5) + 24 + (k << 1) ) = DEREF( Uint16, data );  data += 2;  // Tex vertex
                }
                // Remember which texture it falls under...
                *(helper_data + (triangle << 5) + 30) = (Uint8) i;
                // Look for any triangle that shares 2 vertices...  Count as a neighbor...
                k = first_triangle;

                while (k < triangle)
                {
                    match = 0;
                    repeat(m, 3)
                    {
                        if (DEREF( Uint16, helper_data + (k << 5) + 18 + (m << 1) ) == vertex[0] || DEREF( Uint16, helper_data + (k << 5) + 18 + (m << 1) ) == vertex[1] || DEREF( Uint16, helper_data + (k << 5) + 18 + (m << 1) ) == vertex[2])
                        {
                            match++;
                        }
                    }

                    if (match > 1)
                    {
                        // Triangles are neighbors...
                        // Record in the lower indexed, if possible...
                        if (DEREF( Uint16, helper_data + (k << 5) ) < 8)
                        {
                            m = DEREF( Uint16, helper_data + (k << 5) );
                            DEREF( Uint16, helper_data + (k << 5) + 2 + (m << 1) ) = triangle;
                            DEREF( Uint16, helper_data + (k << 5) ) += 1;
                        }

                        // Record in the current, if possible...
                        if (DEREF( Uint16, helper_data + (triangle << 5) ) < 8)
                        {
                            m = DEREF( Uint16, helper_data + (triangle << 5) );
                            DEREF( Uint16, helper_data + (triangle << 5) + 2 + (m << 1) ) = k;
                            DEREF( Uint16, helper_data + (triangle << 5) ) += 1;
                        }
                    }

                    k++;
                }

                triangle++;
            }
            data += 2;  // Skip number of fans (should be 0)
        }
    }


    // Should now have full connection information...
#ifdef VERBOSE_COMPILE
    data = helper_data;
    repeat(i, num_triangle)
    {
        log_info(1, "Triangle %d", i);
        log_info(1, "%d connections", DEREF( Uint16, data ));
        log_info(1, "Connected to %d, %d, %d, %d", DEREF( Uint16, data + 2 ), DEREF( Uint16, data + 4 ), DEREF( Uint16, data + 6 ), DEREF( Uint16, data + 8 ));
        log_info(1, "Connected to %d, %d, %d, %d", DEREF( Uint16, data + 10 ), DEREF( Uint16, data + 12 ), DEREF( Uint16, data + 14 ), DEREF( Uint16, data + 16 ));
        log_info(1, "Vertices %d, %d, %d", DEREF( Uint16, data + 18 ), DEREF( Uint16, data + 20 ), DEREF( Uint16, data + 22 ));
        data += 32;
    }
#endif


    // Show texture vertices...
#ifdef VERBOSE_COMPILE
    log_info(1, "Texture Shoow...");
    data = helper_data;
    repeat(i, num_triangle)
    {
        log_info(1, "Triangle %d...  Tex vertices %d, %d, %d", i, DEREF( Uint16, data + 24 ), DEREF( Uint16, data + 26 ), DEREF( Uint16, data + 28 ));
        data += 32;
    }
#endif



#ifdef DEVTOOL
    // Turn the triangles into single triangle strips...  Round and about way of doing things...
    data = helper_data;
    repeat(i, num_triangle)
    {
        *new_data = 'S';  new_data++;
        *new_data = *(data + 30);  new_data++;
        DEREF( Uint16, new_data ) = 3;  new_data += 2;
        DEREF( Uint16, new_data ) = DEREF( Uint16, data + 18 );  new_data += 2;
        DEREF( Uint16, new_data ) = DEREF( Uint16, data + 24 );  new_data += 2;
        DEREF( Uint16, new_data ) = DEREF( Uint16, data + 20 );  new_data += 2;
        DEREF( Uint16, new_data ) = DEREF( Uint16, data + 26 );  new_data += 2;
        DEREF( Uint16, new_data ) = DEREF( Uint16, data + 22 );  new_data += 2;
        DEREF( Uint16, new_data ) = DEREF( Uint16, data + 28 );  new_data += 2;
        data += 32;
    }
#else
    // Turn the triangles into strips...  Used to do fans too, but took out...
    data = helper_data;
    repeat(i, num_triangle)
    {
        while ((*(data + 31) & USED_PERMANENT) == 0)
        {
            new_data = ddd_create_strip(helper_data, new_data, i, num_triangle);
        }

        data += 32;
    }
#endif




    *new_data = 0;
    // The new_data should look something like this now...
    // 'F'      (Uint8)     // Start of primitive (F for Fan, S for Strip) (Fans no longer supported)
    // 0        (Uint8)     // Texture
    // 6        (Uint16)    // Number of vertices in primitive
    //   15       (Uint16)    // Vertex
    //   0        (Uint16)    // Tex vertex
    //   16       (Uint16)    // Vertex
    //   1        (Uint16)    // Tex vertex
    //   17       (Uint16)    // Vertex
    //   2        (Uint16)    // Tex vertex
    //   18       (Uint16)    // Vertex
    //   3        (Uint16)    // Tex vertex
    //   19       (Uint16)    // Vertex
    //   4        (Uint16)    // Tex vertex
    //   20       (Uint16)    // Vertex
    //   5        (Uint16)    // Tex vertex
    // 'S'      (Uint8)     // Start of primitive...
    // 0        (Uint8)     // Texture...
    // 5    ...
    // ...
    // ...
    // ...
    // 0        (Uint8)     // Start of primitive (Null terminator)
}

//-----------------------------------------------------------------------------------------------
void ddd_decode_base_model(Uint8** olddata_spot, Uint8** newdata_spot, Uint8** newindex_spot, Uint8* newdata_start, Uint8 detail_level, float scale)
{
    // <ZZ> This function helps out decode_ddd...
    Uint8* olddata;
    Uint8* newdata;
    Uint8* tempdata;
    Uint8* strip_fan_data;
    Uint8* strip_fan_data_start;
    Uint8* triangle_data_start;
    Uint16 num_vertex, num_joint, num_bone;
    Uint16 num_triangle, num_points_in_prim;
    Uint16 i, j, k, b;
    Uint8 mode, alpha, flags;
    Uint16 vertex, tex_vertex;
    float x, y, z;
    Uint16 original_vertex;
    Uint16 triangle_vertex[3];
    Uint8 silhouette_flag;
    Uint16 num_prim;

    // !!!BAD!!!
    // !!!BAD!!!
    // !!!BAD!!!
    // !!!BAD!!!
    // !!!BAD!!!
    // !!!BAD!!!  DETAIL LEVEL 0 SHOULD BE FULL QUALITY!!!!  ALWAYS!!!
    // !!!BAD!!!
    // !!!BAD!!!
    // !!!BAD!!!
    // !!!BAD!!!



    // Get the file positions from the main function...
    olddata = *olddata_spot;
    newdata = *newdata_spot;


    // Read the base model header...
    num_vertex = endian_read_mem_int16(olddata);  olddata += 4;
    num_joint = endian_read_mem_int16(olddata);  olddata += 2;
    num_bone = endian_read_mem_int16(olddata);  olddata += 2;


    if (detail_level == 0)
    {
        global_base_num_vertex = endian_read_mem_int16(olddata - 8);
        global_base_num_tex_vertex = endian_read_mem_int16(olddata - 6);


        // Copy the vertex coordinates, tex_vertex coordinates, and triangle lists into a big chunk of data...
        // This is for the simplify routine...
        global_base_old_vertex_start = olddata;
        tempdata = subbuffer;
        repeat(i, global_base_num_vertex)
        {
            x = ((float) ((Sint16) endian_read_mem_int16(olddata))) * scale;  olddata += 2;
            y = ((float) ((Sint16) endian_read_mem_int16(olddata))) * scale;  olddata += 2;
            z = ((float) ((Sint16) endian_read_mem_int16(olddata))) * scale;  olddata += 2;
            DEREF( float, tempdata ) = x;  tempdata += 4;
            DEREF( float, tempdata ) = y;  tempdata += 4;
            DEREF( float, tempdata ) = z;  tempdata += 4;
            olddata += 3;  // Skip extra junk for now
            tempdata += 18;  // Extra room for normal (12), weight (4), and neighbor count (2)
            // Fill in original vertex (2)
            DEREF( Uint16, tempdata ) = i;
            tempdata += 2;
        }
        global_base_old_tex_vertex_start = olddata;
        repeat(i, global_base_num_tex_vertex)
        {
            x = ((float) ((Sint16) endian_read_mem_int16(olddata))) / 256.0f;  olddata += 2;
            y = ((float) ((Sint16) endian_read_mem_int16(olddata))) / 256.0f;  olddata += 2;
            DEREF( float, tempdata ) = x;  tempdata += 4;
            DEREF( float, tempdata ) = y;  tempdata += 4;
        }
        global_base_old_texture_start = olddata;
        repeat(i, MAX_DDD_TEXTURE)
        {
            *tempdata = *olddata; tempdata++;

            if (*olddata != 0)  // Mode
            {
                olddata += 3;  // Skip mode, flags, alpha
                num_triangle = endian_read_mem_int16(olddata);  olddata += 2;
                DEREF( Uint16, tempdata ) = num_triangle;  tempdata += 2;  // Number of strips
                repeat(j, num_triangle)
                {
                    DEREF( Uint16, tempdata ) = 3;  tempdata += 2;  // Number of points in strip
                    repeat(k, 3)
                    {
                        vertex = endian_read_mem_int16(olddata);  olddata += 2;
                        tex_vertex = endian_read_mem_int16(olddata);  olddata += 2;
                        DEREF( Uint16, tempdata ) = vertex;  tempdata += 2;
                        DEREF( Uint16, tempdata ) = tex_vertex;  tempdata += 2;
                    }
                }
                DEREF( Uint16, tempdata ) = 0;  tempdata += 2;  // Number of fans
            }
            else
            {
                olddata++;
            }
        }
        global_base_old_joint_start = olddata;
        global_base_data_size = ((Uint32) tempdata) - ((Uint32) subbuffer);
    }







    // Simplify the geometry, according to the detail level...
    // Number of vertices and tex_vertices may change in simplification...  Number of triangles too...
    if (detail_level == 1)
    {
        // Prime the simplifier...
        ddd_simplify_geometry(&global_base_num_vertex, &global_base_num_tex_vertex, subbuffer, (Uint8) (255 - (detail_level*6)), global_base_data_size, MAX_DDD_TEXTURE);
    }

    if (detail_level > 1)
    {
        // Continue simplification, reusing data from last run...
        ddd_continue_simplify(&global_base_num_vertex, &global_base_num_tex_vertex, subbuffer, (Uint8) (255 - (detail_level*6)), global_base_data_size, MAX_DDD_TEXTURE, num_vertex);
    }





    // Optimize triangle mesh into strips and fans...  Fans are no longer supported...
    ddd_strip_fan_geometry(global_base_num_vertex, global_base_num_tex_vertex, subbuffer, thirdbuffer, fourthbuffer, MAX_DDD_TEXTURE);
    // Third buffer should now hold connection information for triangles...  32 bytes each...
    // Fourth buffer should now hold final triangle data



    // Write the header for the new, simplified base model...
    *newindex_spot = newdata - ((Uint32) newdata_start);  newindex_spot++;
    DEREF( Uint16, newdata ) = global_base_num_vertex;  newdata += 2;
    DEREF( Uint16, newdata ) = global_base_num_tex_vertex;  newdata += 2;
    DEREF( Uint16, newdata ) = num_joint;  newdata += 2;
    DEREF( Uint16, newdata ) = num_bone;  newdata += 2;


    // Write the new, simplified vertex data
    tempdata = subbuffer;
    repeat(i, global_base_num_vertex)
    {
        x = DEREF( float, tempdata );  tempdata += 4;
        y = DEREF( float, tempdata );  tempdata += 4;
        z = DEREF( float, tempdata );  tempdata += 4;
        DEREF( float, newdata ) = x;  newdata += 4;
        DEREF( float, newdata ) = y;  newdata += 4;
        DEREF( float, newdata ) = z;  newdata += 4;
        // Ignore normal, weight, and neighbor count
        tempdata += 18;
        // Use the original vertex to get bone bindings and weight...
        original_vertex = DEREF( Uint16, tempdata );
        tempdata += 2;
        olddata = global_base_old_vertex_start + (original_vertex * 9) + 6;
        *newdata = *olddata;  newdata++;  olddata++;  // Bone binding 0
        *newdata = *olddata;  newdata++;  olddata++;  // Bone binding 1
        log_info(1, "DDD Weight %d == %d", original_vertex, (*olddata));
        *newdata = *olddata;  newdata++;              // Bone Weighting


        // Bone and normal scalars calculated later...
        repeat(j, 12)
        {
            DEREF( float, newdata ) = 0.0f;  newdata += 4;
        }


        // Pad vertex data, so each is 64 bytes long...  Also used for hide flag in modeler...
        *newdata = 0;  newdata++;
    }


    // Write the new, simplified texture vertex data
    *newindex_spot = newdata - ((Uint32) newdata_start);  newindex_spot++;
    repeat(i, global_base_num_tex_vertex)
    {
        x = DEREF( float, tempdata );  tempdata += 4;
        y = DEREF( float, tempdata );  tempdata += 4;
        DEREF( float, newdata ) = x;  newdata += 4;
        DEREF( float, newdata ) = y;  newdata += 4;
    }



    // Write the new, simplified texture/strip/fan data
    *newindex_spot = newdata - ((Uint32) newdata_start);  newindex_spot++;
    olddata = global_base_old_texture_start;
    triangle_data_start = newdata;
    repeat(i, MAX_DDD_TEXTURE)
    {
        // Was this texture on or off?
        if (*olddata != 0)
        {
            // Texture used to be on...  Get it's info...
            // On...  Write texture header...
            olddata++;
            flags = *olddata;  olddata++;
            alpha = *olddata;  olddata++;
            olddata += (endian_read_mem_int16(olddata) * 12);  olddata += 2;
        }
        else
        {
            // Was off...  Should still be...
            olddata++;
        }



        // Is it currently on?
        mode = *tempdata;  tempdata++;
        *newdata = mode;  newdata++;    // Mode

        if (mode != 0)
        {
            // On...  Write texture header...
            *newdata = flags;  newdata++;   // Flags
            *newdata = alpha;  newdata++;   // Alpha

            // Skip ignored stuff in tempdata...  Just so we don't mess up...
            num_triangle = DEREF( Uint16, tempdata );  tempdata += 2;
            repeat(j, num_triangle)
            {
                num_points_in_prim = DEREF( Uint16, tempdata );  tempdata += 2;  // Number of points in strip
                repeat(k, num_points_in_prim)
                {
                    tempdata += 4;
                }
            }
            tempdata += 2;  // Skip fans...


            // Fill in striperized data...  Fans taken out...
            // First skip through data until we get to our desired texture...
            strip_fan_data = fourthbuffer;

            if (i > 0)
            {
                while (strip_fan_data[0] != 0 && strip_fan_data[1] < i)
                {
                    strip_fan_data += 2;
                    b = DEREF( Uint16, strip_fan_data );  // vertex_count
                    strip_fan_data += 2;
                    strip_fan_data += (b << 2);
                }
            }


            // Now count all of the strips in the texture...
            num_triangle = 0;
            strip_fan_data_start = strip_fan_data;

            while (strip_fan_data[0] != 0 && strip_fan_data[1] == i)
            {
                num_triangle++;
                strip_fan_data += 2;
                b = DEREF( Uint16, strip_fan_data );  // vertex_count
                strip_fan_data += 2;
                strip_fan_data += (b << 2);
            }


            // Now go back through and copy the data...
            DEREF( Uint16, newdata ) = num_triangle;  newdata += 2;
            strip_fan_data = strip_fan_data_start;
            repeat(j, num_triangle)
            {
                strip_fan_data += 2;
                b = DEREF( Uint16, strip_fan_data );  // vertex_count
                log_info(1, "Copied strip %d, Contains %d triangles...", j, b - 2);
                memcpy(newdata, strip_fan_data, (b << 2) + 2);
                newdata += (b << 2) + 2;
                strip_fan_data += (b << 2) + 2;
            }


            // Put fans down as 0, since they've been removed...
            DEREF( Uint16, newdata ) = 0;  newdata += 2;
        }
    }



    // Copy the old joint data
    *newindex_spot = newdata - ((Uint32) newdata_start);  newindex_spot++;
    olddata = global_base_old_joint_start;
    repeat(i, num_joint)
    {
        DEREF( float, newdata ) = (*olddata) * JOINT_COLLISION_SCALE;  newdata += 4;  olddata++;
    }




    // Copy the old bone data
    *newindex_spot = newdata - ((Uint32) newdata_start);  newindex_spot++;
    repeat(i, num_bone)
    {
        *newdata = *olddata;  newdata++;  olddata++;
        k = endian_read_mem_int16(olddata);  olddata += 2;
        b = endian_read_mem_int16(olddata);  olddata += 2;
        DEREF( Uint16, newdata ) = k;  newdata += 2;
        DEREF( Uint16, newdata ) = b;  newdata += 2;
        newdata += 4;  // Reserve room for the bone length
    }



    // Generate the cartoon line data...  Start by assuming there aren't any...
    DEREF( Uint16, newdata ) = 0;



    // Old cartoon line code that finds triangle edges...  Now used for volumetric shadows...
    // For each texture...
    //#ifndef BACKFACE_CARTOON_LINES
    tempdata = triangle_data_start;
    repeat(i, MAX_DDD_TEXTURE)
    {
        // If texture is on...
        mode = *tempdata;  tempdata++;

        if (mode)
        {
            // Does this texture have a silhouette?
            silhouette_flag = !((*tempdata) & NO_LINE_FLAG);  tempdata++;
            tempdata++;


            // For strips and fans...
            repeat(b, 2)
            {
                // For each triangle strip or fan...
                num_prim = DEREF( Uint16, tempdata );  tempdata += 2;
                repeat(j, num_prim)
                {
                    num_points_in_prim = DEREF( Uint16, tempdata );  tempdata += 2;
                    repeat(k, num_points_in_prim)
                    {
                        triangle_vertex[k%3] = DEREF( Uint16, tempdata );  tempdata += 4;

                        // Add each edge of triangle
                        if (silhouette_flag && k > 1)
                        {
                            if (k & 1)
                            {
                                add_cartoon_line(triangle_vertex[2], triangle_vertex[1], triangle_vertex[0], kfalse, (Uint16*) newdata);
                                add_cartoon_line(triangle_vertex[1], triangle_vertex[0], triangle_vertex[2], kfalse, (Uint16*) newdata);
                                add_cartoon_line(triangle_vertex[0], triangle_vertex[2], triangle_vertex[1], kfalse, (Uint16*) newdata);
                            }
                            else
                            {
                                add_cartoon_line(triangle_vertex[0], triangle_vertex[1], triangle_vertex[2], kfalse, (Uint16*) newdata);
                                add_cartoon_line(triangle_vertex[1], triangle_vertex[2], triangle_vertex[0], kfalse, (Uint16*) newdata);
                                add_cartoon_line(triangle_vertex[2], triangle_vertex[0], triangle_vertex[1], kfalse, (Uint16*) newdata);
                            }
                        }
                    }
                }
            }
        }
    }
    remove_cartoon_lines((Uint16*) newdata);
    //#endif


    // Skip over cartoon data...
    newdata += (DEREF( Uint16, newdata )) << 3;
    newdata += 2;



    // Update the file positions in the main function...
    if (detail_level == (DETAIL_LEVEL_MAX - 1))
    {
        // Done with this base model...
        *olddata_spot = olddata;
    }

    *newdata_spot = newdata;
}

//-----------------------------------------------------------------------------------------------
void ddd_decode_bone_frame(Uint8** olddata_spot, Uint8** newdata_spot, Uint8** newindex_spot, Uint8* newdata_start, float scale)
{
    // <ZZ> This function helps out decode_ddd...
    Uint8* olddata;
    Uint8* newdata;
    Uint8* base_model_start;
    Uint8* bone_data;
    Uint8* joint_position_data;
    Uint8 action_name;
    Uint8 action_flags;
    Uint8 base_model;
    Uint8 alpha;
    Uint16 num_vertex, num_bone, num_joint;
    Uint16 i, j;
    Uint16 joint;
    float front_xyz[3];
    float bone_xyz[3];
    float side_xyz[3];
    float x, y, z, distance;
    Sint16 offset;


    // Get the file positions from the main function...
    olddata = *olddata_spot;
    newdata = *newdata_spot;
    *newindex_spot = newdata - ((Uint32) newdata_start);


    // Read the bone frame header...
    action_name  = *olddata;  olddata++;
    *newdata = action_name;  newdata++;
    action_flags = *olddata;  olddata++;
    *newdata = action_flags;  newdata++;
    base_model   = *olddata;  olddata++;
    *newdata = base_model;  newdata++;
    offset = (Sint16) endian_read_mem_int16(olddata);  olddata += 2;
    DEREF( float, newdata ) = offset / 256.0f;  newdata += 4;
    offset = (Sint16) endian_read_mem_int16(olddata);  olddata += 2;
    DEREF( float, newdata ) = offset / 256.0f;  newdata += 4;


    // Figure out how many bones and joints by looking at the base model...
    base_model_start = newdata_start + 6 + (ACTION_MAX << 1) + (MAX_DDD_SHADOW_TEXTURE) + (base_model * 20);
    base_model_start = (DEREF( Uint32, base_model_start )) + newdata_start;
    num_vertex = DEREF( Uint16, base_model_start );  base_model_start += 4;
    num_joint = DEREF( Uint16, base_model_start );  base_model_start += 2;
    num_bone = DEREF( Uint16, base_model_start );  base_model_start += 2;


    bone_data = newdata_start + 6 + (ACTION_MAX << 1) + (MAX_DDD_SHADOW_TEXTURE) + (base_model * 20) + 16;
    bone_data = (DEREF( Uint32, bone_data )) + newdata_start;




    // Copy the bone info
    joint_position_data = olddata + (num_bone * 6);
    repeat(i, num_bone)
    {
        // Forward normal
        x = (float) ((Sint16) endian_read_mem_int16(olddata));  olddata += 2;
        y = (float) ((Sint16) endian_read_mem_int16(olddata));  olddata += 2;
        z = (float) ((Sint16) endian_read_mem_int16(olddata));  olddata += 2;
        //        x = ((float) (*(Sint8*) olddata));  olddata++;
        //        y = ((float) (*(Sint8*) olddata));  olddata++;
        //        z = ((float) (*(Sint8*) olddata));  olddata++;
        distance = SQRT(x * x + y * y + z * z);

        if (distance > 0.001f)
        {
            x = x / distance;  y = y / distance;  z = z / distance;
        }
        else
        {
            x = 1.0f;  y = 0.0f;  z = 0.0f;
        }

        DEREF( float, newdata ) = x;  newdata += 4;
        DEREF( float, newdata ) = y;  newdata += 4;
        DEREF( float, newdata ) = z;  newdata += 4;
        front_xyz[XX] = x;
        front_xyz[YY] = y;
        front_xyz[ZZ] = z;


        // Bone vector
        joint = DEREF( Uint16, bone_data + (i * 9) + 3 );
        bone_xyz[XX] = ((float) ((Sint16) endian_read_mem_int16(joint_position_data + (joint * 6)))) * scale;
        bone_xyz[YY] = ((float) ((Sint16) endian_read_mem_int16(joint_position_data + (joint * 6) + 2))) * scale;
        bone_xyz[ZZ] = ((float) ((Sint16) endian_read_mem_int16(joint_position_data + (joint * 6) + 4))) * scale;
        joint = DEREF( Uint16, bone_data + (i * 9) + 1 );
        bone_xyz[XX] -= ((float) ((Sint16) endian_read_mem_int16(joint_position_data + (joint * 6)))) * scale;
        bone_xyz[YY] -= ((float) ((Sint16) endian_read_mem_int16(joint_position_data + (joint * 6) + 2))) * scale;
        bone_xyz[ZZ] -= ((float) ((Sint16) endian_read_mem_int16(joint_position_data + (joint * 6) + 4))) * scale;


        // Calculate side normal
        cross_product(front_xyz, bone_xyz, side_xyz);
        distance = vector_length(side_xyz);

        if (distance > 0.001f)
        {
            x = side_xyz[XX] / distance;  y = side_xyz[YY] / distance;  z = side_xyz[ZZ] / distance;
        }
        else
        {
            x = 0.0f;  y = 1.0f;  z = 0.0f;
        }


        DEREF( float, newdata ) = x;  newdata += 4;
        DEREF( float, newdata ) = y;  newdata += 4;
        DEREF( float, newdata ) = z;  newdata += 4;
    }
    // Copy the joint info
    repeat(i, num_joint)
    {
        x = ((float) ((Sint16) endian_read_mem_int16(olddata))) * scale;  olddata += 2;
        y = ((float) ((Sint16) endian_read_mem_int16(olddata))) * scale;  olddata += 2;
        z = ((float) ((Sint16) endian_read_mem_int16(olddata))) * scale;  olddata += 2;
        DEREF( float, newdata ) = x;  newdata += 4;
        DEREF( float, newdata ) = y;  newdata += 4;
        DEREF( float, newdata ) = z;  newdata += 4;
    }
    // Copy the shadow info
    repeat(i, MAX_DDD_SHADOW_TEXTURE)
    {
        alpha = *olddata;  olddata++;
        *newdata = alpha;  newdata++;

        if (alpha > 0)
        {
            repeat(j, 4)
            {
                x = ((float) ((Sint16) endian_read_mem_int16(olddata))) * scale;  olddata += 2;
                y = ((float) ((Sint16) endian_read_mem_int16(olddata))) * scale;  olddata += 2;
                DEREF( float, newdata ) = x;  newdata += 4;
                DEREF( float, newdata ) = y;  newdata += 4;
            }
        }
    }


    // Update the file positions in the main function...
    *olddata_spot = olddata;
    *newdata_spot = newdata;
}

//-----------------------------------------------------------------------------------------------
SDF_PHEADER decode_ddd(SDF_PHEADER ddd_header, const char* filename)
{
    // <ZZ> This function decompresses a ddd file that has been stored in memory.  Index is a
    //      pointer to the start of the file's ddd_header in the sdf_archive, and can be gotten from
    //      sdf_archive_find_index_from_filename.  If the function works okay, it should create a new RDY file in the
    //      ddd_header and return ktrue.  It might also delete the original compressed file to save
    //      space, but that's a compile time option.  If it fails it should return kfalse, or
    //      it might decide to crash.

    Uint8* ddd_start;
    Uint8* ddd_data;      // Compressed
    Uint32 ddd_size;      // Compressed

    SDF_PHEADER rdy_header = NULL;
    Uint8* rdy_data;   // Decompressed
    Uint32 rdy_size;   // Decompressed

    int     i, j, k;
    float   scale;
    Uint16  flags;
    Uint8   num_base_model;
    Uint16  num_bone_frame;
    Uint8** new_base_model_offset_start;
    Uint8** new_bone_frame_offset_start;
    Uint8   external_bone_frame;
    char    linkname[16];
    Uint8*  link_data;
    SDF_PHEADER link_header;
    Uint8 temp;


    // Log what we're doing
    log_info(1, "Decoding %s.DDD to %s.RDY", filename, filename);


    // Find the location of the file ddd_data, and its ddd_size...
    ddd_data = sdf_file_get_data(ddd_header);
    ddd_size = sdf_file_get_size(ddd_header);

    // Make sure we have room in the pcx_header for a new file
    if (sdf_archive_free_file_count() <= 0)
    {
        log_error(0, "No room left to add RDY file, program must be restarted");
        goto decode_ddd_fail;
    }

    ddd_start = ddd_data;
    //global_ddd_file_start = ddd_data;
    //global_rdy_file_start = NULL;


    // Make sure we have room in the ddd_header for a new file
    if (sdf_archive_free_file_count() <= 0)
    {
        log_error(0, "No room left to add file, program must be restarted");
        goto decode_ddd_fail;
    }


    // Read the DDD header...  Write the RDY header to the mainbuffer...
    rdy_data = mainbuffer;
    scale = endian_read_mem_int16(ddd_data) / DDD_SCALE_WEIGHT;  ddd_data += 2;
    // RDY doesn't need scale info...
    flags = endian_read_mem_int16(ddd_data);  ddd_data += 2;
    external_bone_frame = kfalse;

    if (flags & DDD_EXTERNAL_BONE_FRAMES)
    {
        external_bone_frame = ktrue;
    }

    DEREF( Uint16, rdy_data ) = flags;  rdy_data += 2;
    ddd_data++;
    *(rdy_data) = DETAIL_LEVEL_MAX;  rdy_data++;
    num_base_model = *ddd_data;  ddd_data++;
    *(rdy_data) = num_base_model;  rdy_data++;
    num_bone_frame = endian_read_mem_int16(ddd_data);  ddd_data += 2;
    DEREF( Uint16, rdy_data ) = num_bone_frame;  rdy_data += 2;
    // Starting frames for each action...
    repeat(i, ACTION_MAX)
    {
        // Not stored in file...
        DEREF( Uint16, rdy_data ) = UINT16_MAX;  rdy_data += 2;
    }
    // Shadow texture indices
    repeat(i, MAX_DDD_SHADOW_TEXTURE)
    {
        *rdy_data = *ddd_data;  ddd_data++;  rdy_data++;
    }

    // External bone frame filename (skipped for now)
    if (external_bone_frame)
    {
        // Actually...  Let's figger out how many frames are in our link'd file...
        temp = ddd_data[8];
        ddd_data[8] = 0;
        sprintf(linkname, "%s", ddd_data);
        ddd_data[8] = temp;
        num_bone_frame = 0;

        link_data   = NULL;
        link_header = sdf_archive_find_filetype(linkname, SDF_FILE_IS_DDD);

        if (link_header)
        {
            link_data = sdf_file_get_data(link_header);
            link_data += 6;
            num_bone_frame = endian_read_mem_int16(link_data);
        }

        // Write the number of bone frames...
        DEREF( Uint16, rdy_data - MAX_DDD_SHADOW_TEXTURE - (ACTION_MAX * 2) - 2 ) = num_bone_frame;


        ddd_data += 8;
    }

    // Offsets...  Fill in later...
    new_base_model_offset_start = (Uint8**) rdy_data;
    repeat(i, DETAIL_LEVEL_MAX)
    {
        repeat(j, num_base_model)
        {
            DEREF( Uint32, rdy_data ) = 0;  rdy_data += 4;
            DEREF( Uint32, rdy_data ) = 0;  rdy_data += 4;
            DEREF( Uint32, rdy_data ) = 0;  rdy_data += 4;
            DEREF( Uint32, rdy_data ) = 0;  rdy_data += 4;
            DEREF( Uint32, rdy_data ) = 0;  rdy_data += 4;
        }
    }
    new_bone_frame_offset_start = (Uint8**) rdy_data;
    repeat(i, num_bone_frame)
    {
        DEREF( Uint32, rdy_data ) = 0;  rdy_data += 4;
    }



    // Done with header, now decode base models...  For each detail level...
    repeat(i, num_base_model)
    {
        repeat(j, DETAIL_LEVEL_MAX)
        {
            log_info(1, "Decoding base %d, detail %d", i, j);
            ddd_decode_base_model(&ddd_data, &rdy_data, new_base_model_offset_start + ((5*i) + (5*num_base_model*j)), mainbuffer, (Uint8) j, scale);
        }
    }



    // Decode bone frames...  Fills in pointer block...  Filled in later for external linking...
    if (!external_bone_frame)
    {
        repeat(i, num_bone_frame)
        {
            ddd_decode_bone_frame(&ddd_data, &rdy_data, new_bone_frame_offset_start, mainbuffer, scale);
            new_bone_frame_offset_start++;
        }
    }





    // Allocate memory for the new file...
    rdy_size = ((Uint32) rdy_data) - ((Uint32) mainbuffer);

    rdy_header = sdf_archive_create_header(mainbuffer, filename, rdy_size, SDF_FILE_IS_RDY);

    if (NULL == rdy_header)
    {
        log_error(0, "Couldn't create RDY file");
        goto decode_ddd_fail;
    }

    rdy_data = sdf_file_get_data(rdy_header);


    // Go back through and turn offsets into pointers...
    ddd_data = rdy_data;
    ddd_data += 6 + (ACTION_MAX << 1) + (MAX_DDD_SHADOW_TEXTURE);
    repeat(i, DETAIL_LEVEL_MAX)
    {
        repeat(j, num_base_model)
        {
            repeat(k, 5)
            {
                DEREF( Uint32, ddd_data ) += (Uint32) rdy_data;  ddd_data += 4;
            }
        }
    }

    if (!external_bone_frame)
    {
        repeat(i, num_bone_frame)
        {
            DEREF( Uint32, ddd_data ) += (Uint32) rdy_data;  ddd_data += 4;
        }
    }


    //    datadump(rdy_data, rdy_size, kfalse);
    return rdy_header;

decode_ddd_fail:

    if (NULL != rdy_header)
    {
        sdf_archive_delete_header(rdy_header);
    };

    return NULL;
}

//-----------------------------------------------------------------------------------------------
void ddd_magic_update_thing(Uint8 mask, float loadin_min, float loadin_max)
{
    // <ZZ> This function generates action lists for all of the RDY models, and also fills in
    //      frame pointers for externally linked files.  Should be called directly after all of
    //      the DDD files are decoded, and also after any file transfer.  Also does lots of math
    //      to pregenerate normals for each vertex of each base model of each detail level of each
    //      RDY file...
    int i;
    Uint16 frame, num_base_model;
    Uint8 detail_level;
    char filename[16];
    char linkname[16];
    char filetype;
    char temp;
    Uint16 flags;
    Uint8* data;
    Uint8* start_data;
    SDF_PHEADER ddd_header;
    Uint8* ddd_data;
    Uint8* link_data;
    SDF_PHEADER link_header;
    Uint16 num_bone_frame;

    bool_t draw_loadin;
    draw_loadin = loadin_max > loadin_min;

    log_info(1, "Magic update called");

    // For each file in index...
    repeat(i, sdf_archive_get_num_files())
    {
        SDF_PHEADER pheader  = sdf_archive_get_header(i);
        Uint8      file_type = sdf_file_get_type(pheader);

		UI_INTERRUPT(draw_loadin, i, loadin_min, loadin_max);

        // Let me choose if I want all files or just updated ones...
        if (file_type & mask)
        {
            // If the file is an RDY file...
            sdf_archive_get_filename(i, filename, &filetype);

            if ((filetype & EXT_TYPE_MASK) == SDF_FILE_IS_RDY)
            {
                // Set up frame pointers if the file is externally linked...
                data = sdf_file_get_data(pheader);
                start_data = data;
                flags = DEREF( Uint16, data );

                if (flags & DDD_EXTERNAL_BONE_FRAMES)
                {
                    // Find the name of the referenced file...
                    ddd_data = NULL;
                    ddd_header = sdf_archive_find_filetype(filename, SDF_FILE_IS_DDD);

                    if (ddd_header)
                    {
                        ddd_data = sdf_file_get_data(ddd_header);
                        temp = ddd_data[20];
                        ddd_data[20] = 0;
                        sprintf(linkname, "%s", ddd_data + 12);
                        ddd_data[20] = temp;

                        link_data   = NULL;
                        link_header = sdf_archive_find_filetype(linkname, SDF_FILE_IS_RDY);

                        if (link_header)
                        {
                            // Copy the frame list from one file to the other...
                            link_data = sdf_file_get_data(link_header);
                            num_bone_frame = DEREF( Uint16, data + 4 );
                            data += data[3] * DETAIL_LEVEL_MAX * 20;
                            data += 6;
                            data += (ACTION_MAX << 1);
                            data += (MAX_DDD_SHADOW_TEXTURE);
                            link_data += link_data[3] * DETAIL_LEVEL_MAX * 20;
                            link_data += 6;
                            link_data += (ACTION_MAX << 1);
                            link_data += (MAX_DDD_SHADOW_TEXTURE);
                            memcpy(data, link_data, num_bone_frame << 2);
                        }
                        else
                        {
                            log_error(0, "Couldn't link %s.RDY to %s.RDY...  File doesn't exist...", filename, linkname);
                        }
                    }
                    else
                    {
                        log_error(0, "Couldn't find %s.DDD...  Weird...", filename);
                    }
                }

                // Figure out the action list...
                ddd_generate_model_action_list(start_data);


                // Figure out the starting normals
                num_base_model = start_data[3];
                repeat(detail_level, DETAIL_LEVEL_MAX)
                {
                    // Assume that the first several frames are used for boning models...
                    repeat(frame, num_base_model)
                    {
                        render_pregenerate_normals(start_data, frame, detail_level);
                    }
                }
            }
        }
    }
}

//-----------------------------------------------------------------------------------------------
#ifdef DEVTOOL

#define EXPORTRDYASDDD
#define TELL_ME_ABOUT_EXPORT
#define SPIT_UNSIGNED_CHAR(A)  { fputc((A&0xFF), openfile); }
#define SPIT_UNSIGNED_SHORT(A) { fputc((A>>8), openfile); fputc((A&0xFF), openfile); }

#ifdef TELL_ME_ABOUT_EXPORT
#    define EXPORT_VERB 1
#else
#    define EXPORT_VERB 0
#endif

Sint8 rdy_to_ddd_export(FILE * openfile, SDF_PHEADER pheader)
{
    // <ZZ> This function writes a single file from memory to disk.  The function should return
    //      kfalse if there's a problem writing, or ktrue if it all worked.

    Uint32 size;
    Uint8 file_type;
    //Uint32 lan_lines;

    //Sint32 length;
    float max_distance;
    Uint16 i, j, k, b;
    Uint8 c;
    float x, y, z;
    Uint8* data_index_list;
    Uint8* data_index_list_start;
    Uint8* data_temp;
    Uint8* data_other;
    char* link_file_name;
    Uint8* start_data;
    Uint16 flags;
    Uint8 num_detail_level;
    Uint8 num_base_model;
    Uint16 num_bone_frame;
    Uint16 num_vertex;
    Uint16 num_tex_vertex;
    Uint16 num_joint;
    Uint16 num_bone;
    Uint16 num_strips;
    Uint16 vertex;
    Uint16 tex_vertex;
    Uint16 base_model;

    SDF_PDATA pdata;


    // See if we can find the file
    if (NULL == openfile || NULL == pheader)
    {
        return kfalse;
    };


    // Get the size and data location
    file_type = sdf_file_get_type(pheader);

    size      = sdf_file_get_size(pheader);

    pdata     = sdf_file_get_data(pheader);


    // Put the name back to how we found it...
    start_data = pdata;


    // Figure out the optimal number to use for scale...
    flags = DEREF( Uint16, pdata );  pdata += 2;

    num_detail_level = *pdata;  pdata++;

    num_base_model = *pdata;  pdata++;

    num_bone_frame = DEREF( Uint16, pdata );  pdata += 2;

    data_index_list = pdata + (ACTION_MAX << 1) + (MAX_DDD_SHADOW_TEXTURE);

    data_index_list_start = data_index_list;

    max_distance = 1.0f;

    log_info(0, "Starting detail levels");

    repeat(i, num_detail_level)
    {
        repeat(j, num_base_model)
        {
            data_temp = DEREF( Uint8*, data_index_list );  data_index_list += 20;
            num_vertex = DEREF( Uint16, data_temp );  data_temp += 8;

            log_info(0, "Num vertex = %d", num_vertex);

            repeat(k, num_vertex)
            {
                x = DEREF( float , data_temp );  data_temp += 4;
                y = DEREF( float , data_temp );  data_temp += 4;
                z = DEREF( float , data_temp );  data_temp += 4;
                log_info(0, "Vertex %d at (%f, %f, %f)", k, x, y, z);

                data_temp += 52;
                ABS(x);
                ABS(y);
                ABS(z);

                if (x > max_distance) max_distance = x;

                if (y > max_distance) max_distance = y;

                if (z > max_distance) max_distance = z;
            }
        }
    }

    log_info(0, "Got vertex scale...  %f", max_distance);

    repeat(i, num_bone_frame)
    {
        // Find out how many joints in the base model for this frame...
        data_temp = DEREF( Uint8*, data_index_list );
        base_model = *(data_temp + 2);
        data_temp = DEREF( Uint8*, data_index_list_start + (base_model * 20) );
        data_temp += 4;
        num_joint = DEREF( Uint16, data_temp );
        data_temp += 2;
        num_bone = DEREF( Uint16, data_temp );
        data_temp = DEREF( Uint8*, data_index_list );  data_index_list += 4;
        data_temp += (num_bone * 24) + 11;
        repeat(j, num_joint)
        {
            x = DEREF( float , data_temp );  data_temp += 4;
            y = DEREF( float , data_temp );  data_temp += 4;
            z = DEREF( float , data_temp );  data_temp += 4;

            log_info(0, "Joint %d at (%f, %f, %f)", j, x, y, z);

            ABS(x);
            ABS(y);
            ABS(z);

            if (x > max_distance) max_distance = x;

            if (y > max_distance) max_distance = y;

            if (z > max_distance) max_distance = z;
        }
        repeat(j, MAX_DDD_SHADOW_TEXTURE)
        {
            k = *data_temp;  data_temp++;

            log_info(0, "Shadow %d alpha = %d", j, k);

            if (k)
            {
                repeat(k, 4)
                {
                    x = DEREF( float , data_temp );  data_temp += 4;
                    y = DEREF( float , data_temp );  data_temp += 4;

                    log_info(0, "Shadow vertex %d at (%f, %f)", k, x, y);

                    ABS(x);
                    ABS(y);

                    if (x > max_distance) max_distance = x;

                    if (y > max_distance) max_distance = y;
                }
            }
        }
    }

    log_info(0, "Got joint/shadow scale...  %f", max_distance);



    // Now write the data...
    i = (Uint16) max_distance;
    i++;

    log_info(0, "Starting to spit DDD file...");

    SPIT_UNSIGNED_SHORT(i);                 // Scale
    max_distance = (float) i;
    log_info(0, "Final max distance = %f", max_distance);

    SPIT_UNSIGNED_SHORT(flags);             // Flags
    log_info(0, "Flags = %d", flags);

    SPIT_UNSIGNED_CHAR(0);                  // Padding
    log_info(0, "Padding = %d", 0);

    SPIT_UNSIGNED_CHAR(num_base_model);     // Number of base models
    log_info(0, "Num_base_model = %d", num_base_model);

    SPIT_UNSIGNED_SHORT(num_bone_frame);    // Frames of animation
    log_info(0, "Num_bone_frame = %d", num_bone_frame);

    // Skip action list...
    pdata += (ACTION_MAX << 1);
    repeat(i, MAX_DDD_SHADOW_TEXTURE)
    {
        j = *pdata;  pdata++;
        SPIT_UNSIGNED_CHAR(j);              // Shadow texture index
        log_info(0, "Shadow texture %d = %d", i, j);
    }

    // Spit out the external linkage, if it's valid...
    if (flags & DDD_EXTERNAL_BONE_FRAMES)
    {
        log_info(0, "External linkage is valid, so spitting filename...");

        link_file_name = render_get_set_external_linkage(start_data, NULL);
        j = 0;
        repeat(i, 8)
        {
            if (j == 0)
            {
                SPIT_UNSIGNED_CHAR(link_file_name[i]);

#ifdef TELL_ME_ABOUT_EXPORT

                if (link_file_name[i] != 0)
                {
                    log_info(0, "%c", link_file_name[i]);
                }
                else
                {
                    log_info(0, "0");
                }

#endif

                if (link_file_name[i] == 0) j++;
            }
            else
            {
                SPIT_UNSIGNED_CHAR(0);
                log_info(0, "0");
            }
        }
    }


    log_info(0, "Done with Action Export...");

    // Jump to first base model of last detail level...
    pdata += ((DETAIL_LEVEL_MAX - 1) * num_base_model * 20);
    data_index_list = pdata;
    repeat(i, num_base_model)
    {
        log_info(0, "Reading base model indices");
        data_temp = DEREF( Uint8*, pdata );  pdata += 20;
        log_info(0, "data_temp = %u", data_temp);
        num_vertex = DEREF( Uint16, data_temp );  data_temp += 2;
        num_tex_vertex = DEREF( Uint16, data_temp );  data_temp += 2;
        num_joint = DEREF( Uint16, data_temp );  data_temp += 2;
        num_bone = DEREF( Uint16, data_temp );  data_temp += 2;
        SPIT_UNSIGNED_SHORT(num_vertex);
        SPIT_UNSIGNED_SHORT(num_tex_vertex);
        SPIT_UNSIGNED_SHORT(num_joint);
        SPIT_UNSIGNED_SHORT(num_bone);
        log_info(0, "num_vertex = %d", num_vertex);
        log_info(0, "num_tex_vertex = %d", num_tex_vertex);
        log_info(0, "num_joint = %d", num_joint);
        log_info(0, "num_bone = %d", num_bone);
        repeat(j, num_vertex)
        {
            // Vertex coordinates
            x = DEREF( float , data_temp ) * DDD_SCALE_WEIGHT / max_distance;  data_temp += 4;
            y = DEREF( float , data_temp ) * DDD_SCALE_WEIGHT / max_distance;  data_temp += 4;
            z = DEREF( float , data_temp ) * DDD_SCALE_WEIGHT / max_distance;  data_temp += 4;
            log_info(0, "vertex position = (%f, %f, %f)", x, y, z);
            log_info(0, "exported vertex position = (%d, %d, %d)", (Uint16) ((Sint16) x), (Uint16) ((Sint16) y), (Uint16) ((Sint16) z));
            k = (Uint16)((Sint16) x);
            SPIT_UNSIGNED_SHORT(k);
            k = (Uint16)((Sint16) y);
            SPIT_UNSIGNED_SHORT(k);
            k = (Uint16)((Sint16) z);
            SPIT_UNSIGNED_SHORT(k);
            // Bone bindings and weighting
            SPIT_UNSIGNED_CHAR(*data_temp);  data_temp++;
            SPIT_UNSIGNED_CHAR(*data_temp);  data_temp++;
            SPIT_UNSIGNED_CHAR(*data_temp);  data_temp++;
            data_temp += 36;  // Skip normal and scalar coordinates
            data_temp += 13;  // Skip padding
        }
        log_info(0, "Done with vertices");
        repeat(j, num_tex_vertex)
        {
            x = DEREF( float , data_temp ) * ((float)0x0100);  data_temp += 4;
            y = DEREF( float , data_temp ) * ((float)0x0100);  data_temp += 4;
            k = (Uint16)((Sint16) x);
            SPIT_UNSIGNED_SHORT(k);
            k = (Uint16)((Sint16) y);
            SPIT_UNSIGNED_SHORT(k);
        }
        log_info(0, "Done with tex vertices");
        repeat(j, MAX_DDD_TEXTURE)
        {
            if (*data_temp)
            {
                SPIT_UNSIGNED_CHAR(*data_temp);  data_temp++;
                SPIT_UNSIGNED_CHAR(*data_temp);  data_temp++;
                SPIT_UNSIGNED_CHAR(*data_temp);  data_temp++;
                num_strips = DEREF( Uint16, data_temp );  data_temp += 2;
                SPIT_UNSIGNED_SHORT(num_strips);
                repeat(k, num_strips)
                {
                    data_temp += 2;  // Skip number of points, assume 3...
                    repeat(b, 3)
                    {
                        vertex = DEREF( Uint16, data_temp );  data_temp += 2;
                        tex_vertex = DEREF( Uint16, data_temp );  data_temp += 2;
                        SPIT_UNSIGNED_SHORT(vertex);
                        SPIT_UNSIGNED_SHORT(tex_vertex);
                    }
                }
                // No fans in Devtool...
                data_temp += 2;
            }
            else
            {
                SPIT_UNSIGNED_CHAR(0);
                data_temp++;
            }
        }
        log_info(0, "Done with textures");
        repeat(j, num_joint)
        {
            k = (Uint16)(DEREF( float, data_temp ) / JOINT_COLLISION_SCALE);
            SPIT_UNSIGNED_CHAR(k);  data_temp += 4;
        }
        log_info(0, "Done with joints");
        repeat(j, num_bone)
        {
            SPIT_UNSIGNED_CHAR(*data_temp);  data_temp++;
            k = DEREF( Uint16, data_temp );  data_temp += 2;
            b = DEREF( Uint16, data_temp );  data_temp += 2;
            SPIT_UNSIGNED_SHORT(k);
            SPIT_UNSIGNED_SHORT(b);
            data_temp += 4;  // Skip bone length
        }
        log_info(0, "Done with bones");
    }



    // Jump to first frame of animation
    if (!(flags & DDD_EXTERNAL_BONE_FRAMES))
    {
        data_temp = DEREF( Uint8*, pdata );
        repeat(i, num_bone_frame)
        {
            SPIT_UNSIGNED_CHAR(*data_temp);  data_temp++;
            SPIT_UNSIGNED_CHAR(*data_temp);  data_temp++;
            base_model = *data_temp;  data_temp++;
            SPIT_UNSIGNED_CHAR(base_model);
            k = (Uint16)((Sint16)(DEREF( float, data_temp ) * ((float)0x0100)));  data_temp += 4;
            SPIT_UNSIGNED_SHORT(k);
            k = (Uint16)((Sint16)(DEREF( float, data_temp ) * ((float)0x0100)));  data_temp += 4;
            SPIT_UNSIGNED_SHORT(k);

            data_other = data_index_list + (base_model * 20);
            data_other = DEREF( Uint8*, data_other );
            data_other += 4;
            num_joint = DEREF( Uint16, data_other );  data_other += 2;
            num_bone = DEREF( Uint16, data_other );
            repeat(j, num_bone)
            {
                // Write forward normal
                x = DEREF( float, data_temp );  data_temp += 4;
                y = DEREF( float, data_temp );  data_temp += 4;
                z = DEREF( float, data_temp );  data_temp += 4;
                x = (x * ((float)0x7FFF));
                y = (y * ((float)0x7FFF));
                z = (z * ((float)0x7FFF));
                SPIT_UNSIGNED_SHORT(((Sint16) x));
                SPIT_UNSIGNED_SHORT(((Sint16) y));
                SPIT_UNSIGNED_SHORT(((Sint16) z));
                // Skip side normal
                data_temp += 12;
            }
            repeat(j, num_joint)
            {
                x = DEREF( float , data_temp ) * DDD_SCALE_WEIGHT / max_distance;  data_temp += 4;
                y = DEREF( float , data_temp ) * DDD_SCALE_WEIGHT / max_distance;  data_temp += 4;
                z = DEREF( float , data_temp ) * DDD_SCALE_WEIGHT / max_distance;  data_temp += 4;
                log_info(0, "joint position = (%f, %f, %f)", x, y, z);
                k = (Uint16)((Sint16) x);
                SPIT_UNSIGNED_SHORT(k);
                k = (Uint16)((Sint16) y);
                SPIT_UNSIGNED_SHORT(k);
                k = (Uint16)((Sint16) z);
                SPIT_UNSIGNED_SHORT(k);
            }
            repeat(k, MAX_DDD_SHADOW_TEXTURE)
            {
                c = *data_temp;  data_temp++;  // Alpha
                SPIT_UNSIGNED_CHAR(c);

                if (c)
                {
                    repeat(c, 4)
                    {
                        x = DEREF( float , data_temp ) * DDD_SCALE_WEIGHT / max_distance;  data_temp += 4;
                        y = DEREF( float , data_temp ) * DDD_SCALE_WEIGHT / max_distance;  data_temp += 4;
                        b = (Uint16)((Sint16) x);
                        SPIT_UNSIGNED_SHORT(b);
                        b = (Uint16)((Sint16) y);
                        SPIT_UNSIGNED_SHORT(b);
                    }
                }
            }
        }
    }


    return ktrue;
}

#endif

#pragma once

#include "soulfu_common.h"
#include "soulfu_endian.h"
#include "soulfu_config.h"


#if (EXTERN_BYTEORDER != SDL_BYTEORDER)
_INLINE Uint8  SF_Swap08(Uint8  val) { return val; }
_INLINE Uint16 SF_Swap16(Uint16 val) { return SDL_Swap16(val); }
_INLINE Uint32 SF_Swap32(Uint32 val) { return SDL_Swap32(val); }
#else
_INLINE Uint8  SF_Swap08(Uint8  val) { return val; }
_INLINE Uint16 SF_Swap16(Uint16 val) { return val; }
_INLINE Uint32 SF_Swap32(Uint32 val) { return val; }
#endif


_INLINE float endian_read_mem_float(Uint8* location);
_INLINE void  endian_write_mem_float(Uint8* location, float val);

_INLINE Uint32 endian_read_mem_int32(Uint8* location);
_INLINE void endian_write_mem_int32(Uint8* location, Uint32 value);

_INLINE Uint16 endian_read_mem_int16(Uint8* location);
_INLINE void endian_write_mem_int16(Uint8* location, Uint16 value);



//-----------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------
_INLINE bool_t endian_write_file_int08(FILE *file, Uint8  val) { bool_t retval; retval = (1 == fwrite(&val, sizeof(Uint8), 1, file)); return retval; }
_INLINE bool_t endian_write_file_int16(FILE *file, Uint16 val) { bool_t retval; Uint16 val2; val2 = SF_Swap16(val); retval = (1 == fwrite(&val2, sizeof(Uint16), 1, file)); return retval; }
_INLINE bool_t endian_write_file_int32(FILE *file, Uint32 val) { bool_t retval; Uint32 val2; val2 = SF_Swap32(val); retval = (1 == fwrite(&val2, sizeof(Uint32), 1, file)); return retval; }

_INLINE bool_t endian_read_file_int08(FILE *file, Uint8  *val) { bool_t retval; Uint8  val2; retval = (1 == fread(&val2, sizeof(Uint8 ), 1, file)); if (NULL != val) *val = val2;            return retval; }
_INLINE bool_t endian_read_file_int16(FILE *file, Uint16 *val) { bool_t retval; Uint16 val2; retval = (1 == fread(&val2, sizeof(Uint16), 1, file)); if (NULL != val) *val = SF_Swap16(val2); return retval; }
_INLINE bool_t endian_read_file_int32(FILE *file, Uint32 *val) { bool_t retval; Uint32 val2; retval = (1 == fread(&val2, sizeof(Uint32), 1, file)); if (NULL != val) *val = SF_Swap32(val2); return retval; }


//-----------------------------------------------------------------------------------------------
_INLINE float endian_read_mem_float(Uint8* location)
{
    // <ZZ> This function reads an float value at the given memory location, assuming that
    //      the memory is stored in big endian format.  The function returns the value it
    //      read.

    union convert_u
    {
        float  f32;
        Uint32 i32;
    } convert;

    convert.i32 = endian_read_mem_int32(location);

    return convert.f32;
}

//-----------------------------------------------------------------------------------------------
_INLINE void endian_write_mem_float(Uint8* location, float val)
{
    // <ZZ> This function writes an float value to the given memory location, assuming that
    //      the memory is stored in big endian format.

    union convert_u
    {
        float  f32;
        Uint32 i32;
    } convert;

    convert.f32 = val;

    endian_write_mem_int32(location, convert.i32);
}

//-----------------------------------------------------------------------------------------------
_INLINE Uint32 endian_read_mem_int32(Uint8* location)
{
    // <ZZ> This function reads an unsigned integer at the given memory location, assuming that
    //      the memory is stored in big endian format.  The function returns the value it
    //      read.

    return SF_Swap32( DEREF(Uint32, location) );
}

//-----------------------------------------------------------------------------------------------
_INLINE void endian_write_mem_int32(Uint8* location, Uint32 value)
{
    // <ZZ> This function writes an unsigned integer to the given memory location, assuming that
    //      the memory is stored in big endian format.

    DEREF(Uint32, location) = SF_Swap32(value);
}

//-----------------------------------------------------------------------------------------------
_INLINE Uint16 endian_read_mem_int16(Uint8* location)
{
    // <ZZ> This function reads an Uint16 at the given memory location, assuming that
    //      the memory is stored in big endian format.  The function returns the value it
    //      read.

    return SF_Swap16( DEREF(Uint16, location) );
}

//-----------------------------------------------------------------------------------------------
_INLINE void endian_write_mem_int16(Uint8* location, Uint16 value)
{
    // <ZZ> This function writes an Uint16 to the given memory location, assuming that
    //      the memory is stored in big endian format.

    DEREF(Uint16, location) = SF_Swap16(value);
}

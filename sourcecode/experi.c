// <ZZ> This file has stuff for giving experience...

enum e_experience_type
{
    EXPERIENCE_GIVE              = 0,
    EXPERIENCE_GET_NEEDED,
    EXPERIENCE_VIRTUE_COMPASSION,
    EXPERIENCE_VIRTUE_DILLIGENCE,
    EXPERIENCE_VIRTUE_HONOR,
    EXPERIENCE_VIRTUE_FAITH,
    EXPERIENCE_VIRTUE_COURAGE,
    MAX_EXPERIENCE_TYPE
};

#define MAX_LEVEL (2*MAX_VIRTUE_TYPE)

Uint16 level_experience_needed[MAX_LEVEL] = {0, 100, 250, 700, 1500, 2500, 4000, 5700, 7700, 10000};
//Uint16 level_experience_needed[MAX_LEVEL] = {0, 200, 500, 900, 1400, 2000, 2700, 3500, 4400, 5400};
//Uint16 level_experience_needed[MAX_LEVEL] = {0, 100, 300, 600, 1000, 1500, 2100, 2800, 3600, 4500};
//Uint16 level_experience_needed[MAX_LEVEL] = {0, 100, 250, 450, 700, 1000, 1350, 1750, 2200, 2700};
Uint8  level_badges_needed[MAX_LEVEL] = {0, 0, 0, 0, 0, 1, 2, 3, 4, 5};


//-----------------------------------------------------------------------------------------------
Uint16 experience_function_character(Uint16 character, Uint8 experience_type, Sint16 experience_amount, Uint8 affecting_single_character)
{
    // <ZZ> This function gives experience to a character.
    CHR_DATA* chr_data;
    Uint16 rider;
    Uint8 can_level_up;
    Uint8 good_guy;
    Uint8 number_of_badges, i;
    Uint8 level;
    Uint8 intelligence;

    // Find the character...
    chr_data = chr_data_get_ptr(character);
    if ( NULL != chr_data && experience_type < MAX_EXPERIENCE_TYPE)
    {
        CHR_DATA * prider;

        // Check some stuff...

        // Pass along effect to rider if target character is a mount...
        rider = chr_data->rider;
        prider = chr_data_get_ptr( rider );
        if ( NULL != prider )
        {
            // Don't pass it along if it's a team effect (or rider would be hit twice)
            if (experience_type != EXPERIENCE_GET_NEEDED && affecting_single_character)
            {
                character = rider;
                chr_data  = prider;
            }
        }



        // What type?
        if (experience_type < EXPERIENCE_VIRTUE_COMPASSION)
        {
            // Really giving experience, not virtue...  Check level...
            // Level 0 characters (monsters) aren't elligible for experience...
            good_guy = (chr_data->team == TEAM_GOOD);  // Bad guys don't worry about badges...
            level = chr_data->level;

            if (chr_data->vir[VIRTUE_DILIGENCE] == 255)
            {
                // The character has the badge of dilligence...  Let's only give 'em half experience...
                experience_amount = (experience_amount + 1) >> 1;
            }

            if (level > 0 && level < MAX_LEVEL && experience_amount > 0)
            {
                // Figure out how much experience is needed for the character's next level...
                number_of_badges = 0;
                repeat(i, MAX_VIRTUE_TYPE)
                {
                    if ( chr_data->vir[i] == 255)  number_of_badges++;
                }
                can_level_up = (number_of_badges >= level_badges_needed[level]) || (good_guy == kfalse);

                if (can_level_up)
                {
                    // Were we really asking for how much we need?
                    if (experience_type == EXPERIENCE_GET_NEEDED)
                    {
                        return level_experience_needed[level];
                    }


                    // Able to gain experience...  So gain it already...
                    // But first modify the amount by our intelligence...
                    intelligence = chr_data->int_min;
                    intelligence = (intelligence > 50) ? 50 : intelligence;
                    intelligence = intelligence + 10;
                    experience_amount = experience_amount * intelligence / 25;

                    if (affecting_single_character && experience_amount == 0)
                    {
                        experience_amount++;
                    }

                    chr_data->exp_ += experience_amount;


                    // Check for level ups...
                    if (chr_data->exp_ >= level_experience_needed[level])
                    {
                        // Character leveled up...  Reset experience to start of level...
                        chr_data->exp_ = level_experience_needed[level];
                        chr_data->level = level + 1;

                        if (paying_customer)
                        {
                            // Paying customers get 5 stat points per level...
                            chr_data->statpts += 5;
                        }
                        else
                        {
                            // Demo players only get 2 stat points per level...
                            chr_data->statpts += 2;
                        }

                        if (chr_data->statpts > 100)
                        {
                            chr_data->statpts = 100;
                        }

                        // Run the level up event...
                        chr_data->event = EVENT_LEVEL_UP;
                        sf_fast_rerun_script(&(chr_data->script_ctxt), FAST_FUNCTION_EVENT);
                    }
                }
            }
        }
        else
        {
            int virtue_type = experience_type - EXPERIENCE_VIRTUE_COMPASSION;

            // Giving virtue to the character...
            // Make sure we don't have a badge already...
            if ( chr_data->vir[virtue_type] != 255)
            {
                // The character doesn't have this badge yet...
                experience_amount += chr_data->vir[virtue_type];
                experience_amount = (experience_amount < 0) ? 0 : experience_amount;
                experience_amount = (experience_amount > 100) ? 100 : experience_amount;
                chr_data->vir[virtue_type] = (Uint8)experience_amount;
            }
        }
    }

    return UINT16_MAX;
}

//-----------------------------------------------------------------------------------------------

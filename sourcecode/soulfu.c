#define EXECUTABLE_VERSION   "1.5.X DEVELOPMENT (NICEWARE)"

#include <SDL.h>
#include <SDL_types.h>
#include <SDL_net.h>

Uint16 onscreen_draw_count;

#include "soulfu.h"

#include "soulfu_common.h"
#include "soulfu_config.h"

#include "soulfu_endian.inl"

#ifdef  _MSC_VER
# include <direct.h>
# define chdir _chdir
#else
# include <unistd.h>
#endif


bool_t update_performed = kfalse;   // Do any files need decompressing?
Uint16 global_block_keyboard_timer = 0;

Uint8 global_render_light_color_rgb[3];
float global_render_light_offset_xy[2];
Uint8* global_ddd_file_start;
Uint8* global_rdy_file_start;
Uint16 local_player_character[MAX_LOCAL_PLAYER];
Uint8 input_active = kfalse;
Uint8 user_language = 0;
int user_language_phrases = 0;
char bad_string[] = "Bad";        // Used as return on language failures...
Uint8* language_file[LANGUAGE_MAX];
Uint8* kanji_data;
Uint8 drawing_world = kfalse;
Uint8 mip_map_active = kfalse;
Uint8 fast_and_ugly_active = kfalse;
Uint16 screen_sizes_xy[MAX_SCREEN_SIZES][2] = {{320, 200},
    {320, 240},
    {640, 400},
    {640, 480},
    {800, 600},
    {1024, 768},
    {1280, 1024},
    {1600, 1200}
};
int screen_x = DEFAULT_W;                     // The x screen size
int screen_y = DEFAULT_H;                     // The y screen size
float window_scale = 0.6f;
float room_water_level = -2.0f;
Uint8 room_water_type = WATER_TYPE_WATER;
Uint8 room_metal_box_item = 0;
char run_string[MAX_STRING][MAX_STRING_SIZE];   //

float global_bump_normal_xyz[3];
Uint16 global_bump_flags;
Uint16 global_bump_char;
Uint8 global_bump_abort;
Uint8 cursor_click[2] = { 10, 0 };
Uint8 cursor_normal[2] = { 9, 0 };
Uint8 cursor_target[2] = {178, 0};

Uint16 num_onscreen_joint;
Uint8 onscreen_joint_active = kfalse;
Uint16 onscreen_joint_character;
CHR_DATA* onscreen_joint_character_data;
Uint8 debug_active = kfalse;
Uint8 color_temp[4];
Uint8 black[4] = {0, 0, 0, 128};
Uint8 red[4] = {255, 0, 0, 128};
Uint8 line_color[4] = {0, 0, 0, 128};
Uint8 dark_red[4] = {150, 0, 0, 128};
Uint8 green[4] = {0, 255, 0, 128};
Uint8 dark_green[4] = {0, 70, 0, 200};
Uint8 light_green[4] = {160, 255, 160, 128};
Uint8 blue[4] = {0, 0, 255, 128};
Uint8 cyan[4] = {0, 255, 255, 128};
Uint8 magenta[4] = {255, 0, 255, 128};
Uint8 grey[4] = {128, 128, 128, 128};
Uint8 white[4] = {255, 255, 255, 80};
Uint8 shadow_color[4] = {255, 255, 255, 255};
Uint8 gold[4] = {230, 198, 25, 128};
Uint8 brown[4] = {84, 30, 9, 200};
Uint8 bronze[4] = {244, 180, 119, 128};
Uint8 yellow[4] = {255, 255, 0, 128};
Uint8 med_yellow[4] = {210, 200, 5, 128};
Uint8 dark_yellow[4] = {170, 143, 5, 128};
Uint8 faded_yellow[4] = {255, 255, 0, 64};
Uint8 whiter[4] = {255, 255, 255, 180};
Uint8 whitest[4] = {255, 255, 255, 255};
Uint8 instrument_color[8][4] =
{
    {0xFF, 0x00, 0x00, 0x80},
    {0x00, 0xFF, 0x00, 0x80},
    {0x00, 0x00, 0xFF, 0x80},
    {0xFF, 0x80, 0xFF, 0x80},
    {0x80, 0xFF, 0xFF, 0x80},
    {0xFF, 0xFF, 0x80, 0x80},
    {0xFF, 0xC0, 0xC0, 0x80},
    {0xFF, 0xFF, 0xFF, 0x80}
};
Uint16 map_room_door_spin = 0;  // For telling players their new spin on room change...
//Uint8* current_object_data;  // Should be filled before run_script...
//Uint16 current_object_item;  // Ditto
bool_t quitgame;
Uint32 main_game_frame;        // Should update at a rate of GAME_FRAMES_PER_SECOND (but only when game is running...)
Uint32 main_video_frame;       // Number of times the screen has been drawn...
Uint32 main_video_frame_skipped;  // Should update at a rate of GAME_FRAMES_PER_SECOND (even when game isn't active...)
Uint16 main_frame_skip = 0;  // Number of game frames in last update...
bool_t main_game_active;         // ktrue if the local machine has joined or started a game
bool_t play_game_active;         // ktrue if the local players are actually playing (ie characters are spawned and runnin' around)
bool_t paying_customer = kfalse;  // Is the local machine a paying customer?
bool_t update_active = kfalse;    // For updating files in the datafile...
Uint32 time_since_i_got_heartbeat;// Frames since I got a heartbeat from somebody...
Uint32 time_since_i_sent_heartbeat;// Frames since I sent a heartbeat from somebody...

#include "sdf_archive.h"

#include "logfile.c"
#include "gameseed.c"
#include "soulfu_common.h"
#include "sdf_archive.h"
#include "random.c"
#include "object.c"
#include "skybox.c"
#include "display.c"
#include "sound.c"
#include "input.c"
#include "tool.c"
#include "dcodesrc.c"
#include "page.c"
#include "message.c"
#include "render.c"
#include "volume.c"
#include "room.c"
#include "map.c"
#include "water.c"
#include "damage.c"
#include "experi.c"
#include "item.c"
#include "runsrc.c"
#include "particle.c"
#include "charact.c"
#include "network.c"

Uint16 global_luck_timer = 0;

#if !defined(_SOULFU_LIB)
//-----------------------------------------------------------------------------------------------
void main_loop(void)
{
    int i, j;
    float x, y, brx, bry;
    char* alt_text;
    bool_t pause_active = kfalse;


    log_info(0, "Starting main loop");
    display_zbuffer_on();
    display_cull_on();


    // Loop as long as escape isn't pressed
    main_game_frame = 0;
    main_video_frame = 0;
    main_video_frame_skipped = 0;
    quitgame = kfalse;

    while (!quitgame)
    {
        main_timer_start();     // Game timer...


        // Draw the world, if we're actually playing the game...
        color_temp[0] = 0;  color_temp[1] = 0;  color_temp[2] = 0;

        if (play_game_active)
        {
            if (map_current_room < MAX_MAP_ROOM)
            {
                if (map_room_data[map_current_room][13] & MAP_ROOM_FLAG_OUTSIDE)
                {
                    color_temp[0] = 64;  color_temp[1] = 64;  color_temp[2] = 255;
                }
            }

            display_clear_buffers();



            if (keyb.pressed[SDLK_F9])
            {
                pause_active = !pause_active;
                DEBUG_STRING[0] = 0;
            }

// !!!BAD!!!
// !!!BAD!!!
// !!!BAD!!!
#ifdef DEVTOOL

#    if defined(SOULFU_CLIENT)
            if (keyb.pressed[SDLK_SPACE])
            {
                if (network_on)
                {
                    cl_transmit_room_update();
                }
            }
#    endif /*SOULFU_CLIENT*/

            if (keyb.pressed[SDLK_F8])
            {
                log_info(0, "Dumping all particle information...");
                repeat(i, MAX_PARTICLE)
                {
                    if (VALID_PRT(i))
                    {
                        obj_get_script_name(main_prt_data[i].script_ctxt.base.base.file_start, DEBUG_STRING);
                        log_info(0, "Particle %d is of type %s.RUN", i, DEBUG_STRING);
                        DEBUG_STRING[0] = 0;
                    }
                    else
                    {
                        log_info(0, "Particle %d is off", i);
                    }
                }
            }

#endif
// !!!BAD!!!
// !!!BAD!!!
// !!!BAD!!!

            // Move characters around...
            main_frame_skip = 0;

            if (main_timer_length >= 16)
            {
                // Only do up to 4 updates per drawn frame to ensure that the game doesn' get totally
                // hosed from being too slow to update (and constantly has more and more frames
                // to catch up on...)
                while (main_timer_length >= 16 && main_frame_skip < 4)
                {
                    character_local_player_control();

                    if (!pause_active)
                    {
                        if (global_luck_timer > 0)
                        {
                            global_luck_timer--;
                        }

                        character_update_all(NULL);
                        character_collide_all();
                        particle_update_all();
                        character_bone_frame_all();
                        character_refresh_items_all();
                    }
                    else
                    {
                        character_bone_frame_all();
                    }


                    main_timer_length -= 16;
                    main_game_frame++;
                    main_frame_skip++;
                }
            }
            else
            {
                character_local_player_control();
                character_bone_frame_all();
            }



            onscreen_draw_count = 0;


            // Figure out the camera position...
            display_camera_position(main_frame_skip, 0.95f, 0.99f);


            // Rotate the camera...
            display_look_at(camera.xyz, camera.target_xyz);
            // Remember camera facing for automap...  Use side, because fore can look down and
            // that screws up the scaling...
            map_side_xy[XX] = camera.side_xyz[XX];
            map_side_xy[YY] = camera.side_xyz[YY];



            // Draw the world scene...
            drawing_world = ktrue;
            room_draw(roombuffer);

            if (volumetric_shadows_on)
            {
                volume_shadow_draw_all();
            }
            else
            {
                character_shadow_draw_all();
            }

            character_draw_all_prime();
            character_draw_all(kfalse, kfalse);

            if (room_water_type == WATER_TYPE_WATER)
            {
                particle_draw_below_water();
            }

            water_draw_room(roombuffer);
            character_draw_all(ktrue, kfalse);

            if (room_water_type == WATER_TYPE_WATER)
            {
                particle_draw_above_water();
            }
            else
            {
                particle_draw_below_water();
                particle_draw_above_water();
            }

            character_draw_all(kfalse, ktrue);






            //sprintf(DEBUG_STRING, "%d chars drawn", onscreen_draw_count);


            // Draw axis arrows...
            //            #ifdef DEVTOOL
            //                if(debug_active)
            //                {
            //                    display_blend_off();
            //                    display_texture_off();
            //                    render_axis();
            //                    display_marker(blue, 0.0f, 0.0f, 6.0f, 1.0f);  // 6ft height marker...
            //                    display_marker(green, 0.0f, 6.0f, 0.1f, 1.0f);  // Y axis marker
            //                    display_marker(red, 6.0f, 0.0f, 0.1f, 1.0f);  // X axis marker
            //                }
            //            #endif
        }
        else
        {
            // Game isn't active, but still run our timers so the window scripts
            // work right...
            pause_active = kfalse;
            display_clear_buffers();
            character_bone_frame_clear();
            main_frame_skip = 0;

            while (main_timer_length >= 16)
            {
                main_frame_skip++;
                main_timer_length -= 16;
            }
        }

        main_video_frame_skipped += main_frame_skip;



        // Draw window stuff...
        drawing_world = kfalse;
        glLoadMatrixf(window_camera_matrix);
        display_zbuffer_off();
        display_texture_on();
        display_blend_trans();
        display_cull_on();

        promotion_count = 0;
        current_win_idx = 0;
        repeat(i, main_used_window_count)
        {
            PSoulfuScriptContext pctxt;
            WIN_DATA * pwin;

            j = main_used_window[i];
            pwin = main_win_data + j;
            pctxt = &(pwin->script_ctxt);

            if (!pwin->on)  continue;

            pctxt->window3d.order = MAX_WINDOW - ((Uint8) i);
            pctxt->screen.scale = window_scale;

            current_win[current_win_idx++] = pwin;
            sf_fast_rerun_script( pctxt, FAST_FUNCTION_REFRESH);
            assert(pwin == current_win[--current_win_idx] );
            current_win[current_win_idx] = NULL;
        }

        win_promote_all();

        // Handle funky enchantment targeting clicks...
        if (g_sf_scr.enc_cursor.active)
        {
            CHR_DATA * pchr;

            // Constantly check for dead character...
            g_sf_scr.enc_cursor.active = kfalse;

            pchr = chr_data_get_ptr(g_sf_scr.enc_cursor.character);
            if ( NULL != pchr )
            {
                if ( pchr->hits > 0)
                {
                    g_sf_scr.enc_cursor.active = ktrue;
                }
            }
        }

        if (mouse.draw && g_sf_scr.enc_cursor.active)
        {
            // Is player still alive?
            if (mouse.pressed[BUTTON0])
            {
                // Was it clicked on a character?
                if ( mouse.current.object.unk == NULL &&  mouse.current.item != NO_ITEM)
                {
                    SoulfuScriptContext loc_sc;

                    // Figger out who the target is...

                    g_sf_scr.enc_cursor.target      = mouse.current.item;
                    g_sf_scr.enc_cursor.target_item = 0;  // Means character itself...
                    call_enchantment_function(&loc_sc);

                    if (ktrue != pxss_var_get_int(&(loc_sc.base.base.return_var)) )
                    {
                        // Revert to normal cursor if EnchantUsage() returns ktrue
                        g_sf_scr.enc_cursor.active = kfalse;
                    }
                }
            }
        }



        // Handle input changes...
        input_update();


        // Draw the selection box
#ifdef DEVTOOL
        selection.close_type = 0;

        if (selection.pick_on && mouse.down[BUTTON0] == 0)
        {
            selection.pick_on = kfalse;
        }

        if (selection.move_on && mouse.down[BUTTON0] == 0)
        {
            selection.move_on = kfalse;
            selection.close_type = BORDER_MOVE;
        }

        if (selection.box_on)
        {
            // It's on...
            render_box();

            // Close if button released
            if (mouse.down[BUTTON0] == 0)
            {
                selection.box_on = kfalse;
                selection.close_type = BORDER_SELECT;

                // Ensure that top left is less than bottom right...
                if (selection.box_tl[XX] > selection.box_br[XX])
                {
                    x = selection.box_tl[XX];
                    selection.box_tl[XX] = selection.box_br[XX];
                    selection.box_br[XX] = x;
                }

                if (selection.box_tl[YY] > selection.box_br[YY])
                {
                    y = selection.box_tl[YY];
                    selection.box_tl[YY] = selection.box_br[YY];
                    selection.box_br[YY] = y;
                }

            }
        }

#endif





        // Draw the cursor...
        display_pick_texture(texture_ascii);

        if (mouse.idle_timer > 1800 && mouse.alpha > 0)
        {
            mouse.alpha--;
        }

        if (mouse.idle_timer < 30 && mouse.alpha < 255)
        {
            mouse.alpha++;
            mouse.alpha = (mouse.alpha + mouse.alpha + mouse.alpha + 255) >> 2;
        }

        color_temp[0] = 255;  color_temp[1] = 255;  color_temp[2] = 255;  color_temp[3] = mouse.alpha;
        display_color_alpha(color_temp);

        if (mouse.draw)
        {
            x = mouse.x;
            y = mouse.y;

            // Draw the cursor
            if (g_sf_scr.enc_cursor.active)
            {
                display_string(cursor_target, x - 7.5f, y - 7.5f, 15);
            }
            else
            {
                if (mouse.down[BUTTON0])
                {
                    // Draw a click cursor...
                    display_string(cursor_click, x, y, 15);
                }
                else
                {
                    // Draw a normal cursor...
                    display_string(cursor_normal, x, y, 15);
                }
            }


            // Draw the alternate text...
            if (mouse.text[0])
            {
                x += 12.0f;
                y += 12.0f;
                alt_text = mouse.text;
                brx = x + g_sf_scr.scale * (strlen(alt_text) + 1);
                bry = y + g_sf_scr.scale * 1.2f;

                if (brx > CAST(float, DEFAULT_W))
                {
                    x = CAST(float, DEFAULT_W) - (brx - x);
                    brx = CAST(float, DEFAULT_W);
                }

                display_color_alpha(whiter);
                display_mouse_text_box(g_sf_scr.scale, x, y, brx, bry, texture_winalt);
                display_color(white);
                display_pick_texture(texture_ascii);
                display_string(alt_text, x + (g_sf_scr.scale*.5f), y + ( g_sf_scr.scale*0.1f), g_sf_scr.scale);

                if (mouse.text_timer == 0)
                {
                    mouse.text[0] = 0;
                }
            }
        }


        // Draw the debug string
        display_color(white);

        if (pxss_compiler_error)
        {
            display_string("Compiler error...  See logfile.txt", 0, 290.0f, 10.0f);
        }
        else
        {
#ifdef DEVTOOL

            if (keyb.down[SDLK_F11])
            {
                sprintf(DEBUG_STRING, "Obj == %p, Item == %u", mouse.last.object.unk, mouse.last.item);
            }

            if (!pause_active)
            {
                display_string(DEBUG_STRING, 0, 290.0f, 10.0f);
            }

#endif
        }

        //#ifndef DEVTOOL
        //        display_string("DEVTOOL OFF", 290.0f, 290.0f, 10.0f);
        //#endif

        if (pause_active)
        {
            if (play_game_active)
            {
                display_string("-Paused-", 140.0, 15.0f, 15.0f);
            }
        }

#ifdef DEVTOOL
        //        if(!pause_active)
        //        {
        //            if(play_game_active)
        //            {
        //                display_string("Play", 0.0, 0.0f, 10.0f);
        //            }
        //            if(main_game_active)
        //            {
        //                display_string("Main", 50.0, 0.0f, 10.0f);
        //            }
        //        }
#endif



        // Do fade out effect...
        display_texture_off();
        display_blend_off();
#if defined(DEVTOOL) && defined(SHOW_JOYSTICK_POSITIONS)
        repeat(i, MAX_LOCAL_PLAYER)
        {
            x = (CAST(float, DEFAULT_W) / (MAX_LOCAL_PLAYER + 1)) * (i + 1);
            y = DEFAULT_H - 25;

            display_color(white);
            display_start_line();
            {
                display_vertex_xy(x - 2.0f, y);
                display_vertex_xy(x + 2.0f, y);
            }
            display_end();

            display_start_line();
            {
                display_vertex_xy(x, y - 2.0f);
                display_vertex_xy(x, y + 2.0f);
            }
            display_end();

            display_color(instrument_color[i]);
            display_start_line();
            {
                display_vertex_xy(x, y);
                x += player_device[i].xy[XX] * 25.0f;
                y += player_device[i].xy[YY] * 25.0f;
                display_vertex_xy(x, y);
            }
            display_end();
        }
#endif
        display_shade_on();
        display_fade();
        display_shade_off();
        display_zbuffer_on();



        // Display everything once it's drawn...
        display_swap();


        // Reset our input...  Do once per display
        input_reset_window_key_pressed();
        input_read();
        fill_music();
        fill_sound_buffer();

#if defined(SOULFU_CLIENT)
        // !!!BAD!!!
        // !!!BAD!!!
        // !!!BAD!!!
        cl_listen();
        // !!!BAD!!!
        // !!!BAD!!!
        // !!!BAD!!!
#endif

        // Handle camera controls...
        input_camera_controls();



        main_video_frame++;
#ifdef DEVTOOL

        if (keyb.pressed[SDLK_F10])
        {
            debug_active = (debug_active + 1) & 1;
        }

        //            if(keyb.pressed[SDLK_F4])
        //            {
        //                display_start_fade(FADE_TYPE_FULL, FADE_OUT, 0, 0, black);
        //            }
        //            if(keyb.pressed[SDLK_F3])
        //            {
        //                display_start_fade(FADE_TYPE_WARNING, FADE_OUT, 0, 0, cyan);
        //            }
        //            if(keyb.pressed[SDLK_F2])
        //            {
        //                display_start_fade(FADE_TYPE_CIRCLE, FADE_OUT, 0.5f * DEFAULT_W, 0.5f * DEFAULT_H, black);
        //            }
#endif

        // --- do all the actual deletions of objects here ----
        obj_delete();

        main_timer_end();
    }
}

//-----------------------------------------------------------------------------------------------
int main(int argc, char *argv[])
{

    SDF_PHEADER script_header;
    Uint8* script;

    Uint8* data;
    SDF_PHEADER pheader;

    Uint8 bit_depth;
    Uint8 z_depth;
    Uint8 full_screen;
    Uint8 screen_size;
	FILE *openfile;

    /* Ensure the application can find the datafile and any shared libraries. */
	openfile = fopen("datafile.sdf", "rb");
	if (openfile != NULL)
		fclose(openfile);
	else if (argc > 0 && NULL != strchr(argv[0], SLASH_CHR))
    {
        char appPath[1024];
        char *sp = strrchr(argv[0], SLASH_CHR);
        size_t appPathLength = sp - argv[0];
        if (appPathLength >= 1024) {
            log_error(0, "application directory path too long '%s', %d", argv[0], appPathLength);
            exit(1);
        }

        strncpy(appPath, argv[0], appPathLength);
        appPath[appPathLength] = '\0';
        chdir(appPath);
    }

    log_setup();
    log_info(0, "------------------------------------------");

    if (!buffer_setup()) { log_error(0, "buffer_setup() failed");  exit(1); }

    if (!sdf_archive_load("datafile.sdf")) { log_error(0, "sdf_archive_load() failed"); exit(1); }

#if defined(SOULFU_CLIENT) || defined(SOULFU_SERVER)
    if (!network_setup()) { log_warning(0, "could not set up network properly"); }
#endif

    display_kanji_setup();

    log_info(0, "SoulFu version %s", EXECUTABLE_VERSION);

    pheader = sdf_archive_find_filetype("VERSION", SDF_FILE_IS_DAT);

    if (pheader)
    {
        data = sdf_file_get_data(pheader);
        log_info(0, "SDF archive version %d.%d", endian_read_mem_int16(data), endian_read_mem_int16(data + 2));
    }
    else
    {
        log_info(0, "SDF archive version file missing");
    }

    generate_game_seed();
    log_info(0, "------------------------------------------");


    // Make sure the game is turned off...
    play_game_active = kfalse;
    main_game_active = kfalse;


    // Try to not break it too bad...
#ifdef DEVTOOL
    sdf_archive_save("backup.sdf");
#endif

    // Load the config file from disk, if there is one...
    sdf_archive_add_file("CONFIG.DAT", NULL);


    // Read the display settings from the config file...
    {
        SDF_PHEADER config_header;
        Uint8* config_data;

        config_header = sdf_archive_find_filetype("CONFIG", SDF_FILE_IS_DAT);

        if (NULL == config_header)
        {
            log_error(0, "No config_data file present");
            exit(1);
        }

        config_data = sdf_file_get_data(config_header);

        if (NULL == config_data)
        {
            log_error(0, "No config_data present");
            exit(1);
        }

        screen_size           = (*(config_data + 68)) % MAX_SCREEN_SIZES;
        bit_depth             = *(config_data + 69);
        z_depth               = *(config_data + 70);
        volumetric_shadows_on = (*(config_data + 71)) & 1;
        full_screen           = kfalse; // *(config_data+72);
        mip_map_active        = *(config_data + 98);
        fast_and_ugly_active  = *(config_data + 101);

        log_info(0, "Config file read okay...");
    }

    if (!display_setup(screen_sizes_xy[screen_size][XX], screen_sizes_xy[screen_size][YY], bit_depth, z_depth, full_screen))
    {
        log_error(0, "display_setup() failed");
        exit(1);
    }

    // Draw a please wait type of thing onscreen...
    display_loadin(0.0f, ktrue);

    //**************************************************

    // initialize the scripting system
    sf_extensions_setup( &g_sf_scr );

    //**************************************************
    //    sdf_archive_add_file("DEFINE.TXT");  // !!!BAD!!!  Allow me to constantly update the global defines

    sine_table_setup();

    log_info(0, "random_setup()");

    if (!random_setup(game_seed)) { log_error(0, "random_setup() failed");  exit(1); }

    // Decode all of the files...
    log_info(0, "decode_sdf_archive()");
    decode_sdf_archive(SDF_FLAG_ALL, 0.0f, 0.57f);

    log_info(0, "sf_stage_compile()");

    // Keep linking until all of the errors go away...
    while (!pxss_compiler_error && !sf_stage_compile(PXSS_FUNCTIONIZE, SDF_FLAG_ALL, 0.57f, 0.62f)) {};

#if defined(DEBUG_SRCDECODE)
    exit(0);
#endif

    log_info(0, "ddd_magic_update_thing()");

    ddd_magic_update_thing(SDF_FLAG_ALL, 0.62f, .66f);

    log_info(0, "render_crunch_all()");

    render_crunch_all(SDF_FLAG_ALL, .66f, 0.71f);

    log_info(0, "sdf_archive_flag_clear()");

    sdf_archive_flag_clear(SDF_FLAG_WAS_UPDATED);

    message_setup();

    damage_setup();
    
    input_setup();

    sound_setup();

    page_setup();

    render_shadow_setup();

    item_type_setup();

    water_setup();

    water_drown_delay_setup();

    character_action_setup();

    display_load_all_textures();

    // Flag certain files to not be updated...  (This is outdated sorta stuff...  No longer do FTP stuff...)
    pheader = sdf_archive_find_header("RANDOM.DAT");

    if (NULL != pheader)
    {
        sdf_file_set_flags(pheader, SDF_FLAG_NO_UPDATE);
    };

    pheader = sdf_archive_find_header("CONFIG.DAT");

    if (NULL != pheader)
    {
        sdf_file_set_flags(pheader, SDF_FLAG_NO_UPDATE);
    };


    // initialize the objects
    obj_setup();

    display_loadin(1.00f, kfalse);

    // initialize the list of active windows
    promotion_count = 0;

    // Start by spawning the first window object...  Main menu/Title...
    script_header = sdf_archive_find_filetype("WSTART", SDF_FILE_IS_RUN);

    if (NULL != script_header)
    {
        script = sdf_file_get_data(script_header);

        if (NULL != script)
        {
            obj_spawn(NULL, NULL, OBJECT_WIN, 0.5f * DEFAULT_W, 0.5f * DEFAULT_H, 0, script, UINT16_MAX);
            win_promote_all();
        }
    }


    // Do the main loop...
    main_loop();

    // Export the config file to disk, so we don't need to save the whole datafile
    sdf_archive_export_file("CONFIG.DAT", NULL);

    return 0;
}

#endif
